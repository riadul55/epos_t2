package com.novo.tech.redmangopos.socket;

import android.content.Context;
import android.graphics.Color;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

public class SocketHelper {
    public static void emitSuccess() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put(SocketHandler.ORDER, "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit(SocketHandler.EMIT_LISTEN_SUCCESS, jsonObject);
    }

    public static void emitBookingList(Context context) {
        try {
            JSONObject newJson = new JSONObject();
            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<20;i++) {
                boolean isBooked = SharedPrefManager.getTableBooked(context, i+1);
                TableBookingModel tableBookingModel = new TableBookingModel(
                        1, 1, i+1, 1, isBooked, 1, 1, ""
                );
                jsonArray.put(TableBookingModel.toJson(tableBookingModel));
            }
            newJson.put("booking_list", jsonArray);
            TheApp.webSocket.emit(SocketHandler.EMIT_BOOKING_LIST, newJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void emitOrdersPref() {
        JSONObject emitGetOrders = new JSONObject();
        try {
            emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            emitGetOrders.put("data", "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);
    }
}
