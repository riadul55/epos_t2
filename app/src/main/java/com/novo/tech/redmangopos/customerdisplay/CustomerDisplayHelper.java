package com.novo.tech.redmangopos.customerdisplay;

import android.app.Activity;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.view.Display;

public class CustomerDisplayHelper {
    public static Display getPresentationDisplays(Activity activity) {
        DisplayManager displayManager = (DisplayManager) activity.getSystemService(Context.DISPLAY_SERVICE);
        Display[] displays = displayManager.getDisplays();
        for (int i = 0; i < displays.length; i++) {
            //    Log.e("MMMMMM", "Screen" + displays[i]);
            if ((displays[i].getFlags() & Display.FLAG_SECURE) != 0
                    && (displays[i].getFlags() & Display.FLAG_SUPPORTS_PROTECTED_BUFFERS) != 0
                    && (displays[i].getFlags() & Display.FLAG_PRESENTATION) != 0) {
                return displays[i];
            }else{
            }
        }
        return null;
//        DisplayManager displayManager = (DisplayManager) activity.getSystemService(Context.DISPLAY_SERVICE);
//        Display[] presentationDisplays = displayManager.getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION);
//        if (presentationDisplays.length > 0) {
//            // If there is more than one suitable presentation display, then we could consider
//            // giving the user a choice.  For this example, we simply choose the first display
//            // which is the one the system recommends as the preferred presentation display.
//            return presentationDisplays[0];
//        }else
//            return null;
    }
}
