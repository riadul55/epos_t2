package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.view.fragment.CustomerInput;

import java.util.List;

public class CustomerAddressListAdapter  extends RecyclerView.Adapter<CustomerAddressListAdapter.CustomerAddressListViewHolder> {


    Context mCtx;
    List<CustomerAddress> addressList;


    public CustomerAddressListAdapter(Context mCtx, List<CustomerAddress> addressList) {
        this.mCtx = mCtx;
        this.addressList = addressList;

    }

    @NonNull
    @Override
    public CustomerAddressListAdapter.CustomerAddressListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.view_customer_address, parent, false);
        return new CustomerAddressListAdapter.CustomerAddressListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerAddressListAdapter.CustomerAddressListViewHolder holder, int _position) {
        CustomerAddress customerAddress = addressList.get(_position);
        CustomerAddressProperties properties = customerAddress.properties;
        holder.sl.setText(String.valueOf(_position+1));
        holder.address.setText((properties.building + " " + properties.street_number + " "+ properties.street_name + " " + properties.city+" "+properties.state+" "+properties.zip).trim());
        if(customerAddress.primary){
            holder.address.setTextColor(ContextCompat.getColor(mCtx, R.color.Green));
        }
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerInput customerInput = (CustomerInput) ((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
                if(customerInput!=null){
                    customerInput.selectedAddress = customerAddress;
                    customerInput.primaryCheckbox.setChecked(customerAddress.primary);
                    if(customerAddress.properties != null){
                        customerInput.street.setText(customerAddress.properties.street_number+" "+customerAddress.properties.street_name);
                        customerInput.town.setText(customerAddress.properties.city);
                        customerInput.zip.setText(customerAddress.properties.zip);
                        customerInput.houseNumber.setText(customerAddress.properties.building);

                    }
                }

            }
        });
        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerInput customerInput = (CustomerInput) ((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
                if(customerInput!=null){
                    customerInput.selectedAddress = customerAddress;
                    customerInput.backToOrder(customerInput.customerModel,customerAddress);
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return addressList.size();
    }


    static class CustomerAddressListViewHolder extends RecyclerView.ViewHolder {
        TextView address;
        TextView sl;
        AppCompatButton edit,select;

        public CustomerAddressListViewHolder(View itemView) {
            super(itemView);
            address = itemView.findViewById(R.id.address);
            sl = itemView.findViewById(R.id.sl);
            edit = itemView.findViewById(R.id.edit);
            select = itemView.findViewById(R.id.select);
        }
    }
}