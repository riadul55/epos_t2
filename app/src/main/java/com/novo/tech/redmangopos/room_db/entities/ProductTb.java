package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ProductFile;
import com.novo.tech.redmangopos.model.ProductProperties;
import com.novo.tech.redmangopos.room_db.DataConverter;

import java.util.List;

@Entity(tableName = "product_tb")
public class ProductTb {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "productType")
    @Nullable
    public String productType;

    @ColumnInfo(name = "productUUid")
    @Nullable
    public String productUUid;

    @ColumnInfo(name = "shortName")
    @Nullable
    public String shortName;

    @ColumnInfo(name = "description")
    @Nullable
    public String description;

    @ColumnInfo(name = "priceUUID")
    @Nullable
    public String priceUUID;

    @ColumnInfo(name = "price")
    @Nullable
    public double price;

    @ColumnInfo(name = "offerPrice")
    @Nullable
    public double offerPrice;

    @ColumnInfo(name = "discountable")
    @Nullable
    public boolean discountable;

    @ColumnInfo(name = "visible")
    @Nullable
    public boolean visible;

    @TypeConverters(DataConverter.class)
    @ColumnInfo(name = "properties")
    @Nullable
    public ProductProperties properties;

    @TypeConverters(DataConverter.class)
    @ColumnInfo(name = "files")
    @Nullable
    public List<ProductFile> files;

    @TypeConverters(DataConverter.class)
    @ColumnInfo(name = "componentList")
    @Nullable
    public List<Component> componentList;
}
