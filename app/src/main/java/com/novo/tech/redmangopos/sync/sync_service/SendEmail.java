package com.novo.tech.redmangopos.sync.sync_service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.AppConfig;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

public class SendEmail extends AsyncTask<String, Void, Void> {
    private Context context;
    private OrderModel orderData;
    private String emailType;
    private String errorBody;

    public SendEmail(Context context, OrderModel orderData, String emailType, String errorBody) {
        this.context = context;
        this.orderData = orderData;
        this.emailType = emailType;
        this.errorBody = errorBody;
    }

    @SuppressLint("WrongThread")
    @Override
    protected Void doInBackground(String... strings) {
        AppConfig appConfig = SharedPrefManager.getAppConfig(context);
        if (orderData == null ||
                orderData.customer == null ||
                orderData.customer.profile == null ||
                orderData.customer.profile.first_name == null ||
                orderData.customer.profile.first_name.isEmpty() ||
                orderData.serverId == 0 ||
                orderData.customer.email == null ||
                orderData.customer.email.isEmpty() ||
                appConfig == null ||
                appConfig.getBusinessName() == null ||
                appConfig.getBusinessPhone() == null ||
                appConfig.getBusinessAddress() == null ||
                !orderData.order_channel.equals("ONLINE")
        ) {
            return null;
        }
        Log.e("SendEmail===>", orderData.order_status);
        Log.e("SendEmail===>", emailType + "");
        Log.e("SendEmail===>", errorBody + "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("consumer_first_name", orderData.customer.profile.first_name);
            jsonObject.put("order_id", orderData.serverId);
            jsonObject.put("customer_email", orderData.customer.email);
            jsonObject.put("order_type", orderData.order_type.toUpperCase());
//                jsonObject.put("current_delivery_time", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
            jsonObject.put("new_delivery_time", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
            jsonObject.put("business_name", appConfig.getBusinessName() + "");
            jsonObject.put("business_phone", appConfig.getBusinessPhone() + "");
            jsonObject.put("business_address", appConfig.getBusinessAddress() + "");

            if (emailType.toUpperCase().equals("TIME_CHANGE")) {
                jsonObject.put("status_update_text", "Delivery Time update");
                jsonObject.put("subject", "Redmango order status update");
                jsonObject.put("email_type", "DELIVERY_TIME_CHANGE");
            } else if (emailType.toUpperCase().equals("REFUNDED")){
                jsonObject.put("status_update_text", "Refund request");
                jsonObject.put("subject", "Refund Request");
                jsonObject.put("email_type", "REFUND");
            } else if (emailType.toUpperCase().equals("ERROR")) {
                jsonObject.put("status_update_text", "Api call failed");
                jsonObject.put("subject", "Api Call Error");
                jsonObject.put("app_name", "Epos");
                jsonObject.put("body", "" + errorBody);
                jsonObject.put("email_type", "ERROR");
            } else {
                jsonObject.put("status_update_text", "Delivery status update");
                jsonObject.put("subject", "Updating Order Request");
                jsonObject.put("email_type", emailType.toUpperCase() + "");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        Log.e("EmailBody==>", jsonObject.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url("https://email.redmango.online/api/email/send")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("Accept","application/json")
                .addHeader("Authorization","Bearer 3|JdUKCKGyctQ3xb7iCBTU2Fnt5eJOJuaBZnCLI9lJ")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            if(response.code() == 200){
                Log.e("Email Response==>", Objects.requireNonNull(response.body()).toString() + " | "+ response.code());
            } else {
                String body = "emailError: code:"+response.code()+" | failed to send email on status update | " + response.message() + " | " + jsonObject.toString() + " | url: https://email.redmango.online/api/email/send";
                new SendEmail(context, orderData, "ERROR", body).execute();
            }
        }
        return null;
    }
}
