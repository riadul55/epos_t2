package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class TransactionModel {
    public int cashId,type,cashSubmitted,orderSubmitted,orderId,serverId,inCash;
    public double amount;
    public String orderStatus,paymentStatus,paymentMethod,note;

    public TransactionModel(int cashId, int type, double amount, int cashSubmitted, int orderSubmitted, int orderId, int serverId, String orderStatus, String paymentStatus, String paymentMethod,String note,int inCash) {
        this.cashId = cashId;
        this.type = type;
        this.amount = amount;
        this.cashSubmitted = cashSubmitted;
        this.orderSubmitted = orderSubmitted;
        this.orderId = orderId;
        this.serverId = serverId;
        this.orderStatus = orderStatus;
        this.paymentStatus = paymentStatus;
        this.paymentMethod = paymentMethod;
        this.note = note;
        this.inCash = inCash;
    }
}
