package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OrderApi {
    Context context;
    public OrderApi(Context context){
        this.context = context;
    }
    public OrderModel getOrderData(int orderId){
        OkHttpClient client = new OkHttpClient();
        OrderModel order  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+orderId)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code() == 200){

                try {
                    String jsonData = response.body().string();

                    JSONObject jsonObject = new JSONObject(jsonData);

                    order = OrderModel.fromJSON(jsonObject);

                    if(order.bookingId != 0){
                        order.tableBookingModel = getBookingData(order.bookingId);
                    }


//                    if(order.customer!=null){
//                        if(order.customer.profile.phone != null)
//                            if(!order.customer.profile.phone.equals("null") && !order.customer.profile.phone.isEmpty())
//                                if(!order.requester_type.equals("ONE_TIME")){
//                                    DBCustomerManager customerManager = new DBCustomerManager(context);
//                                    int id = customerManager.customerExist(order.customer.consumer_uuid);
//                                    if(id == 0){
//                                        try {
//                                            customerManager.addNewCustomer(order.customer, true);
//                                        }catch (Exception e){
//                                            Log.e("mmmmm", "getOrderData: "+ order.serverId +"e");
//                                        }
//                                    }else{
//                                       CustomerModel _customerModel =  CustomerApi.getCustomerData(order.customer.consumer_uuid,context);
//                                       customerManager.updateCustomer(_customerModel,true);
//                                    }
//                                }
//                    }

                } catch (Exception e) {
                    Log.d("mmmmm", "getOrderData: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.e("mmmmm", "getOrderData: RE"+response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return order;
    }

    public List<OrderModel> getOrderList(String endPoint){
        OkHttpClient client = new OkHttpClient();
        List<OrderModel> orders = new ArrayList<>();
        Request request = new Request.Builder()
                .url(BASE_URL+endPoint)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession",providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.d("mmmm", "getOrderList: "+response.code());
            if(response.code() == 200){
                try {
                    String jsonData = response.body().string();
                    JSONArray jsonArray = new JSONArray(jsonData);
                    for (int i = 0;i<jsonArray.length();i++){
                        OrderModel _order = OrderModel.fromJSON(jsonArray.optJSONObject(i));
                        if(_order != null)
                            orders.add(_order);
                    }
                } catch (Exception e) {
                    Log.d("mmmmm", "failedToUpdate: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.d("mmmmm", "failedToUpdate: RE "+response.code());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return orders;
    }

    public TableBookingModel getBookingData(int bookingId){
        OkHttpClient client = new OkHttpClient();
        TableBookingModel tableBookingModel  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"booking/table/"+bookingId)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code() == 200){
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    tableBookingModel = tableBookingModel.fromJSON(jsonObject);

                } catch (Exception e) {
                    Log.d("mmmmm", "getOrderData: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.e("mmmmm", "getOrderData: RE"+response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tableBookingModel;
    }
}
