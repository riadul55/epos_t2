package com.novo.tech.redmangopos.view.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.fragment.PaymentCalculator;
import com.novo.tech.redmangopos.view.fragment.PaymentCart;
import com.novo.tech.redmangopos.view.fragment.PaymentMethod;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;

public class Payment extends BaseActivity {
    OrderModel orderModel;
    int orderID;
    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;
    boolean isCheckout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        orderID = getIntent().getIntExtra("orderData",0);
        isCheckout = getIntent().getBooleanExtra("isCheckout", false);
        orderModel = SharedPrefManager.getOrderData(getApplicationContext(),orderID);
        Log.e("Delivery Charge", orderModel.deliveryCharge + "");

        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);
        callingPanel = findViewById(R.id.CallingView);
        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });

        if(orderModel != null){
            openCartFragment();
            openCalculatorFragment();
            openPaymentMethodFragment();
        }


//        if (orderModel != null && orderModel.bookingId != 0) {
//            orderModel.items = BookingOrdersHelper.getMergedItems(getApplicationContext(), orderModel.bookingId, "");
//            DBOrderManager orderManager = new DBOrderManager(getApplicationContext());
//            List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(orderModel.bookingId, "");
//            for (OrderModel model: ordersByBookingId) {
//                orderModel.totalPaid+=model.totalPaid;
//            }
//            orderModel.dueAmount = orderModel.total - orderModel.totalPaid;
//            Log.e("TotalDueAmount===>",  orderModel.total + " - " + orderModel.totalPaid + " = " +orderModel.dueAmount);
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1){
            if(resultCode == RESULT_OK) {
                CustomerModel customerModel = (CustomerModel) data.getSerializableExtra("customer");
                String orderType  = data.getStringExtra("orderType");
                if(orderType.isEmpty()){
                    Toast.makeText(this, "Please Select Order Type to redirect order create page", Toast.LENGTH_SHORT).show();
                }else{
                    if(customerModel!=null){
                        openOrderPage(orderType,customerModel);
                    }
                }

            }
        }
    }

    void openOrderPage(String orderType,CustomerModel customerModel){

        PaymentCart paymentCart = (PaymentCart) getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCart);
        paymentCart.orderModel.comments = paymentCart.paymentMethod.comments.getText().toString();
        SharedPrefManager.createUpdateOrder2(getApplicationContext(),paymentCart.orderModel);

        Intent in=new Intent(this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType",orderType);
        b.putSerializable("customerModel",customerModel);
        in.putExtras(b);
        startActivity(in);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void openCartFragment(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel", orderModel);
        bundle.putBoolean("isCheckout", isCheckout);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutPaymentCart, PaymentCart.class,bundle)
                .commit();
    }
    void openCalculatorFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutPaymentCenter, PaymentCalculator.class,null)
                .commit();
    }
    void openPaymentMethodFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutPaymentRight, PaymentMethod.class,null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        PaymentCart paymentCart = (PaymentCart) getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCart);
        if (paymentCart != null) {
            paymentCart.backToOrder();
        }
    }

    @Override
    public void onChanged() {

    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}