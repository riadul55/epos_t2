package com.novo.tech.redmangopos.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.fragment.OnlineOrderDetailsAction;
import com.novo.tech.redmangopos.view.fragment.OnlineOrderDetailsCart;
import com.novo.tech.redmangopos.view.fragment.OnlineOrderDetailsConsumerInfo;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;

public class OnlineOrderDetails extends BaseActivity {
    int orderID;
    public OrderModel orderModel;
    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_order_details);
        orderID = getIntent().getIntExtra("orderData",0);
        orderModel = SharedPrefManager.getOrderData(getApplicationContext(),orderID);

//        if (orderModel != null && orderModel.bookingId != 0 && orderModel.tableBookingModel != null) {
//            orderModel.items = BookingOrdersHelper.getMergedItems(getApplicationContext(), orderModel.bookingId, orderModel.order_status);
//        }

        if(orderModel != null){
            openCartFragment();
            openActionFragment();
            openConsumerFragment();
        }
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);
        callingPanel = findViewById(R.id.CallingView);
        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });
    }


    public void openOrderEditPage(OrderModel orderModel){
        finish();
        Intent in=new Intent(OnlineOrderDetails.this,OrderCreate.class);
        Bundle b = new Bundle();
        b.putInt("orderModel", orderModel.db_id);
        b.putString("order", "add");
        in.putExtras(b);
        startActivity(in);
    }
    public void openOrderPageWithTable(OrderModel orderModel){
        finish();
        Intent in=new Intent(this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType","LOCAL");
        b.putString("bookingId", orderModel.bookingId + "");
        b.putSerializable("table", orderModel.tableBookingModel);
        b.putSerializable("customerModel",orderModel.customer);
        b.putSerializable("shippingAddress",orderModel.shippingAddress);
        in.putExtras(b);
        startActivity(in);
    }

    void openCartFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCart, OnlineOrderDetailsCart.class,bundle)
                .commit();
    }
    void openActionFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCenter, OnlineOrderDetailsAction.class,bundle)
                .commit();
    }
    void openConsumerFragment(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel",orderModel);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsRight, OnlineOrderDetailsConsumerInfo.class,bundle)
                .commit();
    }

    void backToHome(){
        Intent gotoScreenVar = new Intent(OnlineOrderDetails.this, Dashboard.class);
//        gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        gotoScreenVar.putExtra("shift","EXISTING");
        gotoScreenVar.putExtra("orders",0);
        startActivity(gotoScreenVar);
    }
    @Override
    public void onBackPressed() {
        backToHome();
    }

    @Override
    public void onChanged() {
    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}