package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.TableSelectCallBack;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.jetbrains.annotations.NotNull;

public class TableListAdapter extends RecyclerView.Adapter<TableListAdapter.ViewHolder> {
    Context mCtx;
    TableSelectCallBack callBack;
    public TableListAdapter(Context mCtx,TableSelectCallBack callBack) {
        this.mCtx = mCtx;
        this.callBack = callBack;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.item_table_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        int tableNo = holder.getAdapterPosition()+1;
        boolean isBooked = SharedPrefManager.getTableBooked(mCtx,tableNo);

        holder.tv_table_number.setText("Table : "+String.valueOf(tableNo));

        if(!isBooked){
            holder.table_card.setCardBackgroundColor(mCtx.getResources().getColor(R.color.DarkGreen));
        }else{
            holder.table_card.setCardBackgroundColor(mCtx.getResources().getColor(R.color.DarkRed));
        }

        holder.table_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onTableSelect(tableNo);
            }
        });

        holder.table_card.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        bookOrFreeNow(tableNo,!isBooked);
                        return true;
                    }
                }
        );

    }

   void bookOrFreeNow(int tableNo,boolean isBooked){
        new AlertDialog.Builder(mCtx)
                .setMessage("Are you sure you want "+(isBooked?"book":"free")+" this table ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.setTableBooked(mCtx,tableNo,!isBooked);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();

    }


//    void freeTheTable(TableBookingModel table) {
//        ProgressDialog dialog = ProgressDialog.show(mCtx, "", "Loading. Please wait...", true);
//        dialog.show();
//        JSONObject customerJson = new JSONObject();
//        OkHttpClient client = new OkHttpClient();
//        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
//        Request request = new Request.Builder()
//                .url(BASE_URL + "config/booking/table/" + (table.free ? "busy" : "free") + "?floor_no=" + table.floor_no + "&local_no=" + table.local_no + "&table_no=" + table.table_no)
//                .patch(requestBody)
//                .addHeader("Content-Type", "application/json")
//                .addHeader("ProviderSession", providerSession)
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//
//            @Override
//            public void onFailure(@NotNull Call call, @NotNull IOException e) {
//                dialog.dismiss();
//                ((Dashboard) mCtx).failedToConnect();
//            }
//
//            @Override
//            public void onResponse(@NotNull Call call, @NotNull Response response) {
//                dialog.dismiss();
//                if (response.code() == 200) {
//                    table.free = !table.free;
//                    updateData(table);
//                } else {
//                    ((Dashboard) mCtx).failedToConnect();
//                }
//            }
//        });
//
//
//    }
//
//    public void updateData(TableBookingModel table) {
//        for (int i=0;i<tableBookingModelArrayList.size();i++) {
//            if (tableBookingModelArrayList.get(i).table_no == table.table_no) {
//                tableBookingModelArrayList.set(i, table);
//                break;
//            }
//        }
//        ((Dashboard) mCtx).runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                notifyDataSetChanged();
//            }
//        });
//    }

    @Override
    public int getItemCount() {
        return 20;
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_table_number;
        CardView table_card;
        LinearLayout ll_table;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_table_number = itemView.findViewById(R.id.tv_table_number);
            table_card = itemView.findViewById(R.id.table_card);
            ll_table = itemView.findViewById(R.id.ll_table);
        }
    }
}
