package com.novo.tech.redmangopos.view.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.view.activity.OrderDetails;
import com.novo.tech.redmangopos.view.activity.Payment;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.ActionButtonsAdapter;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;

public class OrderDetailsAction extends Fragment implements MyCallBack {
    public OrderModel orderModel;
    TextView totalAmount, orderDate, paymentMessage;
    public OrderDetailsCart cartListFragment;
    public OrderDetailsConsumerInfo consumerInfoFragment;
    ActionButtonsAdapter adapter;
    GridView gridView;
    AppCompatButton payNow, closeNow;
    boolean customerOrder = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_order_details_action, container, false);
        customerOrder = requireArguments().getBoolean("customerOrder", false);

        cartListFragment = (OrderDetailsCart) ((FragmentActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        consumerInfoFragment = (OrderDetailsConsumerInfo) ((FragmentActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsRight);
        paymentMessage = v.findViewById(R.id.orderDetailsMessage);
        totalAmount = v.findViewById(R.id.orderDetailsActionsTotalAmount);
        orderDate = v.findViewById(R.id.orderDetailsActionsTotalOrderDate);
        gridView = v.findViewById(R.id.actionGridView);
        payNow = v.findViewById(R.id.payNowButton);
        closeNow = v.findViewById(R.id.closeButton);
        orderModel = cartListFragment.orderModel;
        totalAmount.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.total));
        orderDate.setText(orderModel.orderDateTime);
        if (!customerOrder)
            setActionView();
        setPay();
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerOrder) {
                    copyOrder();
                } else {
                    Intent intent = new Intent(getContext(), Payment.class);
                    intent.putExtra("orderData", orderModel.db_id);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    requireActivity().finish();
                }

            }
        });
        closeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeTheOrder();
            }
        });

        if (customerOrder) {
            payNow.setText("Copy Order");
            closeNow.setVisibility(GONE);
        }

        if (orderModel.bookingId != 0 && orderModel.tableBookingModel != null) {
            gridView.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    public void setActionView() {
        adapter = new ActionButtonsAdapter(getContext(), orderModel, this, false);
        gridView.setAdapter(adapter);
        if (orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("CANCELLED") || orderModel.order_status.equals("REFUNDED")) {
            closeNow.setVisibility(View.INVISIBLE);
        } else {
            closeNow.setVisibility(View.VISIBLE);
        }
    }

    void setPay() {
        if (!orderModel.paymentStatus.equals("PAID")) {
            paymentMessage.setText("Order Was Successfully Placed");
        } else
            paymentMessage.setText("Order was Successfully Paid");

        if (customerOrder) {
            payNow.setVisibility(View.VISIBLE);
        } else if (orderModel.paymentStatus.equals("PAID") || orderModel.order_status.equals("CANCELLED") || orderModel.order_status.equals("REFUNDED")) {
            payNow.setVisibility(GONE);
        } else {
            payNow.setVisibility(View.VISIBLE);
        }
        cartListFragment.paySeal.setText(orderModel.order_status.equals("CANCELLED") ? "REFUNDED" : orderModel.paymentStatus);
    }

    public void afterClose() {
        boolean inCash = false;
        if (orderModel.paymentMethod.equals("CASH")) {
            inCash = true;
        }
        OrderRepository orderManager = new OrderRepository(getContext());
        CashRepository dbCashManager = new CashRepository(getContext());
        if (orderModel.bookingId != 0) {
            OrderModel orderData = orderManager.getOrderData(orderModel.db_id);
            orderModel.items = orderData.items;
        }
//        if (orderModel.bookingId != 0) {
//            List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(orderModel.bookingId, "");
//            for (OrderModel model: ordersByBookingId) {
//                model.order_status = "CLOSED";
//
//                if (orderModel.db_id == model.db_id) {
//                    model.adjustmentAmount = orderModel.adjustmentAmount;
//                    model.adjustmentNote = orderModel.adjustmentNote;
//                }
//
//                boolean inCash1 = false;
//                if (model.paymentMethod.equals("CASH")) {
//                    inCash1 = true;
//                }
//                dbCashManager.open();
//                if (model.discountAmount != 0) {
//                    dbCashManager.addTransaction(getContext(), model.discountAmount, 2, model.db_id, inCash1);
//                }
//                if (model.tips != 0) {
//                    dbCashManager.addTransaction(getContext(), model.tips, 3, model.db_id, inCash1);
//                }
//                if (model.adjustmentAmount > 0) {
//                    dbCashManager.addTransaction(getContext(), model.adjustmentAmount, 5, model.db_id, inCash1);
//                }
//                dbCashManager.close();
//                SharedPrefManager.createUpdateOrder(getContext(), model);
//            }
//        } else {
//
//        }

        if (orderModel.discountAmount != 0) {
            dbCashManager.addTransaction(getContext(), orderModel.discountAmount, 2, orderModel.db_id, inCash);
        }
        if (orderModel.tips != 0) {
            dbCashManager.addTransaction(getContext(), orderModel.tips, 3, orderModel.db_id, inCash);
        }
//        if (orderModel.refundAmount != 0) {
//            dbCashManager.addTransaction(getContext(), orderModel.refundAmount, 4, orderModel.db_id, inCash);
//        }
        if (orderModel.adjustmentAmount > 0) {
            dbCashManager.addTransaction(getContext(), orderModel.adjustmentAmount, 5, orderModel.db_id, inCash);
        }

        SharedPrefManager.createUpdateOrder(getContext(), orderModel);
//
//        long dateTime = System.currentTimeMillis();
//        SharedPrefManager.setUpdateData(getContext(), new UpdateData(orderModel.db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
//
        OrderDetails OrderDetails = ((OrderDetails) getContext());
        if (OrderDetails != null) {
            OrderDetails.onBackPressed();
        }
    }

    @Override
    public void voidCallBack() {

    }

    void closeTheOrder() {
        orderModel = ((OrderDetails) getContext()).orderModel;
        if (!orderModel.paymentStatus.equals("PAID")) {
            Toast.makeText(getContext(), "Unpaid Order can't closed", Toast.LENGTH_LONG).show();
        } else {
            orderModel.order_status = "CLOSED";
            orderModel.adjustmentAmount = cartListFragment.orderModel.adjustmentAmount;
            orderModel.adjustmentNote = cartListFragment.orderModel.adjustmentNote;
            cartListFragment.status.setText(orderModel.order_status);
            afterClose();

        }
    }


    @Override
    public void OrderCallBack(OrderModel order) {
        orderModel = ((OrderDetails) getContext()).orderModel;
        orderModel.order_status = order.order_status;
        SharedPrefManager.createUpdateOrder(getContext(), orderModel);
        cartListFragment.status.setText(orderModel.order_status);

//        long dateTime = System.currentTimeMillis();
//        SharedPrefManager.setUpdateData(getContext(), new UpdateData(orderModel.db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
    }

    void copyOrder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Are sure you want copy this order as new order?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        orderModel.serverId = 0;
                        orderModel.db_id = 0;
                        orderModel.order_id = SharedPrefManager.getLastOrderId(getContext()) + 1;
                        orderModel.order_status = "NEW";
                        orderModel.totalPaid = 0;
                        orderModel.discountAmount = 0;
                        orderModel.tips = 0;
                        orderModel.paymentStatus = "UNPAID";
                        orderModel.order_channel = "EPOS";

                        orderModel.orderDateTime = DateTimeHelper.getTime();
                        int id = SharedPrefManager.createUpdateOrder(getContext(), orderModel);
                        if (id != 0) {
//                            Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//                            gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            gotoScreenVar.putExtra("shift", "EXISTING");
//                            startActivity(gotoScreenVar);

                            Intent in=new Intent(getContext(), OrderCreate.class);
                            Bundle b = new Bundle();
                            b.putInt("orderModel", id);
                            in.putExtras(b);
                            startActivity(in);
                        }
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();

    }

}