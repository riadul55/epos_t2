package com.novo.tech.redmangopos.room_db.repositories;

import android.content.Context;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.ShiftModel;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.dao.ShiftDao;
import com.novo.tech.redmangopos.room_db.entities.Shift;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ShiftRepository {
    private Context context;
    private ShiftDao shiftDao;

    public ShiftRepository(Context context) {
        this.context = context;
        AppDatabase database = AppDatabase.getInstance(context);
        shiftDao = database.ShiftDao();
    }

    public ShiftModel getCurrentShift(){
        ShiftModel currentShift = null;
        Date openTime = null;
        try {
            Shift shift = shiftDao.getCurrentShift();
            if (shift != null) {
                openTime = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault()).parse(shift.openTime);
                currentShift = new ShiftModel(shift.id, shift.cloudSubmitted ? 1:0, openTime, null);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentShift;
    }

    public String getLastShift() {
        Shift lastShift = shiftDao.getLastShift();
        return lastShift != null ? lastShift.closeTime : null;
    }

    public int openShift() {
        Shift shift = new Shift();
        shift.openTime = DateTimeHelper.getDBDateAndTime();
        shift.cloudSubmitted = false;
        return (int) shiftDao.insertShift(shift);
    }

    public void closeShift(int id){
        Shift shift = new Shift();
        shift.id = id;
        shift.closeTime = DateTimeHelper.getDBDateAndTime();
        shiftDao.updateShift(shift);
    }

    public void vacuumDb() {
        shiftDao.vacumDb(new SimpleSQLiteQuery("VACUUM"));
    }
}
