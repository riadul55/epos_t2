package com.novo.tech.redmangopos.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OnlineOrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.view.View.GONE;

public class OnlineOrderDetailsConsumerInfo extends Fragment implements View.OnClickListener {
    LinearLayout btnPrint,refund,voidOrder, addTableOrder;
    TextView firstName,lastName,email,phone,houseNumber,streetName,town,postCode,comments,deliveryTimeTextView,requestDeliveryTimeTextVIew;
    CustomerModel customerModel;
    OnlineOrderDetailsAction actionFragment;
    OnlineOrderDetailsCart cartFragment;
    RelativeLayout deliveryTimeContainer;
    String deliveryTime;
    OrderModel model;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_customer_info, container, false);

        model = (OrderModel) requireArguments().getSerializable("orderModel");
        actionFragment = (OnlineOrderDetailsAction)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
        cartFragment = (OnlineOrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        customerModel = model.customer;

        btnPrint = v.findViewById(R.id.orderDetailsConsumerPrint);
        refund = v.findViewById(R.id.orderDetailsConsumerRefund);
        voidOrder = v.findViewById(R.id.orderDetailsConsumerVoid);
        addTableOrder = v.findViewById(R.id.orderDetailsConsumerAddTableOrder);
        deliveryTimeContainer = v.findViewById(R.id.DeliveryTimeSelect);
        deliveryTimeTextView = v.findViewById(R.id.paymentMethodDeliveryTimeValue);
        requestDeliveryTimeTextVIew = v.findViewById(R.id.requestedPaymentMethodDeliveryTimeValue);
        btnPrint.setOnClickListener(this);
        refund.setOnClickListener(this);
        voidOrder.setOnClickListener(this);
        addTableOrder.setOnClickListener(this);
        deliveryTimeContainer.setOnClickListener(this);

        firstName = v.findViewById(R.id.orderDetailsConsumerFirstName);
        lastName = v.findViewById(R.id.orderDetailsConsumerLastName);
        email = v.findViewById(R.id.orderDetailsConsumerEmail);
        phone = v.findViewById(R.id.orderDetailsConsumerPhone);
        houseNumber = v.findViewById(R.id.orderDetailsConsumerBuilding);
        streetName = v.findViewById(R.id.orderDetailsConsumerStreet);
        town = v.findViewById(R.id.orderDetailsConsumerCity);
        postCode = v.findViewById(R.id.orderDetailsConsumerZip);
        comments = v.findViewById(R.id.commentsText);
        if(customerModel != null){
            if(customerModel.profile!= null){
                firstName.setText(": "+customerModel.profile.first_name);
                lastName.setText(": "+customerModel.profile.last_name);
                email.setText(": "+customerModel.email);
                phone.setText(": "+customerModel.profile.phone);
            }
//            if(customerModel.addresses!=null){
//                if(customerModel.addresses.size()!=0){
//                    if(customerModel.addresses.get(0).properties!=null) {
//                        CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
//                        houseNumber.setText(": "+pro.building);
//                        streetName.setText(": "+pro.street_number+" "+pro.street_name);
//                        town.setText(": "+pro.city);
//                        postCode.setText(": "+pro.zip);
//                    }
//                }
//            }
            if(model.shippingAddress!=null){
                CustomerAddress address =  model.shippingAddress;
                if(address.properties!=null){
                    CustomerAddressProperties pro = address.properties;
                    houseNumber.setText(": "+pro.building);
                    streetName.setText(": "+pro.street_number+" "+pro.street_name);
                    town.setText(": "+pro.city);
                    postCode.setText(": "+pro.zip);
                }
            }
        }
        comments.setText(model.comments);
        requestDeliveryTimeTextVIew.setText(model.requestedDeliveryTime);
        deliveryTimeTextView.setText(model.currentDeliveryTime);
        handleRefund();

        if (model.bookingId != 0 && model.tableBookingModel != null &&
                (model.paymentMethod.equals("CARD") || model.paymentMethod.equals("CASH")) && !model.order_status.equals("CLOSED") && !model.order_status.equals("CANCELLED") && !model.order_status.equals("REFUNDED")) {
            addTableOrder.setVisibility(View.VISIBLE);
        }
        return v;
    }

    public void handleRefund(){
        if(model != null){
            if(!model.paymentStatus.equals("PAID") || model.refundAmount!=0){
                refund.setVisibility(View.INVISIBLE);
            }else{
                refund.setVisibility(View.VISIBLE);
            }
            if(!model.paymentStatus.equals("PAID") && !model.order_status.equals("CANCELLED")){
                voidOrder.setVisibility(View.VISIBLE);
            }
        }

    }
    @Override
    public void onClick(View v) {
        OrderModel orderModel = ((OnlineOrderDetails)getContext()).orderModel;

        if(v.getId() == btnPrint.getId()){
            try {
                CashMemoHelper.printCustomerMemo(orderModel,getContext(),true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(v.getId() == refund.getId()){
            refundAlert();
        }else if(v.getId() == voidOrder.getId()){
            voidAlert();
        }else if(v.getId() == deliveryTimeContainer.getId()){
            //getOptionInput();
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            } else {
                getTimeInput(deliveryTimeTextView);
            }
        } else if (v.getId() == addTableOrder.getId()) {
            if (orderModel.paymentMethod.equals("CASH")) {
                ((OnlineOrderDetails) requireActivity()).openOrderEditPage(orderModel);
            } else {
                ((OnlineOrderDetails) requireActivity()).openOrderPageWithTable(orderModel);
            }
        }
    }
    void voidAlert(){
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Void Order")
                .setMessage("Are you sure ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        actionFragment.orderModel.order_status="CANCELLED";
                        actionFragment.setActionView();

                        actionFragment.payNow.setVisibility(GONE);
                        cartFragment.paySeal.setText("CANCELLED");
                        cartFragment.status.setText("CANCELLED");

                        voidOrder.setVisibility(View.INVISIBLE);
                        refund.setVisibility(View.INVISIBLE);
                        SharedPrefManager.createUpdateOrder(getContext(),actionFragment.orderModel);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void refundAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL);
        inputField.setText(String.format(Locale.getDefault(),"%.2f",(cartFragment.orderModel.totalPaid-cartFragment.orderModel.refundAmount)));
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Refund Order")
                .setView(inputField)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String str = inputField.getText().toString();
                        if(str.equals("")){
                            str = "0";
                        }
                        double amount = Double.parseDouble(str);
                        refund(amount);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void refund(double amount){
        cartFragment.orderModel.refundAmount = amount;
        actionFragment.orderModel.refundAmount = amount;

        CashRepository dbCashManager = new CashRepository(getContext());
        dbCashManager.addTransaction(getContext(),amount,4,actionFragment.orderModel.db_id,true);

        // save status
        actionFragment.orderModel.isSetUpdate = false;
        long dateTime = System.currentTimeMillis();
        SharedPrefManager.setUpdateData(getActivity(), new UpdateData(actionFragment.orderModel.db_id, actionFragment.orderModel.serverId + "", actionFragment.orderModel.order_channel, "REFUNDED", "" + dateTime));
//        if(actionFragment.orderModel.order_status.equals("CLOSED")){
//        }
        DecimalFormat df = new DecimalFormat("###.##");
        if(df.format(actionFragment.orderModel.total).equals(df.format(amount))){
            actionFragment.orderModel.order_status="REFUNDED";
            cartFragment.orderModel.order_status="REFUNDED";
            cartFragment.paySeal.setText("REFUNDED");
            cartFragment.status.setText("REFUNDED");
        } else {
            actionFragment.orderModel.order_status="CLOSED";
            cartFragment.orderModel.order_status="CLOSED";
            cartFragment.paySeal.setText("CLOSED");
            cartFragment.status.setText("CLOSED");
        }
        SharedPrefManager.createUpdateOrder(getContext(),actionFragment.orderModel);
        actionFragment.orderModel.isSetUpdate = true;
        refund.setVisibility(GONE);
        cartFragment.calculateTotal();
        actionFragment.setActionView();


    }
    void getTimeInput(TextView timeView){
        Calendar mcurrentTime = Calendar.getInstance();
        DecimalFormat fm = new DecimalFormat("00");
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        Date currentDate = new java.util.Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());
            currentDate = format.parse(cartFragment.orderModel.currentDeliveryTime);
            hour = currentDate.getHours();
            minute = currentDate.getMinutes();
        }catch (Exception e){
            e.printStackTrace();
        }

        MaterialTimePicker picker =
                new MaterialTimePicker.Builder()
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .setHour(12)
                        .setMinute(10)
                        .setTitleText("Select Time")
                        .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                        .setHour(hour)
                        .setMinute(minute)
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .build();
        Date finalCurrentDate = currentDate;
        picker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedHour = picker.getHour();
                int selectedMinute = picker.getMinute();
                String AM_PM ;
                if(selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    if(selectedHour>12){
                        selectedHour-=12;
                    }
                }
                deliveryTime = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(finalCurrentDate)+" "+String.valueOf(selectedHour)+":"+String.valueOf(selectedMinute)+" "+AM_PM;
                timeView.setText(deliveryTime);
                cartFragment.orderModel.currentDeliveryTime = deliveryTime;
                actionFragment.orderModel.currentDeliveryTime = deliveryTime;

                cartFragment.orderModel.isSetUpdate = false;
                SharedPrefManager.createUpdateOrder2(getContext(), cartFragment.orderModel);
                cartFragment.orderModel.isSetUpdate = true;

                long dateTime = System.currentTimeMillis();
                SharedPrefManager.setUpdateData(getContext(), new UpdateData(cartFragment.orderModel.db_id, cartFragment.orderModel.serverId + "", cartFragment.orderModel.order_channel, "TIME_CHANGE", "" + dateTime));
            }
        });
        picker.show(getChildFragmentManager(), picker.toString());
    }
}