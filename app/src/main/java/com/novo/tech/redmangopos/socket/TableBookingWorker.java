package com.novo.tech.redmangopos.socket;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

public class TableBookingWorker extends AsyncTask<Void, Void, Void> {
    Context context;
    JSONObject jsonObject;

    public TableBookingWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            JSONObject tableJson = jsonObject.getJSONObject("table");
            if (!tableJson.equals("null")) {
                TableBookingModel table = TableBookingModel.fromJSON(tableJson);
                Log.e("tableJson===>", tableJson.toString());
                Log.e("table===>", table.free + "");

                SharedPrefManager.setTableBooked(context,table.table_no, table.free);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        super.onPostExecute(unused);
    }
}
