package com.novo.tech.redmangopos.escpos;

public interface OnDeviceConnect {
    void onConnect(boolean isConnect);
}
