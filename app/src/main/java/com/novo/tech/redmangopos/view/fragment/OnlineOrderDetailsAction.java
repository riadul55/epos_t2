package com.novo.tech.redmangopos.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OnlineOrderDetails;
import com.novo.tech.redmangopos.view.activity.Payment;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.ActionButtonsAdapter;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;
import java.util.Locale;

public class OnlineOrderDetailsAction extends Fragment implements MyCallBack {
    OrderModel orderModel;
    TextView totalAmount,orderDate,paymentMessage;
    public OnlineOrderDetailsCart cartListFragment;
    public OnlineOrderDetailsConsumerInfo consumerInfoFragment;
    ActionButtonsAdapter adapter;
    GridView gridView;
    AppCompatButton payNow,closeButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_action, container, false);
        cartListFragment = (OnlineOrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        consumerInfoFragment = (OnlineOrderDetailsConsumerInfo)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsRight);
        totalAmount = v.findViewById(R.id.orderDetailsActionsTotalAmount);
        orderDate = v.findViewById(R.id.orderDetailsActionsTotalOrderDate);
        gridView = v.findViewById(R.id.actionGridView);
        paymentMessage = v.findViewById(R.id.paymentMessage);
        payNow = v.findViewById(R.id.payNowButton);
        closeButton = v.findViewById(R.id.closeButton);
        orderModel = cartListFragment.orderModel;
        totalAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f",(cartListFragment.orderModel.total)));
        orderDate.setText(orderModel.orderDateTime);

        setActionView();
        setPay();
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // paymentAlert("CARD");
                Intent intent = new Intent(getContext(), Payment.class);
                intent.putExtra("orderData",orderModel.db_id);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeTheOrder();
            }
        });

        if (orderModel.bookingId != 0 && orderModel.tableBookingModel != null) {
            gridView.setVisibility(View.INVISIBLE);
        }
        return v;
    }
    void setActionView(){
        adapter = new ActionButtonsAdapter(getContext(),orderModel,this,false);
        gridView.setAdapter(adapter);
        if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("CANCELLED") || orderModel.order_status.equals("REFUNDED")){
            closeButton.setVisibility(View.INVISIBLE);
        }else{
            closeButton.setVisibility(View.VISIBLE);
        }
    }
    void setPay(){
        if(!orderModel.paymentStatus.equals("PAID")){
            paymentMessage.setText("Order is UNPAID");
            payNow.setVisibility(View.VISIBLE);
        }else{
            payNow.setVisibility(View.GONE);
            paymentMessage.setText("Order was Successfully Paid");
        }

        if(orderModel.paymentStatus.equals("PAID") || orderModel.order_status.equals("CANCELLED") || orderModel.order_status.equals("REFUNDED")){
            payNow.setVisibility(View.GONE);
        }
        cartListFragment.paySeal.setText(orderModel.order_status.equals("CANCELLED")?"CANCELLED":orderModel.order_status.equals("REFUNDED")?"REFUNDED":cartListFragment.orderModel.paymentStatus);
        consumerInfoFragment.handleRefund();
    }

    @Override
    public void voidCallBack() {

    }

    void closeTheOrder(){
        orderModel = ((OnlineOrderDetails)getContext()).orderModel;
        if(!orderModel.paymentStatus.equals("PAID")){
            Toast.makeText(getContext(),"Unpaid Order can't closed",Toast.LENGTH_LONG).show();
        }else{
            orderModel.order_status = "CLOSED";
            cartListFragment.status.setText(orderModel.order_status);
            orderModel.adjustmentAmount =  cartListFragment.orderModel.adjustmentAmount;
            orderModel.adjustmentNote =  cartListFragment.orderModel.adjustmentNote;
            CashRepository dbCashManager = new CashRepository(getContext());
//            if (orderModel.bookingId != 0) {
//                List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(orderModel.bookingId, "");
//                for (OrderModel model: ordersByBookingId) {
//                    model.order_status = "CLOSED";
//
//                    if (orderModel.db_id == model.db_id) {
//                        model.adjustmentAmount = orderModel.adjustmentAmount;
//                        model.adjustmentNote = orderModel.adjustmentNote;
//                    }
//
//                    dbCashManager.open();
//                    if(model.adjustmentAmount !=0){
//                        dbCashManager.addTransaction(getContext(),model.adjustmentAmount,5,model.db_id,true);
//                    }
//                    if(model.tips!=0){
//                        dbCashManager.addTransaction(getContext(),model.tips,3,model.db_id,true);
//                    }
//                    dbCashManager.close();
//                    SharedPrefManager.createUpdateOrder(getContext(), model);
//                }
//            } else {
//            }

            SharedPrefManager.createUpdateOrder(getContext(),orderModel);
            if(orderModel.adjustmentAmount !=0){
                dbCashManager.addTransaction(getContext(),orderModel.adjustmentAmount,5,orderModel.db_id,true);
            }
            if(orderModel.tips!=0){
                dbCashManager.addTransaction(getContext(),orderModel.tips,3,orderModel.db_id,true);
            }
//            if(orderModel.refundAmount!=0){
//                dbCashManager.addTransaction(getContext(),orderModel.refundAmount,4,orderModel.db_id,true);
//            }
            cartListFragment.status.setText(orderModel.order_status);

//            long dateTime = System.currentTimeMillis();
//            SharedPrefManager.setUpdateData(getContext(), new UpdateData(orderModel.db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));


            OnlineOrderDetails onlineOrderDetails = ((OnlineOrderDetails)getContext());
            if(onlineOrderDetails != null){
                onlineOrderDetails.onBackPressed();
            }

        }
    }
    @Override
    public void OrderCallBack(OrderModel order) {
        orderModel = ((OnlineOrderDetails)getContext()).orderModel;
        orderModel.order_status = order.order_status;
        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
        cartListFragment.status.setText(orderModel.order_status);

//        long dateTime = System.currentTimeMillis();
//        SharedPrefManager.setUpdateData(getContext(), new UpdateData(orderModel.db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));

    }
}