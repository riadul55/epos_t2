package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novo.tech.redmangopos.R;

/**
 * This class created by alamin
 */
public class SelectedProductFragment extends Fragment {


    public static RecyclerView selectedProductDetailsRecycler;

    public SelectedProductFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selected_product, container, false);
        selectedProductDetailsRecycler = view.findViewById(R.id.selectedProductDetailsRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        selectedProductDetailsRecycler.setLayoutManager(linearLayoutManager);
        return view;
    }
}