package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import com.novo.tech.redmangopos.callback.CategoryImageCallBack;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

/**
 * This class created by alamin
 */
//todo:: this class didn't use i(alamin) create this class just for image api check

public class CategoryImageHelper extends AsyncTask<String,Void, String> {
    CategoryImageCallBack callBack;
    String imagePath;

    public CategoryImageHelper(String imagePath, CategoryImageCallBack callBack) {
        this.imagePath = imagePath;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... strings) {
        return getCategoryImg(imagePath);
    }

    private String getCategoryImg(String imagePath) {
        String image = "";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(BASE_URL+"config/product/property/category/"+imagePath+"/file")
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                image = response.body().string();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        this.imagePath = image;
        return image;
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onCategoryImage(s);
        super.onPostExecute(s);
    }
}
