package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "orders", indices = @Index(value = {"id", "order_id", "server_id", "order_status", "date"}))
public class Order {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "order_channel")
    @Nullable
    public String orderChannel;

    @ColumnInfo(name = "requester_type")
    @Nullable
    public String requesterType;

    @ColumnInfo(name = "order_id")
    @Nullable
    public int orderId;

    @ColumnInfo(name = "server_id")
    @Nullable
    public int serverId;

    @ColumnInfo(name = "date")
    @Nullable
    public String date;

    @ColumnInfo(name = "orderDateTime")
    @Nullable
    public String orderDateTime;

    @ColumnInfo(name = "requestedDeliveryDateTime")
    @Nullable
    public String requestedDeliveryDateTime;

    @ColumnInfo(name = "currentDeliveryDateTime")
    @Nullable
    public String currentDeliveryDateTime;

    @ColumnInfo(name = "deliveryType")
    @Nullable
    public String deliveryType;

    @ColumnInfo(name = "order_type")
    @Nullable
    public String orderType;

    @ColumnInfo(name = "order_status")
    @Nullable
    public String orderStatus;

    @ColumnInfo(name = "payment_method")
    @Nullable
    public String paymentMethod;

    @ColumnInfo(name = "payment_status")
    @Nullable
    public String paymentStatus;

    @ColumnInfo(name = "comments")
    @Nullable
    public String comments;

    @ColumnInfo(name = "subTotal")
    @Nullable
    public double subTotal;

    @ColumnInfo(name = "total")
    @Nullable
    public double total;

    @ColumnInfo(name = "discountableTotal")
    @Nullable
    public double discountableTotal;

    @ColumnInfo(name = "paidTotal")
    @Nullable
    public double paidTotal;

    @ColumnInfo(name = "dueAmount")
    @Nullable
    public double dueAmount;

    @ColumnInfo(name = "discountPercentage")
    @Nullable
    public int discountPercentage;

    @ColumnInfo(name = "discount")
    @Nullable
    public double discount;

    @ColumnInfo(name = "plasticBagCost")
    @Nullable
    public double plasticBagCost;

    @ColumnInfo(name = "containerBagCost")
    @Nullable
    public double containerBagCost;

    @ColumnInfo(name = "discountCode")
    @Nullable
    public String discountCode;

    @ColumnInfo(name = "refund")
    @Nullable
    public double refund;

    @ColumnInfo(name = "adjustment")
    @Nullable
    public double adjustment;

    @ColumnInfo(name = "adjustmentNote")
    @Nullable
    public String adjustmentNote;

    @ColumnInfo(name = "tips")
    @Nullable
    public double tips;

    @ColumnInfo(name = "deliveryCharge")
    @Nullable
    public double deliveryCharge;

    @ColumnInfo(name = "receive")
    @Nullable
    public double receive;

    @ColumnInfo(name = "cartItems")
    @Nullable
    public String cartItems;

    @ColumnInfo(name = "cloudSubmitted")
    @Nullable
    public boolean cloudSubmitted;

    @ColumnInfo(name = "fixedDiscount")
    @Nullable
    public boolean fixedDiscount;

    @ColumnInfo(name = "isDiscountOverride")
    @Nullable
    public boolean isDiscountOverride;

    @ColumnInfo(name = "shift")
    @Nullable
    public int shift;

    @ColumnInfo(name = "customerId")
    @Nullable
    public int customerId;

    @ColumnInfo(name = "shippingAddress")
    @Nullable
    public String shippingAddress;

    @ColumnInfo(name = "cashId")
    @Nullable
    public int cashId;

    @ColumnInfo(name = "paymentId")
    @Nullable
    public String paymentId;

    @ColumnInfo(name = "tableBookingId")
    @Nullable
    public int tableBookingId;

    @ColumnInfo(name = "tableBookingInfo")
    @Nullable
    public String tableBookingInfo;

    @ColumnInfo(name = "customerInfo")
    @Nullable
    public String customerInfo;
}
