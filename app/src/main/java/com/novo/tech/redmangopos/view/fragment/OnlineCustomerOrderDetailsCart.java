package com.novo.tech.redmangopos.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OnlineCustomerOrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.PaymentCartListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;
import java.util.Locale;

public class OnlineCustomerOrderDetailsCart extends Fragment implements View.OnClickListener {
    public TextView type,orderId,manualDiscount,subTotal,total,refund,tips,status,deliveryCharge,adjustmentAmount,adjustmentNote;
    RelativeLayout refundContainer,creditInputContainer;
    ImageView adjustmentNoteContainer;
    public TextView paySeal;
    RecyclerView recyclerViewList;
    PaymentCartListAdapter adapter;
    LinearLayout back;
    public OrderModel orderModel = new OrderModel();
    public double discountAmount,tipsAmount;
    OnlineCustomerOrderDetailsAction actionFragment;
    LinearLayout btnKitchenPrint;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_cart, container, false);
        String data = requireArguments().getString("orderModel");
        orderModel = GsonParser.getGsonParser().fromJson(data,OrderModel.class);
        type = v.findViewById(R.id.orderDetailsCartOrderType);
        orderId = v.findViewById(R.id.orderDetailsCartOrderNumber);
        manualDiscount = v.findViewById(R.id.orderDetailsCartManualDiscount);
        subTotal = v.findViewById(R.id.orderDetailsCartSubTotal);
        tips = v.findViewById(R.id.orderDetailsCartTips);
        total = v.findViewById(R.id.orderDetailsCartTotal);
        back = v.findViewById(R.id.orderDetailsCartBack);
        status = v.findViewById(R.id.orderDetailsCartOrderStatus);
        refund = v.findViewById(R.id.orderDetailsCartRefundAmount);
        creditInputContainer = v.findViewById(R.id.creditInput);
        refundContainer = (RelativeLayout) v.findViewById(R.id.refundContainer);
        paySeal = v.findViewById(R.id.paidText);
        adjustmentAmount = v.findViewById(R.id.orderDetailsCartAdjustment);
        adjustmentNote = v.findViewById(R.id.orderDetailsCartAdjustmentNote);
        adjustmentNoteContainer = v.findViewById(R.id.iv_orderDetailsCartAdjustmentNote);
        deliveryCharge = v.findViewById(R.id.orderDetailsCartDeliveryCharge);
        recyclerViewList = v.findViewById(R.id.orderDetailsCartRecyclerView);
        btnKitchenPrint = v.findViewById(R.id.orderDetailsCartKitchenPrint);
        orderId.setText("Order : # "+String.valueOf(orderModel.order_id));
        type.setText(orderModel.order_type);
        back.setOnClickListener(this);
        creditInputContainer.setOnClickListener(this);
        adjustmentNoteContainer.setOnClickListener(this);

        //for read only purpose
        creditInputContainer.setVisibility(View.GONE);
        adjustmentNoteContainer.setVisibility(View.GONE);


        status.setText(orderModel.order_status);
        deliveryCharge.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.deliveryCharge));
        actionFragment = (OnlineCustomerOrderDetailsAction) ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
       // paySeal.setText(orderModel.paymentStatus);
        loadData();

        btnKitchenPrint.setVisibility(View.INVISIBLE);
        return v;
    }

    void loadData(){
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        adapter = new PaymentCartListAdapter(getContext(), orderModel.items);
        recyclerViewList.setAdapter(adapter);

        calculateTotal();

    }
    public void calculateTotal(){
        try {
            CashRepository dbCashManager = new CashRepository(requireContext());
            List<TransactionModel> orderTransaction = dbCashManager.getOrderTransaction(orderModel.db_id);
            double totalRefundAmount = 0;
            for (int i=0;i<orderTransaction.size();i++) {
                if (orderTransaction.get(i).type == 4 && orderTransaction.get(i).inCash == 1) {
                    totalRefundAmount+=orderTransaction.get(i).amount;
                }
            }
            orderModel.refundAmount = totalRefundAmount;
        } catch (Exception e) {}

        discountAmount=orderModel.discountAmount;
        orderModel.subTotal = orderModel.total;
        tipsAmount=orderModel.tips;
//        for (CartItem item : orderModel.items){
//            subTotalAmount+=item.subTotal;
//        }
        orderModel.subTotal-=discountAmount;
        orderModel.subTotal-=orderModel.refundAmount;
        orderModel.subTotal-=orderModel.deliveryCharge;


        total.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.total+orderModel.adjustmentAmount));
        manualDiscount.setText("("+orderModel.discountPercentage+"%)  -£ "+String.format("%.2f", discountAmount));
        subTotal.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.subTotal));
        tips.setText("£ "+String.format(Locale.getDefault(),"%.2f", tipsAmount));
        refund.setText("-£ "+String.format(Locale.getDefault(),"%.2f", orderModel.refundAmount));
        adjustmentAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.adjustmentAmount));
        adjustmentNote.setText(" "+orderModel.adjustmentNote);

        if(orderModel.refundAmount !=0){
            refundContainer.setVisibility(View.VISIBLE);
        }else{
            refundContainer.setVisibility(View.GONE);
        }
        if(orderModel.order_status.equals("REFUNDED")){
            paySeal.setText("REFUNDED");
        }if(orderModel.order_status.equals("CANCELLED")){
            paySeal.setText("CANCELLED");
        }else{
            paySeal.setText(orderModel.paymentStatus);
        }



    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.orderDetailsCartBack){
            ((OnlineCustomerOrderDetails)getContext()).onBackPressed();
        }else if(v.getId() == R.id.iv_orderDetailsCartAdjustmentNote){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditNoteAlert();
            }
        }
    }

    void creditNoteAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_TEXT);
        inputField.setText(orderModel.adjustmentNote);
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Credit Note")
                .setView(inputField)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        creditNote(inputField.getText().toString());
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditNote(String str) {
        orderModel.adjustmentNote = str;
        SharedPrefManager.createUpdateOrder2(getContext(),orderModel);

        calculateTotal();
    }
}