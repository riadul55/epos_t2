package com.novo.tech.redmangopos.storage;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.TransactionModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@SuppressLint("Range")
public class DBCashManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBCashManager(Context c) {
        context = c;
    }

    public DBCashManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    double getTotal(String type){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where type=?",new String[]{type});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    double getTotalWithCash(String type){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where type=? AND inCash=1",new String[]{type});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }


    public double getBalance(){
        double totalCashIn = getTotalWithCash("1");
        double totalCashOut = getTotalWithCash("-1");
        double totalCashOrder = getTotalWithCash("0");
        double totalRefund = getTotalWithCash("4");
        return totalCashIn+totalCashOut+totalCashOrder-totalRefund;
    }
    public long addTransaction(Context context,double amount,int type,int orderId,boolean inCash){
        ContentValues contentValue = new ContentValues();
        contentValue.put("amount", amount);
        contentValue.put("dateTime",DateTimeHelper.getDBTime());
        contentValue.put("user", "demo");
        contentValue.put("shift", SharedPrefManager.getCurrentShift(context));
        contentValue.put("type", type);
        contentValue.put("order_id", orderId);
        contentValue.put("cloudSubmitted", 0);
        contentValue.put("inCash", inCash);
        contentValue.put("localPayment", 1);
        return database.insert(DatabaseHelper.CASH_TABLE_NAME,null,contentValue);
    }

    public void addPaidTransaction(Context context, double amount, int type, int orderId, boolean inCash){
        ContentValues contentValue = new ContentValues();
        contentValue.put("amount", amount);
        contentValue.put("dateTime",DateTimeHelper.getDBTime());
        contentValue.put("user", "demo");
        contentValue.put("shift", SharedPrefManager.getCurrentShift(context));
        contentValue.put("type", type);
        contentValue.put("order_id", orderId);
        contentValue.put("cloudSubmitted", 1);
        contentValue.put("inCash", inCash);
        contentValue.put("localPayment", 0);
        database.insert(DatabaseHelper.CASH_TABLE_NAME, null, contentValue);
    }

    public List<TransactionModel> getOrderTransaction(int dbId){
        open();
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select cash.id,cash.order_id,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash where order_id = ?",new String[]{String.valueOf(dbId)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getInt(cursor.getColumnIndex("type")),
                            cursor.getDouble(cursor.getColumnIndex("amount")),
                            cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                            0,
                            cursor.getInt(cursor.getColumnIndex("order_id")),
                            0,
                            "",
                            "",
                            "",
                            "",
                            cursor.getInt(cursor.getColumnIndex("inCash"))
                    );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return  transactionModelList;
    }



    public void deleteOrderTransaction(int dbId){
        open();
        int id = database.delete(DatabaseHelper.CASH_TABLE_NAME,"order_id = ?",new String[]{String.valueOf(dbId)});
        Log.d("mmmm", "deleteOrderTransaction: "+id);
        close();
    }

    public void deleteAllTransaction(){
        open();
        int id = database.delete(DatabaseHelper.CASH_TABLE_NAME," 1",new String[]{});
        close();
    }

    public void deleteLastDaysTransaction(){
        int daysCount = SharedPrefManager.getDaysCount(context);
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -daysCount);
        Date date = calendar.getTime();
        String _lastDate  = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);

        open();
        int id = database.delete(DatabaseHelper.CASH_TABLE_NAME," dateTime < ?",new String[]{_lastDate});
        close();
    }


    public List<TransactionModel> getAllGeneralTransaction(){
            open();
            List<TransactionModel> transactionModelList = new ArrayList<>();
            Cursor cursor = database.rawQuery("select cash.id,cash.order_id,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash where type = ? OR type=? AND cloudSubmitted=0",new String[]{String.valueOf(1),String.valueOf(-1)});
            if (cursor != null) {
                if(cursor.moveToFirst()){
                    do{
                        TransactionModel model =new TransactionModel(
                                cursor.getInt(cursor.getColumnIndex("id")),
                                cursor.getInt(cursor.getColumnIndex("type")),
                                cursor.getDouble(cursor.getColumnIndex("amount")),
                                cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                                0,
                                cursor.getInt(cursor.getColumnIndex("order_id")),
                                0,
                                "",
                                "",
                                "",
                                "",
                                cursor.getInt(cursor.getColumnIndex("inCash"))
                        );
                        if(model.note==null){
                            model.note = "";
                        }
                        transactionModelList.add(model);
                    }while(cursor.moveToNext());
                }
            }
            Objects.requireNonNull(cursor).close();
            close();
            return  transactionModelList;
        }

    public List<TransactionModel> getAllCashTransaction(){
        open();
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select orders.id as orderId,orders.adjustmentNote as adjustmentNote,orders.total,orders.discount,orders.server_id,orders.order_status,orders.payment_method,orders.payment_status,orders.cloudSubmitted as orderSubmitted,cash.id as cashId,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash LEFT OUTER  JOIN  orders on orders.id = cash.order_id AND orders.order_status !=?",new String[]{"NEW"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                        cursor.getInt(cursor.getColumnIndex("cashId")),
                        cursor.getInt(cursor.getColumnIndex("type")),
                        cursor.getDouble(cursor.getColumnIndex("amount")),
                        cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderId")),
                        cursor.getInt(cursor.getColumnIndex("server_id")),
                        cursor.getString(cursor.getColumnIndex("order_status")),
                        cursor.getString(cursor.getColumnIndex("payment_status")),
                        cursor.getString(cursor.getColumnIndex("payment_method")),
                        cursor.getString(cursor.getColumnIndex("adjustmentNote")),
                        cursor.getInt(cursor.getColumnIndex("inCash"))
                        );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
        }
        Objects.requireNonNull(cursor).close();
        close();
        return  transactionModelList;
    }

    public void updateTransaction(int dbID, boolean cloudSubmitted) {
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put("cloudSubmitted",cloudSubmitted);
        database.update(DatabaseHelper.CASH_TABLE_NAME, contentValues, "id=?", new String[]{String.valueOf(dbID)});
        close();
    }

    public int getTotalOrders(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where date BETWEEN ? AND ?",new String[]{_date.get(0), _date.get(1)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public int getOnlineOrders(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where  order_channel = ? AND date BETWEEN ? AND ?",new String[]{"ONLINE",_date.get(0), _date.get(1)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashAmount(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND  type=? AND inCash=?",new String[]{_date.get(0), _date.get(1),"0","1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCardAmount(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND type=? AND inCash=?",new String[]{_date.get(0), _date.get(1),"0","0"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCardAmountLocal(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND  type=? AND inCash=? AND localPayment=?",new String[]{_date.get(0), _date.get(1),"0","0","1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalDiningAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("select SUM(cash.amount) as total from orders INNER  JOIN  cash on orders.id = cash.order_id AND orders.order_channel = ? AND cash.type = 0 ",new String[]{"WAITERAPP"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalDeliveryAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("select SUM(cash.amount) as total from orders INNER  JOIN  cash on orders.id = cash.order_id AND orders.order_type = ? AND cash.type = 0 ",new String[]{"DELIVERY"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalTakeOutAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("select SUM(cash.amount) as total from orders INNER  JOIN  cash on orders.id = cash.order_id AND (orders.order_type = ? OR orders.order_type = ?) AND  cash.type = 0 ",new String[]{"TAKEOUT","COLLECTION"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalOrderAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("select SUM(cash.amount) as total from orders INNER  JOIN  cash on orders.id = cash.order_id AND  cash.type = 0 ",new String[]{});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }

    public double getTotalDiscount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime = ? AND type=?",new String[]{DateTimeHelper.getDBTime(),"2"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalTips(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime = ? AND type=?",new String[]{DateTimeHelper.getDBTime(),"3"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalRefund(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND type=?",new String[]{_date.get(0), _date.get(1),"4"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashIn(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND type=?",new String[]{_date.get(0), _date.get(1),"1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashOut(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where dateTime BETWEEN ? AND ? AND type=?",new String[]{_date.get(0), _date.get(1),"-1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }

}
