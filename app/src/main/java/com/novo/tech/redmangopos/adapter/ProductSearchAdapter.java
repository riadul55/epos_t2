package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class ProductSearchAdapter extends RecyclerView.Adapter<ProductSearchAdapter.ProductListViewHolder> {

    Context mCtx;
    List<Product> productList;
    OrderCartList cartListFragment;


    public ProductSearchAdapter(Context mCtx, List<Product> orderList) {
        this.mCtx = mCtx;
        this.productList = orderList;

    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_product, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int _position) {
        Product product = productList.get(holder.getAdapterPosition());
        int position = holder.getAdapterPosition();

        String price = String.format(Locale.getDefault(),"%.2f",product.price);
        if(product.componentList.size()>0 && !productList.get(position).productType.equals("BUNDLE")){
            price+="+";
        }
        holder.productTitle.setText(product.shortName);
        holder.productPrice.setText("£ "+price);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
                        break;
                    case MotionEvent.ACTION_UP:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
                        onItemClick(position);
                        break;
                }
                return true;
            }

        });
        if(cartListFragment == null){
            cartListFragment = (OrderCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
        }
        boolean added = false;
        if(cartListFragment != null)
            for (CartItem cartItem: cartListFragment.orderModel.items) {
                if(cartItem.uuid.equals(product.productUUid)){
                    added = true;
                    break;
            }
        }

        if(added){
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#DC143C")));

        }else{
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
        }

    }

    void onItemClick(int position){
        if(productList.get(position).componentList.size() > 0 && !productList.get(position).productType.equals("BUNDLE")){
            CategoryAndProduct categoryAndProduct = (CategoryAndProduct)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);
            categoryAndProduct.openComponentPage(productList.get(position));
        }else{
            boolean offer = false;
            String category = "";
            if(productList.get(position).properties!=null){
                offer = productList.get(position).properties.offer;
                category = productList.get(position).properties.category;
            }

            JSONObject _itm = CartItem.toJSONCartItem(productList.get(position),null,1,productList.get(position).price,productList.get(position).discountable,offer,false,category);
            cartListFragment.addToCart(CartItem.fromJSON(_itm));
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
}
