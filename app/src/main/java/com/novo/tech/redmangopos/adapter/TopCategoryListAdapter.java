package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.PropertyCallBack;
import com.novo.tech.redmangopos.model.Property;

import java.util.List;

public class TopCategoryListAdapter extends RecyclerView.Adapter<TopCategoryListAdapter.CategoryListViewHolder> {

    Context mCtx;
    List<Property> propertyList;
    PropertyCallBack callBack;
    Property selected;
    OrderCreate orderCreate;

    public TopCategoryListAdapter(Context mCtx, List<Property> propertyList,PropertyCallBack callBack) {
        this.mCtx = mCtx;
        this.propertyList = propertyList;
        this.callBack = callBack;
         orderCreate = ((OrderCreate)mCtx);
         selected = orderCreate.topCategory;
    }

    @NonNull
    @Override
    public CategoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_category_top, parent, false);
        return new CategoryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Property property = propertyList.get(position);
        holder.title.setText(property.key_name.replaceAll("category_","").replaceAll("category","All Category").toUpperCase());
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onSelect(property);
                selected = property;
                orderCreate.topCategory = selected;
                notifyDataSetChanged();
            }
        });
        if(selected==null){
            if(position == 0) {
                holder.title.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.Orange));
                holder.title.setTextColor(ContextCompat.getColor(mCtx, R.color.White));
            }
            else {
                holder.title.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.LightGrey));
                holder.title.setTextColor(ContextCompat.getColor(mCtx, R.color.Black));
            }

        }else if(selected.key_name.equals(property.key_name)){
            holder.title.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.Orange));
            holder.title.setTextColor(ContextCompat.getColor(mCtx, R.color.White));
        }else{
            holder.title.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.LightGrey));
            holder.title.setTextColor(ContextCompat.getColor(mCtx, R.color.Black));

        }

    }

    @Override
    public int getItemCount() {
        return propertyList.size();
    }

    static class CategoryListViewHolder extends RecyclerView.ViewHolder {

        AppCompatButton title;

        public CategoryListViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.propertyModelTitle);
        }
    }
}
