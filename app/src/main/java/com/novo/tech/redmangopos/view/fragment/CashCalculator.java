package com.novo.tech.redmangopos.view.fragment;

import android.app.Dialog;
import android.graphics.drawable.Animatable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CashKeyboardAdapter;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;

public class CashCalculator extends Fragment implements View.OnClickListener {
    CashKeyboardAdapter adapter;
    GridView gridView;
    EditText inputField;
    String buttonText = "CASH IN";
    int subMitType = 1;
    Button submit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cash_calculator, container, false);
        buttonText = requireArguments().getString("optionType");

        inputField = v.findViewById(R.id.cashCalculatorInputField);
        gridView = v.findViewById(R.id.cashCalculatorInputGridVIew);
        submit = v.findViewById(R.id.cashCalculatorSubmit);
        submit.setText(buttonText);
        submit.setOnClickListener(this);
        if(buttonText.equals("CASH IN")){
            subMitType = 1;
        }else{
            subMitType = -1;
        }
        inputField.setShowSoftInputOnFocus(false);
        inputField.setFocusable(false);
        setUpGridView();
        return v;
    }
    private void setUpGridView() {
        String[] data = {"1", "2", "3","4", "5", "6","7", "8", "9","","0","."};
        adapter = new CashKeyboardAdapter(getContext(),data);
        gridView.setAdapter(adapter);
    }
    public void onInput(String value){
        try {
            String str = inputField.getText().toString();
            if (!value.equals("")) {
                if (str.equals("0")) {
                    str = value;
                } else {
                    str += value;
                }

            } else {
                if (str.length() != 0) {
                    str = str.substring(0, str.length() - 1);
                    if (str.length() == 0) {
                        str = "0";
                    }
                }
            }
            inputField.setText(str);
            CashOptions options = (CashOptions) ((FragmentActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.cashActivityFrameLayoutRight);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void changeSubmitType(String type){
        buttonText = type;
        if(type.equals("CASH IN")){
            subMitType = 1;

        }else{
            subMitType = -1;
        }
        submit.setText(buttonText);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == submit.getId()){
            addTransaction();
        }

    }
    public void addTransaction(){
        CashOptions options = (CashOptions)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.cashActivityFrameLayoutRight);
        double amount = 0.0;
        amount = Double.parseDouble(inputField.getText().toString());
        if (amount>0 && !(options.balanceAmount < amount))
            cashTransactionAlert(amount);
        if(subMitType == -1){
            if(options.balanceAmount < amount){
                Toast.makeText(getContext(),"Cash out amount must be less then balance",Toast.LENGTH_LONG).show();
                return;
            }
        }
        CashRepository dbCashManager = new  CashRepository(getContext());
        dbCashManager.addTransaction(getContext(),amount*subMitType,subMitType,-1,true);
        inputField.setText("0");
        options.loadBalance();
    }

    public void cashTransactionAlert(Double amount)
    {

        final Dialog dialog = new Dialog((FragmentActivity)getContext());
        dialog.setContentView(R.layout.dialog_success_msg);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final ImageView successCheckAnimation = (ImageView) dialog.findViewById(R.id.dialog_success_check);
        final TextView dialog_success_text = (TextView) dialog.findViewById(R.id.dialog_success_text);
        final Button dialog_success_btn = (Button) dialog.findViewById(R.id.dialog_success_btn);

        dialog_success_text.setText("£ "+amount+" has been "+buttonText+" Successfully");
        ((Animatable) successCheckAnimation.getDrawable()).start();
        dialog_success_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);

/*
        new AlertDialog.Builder(getActivity())
                .setIcon(R.drawable.animated_vector_check)
                .setTitle("£ "+amount+" has been added")
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
        */
    }
}