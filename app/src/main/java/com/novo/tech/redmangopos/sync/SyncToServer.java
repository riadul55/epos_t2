package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.AppConfig;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class SyncToServer extends AsyncTask<String, Integer, String> {
    private Context context;
    RefreshCallBack callBack;

    public SyncToServer(Context context, RefreshCallBack callBack){
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.e("BackgroundPush", "Call");
        if (isNetworkAvailable()) {
            OrderRepository repository = new OrderRepository(context);

            List<UpdateData> updatedDataList = SharedPrefManager.getUpdatedData(context);
            for (UpdateData data : updatedDataList) {
                if (data != null) {
                    OrderModel orderData = repository.getOrderData(data.getOrderId());

                    if (data.getOrderChannel() != null && data.getOrderChannel().equals("ONLINE")) {
                        if (
                                data.getStatus().toUpperCase().equals("PROCESSING") ||
                                data.getStatus().toUpperCase().equals("READY") ||
                                data.getStatus().toUpperCase().equals("SENT") ||
                                data.getStatus().toUpperCase().equals("CLOSED") ||
                                data.getStatus().toUpperCase().equals("TIME_CHANGE") ||
                                data.getStatus().toUpperCase().equals("REFUNDED")
                        ) {
                            updateOnlineOrder(orderData, data, data.getStatus().toUpperCase());

                            if (data.getStatus().equals("CLOSED") || data.getStatus().equals("REFUNDED")) {
                                updateTransaction(data, data.getStatus().toUpperCase());
                            }
                        }
                    } else if (data.getOrderChannel() != null && data.getOrderChannel().equals("WAITERAPP")) {
                        if (data.getStatus().toUpperCase().equals("REVIEW")) {
                            createOrUpdate(orderData, data.getStatus().toUpperCase());
                        } else if (data.getStatus().toUpperCase().equals("CLOSED")) {
                            if (orderData.serverId == 0) {
                                createOrUpdate(orderData, data.getStatus().toUpperCase());
                            } else {
                                updateOnlineOrder(orderData, data, data.getStatus().toUpperCase());
                                updateTransaction(data, data.getStatus().toUpperCase());
                            }
                        } else if (data.getStatus().toUpperCase().equals("TIME_CHANGE")) {
                            updateOnlineOrder(orderData, data, data.getStatus().toUpperCase());
                        } else if (data.getStatus().toUpperCase().equals("REFUNDED")) {
                            updateTransaction(data, data.getStatus().toUpperCase());
                        }
                    } else {
                        if (data.getStatus().toUpperCase().equals("REVIEW")) {
                            createOrUpdate(orderData, data.getStatus().toUpperCase());
                        } else if (data.getStatus().toUpperCase().equals("CLOSED")) {
                            if (orderData.serverId == 0) {
                                createOrUpdate(orderData, data.getStatus().toUpperCase());
                            } else {
                                updateOnlineOrder(orderData, data, data.getStatus().toUpperCase());
                                updateTransaction(data, data.getStatus().toUpperCase());
                            }
                        } else if (data.getStatus().toUpperCase().equals("TIME_CHANGE")) {
                            updateOnlineOrder(orderData, data, data.getStatus().toUpperCase());
                        } else if (data.getStatus().toUpperCase().equals("REFUNDED")) {
                            updateTransaction(data, data.getStatus().toUpperCase());
                        }
                    }
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    Log.e("Thread interrupted", e.toString());
                }
            }
        }
        return "process";
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onChanged();
        super.onPostExecute(s);
    }

    void createOrUpdate(OrderModel order, String cacheStatus){
        CustomerRepository customerManager = new CustomerRepository(context);
        if(order.customer != null){
            if(order.customer.dbId > 1)
                order.customer = customerManager.getCustomerData(order.customer.dbId);
        }
        Log.e("dbID=====>", order.db_id + "");
        Log.e("ServerId=====>", order.serverId + "");
        Log.e("Status=====>", cacheStatus + "");
        if (order.order_status != null) {
            if(order.serverId == 0){
                createOrder(order, cacheStatus);
            }else{
                updateOrder(order, cacheStatus);
            }
        }
    }

    void updateTransaction(UpdateData data, String cacheStatus) {
        CashRepository cashManager = new CashRepository(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(data.getOrderId());

        Log.e("TransactionLength==>", allTransactions.size() + "");
        for (TransactionModel model: allTransactions) {
            Log.e("cashSubmitted==>", model.cashSubmitted + "");
            Log.e("cashServerId==>", model.serverId + "");
            Log.e("cashType==>", model.type + "");
            Log.e("cashIn==>", model.inCash + "");
            Log.e("cashAmount==>", model.amount + "");

            if(model.cashSubmitted==0){
                switch (model.type){
                    case 0:
                        String entryType = "CASH_ORDER";
                        if(model.inCash == 0){
                            entryType = "CARD_ORDER";
                        }
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),entryType,"", data, cacheStatus);
                        break;
                    case 2:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"DISCOUNT","", data, cacheStatus);
                        break;
                    case 3:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"TIP","", data, cacheStatus);
                        break;
                    case 4:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"REFUND","", data, cacheStatus);
                        break;
                    case 5:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"ADJUSTMENT",model.note, data, cacheStatus);
                        break;
                    default:
                }
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                Log.e("Thread interrupted", e.toString());
            }
        }
    }

    void createOrder(OrderModel order, String cacheStatus){
        JSONObject jsonObject = new JSONObject();
        JSONObject delivery = new JSONObject();
        JSONObject shipping = new JSONObject();
        JSONObject shippingProperties = new JSONObject();
        JSONObject orderProperties = new JSONObject();
        JSONObject profile = new JSONObject();
        JSONArray items = new JSONArray();

        if(order.deliveryCharge>0){
            CartItem deliveryItem = new CartItem(
                    0,
                    "delivery-charge",
                    "",
                    "",
                    new ArrayList<>(),
                    order.deliveryCharge,
                    0.0,
                    order.deliveryCharge,
                    order.deliveryCharge,
                    1,
                    1,
                    "",
                    "DELIVERY",0,false,false,false, "0","",new ArrayList<>(), true,
                    order.paymentMethod
            );
            order.items.add(deliveryItem);
        }

        try{
            for (CartItem cartItem: order.items) {
                if(cartItem.type.equals("DYNAMIC")){
                    items.put(dynamicItemToJSONOBJECT(cartItem));
                    orderProperties.put("print_"+cartItem.uuid,cartItem.printOrder);
                } else if (cartItem.type.equals("DELIVERY")) {
                    items.put(deliveryItemToJSONOBJECT(cartItem));
                } else
                    items.put(itemToJSONOBJECT(cartItem));
            }

            if(order.customer != null){
                if(order.customer.profile!=null){
                    profile.put("first_name",order.customer.profile.first_name);
                    profile.put("last_name",order.customer.profile.last_name);
                    profile.put("phone",order.customer.profile.phone);
                    profile.put("email",order.customer.email);
                }
                delivery.put("email",order.customer.email);
                delivery.put("profile",profile);

                if(order.shippingAddress!=null){
                    if(order.shippingAddress.properties!=null) {
                        CustomerAddressProperties pro = order.shippingAddress.properties;
                        shippingProperties.put("zip",pro.zip);
                        shippingProperties.put("country",pro.country);
                        shippingProperties.put("city",pro.city);
                        shippingProperties.put("street_number","");
                        shippingProperties.put("state",pro.state);
                        shippingProperties.put("building",pro.building);
                        shippingProperties.put("street_name",pro.street_name);
                    }

                }
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }else{
                profile.put("first_name","");
                profile.put("last_name","");
                profile.put("phone","");
                delivery.put("email","");
                delivery.put("profile",profile);

                shippingProperties.put("zip","");
                shippingProperties.put("country","");
                shippingProperties.put("city","");
                shippingProperties.put("street_number","");
                shippingProperties.put("state","");
                shippingProperties.put("building","");
                shippingProperties.put("street_name","");
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }
            jsonObject.put("order_type",order.order_type.toUpperCase().replaceAll("COLLECTION","TAKEOUT"));
            jsonObject.put("requested_delivery_timestamp",new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date()));
            jsonObject.put("order_provider_uuid", providerID);
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            jsonObject.put("order_channel","EPOS");
            orderProperties.put("comment",order.comments);
            jsonObject.put("requester",delivery);
            jsonObject.put("shipping_address",shipping);
            jsonObject.put("order_properties",orderProperties);
            jsonObject.put("order_items",items);

        }catch (JSONException e){
            e.printStackTrace();
        }

        Log.e("order", "createOrder: "+jsonObject);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }

        if(response != null){
            if(response.code() == 201){
                String generatedId = response.header("Location").replaceAll("/delivery/order/","");
                Log.e("Server Id", generatedId);
                order.serverId = Integer.parseInt(generatedId);
                order.cloudSubmitted = false;
                OrderRepository db = new OrderRepository(context);
                db.updateAfterPushed(order);

                removeUpdatedOrder(order.db_id, cacheStatus);


//                long dateTime = System.currentTimeMillis();
//                SharedPrefManager.setUpdateData(context, new UpdateData(order.db_id, generatedId + "", order.order_channel, "REVIEW", "" + dateTime));
            }else{
                Log.e("mmmmm", "createOrder: "+response.code()+"failed to create order"+ jsonObject);
            }
        }
    }

    JSONObject deliveryItemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            product.putOpt("product_uuid", cartItem.uuid);
            product.putOpt("product_type", cartItem.type);
            product.putOpt("units", cartItem.quantity);
            product.putOpt("net_amount", cartItem.total);
            product.putOpt("comment", cartItem.comment);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }

    JSONObject itemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            JSONArray items = new JSONArray();
            for (ComponentSection section: cartItem.componentSections) {
                JSONObject obj = new JSONObject();
                obj.putOpt("product_uuid",section.selectedItem.productUUid);
                obj.putOpt("product_type","COMPONENT");
                obj.putOpt("units",1);
                items.put(obj);
            }
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_type",cartItem.type.isEmpty()?"ITEM":cartItem.type);
            product.putOpt("units",cartItem.quantity);
            product.putOpt("comment",cartItem.comment);
            product.putOpt("items",items);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }
    JSONObject dynamicItemToJSONOBJECT(CartItem cartItem){
        double price = 0;
        if(cartItem.offered){
            price = cartItem.total;
        }else{
            price = cartItem.price * cartItem.quantity;
        }

        JSONObject product = new JSONObject();
        try{
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_short_name",cartItem.shortName);
            product.putOpt("product_type","DYNAMIC");
            product.putOpt("units",cartItem.quantity);
            product.putOpt("net_amount",price);
            product.putOpt("currency","GBP");
            product.putOpt("discountable",true);
            product.putOpt("comment",cartItem.comment);

        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }

    void updateOnlineOrder(OrderModel orderData, UpdateData order, String cacheStatus){
        if (!order.getServerId().equals("") && !order.getServerId().equals("0")) {
            JSONObject jsonObject = new JSONObject();

            try{
                if (order.getStatus().equals("TIME_CHANGE")) {
                    if(orderData.currentDeliveryTime == null){
                        orderData.currentDeliveryTime = DateTimeHelper.getTime();
                    }
                    Log.d("mmmmm", "Delivery time updateOrder: "+orderData.currentDeliveryTime);
                    jsonObject.put("current_delivery_timestamp", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
                    jsonObject.put("driver_provider_uuid", providerID);
                } else {
                    jsonObject.put("order_status", order.getStatus());
                    jsonObject.put("driver_provider_uuid", providerID);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.d("mmmmm", "updateOrder: "+jsonObject.toString());
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
            Request request = new Request.Builder()
                    .url(BASE_URL+"order/"+order.getServerId())
                    .patch(requestBody)
                    .addHeader("Content-Type","application/json")
                    .addHeader("ProviderSession", providerSession)
//                .addHeader("ProviderSession", "49cfb910-7c1e-4261-aed6-57afa4fd2e76")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(response != null){
                if(response.code() == 202){
                    sendEmail(orderData, order);
                    removeUpdatedOrder(order.getOrderId(), cacheStatus);
                }
            }
        } else {
            removeUpdatedOrder(order.getOrderId(), cacheStatus);

//            long dateTime = System.currentTimeMillis();
//            SharedPrefManager.setUpdateData(context, new UpdateData(orderData.db_id, orderData.serverId + "", orderData.order_channel, orderData.order_status, "" + dateTime));
        }

    }

    void sendEmail(OrderModel orderData, UpdateData data) {
        // email sending only for refund and time change
        if (orderData.order_channel.equals("ONLINE") &&
                (data.getStatus().toUpperCase().equals("TIME_CHANGE") ||
                data.getStatus().toUpperCase().equals("REFUNDED"))) {

            AppConfig appConfig = SharedPrefManager.getAppConfig(context);
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("consumer_first_name", orderData.customer.profile.first_name);
                jsonObject.put("order_id", orderData.serverId);
                jsonObject.put("customer_email", orderData.customer.email);
//                jsonObject.put("current_delivery_time", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
                jsonObject.put("new_delivery_time", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
                jsonObject.put("business_name", appConfig.getBusinessName());
                jsonObject.put("business_phone", appConfig.getBusinessPhone());
                jsonObject.put("business_address", appConfig.getBusinessAddress());

                if (data.getStatus().toUpperCase().equals("TIME_CHANGE")) {
                    jsonObject.put("status_update_text", "Delivery Time update");
                    jsonObject.put("subject", "Redmango order status update");
                    jsonObject.put("status_update_text", "Delivery time  update");
                    jsonObject.put("email_type", "DELIVERY_TIME_CHANGE");
                } else {
                    jsonObject.put("status_update_text", "Refund request");
                    jsonObject.put("subject", "Refund Request");
                    jsonObject.put("status_update_text", "Refund request");
                    jsonObject.put("email_type", "REFUND");
                }
            } catch (Exception e){
                e.printStackTrace();
            }

            Log.e("EmailBody==>", jsonObject.toString());
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
            Request request = new Request.Builder()
                    .url("https://email.redmango.online/api/email/send")
                    .post(requestBody)
                    .addHeader("Content-Type","application/json")
                    .addHeader("Accept","application/json")
                    .addHeader("Authorization","Bearer 3|JdUKCKGyctQ3xb7iCBTU2Fnt5eJOJuaBZnCLI9lJ")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(response != null){
                if(response.code() == 200){
                    Log.e("Email Response==>", response.body().toString() + " | "+ response.code());
                }
            }
        }
    }

    void updateOrder(OrderModel order, String cacheStatus){
        JSONObject jsonObject = new JSONObject();

        try{
            jsonObject.put("order_status", order.order_status);
            if(order.currentDeliveryTime == null){
                order.currentDeliveryTime = DateTimeHelper.getTime();
            }
            Log.d("mmmmm", "Delivery time updateOrder: "+order.currentDeliveryTime);
            jsonObject.put("current_delivery_timestamp", DateTimeHelper.formatLocalDateTimeForServer(order.currentDeliveryTime));
            Log.d("mmmmm", "Delivery time updateOrder: "+DateTimeHelper.formatLocalDateTimeForServer(order.currentDeliveryTime));

            jsonObject.put("driver_provider_uuid", providerID);


        }catch (Exception e){
            e.printStackTrace();
        }
        Log.d("mmmmm", "updateOrder: "+jsonObject.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+order.serverId)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
//                .addHeader("ProviderSession", "49cfb910-7c1e-4261-aed6-57afa4fd2e76")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            if(response.code() == 202){
                removeUpdatedOrder(order.db_id, cacheStatus);
            }
        }
    }

    void submitCashOrder(int dbID,double amount,String orderId,String entryType,String comment, UpdateData data, String cacheStatus){
        Log.e("mmmmmm", "submitCashOrder: "+orderId+" => "+entryType+" => "+amount);
        if (!orderId.equals("") && !orderId.equals("0")) {
            OkHttpClient client = new OkHttpClient();
            String token = providerSession;
            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("entry_type", entryType);
                jsonObject.put("amount",amount);
                jsonObject.put("comment",comment);
                jsonObject.put("payment_uuid","");
                jsonObject.put("provider_uuid", providerID);
            }catch (Exception e){
                e.printStackTrace();
            }
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());

            Request request = new Request.Builder()
                    .url(BASE_URL+"order/"+orderId+"/cash_entry")
                    .addHeader("Content-Type","application/json")
                    .post(requestBody)
                    .addHeader("ProviderSession",token==null?"":token)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                if(response!=null){
                    if(response.code() == 201){
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,true);


                        removeUpdatedOrder(data.getOrderId(), cacheStatus);
                    }else if(response.code() == 409){
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,true);

                        removeUpdatedOrder(data.getOrderId(), cacheStatus);
                    } else{
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,false);
                        Log.d("mmmmm", "submitCashOrder: "+orderId+"  "+response.body().string()+"  => "+jsonObject);
                    }

                    Log.d("mmmmmm", "submitCashOrder: result code = "+response.code());
                }

            }catch (Exception e){
                FirebaseCrashlytics.getInstance().recordException(e);
            }
        }
    }

    void removeUpdatedOrder(int db_id, String cacheStatus) {
        List<UpdateData> removeList = new ArrayList<>();
        List<UpdateData> updateDataList = SharedPrefManager.getUpdatedData(context);
        if (updateDataList != null && updateDataList.size() > 0) {
            for (UpdateData d : updateDataList) {
                if (d.getOrderId() == db_id && d.getStatus().equals(cacheStatus)) {
                    removeList.add(d);
                }
                if ((d.getOrderChannel().equals("EPOS") || d.getOrderChannel().equals("WAITERAPP") )&&
                        (!d.getStatus().equals("REVIEW") && !d.getStatus().equals("CLOSED"))) {
                    removeList.add(d);
                }
            }

            updateDataList.removeAll(removeList);
            SharedPreferences.Editor editor = SharedPrefManager.getSharedPreferences(context).edit();
            Gson gson = new Gson();
            String json = gson.toJson(updateDataList);
            Log.e("PrefJson", json);
            editor.putString("updatedDatas", json);
            editor.apply();
        }
    }
}
