package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.AddressHelperModel;
import com.novo.tech.redmangopos.model.JsonAddress;

import java.util.ArrayList;
import java.util.List;

public interface ZipCallBack {
    void onResponseFromServer(List<String> str);
    void onAddressResponse(List<JsonAddress> data);
}
