package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;
import com.novo.tech.redmangopos.model.ShiftReportModel;

import java.util.ArrayList;

public interface ShiftInfoOrderCallBack {
    void onShiftInfoOrderResponse(ShiftReportModel data);
}
