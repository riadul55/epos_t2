package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.CustomerInput;
import com.novo.tech.redmangopos.view.fragment.CustomerOrderList;
import com.novo.tech.redmangopos.view.fragment.CustomerSearch;

public class CustomerSelect extends BaseActivity {
    LinearLayout back;
    String customerPhone="";


    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_select);
        Bundle bundle = getIntent().getExtras();
        customerPhone = bundle.getString("customerPhone","");
        Log.e("customerPhone", customerPhone);

        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);
        callingPanel = findViewById(R.id.CallingView);

        back = (LinearLayout) findViewById(R.id.customerSelectBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        openCustomerInputFragment();
        openCustomerSearchFragment();
        openCustomerOrderListFragment();


        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    void openCustomerSearchFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("customerPhone",customerPhone);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutCustomerSelect, CustomerSearch.class,bundle)
                .commit();
    }
    void openCustomerInputFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("customerPhone",customerPhone);

        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutCustomerInput, CustomerInput.class,bundle)
                .commit();
    }

    private void openCustomerOrderListFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("customerPhone",customerPhone);

        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutPaymentRight, CustomerOrderList.class,bundle)
                .commit();
    }

    @Override
    public void onChanged() {

    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}