package com.novo.tech.redmangopos.extra;

import android.content.Context;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.KitchenPrintService;
import com.novo.tech.redmangopos.utils.SunmiPrintHelper;

import java.util.Locale;


public class CashMemoHelper {
    public enum printType {
        INVOICE,
        KITCHEN,
        BAR,
    }

    public static void printCustomerMemo(OrderModel orderModel, Context context, boolean multiplePrint) {
        PrintingService service = new PrintingService(context, orderModel, multiplePrint, printType.INVOICE);
        service.execute();
    }

    public static void printKitchenMemo(OrderModel orderModel, Context context, boolean multiplePrint) {
        PrintingService service = new PrintingService(context, orderModel, multiplePrint, printType.KITCHEN);
        service.execute();
    }

    public static void printKitchenFromWaiter(OrderModel orderModel, Context context, boolean multiplePrint, String printerIp) {
        KitchenPrintService service = new KitchenPrintService(context, orderModel, multiplePrint, printerIp, printType.KITCHEN);
        service.execute();
    }

    public static void printShiftReport(Context context) {
        int printSize = SharedPrefManager.getPrintSize(context);
        CashRepository cash = new CashRepository(context);

        String header = DateTimeHelper.getTime() + "\n" +
                SharedPrefManager.getBusinessName(context) + "\n" +
                SharedPrefManager.getBusinessLocation(context) + "\n" +
                "...............................\n";
        String info = "Total Orders            " + String.valueOf(cash.getTotalOrders()) +
                "\nOnline Orders           " + String.valueOf(cash.getOnlineOrders()) +
                "\nOnline Card Payment     £" + String.format(Locale.getDefault(), "%.2f", cash.getTotalCardAmount() - cash.getTotalCardAmountLocal()) +
                "\nLocal Card Payment      £" + String.format(Locale.getDefault(), "%.2f", cash.getTotalCardAmountLocal()) +
                "\nTotal Cash              £" + String.format(Locale.getDefault(), "%.2f", cash.getTotalCashAmount()) +
                "\nTotal Cash In           £" + String.format(Locale.getDefault(), "%.2f", cash.getTotalCashIn()) +
                "\nTotal Cash Out          £" + String.format(Locale.getDefault(), "%.2f", cash.getTotalCashOut()) +
                "\nRefunded Amount         £" + String.valueOf(cash.getTotalRefund()) +
                "\nBalance                 £" + String.format(Locale.getDefault(), "%.2f", cash.getBalance());


        SunmiPrintHelper.getInstance().printText(header, printSize, false, false, 1);
        SunmiPrintHelper.getInstance().printText(info, printSize, false, false, 0);
        SunmiPrintHelper.getInstance().feedPaper();
        SunmiPrintHelper.getInstance().cutpaper();
    }

}
