package com.novo.tech.redmangopos.room_db.repositories;

import android.app.Application;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.dao.CustomerDao;
import com.novo.tech.redmangopos.room_db.dao.OrderDao;
import com.novo.tech.redmangopos.room_db.entities.Customer;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.entities.OrderBackup;
import com.novo.tech.redmangopos.socket.SocketHelper;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.DatabaseHelper;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class OrderRepository {
    private Context context;
    private OrderDao orderDao;
    private CustomerDao customerDao;

    private LiveData<List<Order>> allOrders;

    public OrderRepository(Context context) {
        this.context = context;
        AppDatabase database = AppDatabase.getInstance(context);
        orderDao = database.OrderDao();
        customerDao = database.CustomerDao();
        allOrders = orderDao.getAll();
    }

    public LiveData<List<Order>> getAllOrders() {
        return allOrders;
    }

    public List<OrderModel> getActiveOrder() {
        List<String> timeRange = DateTimeHelper.getTimeRange(context);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new Object[]{"CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)});
        List<Order> activeOrders = orderDao.getOrders(query);

        List<OrderModel> orders = new ArrayList<>();
        List<OrderModel> tableOrders = new ArrayList<>();
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            if (orderModel.bookingId != 0) {
                List<OrderModel> ordersByBookingId = getOrdersByBookingId(orderModel.bookingId, "");
                boolean isLocalExist = false;
                for (OrderModel model: ordersByBookingId) {
                    if (!model.order_channel.equals("ONLINE")) {
                        boolean exist = orderExist(tableOrders, model.db_id);
                        if (!exist) {
                            tableOrders.add(model);
                        }
                        isLocalExist = true;
                        break;
                    }
                }
                if (!isLocalExist && ordersByBookingId.size() > 0) {
                    OrderModel orderData = null;
                    for (OrderModel model: ordersByBookingId) {
                        if (model.paymentMethod.equals("CASH")) {
                            orderData = model;
                            break;
                        }
                    }
                    if (orderData != null) {
                        boolean exist = orderExist(tableOrders, orderData.db_id);
                        if (!exist) {
                            tableOrders.add(orderData);
                        }
                    } else {
                        boolean exist = orderExist(tableOrders, ordersByBookingId.get(0).db_id);
                        if (!exist) {
                            tableOrders.add(ordersByBookingId.get(0));
                        }
                    }
                }
            } else {
                orders.add(orderModel);
            }
        }
        orders.addAll(tableOrders);
        return orders;
    }

    public boolean orderExist(List<OrderModel> orders, int dbId) {
        boolean exist = false;
        for (OrderModel model: orders) {
            if (model.db_id == dbId) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    public Order getOrderById(int id) {
        return orderDao.getById(id);
    }

    public OrderModel getOrderData(int id) {
        return getOrderDataFromEntity(orderDao.getById(id));
    }

    public List<OrderModel> getOrdersByBookingId(int id, String status) {
        List<String> timeRange = DateTimeHelper.getTimeRange(context);
        List<OrderModel> orders = new ArrayList<OrderModel>();
        SimpleSQLiteQuery query;
        if (status.equals("CLOSED")) {
            query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE tableBookingId = ? AND order_status = ? AND date BETWEEN ? AND ?", new Object[]{String.valueOf(id), "CLOSED", timeRange.get(0), timeRange.get(1)});
        } else if (status.equals("REFUNDED")) {
            query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE tableBookingId = ? AND order_status = ? OR order_status=? AND date BETWEEN ? AND ?", new Object[]{String.valueOf(id), "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)});
        } else if (status.equals("CANCELLED")) {
            query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE tableBookingId = ? AND order_status = ? AND date BETWEEN ? AND ?", new Object[]{String.valueOf(id), "CANCELLED", timeRange.get(0), timeRange.get(1)});
        } else if (status.equals("ONLINE")) {
            query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE tableBookingId = ? AND order_channel = ? AND order_status !=? AND order_status != ? AND order_status != ? AND date BETWEEN ? AND ?", new Object[]{String.valueOf(id), "ONLINE", "CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)});
        } else {
            query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE tableBookingId = ? AND order_status != ? AND order_status != ? AND order_status != ? AND date BETWEEN ? AND ?", new Object[]{String.valueOf(id), "CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)});
        }
        List<Order> orderList = orderDao.getByBookingId(query);
        for (Order order : orderList) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> getActiveOrderForWaiter() {
        List<OrderModel> orders = new ArrayList<>();
        List<String> timeRange = DateTimeHelper.getTimeRange(context);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status != ? AND order_status != ? AND order_status != ? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new Object[]{"CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<Order> getTodayOrder() {
        String dateStr = DateTimeHelper.getDBTime();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE date = ?", new Object[]{dateStr});
        return orderDao.getOrders(query);
    }


    public void dayCloseNow() {
        OrderRepository orderManager = new OrderRepository(context);
        List<Order> allActiveOrders = getTodayOrder();
        for (Order order : allActiveOrders) {
            OrderModel _order = getOrderDataFromEntity(order);
            orderManager.addOrderToHistory(_order);
        }
        int daysCount = SharedPrefManager.getDaysCount(context);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -daysCount);
        Date date = calendar.getTime();
        String _lastDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        orderDao.deleteOldOrder(_lastDate);
    }

    public int addOrUpdateOrder2(OrderModel orderModel) {
        int db_id = 0;
        Order orderById = getOrderById(orderModel.db_id);
        if (orderById != null) {
            Order orderData = getEntityFromOrder(orderModel);
            orderData.id = orderById.id;
            orderDao.updateOne(orderData);
            db_id = orderById.id;

            if (orderModel.tableBookingModel != null && orderModel.order_status.equals("CLOSED")) {
                SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, true);
            }
        } else {
            orderModel.order_id = getOrderMax() + 1;
            long rowId = orderDao.insertOne(getEntityFromOrder(orderModel));
            db_id = (int) rowId;

            if (orderModel.tableBookingModel != null) {
                SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
            }
        }
        SocketHelper.emitSuccess();
        SocketHelper.emitBookingList(context);
        SocketHelper.emitOrdersPref();

        return db_id;
    }


    public int addOrUpdateOrder(OrderModel orderModel) {
        int db_id = 0;
        Order orderById = getOrderById(orderModel.db_id);
        if (orderById != null) {
            Order orderData = getEntityFromOrder(orderModel);
            orderData.id = orderById.id;
            orderDao.updateOne(orderData);
            db_id = orderById.id;

            if (orderModel.tableBookingModel != null && orderModel.order_status.equals("CLOSED")) {
                SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, true);
            }

            if (!orderModel.order_status.equals("REFUNDED")) {
                long dateTime = System.currentTimeMillis();
                SharedPrefManager.setUpdateData(context, new UpdateData(db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
            }
        } else {
            orderModel.order_id = getOrderMax() + 1;
            long rowId = orderDao.insertOne(getEntityFromOrder(orderModel));
            db_id = (int) rowId;

            if (orderModel.tableBookingModel != null) {
                SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
            }

            if (!orderModel.order_status.equals("REFUNDED")) {
                long dateTime = System.currentTimeMillis();
                SharedPrefManager.setUpdateData(context, new UpdateData(db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
            }
        }
        SocketHelper.emitOrdersPref();
        return db_id;
    }

    public List<OrderModel> fetchAllOrderData() {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE cloudSubmitted = ?", new Object[]{"0"});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> getSearchOrder(String orderID) {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_id = ?", new Object[]{orderID});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> getRefundedOrder() {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status = ? OR order_status = ? ORDER BY date DESC LIMIT 0,30", new Object[]{"CLOSED", "REFUNDED"});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            if (orderModel.order_status.equals("REFUNDED"))
                orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> getCanceledOrder() {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status = ? ORDER BY date DESC LIMIT 0,30", new Object[]{"CANCELLED"});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> getCloseOrder() {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status = ? ORDER BY date DESC LIMIT 0,30", new Object[]{"CLOSED"});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public List<OrderModel> fetchDashboardData() {
        List<OrderModel> orders = new ArrayList<>();
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("SELECT * FROM orders WHERE order_status != ? AND order_status != ?", new Object[]{"CLOSED", "CANCELLED"});
        List<Order> activeOrders = orderDao.getOrders(query);
        for (Order order: activeOrders) {
            OrderModel orderModel = getOrderDataFromEntity(order);
            orders.add(orderModel);
        }
        return orders;
    }

    public void addOnlineOrder(OrderModel orderModel) {
        int id = getOrderMax();
        if (id == -1) {
            id = 0;
        }
        orderModel.order_id = id + 1;
        long insertedId = -1;
        insertedId = orderDao.insertOne(getEntityFromOrder(orderModel));

        if (insertedId != -1) {
            double subTotalAmount = 0;
            for (CartItem item : orderModel.items) {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);
                    for (CartItem extraItem : item.extra) {
                        price += extraItem.price;
                    }
                } else
                    price = item.total;

                subTotalAmount += price;
            }

            orderModel.db_id = (int) insertedId;
            orderModel.subTotal = subTotalAmount;
            orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips;

            String paymentStatus = "UNPAID";
            if (orderModel.paymentMethod.equals("CARD")) {
                paymentStatus = "PAID";
                orderModel.totalPaid = orderModel.total;
                orderModel.receive = orderModel.totalPaid;
                orderModel.cardPaid = orderModel.totalPaid;
                CashRepository cashManager = new CashRepository(context);
                cashManager.addPaidTransaction(context, orderModel.total, 0, orderModel.db_id, false);
            }
            orderModel.paymentStatus = paymentStatus;
            Order orderData = getEntityFromOrder(orderModel);
            orderData.id = orderModel.db_id;
            orderDao.updateOne(orderData);


            // book table for new order
            if (orderModel.tableBookingModel != null) {
                SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
            }

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
            if (SharedPrefManager.getPrintOnOnlineOrder(context)) {
                orderModel = getOrderData(orderModel.db_id);
                CashMemoHelper.printKitchenMemo(orderModel, context, true);

            }

        }
    }

    public int getOrderMax() {
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return orderDao.getOrderMax(_date.get(0), _date.get(1));
    }

    public void deleteAllOrder() {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                orderDao.deleteAllOrders();
            }
        }).execute();
    }


    public int onlineTableOrderExist(int serverId) {
        return orderDao.existOnlineTableOrder(String.valueOf(serverId));
    }

    public int onlineOrderExist(int serverId) {
        return orderDao.existOnlineOrder(String.valueOf(serverId));
    }

    public int orderExist(int db_id) {
        return orderDao.existOrder(String.valueOf(db_id));
    }

    public void updateAfterPushed(OrderModel orderModel) {
        Order order = getOrderById(orderModel.db_id);
        if (order != null) {
            order.cloudSubmitted = orderModel.cloudSubmitted;
            order.serverId = orderModel.serverId;
            orderDao.updateOne(order);
        }
    }

    public void updateFromServerStatus(OrderModel orderModel) {
        Order order = getOrderById(orderModel.db_id);
        if (order != null) {
            order.orderStatus = orderModel.order_status;
            order.cloudSubmitted = orderModel.cloudSubmitted;
            order.serverId = orderModel.serverId;
            orderDao.updateOne(order);
        }
    }

    public void updateOnlineOrder(int id, List<CartItem> items) {
        OrderModel orderData = getOrderData(id);
        List<CartItem> mergedItems = new ArrayList<>();
        for (CartItem item: items) {
            boolean flag = false;
            for (int j=0;j<orderData.items.size();j++) {
                if (item.localId == orderData.items.get(j).localId) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                mergedItems.add(item);
            }
        }
        mergedItems.addAll(orderData.items);
        orderData.items = mergedItems;

        Order updateOrder = getEntityFromOrder(orderData);
        updateOrder.id = orderData.db_id;
        orderDao.updateOne(updateOrder);
    }

    public int update(OrderModel orderModel) {
        return (int) orderDao.updateOne(getEntityFromOrder(orderModel));
    }

    public void delete(OrderModel orderModel) {
        OrderModel orderData = getOrderData(orderModel.db_id);

        if (orderData.serverId != 0) {
            long dateTime = System.currentTimeMillis();
            SharedPrefManager.setUpdateData(context, new UpdateData(orderData.db_id, orderData.serverId + "", orderData.order_channel, "CANCELLED", "" + dateTime));

            deleteQuery(orderData.db_id);
        } else {
            deleteQuery(orderData.db_id);
        }
    }

    public void deleteQuery(int id) {
        orderDao.deleteById(id);
    }

    public void deleteOldOrder(int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -day);
        Date date = calendar.getTime();
        String _lastDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        orderDao.deleteOldOrder(_lastDate);
    }

    public void vacuumDb() {
        orderDao.vacumDb(new SimpleSQLiteQuery("VACUUM"));
    }


    //--------------- order history backup
    public OrderModel getHistoryOrderData(int id) {
        OrderBackup historyOrderData = orderDao.getHistoryOrderData(id);
        return getOrderDataFromBackupEntity(historyOrderData);
    }

    public List<OrderModel> fetchCustomerOrderData(String dbId) {
        int limit = SharedPrefManager.getCustomerOrderCount(context);
        List<OrderModel> orders = new ArrayList<>();
        List<OrderBackup> orderBackups = orderDao.getBackupOrdersByCustomer(dbId, String.valueOf(limit));
        for (OrderBackup backup: orderBackups) {
            orders.add(getOrderDataFromBackupEntity(backup));
        }
        return orders;
    }

    public int addOrderToHistory(OrderModel orderModel) {
        orderModel.order_id = getOrderHistoryMax() + 1;
        return (int) orderDao.insertOne(getBackupEntityFromOrder(orderModel));
    }

    public int getOrderHistoryMax() {
        String _date = DateTimeHelper.getDBTime();
        return orderDao.getOrderHistoryMax(_date);
    }

    public void deleteAllCustomerOrder(int keepOrder) {
        List<Customer> allCustomer = customerDao.getAllCustomer();
        List<Integer> ids = new ArrayList<>();
        for (Customer item: allCustomer) {
            List<Integer> backupOrderIdsByCustomer = orderDao.getBackupOrderIdsByCustomer(item.id, keepOrder);
            ids.addAll(backupOrderIdsByCustomer);
        }

        List<Integer> ids2 = new ArrayList<>();
        List<Integer> backupOrderIds = orderDao.getBackupOrderIds();
        for (Integer id: backupOrderIds) {
            if (!ids.contains(id)) {
                ids2.add(id);
            }
        }

        for (int i : ids2) {
            orderDao.deleteBackupOrderById(i);
        }
    }
    //--------------- end order history backup

    private static class BackgroundTask extends AsyncTask<Order, Void, Void> {
        private OnListener listener;
        private BackgroundTask(OnListener listener) {
            this.listener = listener;
        }
        @Override
        protected Void doInBackground(Order... model) {
            listener.doInBackground();
            return null;
        }

        interface OnListener {
            void doInBackground();
        }
    }


    private OrderModel getOrderDataFromEntity(Order order) {
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        CustomerAddress shippingAddress = null;
        TableBookingModel tableBookingModel = null;
        String order_type = order.orderType;
        if (order.customerId != 0 && order.customerId != -1) {
            CustomerRepository customerManager = new CustomerRepository(context);
            customerModel = customerManager.getCustomerData(order.customerId);
        } else {
            customerModel = GsonParser.getGsonParser().fromJson(order.customerInfo, CustomerModel.class);
        }

        try {
            shippingAddress = GsonParser.getGsonParser().fromJson(order.shippingAddress, CustomerAddress.class);

            if (order.tableBookingInfo != null) {
                tableBookingModel = GsonParser.getGsonParser().fromJson(order.tableBookingInfo, TableBookingModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (order.cartItems != null) {
                JSONArray _data = new JSONArray(order.cartItems.replaceAll("NaN", "0.0"));
                for (int i = 0; i < _data.length(); i++) {
                    cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (order_type == null) {
            order_type = "N/A";
        }

        double cashPayment = 0;
        double cardPayment = 0;
        CashRepository cashManager = new CashRepository(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(order.id);

        for (TransactionModel model : allTransactions) {
            Log.e("inCash", model.inCash + "");
            Log.e("type", model.type + "");
            Log.e("amount", model.amount + "");
            if (model.inCash == 0) {
                if (model.type == 4) {
                    cardPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cardPayment -= model.amount;
                } else {
                    cardPayment += model.amount;
                }
            } else {
                if (model.type == 4) {
                    cashPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cashPayment -= model.amount;
                } else {
                    cashPayment += model.amount;
                }
            }
        }

        String orderChannel = order.orderChannel != null ? order.orderChannel : "EPOS";

        OrderModel orderModel = new OrderModel(order.orderId, orderChannel, order_type, order.orderStatus, order.paymentMethod, order.subTotal, order.total, order.discountableTotal, cardPayment + cashPayment, order.dueAmount, order.discount, order.discountCode, order.tips, cartItemList, order.orderDateTime, order.currentDeliveryDateTime, order.requestedDeliveryDateTime, order.deliveryType, order.paymentStatus,
                order.receive, order.id, order.date, order.comments, order.shift, order.cloudSubmitted, order.fixedDiscount, order.isDiscountOverride, order.serverId, customerModel, order.cashId, order.paymentId, order.deliveryCharge, order.discountPercentage, order.refund, order.adjustment, order.adjustmentNote,
                order.requesterType, new ArrayList<>(), order.plasticBagCost, order.containerBagCost, cashPayment, cardPayment, order.tableBookingId, tableBookingModel, shippingAddress);
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        for (CartItem item : orderModel.items) {
            if (item.uuid.equals("plastic-bag")) {
                orderModel.plasticBagCost = item.quantity * item.price;
            } else if (item.uuid.equals("container")) {
                orderModel.containerBagCost = item.quantity * item.price;
            } else {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else {
                    price = item.total;
                }

                subTotalAmount += price;

                if (item.discountable) {
                    _discountableTotal += (item.subTotal * item.quantity);
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.discountableTotal = _discountableTotal;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips + orderModel.plasticBagCost + orderModel.containerBagCost;

        if (order.refund >= order.total && order.total != 0) {
            orderModel.order_status = "REFUNDED";
        }
        return orderModel;
    }

    private Order getEntityFromOrder(OrderModel orderModel) {
        Order order = new Order();
        order.orderId = orderModel.order_id;
        if (orderModel.serverId != 0) {
            order.serverId = orderModel.serverId;
        }
        order.orderChannel = orderModel.order_channel;
        order.date = DateTimeHelper.formatDBDate(orderModel.orderDateTime);
        order.orderDateTime = orderModel.orderDateTime;
        order.requestedDeliveryDateTime = orderModel.requestedDeliveryTime;
        order.currentDeliveryDateTime = orderModel.currentDeliveryTime;
        order.deliveryType = orderModel.deliveryType;
        order.requesterType = orderModel.requester_type;
        order.orderType = orderModel.order_type;
        order.orderStatus = orderModel.order_status;
        order.paymentMethod = orderModel.paymentMethod;
        order.paymentStatus = orderModel.paymentStatus;
        order.comments = orderModel.comments;
        order.subTotal = orderModel.subTotal;
        order.total = orderModel.total;
        order.discountableTotal = orderModel.discountableTotal;
        order.discount = orderModel.discountAmount;
        order.discountCode = orderModel.discountCode;
        order.plasticBagCost = orderModel.plasticBagCost;
        order.containerBagCost = orderModel.containerBagCost;
        order.adjustment = orderModel.adjustmentAmount;
        order.adjustmentNote = orderModel.adjustmentNote;
        order.discountPercentage = orderModel.discountPercentage;
        order.refund = orderModel.refundAmount;
        order.deliveryCharge = orderModel.deliveryCharge;
        order.tips = orderModel.tips;
        order.shift = orderModel.shift;
        order.dueAmount = orderModel.dueAmount;
        order.paidTotal = orderModel.totalPaid;
        order.receive = orderModel.receive;
        order.paymentId = orderModel.paymentUid == null ? "-1" : orderModel.paymentUid;
        order.cashId = orderModel.cashId == 0 ? -1 : orderModel.cashId;
        order.customerId = orderModel.customer == null ? 0 : orderModel.customer.dbId;
        order.customerInfo = orderModel.customer != null ? GsonParser.getGsonParser().toJson(orderModel.customer) : "";
        order.shippingAddress = orderModel.shippingAddress != null ? GsonParser.getGsonParser().toJson(orderModel.shippingAddress) : "";
        order.tableBookingId = orderModel.bookingId;
        order.tableBookingInfo = orderModel.tableBookingModel != null ? GsonParser.getGsonParser().toJson(orderModel.tableBookingModel) : "";
        order.cloudSubmitted = orderModel.cloudSubmitted;
        order.fixedDiscount = orderModel.fixedDiscount;
        order.cartItems = GsonParser.getGsonParser().toJson(orderModel.items);
        return order;
    }

    private OrderModel getOrderDataFromBackupEntity(OrderBackup order) {
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        CustomerAddress shippingAddress = null;
        TableBookingModel tableBookingModel = null;
        String order_type = order.orderType;
        if (order.customerId != 0 && order.customerId != -1) {
            CustomerRepository customerManager = new CustomerRepository(context);
            customerModel = customerManager.getCustomerData(order.customerId);
        } else {
            customerModel = GsonParser.getGsonParser().fromJson(order.customerInfo, CustomerModel.class);
        }

        try {
            shippingAddress = GsonParser.getGsonParser().fromJson(order.shippingAddress, CustomerAddress.class);

            if (order.tableBookingInfo != null) {
                tableBookingModel = GsonParser.getGsonParser().fromJson(order.tableBookingInfo, TableBookingModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONArray _data = new JSONArray(order.cartItems.replaceAll("NaN", "0.0"));
            for (int i = 0; i < _data.length(); i++) {
                cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (order_type == null) {
            order_type = "N/A";
        }

        double cashPayment = 0;
        double cardPayment = 0;
        CashRepository cashManager = new CashRepository(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(order.id);

        for (TransactionModel model : allTransactions) {
            Log.e("inCash", model.inCash + "");
            Log.e("type", model.type + "");
            Log.e("amount", model.amount + "");
            if (model.inCash == 0) {
                if (model.type == 4) {
                    cardPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cardPayment -= model.amount;
                } else {
                    cardPayment += model.amount;
                }
            } else {
                if (model.type == 4) {
                    cashPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cashPayment -= model.amount;
                } else {
                    cashPayment += model.amount;
                }
            }
        }

        OrderModel orderModel = new OrderModel(order.orderId, order.orderChannel, order_type, order.orderStatus, order.paymentMethod, order.subTotal, order.total, order.discountableTotal, cardPayment + cashPayment, order.dueAmount, order.discount, order.discountCode, order.tips, cartItemList, order.orderDateTime, order.currentDeliveryDateTime, order.requestedDeliveryDateTime, order.deliveryType, order.paymentStatus,
                order.receive, order.id, order.date, order.comments, order.shift, order.cloudSubmitted, order.fixedDiscount, order.isDiscountOverride, order.serverId, customerModel, order.cashId, order.paymentId, order.deliveryCharge, order.discountPercentage, order.refund, order.adjustment, order.adjustmentNote,
                order.requesterType, new ArrayList<>(), order.plasticBagCost, order.containerBagCost, cashPayment, cardPayment, order.tableBookingId, tableBookingModel, shippingAddress);
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        for (CartItem item : orderModel.items) {
            if (item.uuid.equals("plastic-bag")) {
                orderModel.plasticBagCost = item.quantity * item.price;
            } else if (item.uuid.equals("container")) {
                orderModel.containerBagCost = item.quantity * item.price;
            } else {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else {
                    price = item.total;
                }

                subTotalAmount += price;

                if (item.discountable) {
                    _discountableTotal += (item.subTotal * item.quantity);
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.discountableTotal = _discountableTotal;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips + orderModel.plasticBagCost + orderModel.containerBagCost;

        if (order.refund >= order.total && order.total != 0) {
            orderModel.order_status = "REFUNDED";
        }
        return orderModel;
    }

    private OrderBackup getBackupEntityFromOrder(OrderModel orderModel) {
        if (orderModel.comments == null) {
            orderModel.comments = "";
        }
        if (orderModel.shift == 0) {
            orderModel.shift = SharedPrefManager.getCurrentShift(context);
        }

        double subTotalAmount = 0;
        for (CartItem item : orderModel.items) {
            double price = 0;
            if (!item.offered) {
                price = (item.subTotal * item.quantity);

                if (item.extra != null) {
                    for (CartItem extraItem : item.extra) {
                        price += extraItem.price;
                    }
                }
            } else
                price = item.total;

            subTotalAmount += price;
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips;

        String paymentStatus = "UNPAID";
        if (orderModel.paymentMethod.equals("CARD")) {
            paymentStatus = "PAID";
            orderModel.totalPaid = orderModel.total;
            orderModel.receive = orderModel.totalPaid;
            orderModel.cardPaid = orderModel.totalPaid;
        }

        OrderBackup order = new OrderBackup();
        order.orderId = orderModel.order_id;
        if (orderModel.serverId != 0) {
            order.serverId = orderModel.serverId;
        }
        order.orderChannel = orderModel.order_channel;
        order.date = DateTimeHelper.formatDBDate(orderModel.orderDateTime);
        order.orderDateTime = orderModel.orderDateTime;
        order.requestedDeliveryDateTime = orderModel.requestedDeliveryTime;
        order.currentDeliveryDateTime = orderModel.currentDeliveryTime;
        order.deliveryType = orderModel.deliveryType;
        order.requesterType = orderModel.requester_type;
        order.orderType = orderModel.order_type;
        order.orderStatus = orderModel.order_status;
        order.paymentMethod = orderModel.paymentMethod;
        order.paymentStatus = paymentStatus;
        order.comments = orderModel.comments;
        order.subTotal = orderModel.subTotal;
        order.total = orderModel.total;
        order.discountableTotal = orderModel.discountableTotal;
        order.discount = orderModel.discountAmount;
        order.discountCode = orderModel.discountCode;
        order.plasticBagCost = orderModel.plasticBagCost;
        order.containerBagCost = orderModel.containerBagCost;
        order.adjustment = orderModel.adjustmentAmount;
        order.adjustmentNote = orderModel.adjustmentNote;
        order.discountPercentage = orderModel.discountPercentage;
        order.refund = orderModel.refundAmount;
        order.deliveryCharge = orderModel.deliveryCharge;
        order.tips = orderModel.tips;
        order.shift = orderModel.shift;
        order.dueAmount = orderModel.dueAmount;
        order.paidTotal = orderModel.totalPaid;
        order.receive = orderModel.receive;
        order.paymentId = orderModel.paymentUid == null ? "-1" : orderModel.paymentUid;
        order.cashId = orderModel.cashId == 0 ? -1 : orderModel.cashId;
        order.customerId = orderModel.customer == null ? 0 : orderModel.customer.dbId;
        order.customerInfo = orderModel.customer != null ? GsonParser.getGsonParser().toJson(orderModel.customer) : "";
        order.shippingAddress = orderModel.shippingAddress != null ? GsonParser.getGsonParser().toJson(orderModel.shippingAddress) : "";
        order.tableBookingId = orderModel.bookingId;
        order.tableBookingInfo = orderModel.tableBookingModel != null ? GsonParser.getGsonParser().toJson(orderModel.tableBookingModel) : "";
        order.cloudSubmitted = orderModel.cloudSubmitted;
        order.fixedDiscount = orderModel.fixedDiscount;
        order.cartItems = GsonParser.getGsonParser().toJson(orderModel.items);
        return order;
    }
}
