package com.novo.tech.redmangopos.room_db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.novo.tech.redmangopos.room_db.dao.CashDao;
import com.novo.tech.redmangopos.room_db.dao.CustomerDao;
import com.novo.tech.redmangopos.room_db.dao.OrderDao;
import com.novo.tech.redmangopos.room_db.dao.ProductDao;
import com.novo.tech.redmangopos.room_db.dao.ShiftDao;
import com.novo.tech.redmangopos.room_db.entities.Address;
import com.novo.tech.redmangopos.room_db.entities.Cash;
import com.novo.tech.redmangopos.room_db.entities.Customer;
import com.novo.tech.redmangopos.room_db.entities.Inventory;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.entities.OrderBackup;
import com.novo.tech.redmangopos.room_db.entities.PostCode;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.Shift;

import org.jetbrains.annotations.NotNull;

@Database(entities = {
        Order.class,
        OrderBackup.class,
        Customer.class,
        Cash.class,
        Address.class,
        PostCode.class,
        Shift.class,
        ProductTb.class,
        Category.class,
        Inventory.class,
}, version = 2)
@TypeConverters(DataConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;

    public abstract OrderDao OrderDao();

    public abstract CustomerDao CustomerDao();

    public abstract ShiftDao ShiftDao();

    public abstract CashDao CashDao();

    public abstract ProductDao PropertyDao();

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "NOVO_TECH_DB")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static final RoomDatabase.Callback roomCallback = new Callback() {
        @Override
        public void onCreate(@NonNull @NotNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        PopulateDbAsyncTask(AppDatabase instance) {
            OrderDao orderDao = instance.OrderDao();
            CustomerDao customerDao = instance.CustomerDao();
            ShiftDao shiftDao = instance.ShiftDao();
            CashDao cashDao = instance.CashDao();
            ProductDao propertyDao = instance.PropertyDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
