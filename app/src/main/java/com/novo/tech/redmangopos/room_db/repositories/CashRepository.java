package com.novo.tech.redmangopos.room_db.repositories;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.dao.CashDao;
import com.novo.tech.redmangopos.room_db.entities.Cash;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CashRepository {
    private Context context;
    private CashDao cashDao;

    public CashRepository(Context context) {
        this.context = context;
        AppDatabase database = AppDatabase.getInstance(context);
        cashDao = database.CashDao();
    }

    public double getTotalWithCash(String type){
        return cashDao.getTotalCash(type);
    }

    public double getBalance(){
        double totalCashIn = getTotalWithCash("1");
        double totalCashOut = getTotalWithCash("-1");
        double totalCashOrder = getTotalWithCash("0");
        double totalRefund = getTotalWithCash("4");
        return totalCashIn+totalCashOut+totalCashOrder-totalRefund;
    }

    public long addTransaction(Context context,double amount,int type,int orderId,boolean inCash){
        Cash cash = new Cash();
        cash.amount = amount;
        cash.dateTime = DateTimeHelper.getDBTime();
        cash.user = "demo";
        cash.shift = SharedPrefManager.getCurrentShift(context);
        cash.type = type;
        cash.orderId = orderId;
        cash.cloudSubmitted = false;
        cash.inCash = inCash;
        cash.localPayment = 1;
        return cashDao.insertCash(cash);
    }

    public void addPaidTransaction(Context context, double amount, int type, int orderId, boolean inCash){
        Cash cash = new Cash();
        cash.amount = amount;
        cash.dateTime = DateTimeHelper.getDBTime();
        cash.user = "demo";
        cash.shift = SharedPrefManager.getCurrentShift(context);
        cash.type = type;
        cash.orderId = orderId;
        cash.cloudSubmitted = true;
        cash.inCash = inCash;
        cash.localPayment = 0;
        cashDao.insertCash(cash);
    }

    public List<TransactionModel> getOrderTransaction(int dbId){
        List<TransactionModel> transactionModelList = new ArrayList<>();
        List<Cash> orderTransactionById = cashDao.getOrderTransactionById(dbId);
        for (Cash cash: orderTransactionById) {
            TransactionModel model =new TransactionModel(
                    cash.id,
                    cash.type,
                    cash.amount != null ? cash.amount : 0.00,
                    cash.cloudSubmitted ? 1 : 0,
                    0,
                    cash.orderId,
                    0,
                    "",
                    "",
                    "",
                    "",
                    cash.inCash ? 1 : 0
            );
            if(model.note==null){
                model.note = "";
            }
            transactionModelList.add(model);
        }
        return transactionModelList;
    }

    public void deleteOrderTransaction(int dbId){
        cashDao.deleteTransactionByOrderId(dbId);
    }

    public void deleteAllTransaction(){
        cashDao.deleteAll();
    }

    public void deleteLastDaysTransaction(){
        int daysCount = SharedPrefManager.getDaysCount(context);
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -daysCount);
        Date date = calendar.getTime();
        String _lastDate  = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        cashDao.deleteLastDaysCash(_lastDate);
    }

    public List<TransactionModel> getAllGeneralTransaction(){
        List<TransactionModel> transactionModelList = new ArrayList<>();
        List<Cash> allGeneralCash = cashDao.getAllGeneralCash("1", "-1");
        for (Cash cash: allGeneralCash) {
            TransactionModel model =new TransactionModel(
                    cash.id,
                    cash.type,
                    cash.amount != null ? cash.amount : 0.00,
                    cash.cloudSubmitted ? 1 : 0,
                    0,
                    cash.orderId,
                    0,
                    "",
                    "",
                    "",
                    "",
                    cash.inCash ? 1 : 0
            );
            if(model.note==null){
                model.note = "";
            }
            transactionModelList.add(model);
        }
        return transactionModelList;
    }

    @SuppressLint("Range")
    public List<TransactionModel> getAllCashTransaction(){
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = cashDao.getAllCashTransaction("NEW");
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                        cursor.getInt(cursor.getColumnIndex("cashId")),
                        cursor.getInt(cursor.getColumnIndex("type")),
                        cursor.getDouble(cursor.getColumnIndex("amount")),
                        cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderId")),
                        cursor.getInt(cursor.getColumnIndex("server_id")),
                        cursor.getString(cursor.getColumnIndex("order_status")),
                        cursor.getString(cursor.getColumnIndex("payment_status")),
                        cursor.getString(cursor.getColumnIndex("payment_method")),
                        cursor.getString(cursor.getColumnIndex("adjustmentNote")),
                        cursor.getInt(cursor.getColumnIndex("inCash"))
                    );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        return transactionModelList;
    }

    public void updateTransaction(int dbID, boolean cloudSubmitted) {
        Cash cash = new Cash();
        cash.id = dbID;
        cash.cloudSubmitted = cloudSubmitted;
        cashDao.updateCash(cash);
    }

    public int getTotalOrders(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalOrders(_date.get(0), _date.get(1));
    }

    public int getOnlineOrders(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getOnlineOrders("ONLINE", _date.get(0), _date.get(1));
    }

    public double getTotalCashAmount(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1), "0", "1");
    }

    public double getTotalCardAmount(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1), "0", "0");
    }

    public double getTotalCardAmountLocal(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1), "0", "0", "1");
    }

    public double getTotalRefund(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1),"4");
    }

    public double getTotalCashIn(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1),"1");
    }

    public double getTotalCashOut(){
        List<String> _date = DateTimeHelper.getTimeRange(context);
        return cashDao.getTotalAmount(_date.get(0), _date.get(1),"-1");
    }
}
