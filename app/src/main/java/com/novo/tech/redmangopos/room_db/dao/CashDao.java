package com.novo.tech.redmangopos.room_db.dao;

import android.database.Cursor;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.novo.tech.redmangopos.room_db.entities.Cash;

import java.util.List;

@Dao
public interface CashDao {
    @Query("SELECT IFNULL(SUM(amount), 0.00) AS total FROM cash WHERE type=:type AND in_cash=1")
    double getTotalCash(String type);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCash(Cash cash);

    @Update
    int updateCash(Cash cash);

    @Query("SELECT * FROM cash WHERE order_id =:id")
    List<Cash> getOrderTransactionById(int id);

    @Query("DELETE FROM cash WHERE order_id =:id")
    void deleteTransactionByOrderId(int id);

    @Query("DELETE FROM cash")
    void deleteAll();

    @Query("DELETE FROM cash WHERE date_time < :lastDate")
    void deleteLastDaysCash(String lastDate);

    @Query("SELECT * FROM cash WHERE type=:type1 OR type=:type2 AND cloud_submitted=0")
    List<Cash> getAllGeneralCash(String type1, String type2);

    @Query("select orders.id as orderId,orders.adjustmentNote as adjustmentNote,orders.total,orders.discount,orders.server_id,orders.order_status,orders.payment_method,orders.payment_status,orders.cloudSubmitted as orderSubmitted,cash.id as cashId,cash.amount,cash.in_cash,cash.type,cash.cloud_submitted as cashSubmitted from cash LEFT OUTER  JOIN  orders on orders.id = cash.order_id AND orders.order_status !=:status")
    Cursor getAllCashTransaction(String status);

    @Query("SELECT COUNT(id) as total FROM orders WHERE date BETWEEN :date1 AND :date2")
    int getTotalOrders(String date1, String date2);

    @Query("SELECT COUNT(id) as total FROM orders WHERE order_channel =:channel AND date BETWEEN :date1 AND :date2")
    int getOnlineOrders(String channel, String date1, String date2);

    @Query("SELECT IFNULL(SUM(amount), 0.00) AS total FROM cash WHERE date_time BETWEEN :date1 AND :date2 AND type=:type AND in_cash=:inCash")
    double getTotalAmount(String date1, String date2, String type, String inCash);

    @Query("SELECT IFNULL(SUM(amount), 0.00) AS total FROM cash WHERE date_time BETWEEN :date1 AND :date2 AND type=:type")
    double getTotalAmount(String date1, String date2, String type);

    @Query("SELECT IFNULL(SUM(amount), 0.00) AS total FROM cash WHERE date_time BETWEEN :date1 AND :date2 AND type=:type AND in_cash=:inCash AND local_payment=:local")
    double getTotalAmount(String date1, String date2, String type, String inCash, String local);
}
