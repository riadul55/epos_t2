package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.model.TableBookingModel;

import org.json.JSONArray;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

public class TableBookingApiHelper extends AsyncTask<Void,String, ArrayList<TableBookingModel>> {
    TableBookingCallBack callBack;

    public TableBookingApiHelper(TableBookingCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected ArrayList<TableBookingModel> doInBackground(Void... voids) {
        return tableBookingInfo();
    }
    public ArrayList<TableBookingModel> tableBookingInfo(){
        ArrayList<TableBookingModel> allInfo = new ArrayList<>();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(BASE_URL+"config/booking/table")
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    TableBookingModel obj = TableBookingModel.fromJSON(jsonArray.optJSONObject(i));
                    allInfo.add(obj);
                    Log.d("mmmmm", "tableBookingInfo: "+obj.table_no + " | " + obj.free);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return allInfo;
    }
    @Override
    protected void onPostExecute(ArrayList<TableBookingModel> arrayList) {
        callBack.onTableBookingInfoResponse(arrayList);
        super.onPostExecute(arrayList);
    }


    public interface TableBookingCallBack {
        void onTableBookingInfoResponse(ArrayList<TableBookingModel> data);
    }
}
