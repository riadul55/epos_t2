package com.novo.tech.redmangopos.callerId.app;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;


import androidx.work.WorkManager;

import com.novo.tech.redmangopos.callerId.model.Channel;
import com.novo.tech.redmangopos.callerId.usbcontroller.DeviceID;
import com.novo.tech.redmangopos.callerId.usbcontroller.IUsbConnectionHandler;
import com.novo.tech.redmangopos.callerId.usbcontroller.UsbController;
import com.novo.tech.redmangopos.callerId.utils.Constants;
import com.novo.tech.redmangopos.callerId.utils.DtmfData;
import com.novo.tech.redmangopos.callerId.utils.FileLog;
import com.novo.tech.redmangopos.escpos.PosServiceBinding;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.CallerHistoryModel;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.socket.SocketListeners;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.util.AppConstant;
import com.zxy.tiny.Tiny;

import net.posprinter.posprinterface.UiExecute;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.socket.client.Socket;

public class TheApp extends Application
{

	public static Context mContext;
	public static String DeviceSN = "", DeviceVer = "";			//Device Serial Number and Device Version
	public static int selectedChannel = 0;//Selected Channel Number
    public static List<Channel> channelList = new ArrayList<Channel>();		//Channel List

    private UsbManager mManager;
    public static UsbController sUsbController = null;
    public List<DeviceID> devices = new ArrayList<DeviceID>();
  //  public static MainActivity mainActivity;
    public static String deviceConnect = "";
    public static int callingStatus = 0;
    public static boolean canceled = false;

    public static PosServiceBinding serviceBinding;

    public static WorkManager mWorkManager;

    public static Socket webSocket = null;

    public static List<Bitmap> pendingPrints = new ArrayList<>();

	public static String milliToString(long millis) {

        long hrs = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long min = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long sec = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;
        long mls = millis % 1000;
        String toRet = String.format(Locale.ENGLISH, "%02d:%02d:%02d.%03d", hrs, min, sec, mls);
        return toRet;
    }

    public static final String ORDER_ALERT_ACTION_PLAY = "ORDER_ALERT_INTENT_ACTION_PLAY";
    public static final String ORDER_ALERT_ACTION_STOP = "ORDER_ALERT_INTENT_ACTION_STOP";
    public static final String PRINTER_ALERT_ACTION = "PRINTER_ALERT_INTENT_ACTION";

    public static Ringtone ringtone;

	@Override
	public void onCreate()
    {
		mContext = getApplicationContext();
//        if(BuildConfig.DEBUG)
//            StrictMode.enableDefaults();
        super.onCreate();
        DtmfData.DtmfDataInit1();
        DtmfData.DtmfDataInit2();

        File folder = new File(Environment.getExternalStorageDirectory() + "/AD800");
        if(!folder.exists()) folder.mkdir();        
        
        //Open Log.txt
        File Root = Environment.getExternalStorageDirectory();        
        if(Root.canWrite())
        {
        	File  LogFile = new File(Root, "Log.txt");        
        	if(LogFile.exists()) LogFile.delete();
        	FileLog.open( LogFile.getAbsolutePath(), Log.VERBOSE, 1000000 );
        	FileLog.v("FileLog", "start");
        }

        //Data Initialize
        TheApp.selectedChannel = 0;
        TheApp.channelList.clear();
        for(int i=0;i<8;i++) {
            Channel channel = new Channel();
            channel.m_Channel = i;
            TheApp.channelList.add(channel);
        }
        UsbInit();


        Tiny.getInstance().init(this);
        serviceBinding = new PosServiceBinding(getApplicationContext());
        serviceBinding.initBinding();

        mWorkManager = WorkManager.getInstance(getApplicationContext());

        // socket connections
        webSocket = new SocketHandler().init();
        SocketListeners listeners = new SocketListeners(getApplicationContext());
        webSocket.on(SocketHandler.LISTEN_ORDER_CREATE + AppConstant.business, listeners.onOrderCreate);
        webSocket.on(SocketHandler.LISTEN_ORDER_UPDATE + AppConstant.business, listeners.onOrderUpdate);
        webSocket.on(SocketHandler.LISTEN_GET_ITEMS + AppConstant.business, listeners.onListenGetItems);
        webSocket.on(SocketHandler.LISTEN_ORDER_PRINT + AppConstant.business, listeners.onListenOrderPrint);
        webSocket.on(SocketHandler.LISTEN_TABLE_BOOKED + AppConstant.business, listeners.onListenTableBooked);
        webSocket.on(SocketHandler.LISTEN_SET_ORDERS_PREF + AppConstant.business, listeners.onListenSetOrderPref);
//        webSocket.on(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business, listeners.onOrderList);
        // this listener for waiter app
        webSocket.connect();



        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent("ACTION_TIMER_CALL");
                mContext.sendBroadcast(intent);
            }
        }, 0, 60*1000);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ringtone.setLooping(true);
        }
        AppDatabase.getInstance(this);
    }

    @Override
    public void onTerminate() {
	    serviceBinding.disposeBinding();
        super.onTerminate();
    }

    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                Toast.makeText(mContext, device.getVendorId()+" | " + device.getProductId(), Toast.LENGTH_SHORT).show();

                if (device.getVendorId() == Constants.DEVICE_VENDOR_ID && device.getProductId() == Constants.DEVICE_PRODUCT_ID){
                    setDeviceConnect(true);
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device.getVendorId() == Constants.DEVICE_VENDOR_ID && device.getProductId() == Constants.DEVICE_PRODUCT_ID){
                    setDeviceConnect(false);
                }
            }
        }
    };


    public void DeviceDataInit()
    {
        selectedChannel = 0;
        for(int i=0;i<8;i++) {
            TheApp.channelList.get(i).init();
        }

    }

    public void setDeviceConnect(boolean isConn){
        if(isConn) {
            //UsbController Init
            devices.clear();
            devices.add(new DeviceID(Constants.DEVICE_VENDOR_ID, Constants.DEVICE_PRODUCT_ID));
            if(sUsbController == null) {
                sUsbController = new UsbController(mContext, mConnectionHandler, devices);
            }
            Toast.makeText(mContext, "Device Connected", Toast.LENGTH_LONG).show();
            deviceConnect = "Connected";

        } else {
            if(sUsbController != null){
                sUsbController.stop();
                sUsbController = null;
            }
            DeviceDataInit();
            Toast.makeText(mContext, "Failed to Connected", Toast.LENGTH_LONG).show();
            deviceConnect = "Disconnect";


        }
    }
    public static Handler DeviceMsgHandler = new Handler(Looper.myLooper()) {
        public void handleMessage(Message msg) {
            int iChannel = msg.arg1;
            selectedChannel = iChannel;
            switch(msg.what)
            {
                case Constants.AD800_LINE_STATUS:
                    switch(msg.arg2) {
                        case Constants.CHANNELSTATE_POWEROFF:
                            TheApp.channelList.get(iChannel).LineStatus = "Disconnect";
                            break;
                        case Constants.CHANNELSTATE_IDLE:
                            if(!TheApp.channelList.get(iChannel).CallerId.isEmpty()){
                                SharedPrefManager.addNewCall(mContext,new CallerHistoryModel(TheApp.channelList.get(iChannel).CallerId, DateTimeHelper.getTime(),null));
                            }
                            TheApp.channelList.get(iChannel).LineStatus = "Idle";			//hook on
                            TheApp.channelList.get(iChannel).CallerId = "";
                            TheApp.channelList.get(iChannel).Dtmf = "";
                            callingStatus = 0;
                            sendBroadCast(0);
                            break;
                        case Constants.CHANNELSTATE_PICKUP:
                            TheApp.channelList.get(iChannel).LineStatus = "Dialing";		//hook off
                            break;
                        case Constants.CHANNELSTATE_RINGON:
                            TheApp.channelList.get(iChannel).LineStatus = "Ring On";//ring
                            if (callingStatus!=1)
                                sendBroadCast(1);

                            break;
                        case Constants.CHANNELSTATE_RINGOFF:
                            TheApp.channelList.get(iChannel).LineStatus = "Ring Off";		//ring

                            break;
                        case Constants.CHANNELSTATE_ANSWER:
                            TheApp.channelList.get(iChannel).LineStatus =  "Incoming Call";  //Answer		hook off
                            if (callingStatus!=1)
                                sendBroadCast(1);

                            break;
                        case Constants.CHANNELSTATE_OUTGOING:
                            TheApp.channelList.get(iChannel).LineStatus =  "Outgoing Call";  //	hook off

                            break;
                        default:
                            break;
                    }

                    break;
                case Constants.AD800_LINE_VOLTAGE:			//TheApp.channelList.get(iChannel).LineVoltage
                    int oldVoltage = TheApp.channelList.get(iChannel).LineVoltage;
                    int newVoltage = msg.arg2;
                    if(oldVoltage == 0 || Math.abs(oldVoltage-newVoltage)*100/oldVoltage >= 10)
                        TheApp.channelList.get(iChannel).LineVoltage = msg.arg2;
                    else return;
                    break;
                case Constants.AD800_DEVICE_CONNECTION:
                case Constants.AD800_LINE_POLARITY:
                case Constants.AD800_LINE_CALLERID:
                case Constants.AD800_LINE_DTMF:
                case Constants.AD800_REC_DATA:
                case Constants.AD800_PLAY_FINISHED:
                case Constants.AD800_VOICETRIGGER:
                case Constants.AD800_BUSYTONE:
                case Constants.AD800_DTMF_FINISHED:
                    break;
            }
        }
    };

    static void sendBroadCast(int status){
        callingStatus = status;
        Intent i = new Intent("PHONE_CALL");
        i.putExtra("Number",TheApp.channelList.get(selectedChannel).CallerId);
        i.putExtra("LineStatus",TheApp.channelList.get(selectedChannel).LineStatus);
        i.putExtra("CallerId",TheApp.channelList.get(selectedChannel).CallerId);
        i.putExtra("state",status);
        mContext.sendBroadcast(i);
    }


    private final IUsbConnectionHandler mConnectionHandler = new IUsbConnectionHandler() {
        @Override
        public void onUsbStopped() {
            Log.e("mmmmm","Usb stopped!");
        }

        @Override
        public void onErrorLooperRunningAlready() {
            Log.e("mmmmm","Looper already running!");
        }

        @Override
        public void onDeviceNotFound() {
            if(sUsbController != null){
                sUsbController.stop();
                sUsbController = null;
            }
        }
    };


    private void UsbInit()
    {
        //First Device connect status checking
        mManager = (UsbManager)getSystemService(Context.USB_SERVICE);
        for (UsbDevice device :  mManager.getDeviceList().values()) {
            if (device.getVendorId() == Constants.DEVICE_VENDOR_ID && device.getProductId() == Constants.DEVICE_PRODUCT_ID){
                setDeviceConnect(true);
            }
        }

        //Register Device attached or detached receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        //for testing
        devices.add(new DeviceID(Constants.DEVICE_VENDOR_ID, Constants.DEVICE_PRODUCT_ID));
        if(sUsbController == null) {
            sUsbController = new UsbController(mContext, mConnectionHandler, devices);
        }
    }
}