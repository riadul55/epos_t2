package com.novo.tech.redmangopos.adapter;
import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.novo.tech.redmangopos.view.activity.ProductDetailsEditActivity;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.sync.ProductAvailabilityUpdateHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * This class created by alamin
 */
public class SelectedProductDetailsAdapter extends RecyclerView.Adapter<SelectedProductDetailsAdapter.MyViewHolder> {

    Intent imageIntent;
    int imageResultCode;

    //dialog variable
    EditText et_price_product_details_dialog;
    EditText et_short_order_product_details_dialog;
    EditText et_title_product_details_dialog;
    Spinner sp_property_product_details_dialog;
    Spinner sp_category_product_details_dialog;
    Spinner sp_availability_product_details_dialog;
    EditText et_description_product_details_dialog;
    CheckBox cb_discountable_product_details_dialog;
    public static ImageView iv_product_image_product_details_dialog;
    String shortOrder,title,property_name,category,availability,description,imgUrl;
    double price;
    Boolean isDiscountable;
    RecyclerView recycler_component_edit;

    String[] available = { "Yes", "No"};
    int availabilityPosition = 1;


    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList = new ArrayList<>();
    Context context;

    CategoryDetailsModel categoryDetailsModel;
    public static CategoryDetailsModel categoryDetailsModel2;


    public SelectedProductDetailsAdapter(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList, Context context) {
        this.categoryDetailsModelArrayList = categoryDetailsModelArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.product_item_details, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int pos) {
        int position = holder.getAdapterPosition();
        categoryDetailsModel = categoryDetailsModelArrayList.get(position);
        if (categoryDetailsModel != null) {
            holder.sortOrderNo.setText(categoryDetailsModel.getProperties().getSort_order());
            shortOrder = categoryDetailsModel.getProperties().getSort_order();
            Log.d("size--- ",String.valueOf(categoryDetailsModel.files.size()));
            if (categoryDetailsModel.files != null && categoryDetailsModel.files.size()>0) {
                if ((categoryDetailsModel.files.get(0).itemId) != null && (categoryDetailsModel.files.get(0).fileUid) != null) {
                    Glide.with(context).load(BASE_URL+"product/" + categoryDetailsModel.files.get(0).itemId + "/file/" + categoryDetailsModel.files.get(0).fileUid).into(holder.iv_image_product_item);
                }
            }
            holder.tv_productName.setText(categoryDetailsModel.getShort_name());
            title = categoryDetailsModel.getShort_name();
            holder.tv_product_details.setText(categoryDetailsModel.getDescription());
            description = categoryDetailsModel.getDescription();

            if (categoryDetailsModel.getProperties().getAvailable().equals(String.valueOf(0))){
                holder.product_item_details_availability_check.setChecked(false);

            }else {
                holder.product_item_details_availability_check.setChecked(true);
            }
            availability = categoryDetailsModel.getProperties().getAvailable();
            holder.product_item_details_availability_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        new ProductAvailabilityUpdateHelper(categoryDetailsModelArrayList.get(position).getProduct_uuid(),true).execute();
                    }else {

                        new ProductAvailabilityUpdateHelper(categoryDetailsModelArrayList.get(position).getProduct_uuid(),false).execute();
                    }

                }
            });

            if (categoryDetailsModel.getPrice() != null){
                holder.product_item_details_price.setText(categoryDetailsModel.getPrice().getCurrency()+" "+categoryDetailsModel.getPrice().getPrice());
                price = categoryDetailsModel.getPrice().getPrice();
            }


            holder.tv_product_approval.setText(categoryDetailsModel.getStatus());
            LinearLayoutManager layoutManager= new LinearLayoutManager(holder .product_item_recycler.getContext(),
                    LinearLayoutManager.VERTICAL,
                    false);
            layoutManager.setInitialPrefetchItemCount(categoryDetailsModel.getComponents().size());
            SelectedProductComponentAdapter selectedProductComponentAdapter = new SelectedProductComponentAdapter(categoryDetailsModelArrayList.get(position).getComponents(),context);
            holder.product_item_recycler.setLayoutManager(layoutManager);
            holder.product_item_recycler.setAdapter(selectedProductComponentAdapter);
            holder.product_item_recycler.setRecycledViewPool(viewPool);

           /* ArrayAdapter categoryAdapter = new ArrayAdapter(context,android.R.layout.simple_spinner_item, ProductActivity.categoryNameList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ArrayAdapter availabilityAdapter = new ArrayAdapter(context,android.R.layout.simple_spinner_item, available);
            availabilityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/

            holder.product_item_details_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDetailsModel2 = categoryDetailsModelArrayList.get(position);
                    //OpenDialog(categoryDetailsModelArrayList.get(position));
                    Intent intent = new Intent(context, ProductDetailsEditActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount(){
        return categoryDetailsModelArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sortOrderNo;
        ImageView iv_image_product_item;
        TextView tv_productName;
        TextView tv_product_details;
        TextView tv_product_approval;
        RecyclerView product_item_recycler;
        TextView product_item_details_price;
        //CheckBox product_item_details_visibility_check;
        CheckBox product_item_details_availability_check;
        ImageView product_item_details_action;

        public MyViewHolder(View itemView) {
            super(itemView);
            sortOrderNo = itemView.findViewById(R.id.sortOrderNo);
            iv_image_product_item = itemView.findViewById(R.id.iv_image_product_item);
            tv_productName = itemView.findViewById(R.id.tv_productName);
            tv_product_details = itemView.findViewById(R.id.tv_product_details);
            tv_product_approval = itemView.findViewById(R.id.tv_product_approval);
            product_item_recycler = itemView.findViewById(R.id.product_item_recycler);
            product_item_details_price = itemView.findViewById(R.id.product_item_details_price);
            //product_item_details_visibility_check = itemView.findViewById(R.id.product_item_details_visibility_check);
            product_item_details_availability_check = itemView.findViewById(R.id.product_item_details_availability_check);
            product_item_details_action = itemView.findViewById(R.id.product_item_details_action);
        }
    }
}
