package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.PaymentCalculator;

import java.util.List;

public class CalculatorButtonAdapter extends RecyclerView.Adapter<CalculatorButtonAdapter.OrderListViewHolder> implements View.OnClickListener {

    Context mCtx;
    List<String> data;

    public CalculatorButtonAdapter(Context mCtx, List<String> data) {
        this.mCtx = mCtx;
        this.data = data;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_button_calulator, parent, false);
        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        if(data.get(position).equals("")){
            holder.text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_back_black_24, 0, 0, 0);
            holder.text.setText(null);
        }else{
            holder.text.setText(data.get(position));
        }
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentCalculator paymentCalculator = (PaymentCalculator)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCenter);
                paymentCalculator.onInput(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        LinearLayout container;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.buttonText);
            container = itemView.findViewById(R.id.ButtonContainer);
        }
    }
}