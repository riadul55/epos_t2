package com.novo.tech.redmangopos.view.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.escpos.OnDeviceConnect;
import com.novo.tech.redmangopos.escpos.OnPrintProcess;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.CheckUpdatedOrder;
import com.novo.tech.redmangopos.sync.SendEmail;
import com.novo.tech.redmangopos.sync.SyncFromServer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

abstract public class BaseActivity extends AppCompatActivity implements RefreshCallBack, PhoneCallInterface {

    public static final String NOTIFY_ACTIVITY_ACTION = "notify_activity";
    public static final String ACTION_USB_PERMISSION = "net.xprinter.xprintersdk.USB_PERMISSION";
    private BroadcastReceiver broadcastReceiver;

    PhoneCallInterface phoneCallInterface;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(() -> {
            try {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } catch (Exception e) {}

            removeOldUpdates();
        }).start();
        phoneCallInterface = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
//                    case "PRINTER_STATE_CHANGE":
//                        Log.e("Status===>", intent.getBooleanExtra("state", true) + "");
//                        int connectionIndex = SharedPrefManager.getConnectionIndex(context);
//                        if (connectionIndex == Conts.CONNECTION_IP) {
//                            TheApp.serviceBinding.connetNet(new OnDeviceConnect() {
//                                @Override
//                                public void onConnect(boolean isConnect) {
//                                    if (!isConnect) {
//                                        Toast.makeText(context, "Please connect to printer!", Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            });
//                        } else if (connectionIndex == Conts.CONNECTION_USB){
//                            TheApp.serviceBinding.connetUSB(new OnDeviceConnect() {
//                                @Override
//                                public void onConnect(boolean isConnect) {
//                                    if (!isConnect) {
//                                        Toast.makeText(context, "Please connect to printer!", Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            });
//                        }
//                        break;
                    case "PHONE_CALL":
                        Log.e("PHONE_CALL_LOG", intent.getStringExtra("Number"));
                        refreshCallingView(intent);
                        break;
                    case NOTIFY_ACTIVITY_ACTION:
                        String notifyDate = SharedPrefManager.getNotifyDate(getApplicationContext());
                        String order_date = intent.getStringExtra("order_date");
                        String order_id = intent.getStringExtra("order_id");
                        Log.e("NotifyDate===>", notifyDate + " | "+ order_date);
                        new Thread(() -> checkDataOnline(true, order_date, "FCM", order_id)).start();
                        break;
                    case "ACTION_TIMER_CALL":
                        callTimerExecutes();
                        break;
                    case ACTION_USB_PERMISSION:
                        sentPendingPrints(TheApp.pendingPrints);
                        break;
                    case TheApp.ORDER_ALERT_ACTION_PLAY:
                        if (TheApp.ringtone != null && !TheApp.ringtone.isPlaying()) {
                            TheApp.ringtone.play();
                        }
                        break;
                    case TheApp.ORDER_ALERT_ACTION_STOP:
                        if (TheApp.ringtone != null && TheApp.ringtone.isPlaying()) {
                            TheApp.ringtone.stop();
                        }
                        break;
                    case TheApp.PRINTER_ALERT_ACTION:
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Warning!");
                        builder.setMessage("Could not connect to printer!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                                sendBroadcast(intent);
                            }
                        });
                        builder.create();
                        builder.show();
                        break;
                }
            }
        };


        registerReceiver(broadcastReceiver, new IntentFilter( NOTIFY_ACTIVITY_ACTION ));
        registerReceiver(broadcastReceiver, new IntentFilter("PHONE_CALL"));
        registerReceiver(broadcastReceiver, new IntentFilter("ACTION_TIMER_CALL"));
        registerReceiver(broadcastReceiver, new IntentFilter(ACTION_USB_PERMISSION));
//        registerReceiver(broadcastReceiver, new IntentFilter("PRINTER_STATE_CHANGE"));
        registerReceiver(broadcastReceiver, new IntentFilter(TheApp.ORDER_ALERT_ACTION_PLAY));
        registerReceiver(broadcastReceiver, new IntentFilter(TheApp.ORDER_ALERT_ACTION_STOP));
        registerReceiver(broadcastReceiver, new IntentFilter(TheApp.PRINTER_ALERT_ACTION));
    }

    private void sentPendingPrints(List<Bitmap> bitmaps) {
        if (bitmaps != null && bitmaps.size() > 0) {
            for (int i=0;i<bitmaps.size();i++) {
                int index = i;
                if (SharedPrefManager.getPrinterIndex(getApplicationContext()) != Conts.PRINTER_SUNMI) {
                    int connectionIndex = SharedPrefManager.getConnectionIndex(getApplicationContext());
                    if (connectionIndex == Conts.CONNECTION_IP) {
                        String ipAddress = SharedPrefManager.getPrinterIp(getApplicationContext());
                        TheApp.serviceBinding.connetNet(ipAddress, new OnDeviceConnect() {
                            @Override
                            public void onConnect(boolean isConnect) {
                                if (isConnect) {
                                    Log.e("Printing", "connected");
                                    TheApp.serviceBinding.printBitmap(bitmaps.get(index), new OnPrintProcess() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("mmmmm", "printImageMemo: 666");
                                        }

                                        @Override
                                        public void onError(String msg) {
                                            Log.d("mmmmm", "printImageMemo: " + msg);
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        TheApp.serviceBinding.connetUSB(new OnDeviceConnect() {
                            @Override
                            public void onConnect(boolean isConnect) {
                                if (isConnect) {
                                    TheApp.serviceBinding.printBitmap(bitmaps.get(index), new OnPrintProcess() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("mmmmm", "printImageMemo: 666");
                                        }

                                        @Override
                                        public void onError(String msg) {
                                            Log.d("mmmmm", "printImageMemo: " + msg);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private void callTimerExecutes() {
        Log.e("callTimerExecutes", "Executing..");
        String notifyDate = SharedPrefManager.getNotifyDate(getApplicationContext());
        if (SharedPrefManager.getEnableAutoRefresh(getApplicationContext())) {
            checkDataOnline(notifyDate != null, notifyDate, "Manual", null);
        }
        pushData();
        try {
            long sendingTime = SharedPrefManager.getEmailSendingTime(getApplicationContext());
            if (DateUtils.isToday(sendingTime)) {
                long diff = System.currentTimeMillis() - sendingTime;
                long hours = TimeUnit.MILLISECONDS.toHours(diff);
                if (hours > 2) {
                    SendEmail sendEmail = new SendEmail(getApplicationContext());
                    sendEmail.execute();
                }
            } else {
                SharedPrefManager.setEmailSendingTime(getApplicationContext(), System.currentTimeMillis());
            }
        } catch (Exception e) {
            SharedPrefManager.setEmailSendingTime(getApplicationContext(), System.currentTimeMillis());
        }
    }

    void checkDataOnline(boolean isPull, String orderDate, String from, String orderId){
//        if (isPull && orderDate != null) {
//            if (DateUtils.isToday(DateTimeHelper.getStringTimeToLong(orderDate))) {
//                pullData(from);
//            }
//        }
//        if (DateUtils.isToday(System.currentTimeMillis()))
        if (SharedPrefManager.getEnableOrderPull(getApplicationContext())) {
            pullData(from, orderId);
        }
//        pushData();
    }

    void pullData(String from, String orderId) {
        Log.d("mmmmm", "checkDataOnline: order checking from server");
        SyncFromServer sync = new SyncFromServer(getApplicationContext(),this, from, orderId);
        sync.execute();
        if (!from.equals("FCM")) {
            CheckUpdatedOrder check = new CheckUpdatedOrder(getApplicationContext());
            check.execute();
        }
    }

    void pushData() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        if (cn.getShortClassName().equals("."+this.getLocalClassName())) {
            List<UpdateData> updatedData = SharedPrefManager.getUpdatedData(getApplicationContext());
            if (updatedData != null && updatedData.size() > 0) {
                for (int i=0;i<updatedData.size();i++) {
                    SharedPrefManager.setUpdateData(getApplicationContext(), updatedData.get(i));
                }
            }
//            Log.e("Server Pushing", "From==>" + cn.getClassName());
//            SyncToServer push = new SyncToServer(this,this);
//            push.execute();
        }
        // ServerPusher push = new ServerPusher(Dashboard.this,this);
        //  push.execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void refreshCallingView(Intent intent){

        // will remove
//        if (intent != null) {
//            String phoneNumber = intent.getStringExtra("Number");
//            int state = intent.getIntExtra("state", 0);
//            Log.e("PHONE_CALL_LOG", "refresh call " + state + " | " + phoneNumber);
//            if (state == 1) {
////                SharedPrefManager.addNewCall(getApplicationContext(), new CallerHistoryModel(phoneNumber, DateTimeHelper.getTime(),null));
//                phoneCallInterface.onPhoneCall(true, phoneNumber.replaceAll("[^\\d.]", ""));
//            } else {
//                phoneCallInterface.onPhoneCall(false, phoneNumber);
//            }
//        }
        // end will remove

        if(TheApp.channelList.get(TheApp.selectedChannel).LineStatus.equals("Ring On") ||
                TheApp.channelList.get(TheApp.selectedChannel).LineStatus.equals("Incoming Call") ||
                TheApp.channelList.get(TheApp.selectedChannel).LineStatus.equals("Ring Off")){
            try {
                String phone = TheApp.channelList.get(TheApp.selectedChannel).CallerId.replaceAll("[^\\d.]", "");
                phoneCallInterface.onPhoneCall(true, phone);
            } catch (Exception e) {
                e.printStackTrace();

                String phone = TheApp.channelList.get(TheApp.selectedChannel).CallerId;
                phoneCallInterface.onPhoneCall(true, phone);
            }
        }else {
            phoneCallInterface.onPhoneCall(false, TheApp.channelList.get(TheApp.selectedChannel).CallerId);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1){
            if(resultCode == RESULT_OK && data != null) {
                CustomerModel customerModel = (CustomerModel) data.getSerializableExtra("customer");
                CustomerAddress shippingAddress = (CustomerAddress) data.getSerializableExtra("shippingAddress");
                String orderType  = data.getStringExtra("orderType");
                if(orderType.isEmpty()){
                    Toast.makeText(this, "Please Select Order Type to redirect order create page", Toast.LENGTH_SHORT).show();
                }else{
                    if(customerModel!=null && shippingAddress != null){
                        openOrderPage(orderType,customerModel, shippingAddress);
                    }
                }

            }
        }
    }



    void openOrderPage(String orderType,CustomerModel customerModel, CustomerAddress shippingAddress){
        Intent in=new Intent(this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType",orderType);
        b.putSerializable("customerModel",customerModel);
        b.putSerializable("shippingAddress", shippingAddress);
        in.putExtras(b);
        startActivity(in);
    }

    public void createOrSelect(String phoneNo){
        Intent in=new Intent(this, CustomerSelect.class);
        Bundle b = new Bundle();
        b.putString("customerPhone",phoneNo);
        in.putExtras(b);
        startActivityForResult(in,1);
    }

    void removeOldUpdates() {
        List<UpdateData> removeList = new ArrayList<>();
        List<UpdateData> updateDataList = SharedPrefManager.getUpdatedData(getApplicationContext());
        if (updateDataList != null && updateDataList.size() > 0) {
            for (UpdateData d : updateDataList) {
                if (!DateUtils.isToday(Long.parseLong(d.getDateTime()))) {
                    removeList.add(d);
                }
            }

            updateDataList.removeAll(removeList);
            SharedPreferences.Editor editor = SharedPrefManager.getSharedPreferences(getApplicationContext()).edit();
            Gson gson = new Gson();
            String json = gson.toJson(updateDataList);
            Log.e("PrefJson", json);
            editor.putString("updatedDatas", json);
            editor.apply();
        }
    }

}

interface PhoneCallInterface {
    void onPhoneCall(boolean isVisible, String phone);
}