package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.Property;

public interface PropertyCallBack {
    void onSelect(Property property);
}
