package com.novo.tech.redmangopos.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.room_db.entities.Inventory;
import com.novo.tech.redmangopos.room_db.repositories.ProductRepository;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.sync_service.UpdateItemAsync;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.ProductFile;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<Product> productList;
    OrderCartList cartListFragment;
    boolean extend;
    OnItemUpdateListener listener;
    ProductRepository repository;


    public ProductListAdapter(Context mCtx, List<Product> orderList,boolean extend, OnItemUpdateListener listener) {
        this.mCtx = mCtx;
        this.extend = extend;
        this.productList = orderList;
        this.listener = listener;
        repository = new ProductRepository(mCtx);
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(extend?R.layout.model_search_product:R.layout.model_product, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int _position) {
        Product product = productList.get(holder.getAdapterPosition());
        int position = holder.getAdapterPosition();
        String catName ="";


        String price = String.format(Locale.getDefault(),"%.2f",product.price);
        if(product.componentList.size()>0 && !productList.get(position).productType.equals("BUNDLE")){
            if (holder.imgHasSubItem != null) holder.imgHasSubItem.setVisibility(View.VISIBLE);
//            price+="+";
        } else {
            if (holder.imgHasSubItem != null) holder.imgHasSubItem.setVisibility(View.GONE);
        }
        Inventory stockByUuId = repository.getStockByUuId(product.productUUid);
        int stock = stockByUuId != null ? stockByUuId.stock : 0;
        holder.productTitle.setText(product.shortName + " (" + stock + ")");
        holder.productPrice.setText("£ "+price);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(productList.get(position).properties.available.equals("1")){
                    onItemClick(position);
                }else{
                    Toast.makeText(mCtx, "This product is currently unavailable!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showEditItemDialog(mCtx,position);

                return true;
            }
        });
//        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                switch(event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
//                        onItemClick(position);
//                        break;
//                }
//                showEditItemDialog(mCtx);
//                return true;
//            }
//
//        });
        if(cartListFragment == null){
            cartListFragment = (OrderCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
        }
        boolean added = false;
        if(cartListFragment != null)
            for (CartItem cartItem: cartListFragment.orderModel.items) {
                if(cartItem.uuid.equals(product.productUUid)){
                    added = true;
                    break;
            }
        }

        if(added){
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#DC143C")));

        }else{
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
        }

        if(extend){
            if(product.properties==null){
                holder.productCategory.setVisibility(View.INVISIBLE);
            }else if(product.productType.equals("COMPONENT")){
                Product _pro =  SharedPrefManager.getProductDetailsByComponent(mCtx,product.productUUid);
                if(_pro != null){
                    catName = _pro.shortName;
                    if(_pro.properties!=null){
                        if(!_pro.properties.category.isEmpty()){
                            catName+=("/\n"+_pro.properties.category);
                        }
                    }
                }

            }else{
                if(!product.properties.category.isEmpty()){
                    catName = product.properties.category;
                }
            }
            if(catName.isEmpty()){
                catName = "N/A";
            }
            holder.productCategory.setText(catName.toUpperCase());

        }

    }

    private void showEditItemDialog(Context context,int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialog.setContentView(R.layout.item_update_popup_layout);

        TextView itemName = (EditText) dialog.findViewById(R.id.updateItemName);
        TextView itemPrice = (EditText) dialog.findViewById(R.id.updateItemPrice);
        TextView itemDescription = (EditText) dialog.findViewById(R.id.updateItemDescription);
        MaterialCheckBox isAvailable = (MaterialCheckBox) dialog.findViewById(R.id.isAvailableCheckBox);
        LinearLayout availableLinerLayout = (LinearLayout) dialog.findViewById(R.id.availableLinerLayout);

        Button closeButton = (Button) dialog.findViewById(R.id.updateItemDetailsCloseButton);
        Button saveButton = (Button) dialog.findViewById(R.id.updateItemDetailsSaveButton);

        itemName.setText(productList.get(position).shortName);
//        itemPrice.setText(""+productList.get(position).price);
        itemPrice.setText(String.format(Locale.getDefault(),"%.2f",productList.get(position).price));
        itemDescription.setText(productList.get(position).description);

        if (productList.get(position).properties.available != null) {
            isAvailable.setChecked(productList.get(position).properties.available.equals("1"));
        }

        double previousPrice=Double.parseDouble(itemPrice.getText().toString());

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ProgressDialog dialog2 = ProgressDialog.show(context, "", "Loading. Please wait...", true);
//                dialog2.show();
                boolean isPriceChange = previousPrice != (Double.parseDouble(itemPrice.getText().toString()));
                String available=productList.get(position).properties.available;
                if(isAvailable.isChecked()){
                    available="1";
                }else if(!isAvailable.isChecked()){
                    available="0";
                }
                UpdateItemAsync async = new UpdateItemAsync(
                        context,
                        productList.get(position).productUUid,
                        productList.get(position).productType,
                        itemName.getText().toString(),
                        itemDescription.getText().toString(),
                        available,
                        productList.get(position).priceUUID,
                        Double.parseDouble(itemPrice.getText().toString()),
                        isPriceChange,
                        listener,null
                );
                async.execute();
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }

            }
        });



        dialog.create();
        dialog.show();
    }

    void onItemClick(int position){
        if(productList.get(position).componentList.size() > 0 && !productList.get(position).productType.equals("BUNDLE")){
            CategoryAndProduct categoryAndProduct = (CategoryAndProduct)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);
            categoryAndProduct.openComponentPage(productList.get(position));
//            if (productList.get(position).properties != null && productList.get(position).properties.category != null) {
//                categoryAndProduct.getCategoryProduct(productList.get(position).properties.category);
//            }
        }else if(productList.get(position).productType.equals("COMPONENT")){
            CategoryAndProduct categoryAndProduct = (CategoryAndProduct)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);
            Product pro = SharedPrefManager.getProductDetailsByComponent(mCtx,productList.get(position).productUUid);
            categoryAndProduct.openComponentPage(pro);

        }else{
            boolean offer = false;
            String category = "";
            if(productList.get(position).properties!=null){
                offer = productList.get(position).properties.offer;
                category = productList.get(position).properties.category;
            }

            JSONObject _itm = CartItem.toJSONCartItem(productList.get(position),null,1,productList.get(position).price,productList.get(position).discountable,offer,false,category);
            cartListFragment.addToCart(CartItem.fromJSON(_itm));
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;
        TextView productCategory;
        ImageView imgHasSubItem;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
            productCategory = itemView.findViewById(R.id.modelProductParent);
            imgHasSubItem = itemView.findViewById(R.id.imgHasSubItem);
        }
    }

    public interface OnItemUpdateListener {
        void onClickAction();
    }
}
