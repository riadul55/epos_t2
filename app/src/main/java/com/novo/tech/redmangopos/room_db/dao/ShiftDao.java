package com.novo.tech.redmangopos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.novo.tech.redmangopos.room_db.entities.Shift;

@Dao
public interface ShiftDao {
    @Query("SELECT * FROM shift WHERE ifnull(close_time, '') = ''")
    Shift getCurrentShift();

    @Query("SELECT * FROM shift ORDER BY id DESC")
    Shift getLastShift();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertShift(Shift shift);

    @Update
    void updateShift(Shift shift);

    @RawQuery
    int vacumDb(SupportSQLiteQuery supportSQLiteQuery);
}
