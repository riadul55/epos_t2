package com.novo.tech.redmangopos.sync;
import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.ShiftInfoCallBack;
import com.novo.tech.redmangopos.callback.ShiftInfoOrderCallBack;
import com.novo.tech.redmangopos.model.ShiftInfoModel;
import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;
import com.novo.tech.redmangopos.model.ShiftSummeryModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ShiftInfoHelper extends AsyncTask<String,Void, ShiftSummeryModel> {

    String date;
    ShiftSummeryModel shiftSummeryModel = new ShiftSummeryModel(0,0,0,0,0,0,0,0);
    ShiftInfoCallBack callBack;

    public ShiftInfoHelper(String date, ShiftInfoCallBack callBack) {
        this.date = date;
        this.callBack = callBack;
    }
    @Override
    protected ShiftSummeryModel doInBackground(String... strings) {
        return getShiftInfo(date);
    }
    ShiftSummeryModel getShiftInfo(String mDate){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(BASE_URL+"cash/entry?start="+mDate+" 00:00:00&end_date="+mDate+" 23:59:59")
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession",providerSession)
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    JSONObject data = jsonArray.optJSONObject(i);
                    if(data != null){
                        String entryType = data.optString("entry_type","");
                        if(entryType.equals("CASH_ORDER")){
                            shiftSummeryModel.totalCashPaid+=data.optDouble("amount",0);
                        }else if(entryType.equals("CARD_ORDER")){
                            shiftSummeryModel.totalCardPaid+=data.optDouble("amount",0);
                        }else if(entryType.equals("REFUND")){
                            shiftSummeryModel.refundedAmount+=data.optDouble("amount",0);
                        }else if(entryType.equals("TIP")){
                            shiftSummeryModel.tipsAmount+=data.optDouble("amount",0);
                        }else if(entryType.equals("CASH_IN")){
                            shiftSummeryModel.cashInAmount+=data.optDouble("amount",0);
                        }else if(entryType.equals("CASH_OUT")){
                            shiftSummeryModel.cashOutAmount+=data.optDouble("amount",0);
                        }
                    }



                }
            }




        }catch (Exception e){
            e.printStackTrace();
        }
        return shiftSummeryModel;
    }
    @Override
    protected void onPostExecute(ShiftSummeryModel model) {
        callBack.onShiftInfoResponse(model);
        super.onPostExecute(model);
    }
}
