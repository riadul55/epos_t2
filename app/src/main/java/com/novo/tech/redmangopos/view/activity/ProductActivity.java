package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CategoryAdapter;
import com.novo.tech.redmangopos.adapter.SelectedProductDetailsAdapter;
import com.novo.tech.redmangopos.callback.CategoryCallBack;
import com.novo.tech.redmangopos.view.fragment.SelectedProductFragment;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.model.CategoryModel;
import com.novo.tech.redmangopos.sync.CategoryCodeHelper;

import java.util.ArrayList;

import static com.novo.tech.redmangopos.adapter.SelectedProductDetailsAdapter.iv_product_image_product_details_dialog;

/**
 * This class created by alamin
 */
public class ProductActivity extends AppCompatActivity {
    LinearLayout productBAck;
    RecyclerView category_recycler;
    public static ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList = new ArrayList<>();
    public static ArrayList<String> categoryNameList = new ArrayList<>();
    CategoryAdapter categoryAdapter;
    CategoryCallBack callBack;
    SelectedProductDetailsAdapter selectedProductDetailsAdapter;

    Intent imageIntent;
    int imageResultCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        productBAck = findViewById(R.id.productBAck);
        category_recycler = findViewById(R.id.category_recycler);

        new CategoryCodeHelper(categoryModelArrayList, new CategoryCallBack() {
            @Override
            public void onCategoryResponse(ArrayList<CategoryModel> data) {
                if (data.size()>0){
                    for (CategoryModel categoryModel1 : data){
                        categoryNameList.add(categoryModel1.getDisplay_name());
                    }
                    categoryAdapter = new CategoryAdapter(ProductActivity.this,data);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                    category_recycler.setLayoutManager(linearLayoutManager);
                    category_recycler.setAdapter(categoryAdapter);
                }
            }
        }).execute();

        openSelectedProductFragment();
        productBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductActivity.this,Setting.class);
                startActivity(intent);
                finish();
            }
        });
    }

  /*  CategoryCallBack categoryCallBack = new CategoryCallBack() {
        @Override
        public void onCategoryResponse(ArrayList<CategoryModel> data) {
            if (categoryModelArrayList.size()>0){
                categoryAdapter = new CategoryAdapter(ProductActivity.this,categoryModelArrayList);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                category_recycler.setLayoutManager(linearLayoutManager);
                category_recycler.setAdapter(categoryAdapter);
            }
        }
    };
*/

    void openSelectedProductFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.selectedProductFragmentView, SelectedProductFragment.class,null)
                .commit();
    }


   /* OnIntentReceived onIntentReceived = new OnIntentReceived() {
        @Override
        public void onIntent(Intent i, int resultCode) {
            startActivityForResult(i,resultCode);
        }
    };*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri selectedImageURI = data.getData();
                Glide.with(ProductActivity.this).load(selectedImageURI).into(iv_product_image_product_details_dialog);
            }
        }
    }
}