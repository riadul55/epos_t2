package com.novo.tech.redmangopos.socket;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PrintOrdersWorker extends AsyncTask<Void, Void, Void> {
    Context context;
    JSONObject jsonObject;

    public PrintOrdersWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String printType = jsonObject.optString("print_type");
            int orderId = jsonObject.optInt("order_id");
            String printItemsStr = jsonObject.optString("print_items");
            String printerIpStr = jsonObject.optString("printer_ip");

            OrderRepository manager = new OrderRepository(context);
            OrderModel orderData = manager.getOrderData(orderId);
            List<CartItem> printItems = new ArrayList<>();
            try {
                JSONArray _data = new JSONArray(printItemsStr.replaceAll("NaN", "0.0"));
                for (int i = 0; i < _data.length(); i++) {
                    printItems.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
                }
                orderData.items = printItems;

                double total = 0.0;
                for (int i=0;i<orderData.items.size();i++) {
                    double price = 0;

                    if(!orderData.items.get(i).offered){
                        price=(orderData.items.get(i).subTotal*orderData.items.get(i).quantity);

                        if (orderData.items.get(i).extra != null) {
                            for (CartItem extraItem: orderData.items.get(i).extra) {
                                price+=extraItem.price;
                            }
                        }

                    }
                    else {
                        price = orderData.items.get(i).total;
                    }

                    total += price;
                }
                orderData.subTotal = total;
            } catch (Exception e) {
                e.printStackTrace();
            }

            orderData.items.removeIf((a) -> a.uuid.equals("plastic-bag"));
            orderData.items.removeIf((a) -> a.uuid.equals("container"));
            if (printType.equals("invoice")) {
                CashMemoHelper.printCustomerMemo(orderData, context, true);
            } else if (printType.equals("kitchen")){
                if (printerIpStr.isEmpty()) {
                    CashMemoHelper.printKitchenMemo(orderData, context, true);
                } else {
                    CashMemoHelper.printKitchenFromWaiter(orderData, context, true, printerIpStr);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        super.onPostExecute(unused);
    }
}
