package com.novo.tech.redmangopos.model;

public class DeliveryCharge {
    public String shortName,productUuid;
    public double cost;
    public DeliveryCharge(String shortName, String productUuid, double cost) {
        this.shortName = shortName;
        this.productUuid = productUuid;
        this.cost = cost;
    }
}
