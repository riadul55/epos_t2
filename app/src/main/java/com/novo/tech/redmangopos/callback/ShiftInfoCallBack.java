package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.ShiftInfoModel;
import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;
import com.novo.tech.redmangopos.model.ShiftSummeryModel;

import java.util.ArrayList;

public interface ShiftInfoCallBack {
    void onShiftInfoResponse(ShiftSummeryModel data);
}
