package com.novo.tech.redmangopos.model;
/**
 * This class created by alamin
 */
public class PropertiesModel {
    String D;
    String V;
    String categories_key;
    String G;
    public String available;
    String categories;
    String sort_order;
    String N;
    String spiced;
    String VE;

    public PropertiesModel() {
    }

    public PropertiesModel(String d, String v, String categories_key, String g, String available, String categories, String sort_order, String n, String spiced, String VE) {
        D = d;
        V = v;
        this.categories_key = categories_key;
        G = g;
        this.available = available;
        this.categories = categories;
        this.sort_order = sort_order;
        N = n;
        this.spiced = spiced;
        this.VE = VE;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getV() {
        return V;
    }

    public void setV(String v) {
        V = v;
    }

    public String getCategories_key() {
        return categories_key;
    }

    public void setCategories_key(String categories_key) {
        this.categories_key = categories_key;
    }

    public String getG() {
        return G;
    }

    public void setG(String g) {
        G = g;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getN() {
        return N;
    }

    public void setN(String n) {
        N = n;
    }

    public String getSpiced() {
        return spiced;
    }

    public void setSpiced(String spiced) {
        this.spiced = spiced;
    }

    public String getVE() {
        return VE;
    }

    public void setVE(String VE) {
        this.VE = VE;
    }
}
