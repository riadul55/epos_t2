package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.CashCalculator;

public class CashKeyboardAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    String[] keyList;
    CashCalculator cashCalculator;

    public CashKeyboardAdapter(Context applicationContext, String[] keyList) {
        this.context = applicationContext;
        this.keyList = keyList;
        inflter = (LayoutInflater.from(applicationContext));
        cashCalculator = (CashCalculator)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.cashActivityFrameLayoutCenter);
    }
    @Override
    public int getCount() {
        return keyList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.model_key, null);
        TextView key = view.findViewById(R.id.modelKeyText);
        if(keyList[position].equals("")){
            key.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_back_black_24, 0, 0, 0);
            key.setText(null);
        }else{
            key.setText(keyList[position]);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);


        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280)
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 110));
        else
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 135));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cashCalculator.onInput(keyList[position]);
            }
        });
        return view;
    }
}
