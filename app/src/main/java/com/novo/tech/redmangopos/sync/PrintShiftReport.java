package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.izettle.html2bitmap.Html2Bitmap;
import com.izettle.html2bitmap.content.WebViewContent;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.escpos.OnDeviceConnect;
import com.novo.tech.redmangopos.escpos.OnPrintProcess;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.utils.AlertUtil;

import java.util.Locale;


public class PrintShiftReport extends AsyncTask<String, Void, Bitmap> {
    private Context context;

    public PrintShiftReport(Context context) {
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        try {
            CashRepository cash = new CashRepository(context);
            int printSize = SharedPrefManager.getPrintSize(context);
            String htmlDocument = generateHtmlDocuments(
                    printSize,
                    DateTimeHelper.getTime(),
                    SharedPrefManager.getBusinessName(context),
                    SharedPrefManager.getBusinessLocation(context),
                    String.valueOf(cash.getTotalOrders()),
                    String.valueOf(cash.getOnlineOrders()),
                    String.format(Locale.getDefault(),"%.2f",cash.getTotalCardAmount()-cash.getTotalCardAmountLocal()),
                    String.format(Locale.getDefault(),"%.2f",cash.getTotalCardAmountLocal()),
                    String.format(Locale.getDefault(),"%.2f",cash.getTotalCashAmount()),
                    String.format(Locale.getDefault(),"%.2f",cash.getTotalCashIn()),
                    String.format(Locale.getDefault(),"%.2f",cash.getTotalCashOut()),
                    String.valueOf(cash.getTotalRefund()),
                    String.format(Locale.getDefault(),"%.2f",cash.getBalance())
            );

            return new Html2Bitmap.Builder().setContext(context).setContent(WebViewContent.html(htmlDocument)).setTimeout(60000L).build().getBitmap();
        } catch (Exception e) {
            Log.e("Error==>", e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap != null) {
            int connectionIndex = SharedPrefManager.getConnectionIndex(context);
            if (connectionIndex == Conts.CONNECTION_IP) {
                String ipAddress = SharedPrefManager.getPrinterIp(context);
                TheApp.serviceBinding.connetNet(ipAddress, new OnDeviceConnect() {
                    @Override
                    public void onConnect(boolean isConnect) {
                        if (isConnect) {
                            TheApp.serviceBinding.printBitmap(bitmap, new OnPrintProcess() {
                                @Override
                                public void onSuccess() {
                                    Log.e("Printing", "success");
                                }

                                @Override
                                public void onError(String msg) {
                                    Log.e("Printing", "failed " + msg);
                                }
                            });
                        } else {
                            Intent intent = new Intent(TheApp.PRINTER_ALERT_ACTION);
                            context.sendBroadcast(intent);
//                            Toast.makeText(context, "Could not connect to printer!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } else {
                TheApp.serviceBinding.connetUSB(new OnDeviceConnect() {
                    @Override
                    public void onConnect(boolean isConnect) {
                        if (isConnect) {
                            TheApp.serviceBinding.printBitmap(bitmap, new OnPrintProcess() {
                                @Override
                                public void onSuccess() {
                                    Log.e("Printing", "success");
                                }

                                @Override
                                public void onError(String msg) {
                                    Log.e("Printing", "failed " + msg);
                                }
                            });
                        } else {
                            Intent intent = new Intent(TheApp.PRINTER_ALERT_ACTION);
                            context.sendBroadcast(intent);
//                            Toast.makeText(context, "Could not connect to printer!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }
    }

    public static String generateHtmlDocuments(
            int printSize,
            String dateTime,
            String businessName,
            String businessLocation,
            String totalOrderCount,
            String onlineOrderCount,
            String onlineCardPayment,
            String localCardPayment,
            String totalCash,
            String totalCashIn,
            String totalCashOut,
            String refundedAmount,
            String balance
    ) {
        return "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Riad</title>\n" +
                "\n" +
                "    <style>\n" +
                "        p {\n" +
                "           font-size: "+printSize+"px;\n" +
                "        }\n" +
                "        .text-center{\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "        .section-top {\n" +
                "            margin-bottom: 30px;\n" +
                "        }\n" +
                "        .table1 {\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "\n" +
                "        .table2 {\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "\t\t\n" +
                "\t\thr {\n" +
                "\t\t\tborder-top:1px dashed black;\n" +
                "\t\t}\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "    <!-- Upper section -->\n" +
                "    <div class=\"section-top\">\n" +
                "        <table class=\"table1\">\n" +
                "            <tr>\n" +
                "                <td class=\"text-center\"><p>"+dateTime+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td class=\"text-center\"><p>"+businessName+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td class=\"text-center\"><p>"+businessLocation+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td class=\"text-center\"><hr></td>\n" +
                "            </tr>\n" +
                "        </table>\n" +
                "    </div>\n" +
                "    \n" +
                "\n" +
                "    <div class=\"section-botton\">\n" +
                "        <table class=\"table2\">\n" +
                "            <tr>\n" +
                "                <td><p>Total Orders</p></td>\n" +
                "                <td><p>"+totalOrderCount+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Online Orders</p></td>\n" +
                "                <td><p>"+onlineOrderCount+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Online Card Payment</p></td>\n" +
                "                <td><p>£"+onlineCardPayment+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Local Card Payment</p></td>\n" +
                "                <td><p>£"+localCardPayment+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Total Cash</p></td>\n" +
                "                <td><p>£"+totalCash+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Total Cash In</p></td>\n" +
                "                <td><p>£"+totalCashIn+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Total Cash Out</p></td>\n" +
                "                <td><p>£"+totalCashOut+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Refunded Amount</p></td>\n" +
                "                <td><p>£"+refundedAmount+"</p></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><p>Balance</p></td>\n" +
                "                <td><p>£"+balance+"</p></td>\n" +
                "            </tr>\n" +
                "        </table>\n" +
                "    </div>\n" +
                "    \n" +
                "</body>\n" +
                "</html>";
    }
}