package com.novo.tech.redmangopos.model;
/**
 * This class created by alamin
 */
public class PriceModel {
    String price_uuid;
    double price;
    String currency;
    String start_date;

    public PriceModel(String price_uuid, double price, String currency, String start_date) {
        this.price_uuid = price_uuid;
        this.price = price;
        this.currency = currency;
        this.start_date = start_date;
    }

    public String getPrice_uuid() {
        return price_uuid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

}
