package com.novo.tech.redmangopos.socket;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CreateOrderWorker extends AsyncTask<Void, Void, JSONObject> {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private JSONObject jsonObject;

    public CreateOrderWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        JSONObject newJson = new JSONObject();
        try {
            JSONObject orderModelJson = (JSONObject) jsonObject.get(SocketHandler.ORDER);
            OrderModel orderModel = OrderModel.fromJSON(orderModelJson);
            SharedPrefManager.createUpdateOrder(context, orderModel);

            OrderRepository repository = new OrderRepository(context);
            List<OrderModel> activeOrders = repository.getActiveOrderForWaiter();

            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<activeOrders.size();i++) {
                jsonArray.put(OrderModelToJson.fromModel(activeOrders.get(i), context));
            }

            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            newJson.put(SocketHandler.ORDER_LIST, jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newJson;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        try {
            TheApp.webSocket.emit(SocketHandler.EMIT_ORDER_LIST, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject newJson = new JSONObject();
            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<20;i++) {
                boolean isBooked = SharedPrefManager.getTableBooked(context, i+1);
                TableBookingModel tableBookingModel = new TableBookingModel(
                        1, 1, i+1, 1, isBooked, 1, 1, ""
                );
                jsonArray.put(TableBookingModel.toJson(tableBookingModel));
            }
            newJson.put("booking_list", jsonArray);
            TheApp.webSocket.emit(SocketHandler.EMIT_BOOKING_LIST, newJson);
        } catch (Exception e) {
            e.printStackTrace();
        }


        JSONObject emitGetOrders = new JSONObject();
        try {
            emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            emitGetOrders.put("data", "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);

    }
}
