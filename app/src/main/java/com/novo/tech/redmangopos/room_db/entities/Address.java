package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "address")
public class Address {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "customer_id")
    @Nullable
    public int customerId;

    @ColumnInfo(name = "uuid")
    @Nullable
    public String uuid;

    @ColumnInfo(name = "type")
    @Nullable
    public String type;

    @ColumnInfo(name = "building")
    @Nullable
    public String building;

    @ColumnInfo(name = "street_name")
    @Nullable
    public String streetName;

    @ColumnInfo(name = "city")
    @Nullable
    public String city;

    @ColumnInfo(name = "state")
    @Nullable
    public String state;

    @ColumnInfo(name = "zip")
    @Nullable
    public String zip;

    @ColumnInfo(name = "isPrimary")
    @Nullable
    public boolean isPrimary;
}
