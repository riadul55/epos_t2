package com.novo.tech.redmangopos.sync.sync_service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ApiSyncService extends Worker {
    private static final String TAG = ApiSyncService.class.getSimpleName();

    public ApiSyncService(@NonNull @NotNull Context context, @NonNull @NotNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @SuppressLint("WrongThread")
    @NonNull
    @NotNull
    @Override
    public Result doWork() {
        Map<String, Object> mapData = getInputData().getKeyValueMap();
        UpdateData data = UpdateData.fromMap(mapData);
        if (isNetworkAvailable()) {
            try {
                if (data.getOrderChannel() != null && data.getOrderChannel().equals("ONLINE")) {
                    new UpdateOnlineStatus(getApplicationContext(), data).execute();
                } else {
                    if (data.getStatus().equals("CLOSED")) {
                        Log.e("Triggered==>", "closed");
                        new CreateOrder(getApplicationContext(), data).execute();
                    }
                }
                return Result.success();
            } catch (Exception e) {
                return Result.retry();
            }
        } else {
            List<UpdateData> updatedData = SharedPrefManager.getUpdatedData(getApplicationContext());
            if (!isUpdateDataExist(updatedData, data)) {
                SharedPrefManager.saveForLaterUpdate(getApplicationContext(), data);
            }
            return Result.success();
        }
    }

    private boolean isUpdateDataExist(List<UpdateData> updateData, UpdateData data) {
        boolean found = false;
        for (int i=0;i<updateData.size();i++) {
            if (updateData.get(i).getOrderId() == data.getOrderId() && updateData.get(i).getStatus().equals(data.getStatus())) {
                found = true;
                break;
            }
        }
        return found;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
