package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.TableBookingModel;

public interface TableSelectCallBack {
    void onTableSelect(int index);
}
