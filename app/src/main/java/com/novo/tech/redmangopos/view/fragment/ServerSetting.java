package com.novo.tech.redmangopos.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.button.MaterialButton;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.CustomerExportCallBack;
import com.novo.tech.redmangopos.sync.CustomerExport;
import com.novo.tech.redmangopos.sync.CustomerImport;

import java.util.List;

public class ServerSetting extends Fragment {
    EditText inputField;
    Button saveButton;
    MaterialButton btnImport,btnExport;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_server_setting, container, false);
        inputField = v.findViewById(R.id.settingServerAddressInput);
//        inputField.setText(SharedPrefManager.getBaseUrl(getContext()));
        saveButton = v.findViewById(R.id.settingServerAddressSaveButton);
        btnExport = v.findViewById(R.id.btnExport);
        btnImport = v.findViewById(R.id.btnImport);

        saveButton.setOnClickListener(v1 -> {
//            SharedPrefManager.setBaseUrl(getContext(),inputField.getText().toString());
            inputField.setEnabled(false);
            inputField.setEnabled(true);
        });

        btnImport.setOnClickListener(v12 -> {
            getDataFromServer();
        });
        btnExport.setOnClickListener(v13 -> {
            sendDataToServer();
        });
        return v;
    }


    void sendDataToServer(){
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Exporting data to server");
        progressDialog.setCancelable(false);
        progressDialog.show();
        new CustomerExport(getContext(), new CustomerExportCallBack() {
            @Override
            public void onSuccess(List<Boolean> results) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                showMessage(results);

            }
        }).execute();
    }

    void getDataFromServer(){
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Importing data from server");

        if(getActivity() != null && !getActivity().isFinishing()) {
            progressDialog.show();
        }


        new CustomerImport(getContext(), results -> {
            if (progressDialog != null && progressDialog.isShowing()){
                progressDialog.cancel();
            }

            showMessage(results);

        }).execute();
    }

    void showMessage(List<Boolean> results){
        try {
            int done = 0;
            int failed = 0;

            for (Boolean result : results) {
                if(result){
                    done++;
                }else{
                    failed++;
                }
            }

            if (getContext() != null) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Process Done")
                        .setMessage("Success : "+String.valueOf(done)+" \nFailed : "+String.valueOf(failed))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}