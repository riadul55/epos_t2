package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.CategoryDetailsCallBack;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

public class GetSelectComponentProductHelper extends AsyncTask<String, Void, ArrayList> {
    ArrayList<CategoryDetailsModel> componentArrayListH = new ArrayList<CategoryDetailsModel>();
    CategoryDetailsCallBack callBack;
    String componentProdcutUid;

    public GetSelectComponentProductHelper(ArrayList<CategoryDetailsModel> componentArrayListH, CategoryDetailsCallBack callBack, String componentProdcutUid) {
        this.componentArrayListH = componentArrayListH;
        this.callBack = callBack;
        this.componentProdcutUid = componentProdcutUid;
    }

    @Override
    protected ArrayList doInBackground(String... strings) {
        getSelectedComponentProduct(componentProdcutUid);
        return null;
    }

    private ArrayList<CategoryDetailsModel> getSelectedComponentProduct(String componentProdcutUid) {
        OkHttpClient client = new OkHttpClient();
        ArrayList<CategoryDetailsModel> componentProductArrayList  = new ArrayList<>();
        Request request = new Request.Builder()
                .get()
                .url(BASE_URL+"product/"+componentProdcutUid)
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){

               String jsonData = response.body().string();

                //JSONArray jsonArray = new JSONArray(jsonData);
               // JSONObject jsonArray = new JSONObject();
                JSONObject jsonArray = new JSONObject(jsonData);
                CategoryDetailsModel obj = CategoryDetailsModel.fromJSON(jsonArray);
                componentProductArrayList.add(obj);
                for (int i= 1;i<=jsonArray.length();i++){

                }
                componentArrayListH = componentProductArrayList;

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return componentProductArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList s) {
        callBack.onCategoryDetailsResponse(componentArrayListH);
        super.onPostExecute(s);
    }
}
