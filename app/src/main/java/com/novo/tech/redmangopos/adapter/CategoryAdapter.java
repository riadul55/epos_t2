package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.CategoryDetailsCallBack;
import com.novo.tech.redmangopos.view.fragment.SelectedProductFragment;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.model.CategoryModel;
import com.novo.tech.redmangopos.sync.CategoryDetailsHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * This class created by alamin
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{
    ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList = new ArrayList<>();

    SelectedProductDetailsAdapter selectedProductDetailsAdapter;
    public static int categoryPositionSelected = 0;
    int index = 0;
    int firstIndex = 0;

    Context context;
    ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    CategoryModel categoryModel;

    public CategoryAdapter(Context context, ArrayList<CategoryModel> categoryModelArrayList) {
        this.context = context;
        this.categoryModelArrayList = categoryModelArrayList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.product_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        categoryModel = categoryModelArrayList.get(position);
        holder.display_name.setText(categoryModel.getDisplay_name());
        holder.sort_order.setText(categoryModel.getSort_order());
        Glide.with(context).load("https://labapi.yuma-technology.co.uk:8443/delivery/config/product/property/category/"+categoryModel.getValue()+"/file").into(holder.filename);
        holder.product_item_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = position;

                categoryPositionSelected = position;
                new CategoryDetailsHelper(categoryDetailsModelArrayList, new CategoryDetailsCallBack() {
                    @Override
                    public void onCategoryDetailsResponse(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList) {
                        if (categoryDetailsModelArrayList.size()>0){
                            selectedProductDetailsAdapter = new SelectedProductDetailsAdapter(categoryDetailsModelArrayList,context);
                            SelectedProductFragment.selectedProductDetailsRecycler.setAdapter(selectedProductDetailsAdapter);
                           // Toast.makeText(context, "size : "+categoryDetailsModelArrayList.size(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },String.valueOf(categoryModelArrayList.get(position).getValue())).execute();
                notifyDataSetChanged();
            }
        });

        if(firstIndex==0) {
            firstIndex++;
            new CategoryDetailsHelper(categoryDetailsModelArrayList, new CategoryDetailsCallBack() {
                @Override
                public void onCategoryDetailsResponse(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList) {
                    if (categoryDetailsModelArrayList.size() > 0) {
                        selectedProductDetailsAdapter = new SelectedProductDetailsAdapter(categoryDetailsModelArrayList, context);
                        SelectedProductFragment.selectedProductDetailsRecycler.setAdapter(selectedProductDetailsAdapter);
                    }
                }
            }, String.valueOf(categoryModelArrayList.get(0).getValue())).execute();
        }

        if(index==position){
            holder.product_item_card.setBackgroundColor(Color.parseColor("#F0E68C"));
        }
        else
        {
            holder.product_item_card.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView filename;
        public TextView key_name;
        public TextView value;
        public TextView display_name;
        public TextView description;
        public TextView creation_date;
        public TextView last_update;
        public TextView sort_order;
        public RelativeLayout product_item_parent;
        public CardView product_item_card;
        public ViewHolder(View itemView) {
            super(itemView);
            filename = (ImageView) itemView.findViewById(R.id.product_img);
            display_name = (TextView) itemView.findViewById(R.id.product_display_name);
            sort_order = (TextView) itemView.findViewById(R.id.tv_product_serial);
            product_item_parent = (RelativeLayout)itemView.findViewById(R.id.product_item_parent);
            product_item_card = (CardView)itemView.findViewById(R.id.product_item_card);
            //key_name = (TextView) itemView.findViewById(R.id.key_name);
           /* description = (TextView) itemView.findViewById(R.id.description);
            creation_date = (TextView) itemView.findViewById(R.id.creation_date);
            last_update = (TextView) itemView.findViewById(R.id.last_update);*/

        }
    }
}

