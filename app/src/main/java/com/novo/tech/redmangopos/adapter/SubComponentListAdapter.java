package com.novo.tech.redmangopos.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.SubComponent;
import com.novo.tech.redmangopos.sync.sync_service.UpdateItemAsync;
import com.novo.tech.redmangopos.view.fragment.OrderComponentSubCartList;

import java.util.List;
import java.util.Locale;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;
public class SubComponentListAdapter extends RecyclerView.Adapter<SubComponentListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<SubComponent> subComponentList;
    OrderComponentSubCartList cartListFragment ;
    ComponentSection section;
    int select_position;
    RefreshCallBack callBack;
    ComponentListAdapter.OnItemUpdateListener listener;

    public SubComponentListAdapter(Context mCtx, List<SubComponent> subComponentList, ComponentSection section, ComponentListAdapter.OnItemUpdateListener listener, RefreshCallBack callBack) {
        this.mCtx = mCtx;
        this.subComponentList = subComponentList;
        this.section = section;
        this.callBack = callBack;
        cartListFragment = (OrderComponentSubCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_components, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        int _position = holder.getAdapterPosition();
        SubComponent component = subComponentList.get(_position);
        String price = String.format(Locale.getDefault(),"%.2f",component.price);
        holder.productTitle.setText(component.shortName);
        holder.productPrice.setText("£ "+price);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(_position);select_position = _position;
                notifyDataSetChanged();
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showEditItemDialog(mCtx,_position);
                return true;
            }
        });


//        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                switch(event.getAction()) {
//
//                    case MotionEvent.ACTION_DOWN:
//                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
//                        onItemClick(_position);
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        if (_position == select_position)
//                            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));
//                        else
//                            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
//                        select_position = _position;
//                        notifyDataSetChanged();
//                        break;
//                }
//
//
//                return true;
//            }
//
//        });


    }

    void onItemClick(int position){
        section.selectedItem.subComponentSelected = subComponentList.get(position);
        cartListFragment.onComponentChange(section);
        callBack.onChanged();

    }


    private void showEditItemDialog(Context context,int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialog.setContentView(R.layout.item_update_popup_layout);

        TextView itemName = (EditText) dialog.findViewById(R.id.updateItemName);
        TextView itemPrice = (EditText) dialog.findViewById(R.id.updateItemPrice);
        TextView itemDescription = (EditText) dialog.findViewById(R.id.updateItemDescription);
        MaterialCheckBox isAvailable = (MaterialCheckBox) dialog.findViewById(R.id.isAvailableCheckBox);
        LinearLayout availableLinerLayout = (LinearLayout) dialog.findViewById(R.id.availableLinerLayout);

        Button closeButton = (Button) dialog.findViewById(R.id.updateItemDetailsCloseButton);
        Button saveButton = (Button) dialog.findViewById(R.id.updateItemDetailsSaveButton);

        availableLinerLayout.setVisibility(View.GONE);
        itemName.setText(subComponentList.get(position).shortName);
        itemPrice.setText(String.format(Locale.getDefault(),"%.2f",subComponentList.get(position).price));
        itemDescription.setText(subComponentList.get(position).description);

        double previousPrice=Double.parseDouble(itemPrice.getText().toString());
        if (subComponentList.get(position).properties.available != null) {
            isAvailable.setChecked(subComponentList.get(position).properties.available.equals("1"));
        }
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPriceChange = previousPrice != (Double.parseDouble(itemPrice.getText().toString()));
                UpdateItemAsync async = new UpdateItemAsync(
                        context,
                        subComponentList.get(position).productUUid,
                        subComponentList.get(position).productType,
                        itemName.getText().toString(),
                        itemDescription.getText().toString(),
                        subComponentList.get(position).properties.available,
                        subComponentList.get(position).priceUUID,
                        Double.parseDouble(itemPrice.getText().toString()),
                        isPriceChange,
                        null, new ComponentListAdapter.OnItemUpdateListener() {
                    @Override
                    public void onClickAction() {
                        listener.onClickAction();
                    }
                }
                );
                async.execute();
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
            }
        });

        dialog.create();
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return subComponentList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
}
