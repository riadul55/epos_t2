package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;

public class CustomerDetails extends AppCompatActivity {
    TextView firstName,lastName,email,phone,houseNumber,streetName,town,postCode;
    CustomerModel customerModel;
    CustomerRepository customerManager;
    AppCompatButton delivery,collection;
    LinearLayout backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        customerManager = new CustomerRepository(CustomerDetails.this);
        Bundle bundle = getIntent().getExtras();
        int customerDbId = bundle.getInt("customerDBId",0);
        customerModel = customerManager.getCustomerData(customerDbId);
        firstName = findViewById(R.id.orderDetailsConsumerFirstName);
        lastName = findViewById(R.id.orderDetailsConsumerLastName);
        email = findViewById(R.id.orderDetailsConsumerEmail);
        phone = findViewById(R.id.orderDetailsConsumerPhone);
        houseNumber = findViewById(R.id.orderDetailsConsumerBuilding);
        streetName = findViewById(R.id.orderDetailsConsumerStreet);
        town = findViewById(R.id.orderDetailsConsumerCity);
        postCode = findViewById(R.id.orderDetailsConsumerZip);
        delivery = findViewById(R.id.deliveryCreate);
        collection = findViewById(R.id.collectionCreate);
        backButton = findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(customerModel != null){
            if(customerModel.profile!= null){
                firstName.setText(": "+customerModel.profile.first_name);
                lastName.setText(": "+customerModel.profile.last_name);
                email.setText(": "+customerModel.profile.email);
                phone.setText(": "+customerModel.profile.phone);
            }
            if(customerModel.addresses!=null){
                if(customerModel.addresses.size()!=0){
                    if(customerModel.addresses.get(0).properties!=null) {
                        CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
                        houseNumber.setText(": "+pro.building);
                        streetName.setText(": "+pro.street_number+" "+pro.street_name);
                        town.setText(": "+pro.city);
                        postCode.setText(": "+pro.zip);
                    }
                }
            }
        }
        delivery.setOnClickListener(view -> {
            openOrderPage("DELIVERY");
        });
        collection.setOnClickListener(view -> {
            openOrderPage("COLLECTION");
        });

        if(customerModel==null){
            delivery.setVisibility(View.INVISIBLE);
            collection.setVisibility(View.INVISIBLE);
        }


    }

    void openOrderPage(String orderType){
        Intent in=new Intent(CustomerDetails.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType",orderType);
        b.putSerializable("customerModel",customerModel);
        in.putExtras(b);
        startActivity(in);
    }


}