package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.socket.OrderListWorker;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TableService extends AsyncTask<String, Integer, String>{
    private Context context;
    TableBookingModel tableBookingModel;

    public TableService(Context context,TableBookingModel bookingModel){
     this.context = context;
     this.tableBookingModel = bookingModel;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            freeTheTable(tableBookingModel);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "mmmmm";
    }

    void freeTheTable(TableBookingModel tableBookingModel){

        Log.d("TAG", "freeTheTable: "+(tableBookingModel.free?"booked":"free"));

        JSONObject customerJson = new JSONObject();
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"config/booking/table/"+(!tableBookingModel.free?"busy":"free")+"?floor_no="+tableBookingModel.floor_no+"&local_no="+tableBookingModel.local_no+"&table_no="+tableBookingModel.table_no)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response){
                Log.d("mmmmm", "onResponse: table Booked "+String.valueOf(response.code()));
            }
        });


    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        OrderListWorker worker = new OrderListWorker(context);
        worker.execute();
    }


}
