package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "post_codes")
public class PostCode {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "postcode")
    @Nullable
    public String postcode;

    @ColumnInfo(name = "posttown")
    @Nullable
    public String posttown;

    @ColumnInfo(name = "dependent_locality")
    @Nullable
    public String dependentLocality;

    @ColumnInfo(name = "double_dependent_locality")
    @Nullable
    public String doubleDependentLocality;

    @ColumnInfo(name = "thoroughfare")
    @Nullable
    public String thoroughfare;

    @ColumnInfo(name = "dependent_thoroughfare")
    @Nullable
    public String dependentThoroughfare;

    @ColumnInfo(name = "building_number")
    @Nullable
    public String buildingNumber;

    @ColumnInfo(name = "building_name")
    @Nullable
    public String buildingName;

    @ColumnInfo(name = "sub_building_name")
    @Nullable
    public String subBuildingName;

    @ColumnInfo(name = "po_box")
    @Nullable
    public String poBox;

    @ColumnInfo(name = "department_name")
    @Nullable
    public String departmentName;

    @ColumnInfo(name = "organisation_name")
    @Nullable
    public String organisationName;

    @ColumnInfo(name = "udprn")
    @Nullable
    public String udprn;

    @ColumnInfo(name = "postcode_type")
    @Nullable
    public String postcodeType;

    @ColumnInfo(name = "su_org_indicator")
    @Nullable
    public String suOrgIndicator;

    @ColumnInfo(name = "delivery_point_suffix")
    @Nullable
    public String deliveryPointSuffix;
}
