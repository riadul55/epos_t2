package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.PaymentCalculator;

public class PaymentKeyboardAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    String[] keyList;

    public PaymentKeyboardAdapter(Context applicationContext, String[] keyList) {
        this.context = applicationContext;
        this.keyList = keyList;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return keyList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.model_key, null);
        TextView key = view.findViewById(R.id.modelKeyText);
        if(keyList[position].equals("")){
            key.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_back_black_24, 0, 0, 0);
            key.setText(null);
        }else{
            key.setText(keyList[position]);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);


        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280)
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 100));
        else
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 130));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentCalculator cashCalculator = (PaymentCalculator)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCenter);
                cashCalculator.onInput(keyList[position]);
            }
        });
        return view;
    }
}
