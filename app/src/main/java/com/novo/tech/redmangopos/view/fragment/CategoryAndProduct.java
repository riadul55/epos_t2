package com.novo.tech.redmangopos.view.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.sync.ui_threads.CategoryProductWorker;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CategoryListAdapter;
import com.novo.tech.redmangopos.adapter.ProductListAdapter;
import com.novo.tech.redmangopos.adapter.TopCategoryListAdapter;
import com.novo.tech.redmangopos.callback.PropertyCallBack;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CategoryAndProduct extends Fragment implements PropertyCallBack, ProductListAdapter.OnItemUpdateListener {
    RecyclerView recyclerViewCategory;
    RecyclerView recyclerViewCategoryProduct;
    RecyclerView topCategoryRecyclerView;
    FrameLayout categoryAndProduct,componentLayout;
    public CategoryListAdapter categoryListAdapter;
    ProductListAdapter productListAdapter;
    TopCategoryListAdapter propertyAdapter;
    String category,productName="";
    List<Product> productList = new ArrayList<>(),allProducts=new ArrayList<>();
    List<Property> propertyList = new ArrayList<>();
    Property selectedProperty;
    View view;
    EditText searchBox;
    OrderCreate orderCreate;

    OnProductUpdateListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnProductUpdateListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement " + OnProductUpdateListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_category_and_product, container, false);
        categoryAndProduct = v.findViewById(R.id.frameLayoutOrderCreateCategory);
        componentLayout = v.findViewById(R.id.frameLayoutOrderCreateComponents);
        recyclerViewCategory = v.findViewById(R.id.categoryRecyclerView);
        searchBox = v.findViewById(R.id.productSearchBox);
        recyclerViewCategory.setAlpha(0f);
        recyclerViewCategory.setTranslationY(50);
        recyclerViewCategory.animate().alpha(1f).translationYBy(-50).setDuration(700);

        recyclerViewCategoryProduct = v.findViewById(R.id.categoryProductRecyclerView);
        recyclerViewCategoryProduct.setAlpha(0f);
        recyclerViewCategoryProduct.setTranslationY(50);
        recyclerViewCategoryProduct.animate().alpha(1f).translationYBy(-50).setDuration(700);

        recyclerViewCategory.setLayoutManager(new GridLayoutManager(getContext(),9));
        recyclerViewCategory.addItemDecoration(new MediaSpaceDecoration(14,5));


        topCategoryRecyclerView = (RecyclerView) v.findViewById(R.id.topCategoryRecyclerView);
        topCategoryRecyclerView.setAlpha(0f);
        topCategoryRecyclerView.setTranslationY(50);
        topCategoryRecyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);
        topCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));


        view = v;
        category = requireArguments().getString("category");
//        allProducts = SharedPrefManager.getAvailableProduct(getContext());
//        propertyList = SharedPrefManager.getAllProperty(getContext());
        orderCreate = ((OrderCreate) getActivity());

        new CategoryProductWorker(getContext(), new CategoryProductWorker.OnGetProductsListener() {
            @Override
            public void onGetProduct(List<Product> products, List<Property> properties) {
                allProducts = products;
                propertyList = properties;

                loadTopCategory();
                setProductListView();
                searchBox.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        productName = s.toString();
                        showSearchProduct();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                searchBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        productName = searchBox.getText().toString();
                        showSearchProduct();
                    }
                });
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return  v;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View v, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
    }

    void setProductListView(){
        productList = allProducts;
       // productList.sort((a,b)->Integer.compare(a.properties.sort_order,b.properties.sort_order));
        productList.sort((a,b)->a.shortName.compareTo(b.shortName));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) requireActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280) {
            recyclerViewCategoryProduct.setLayoutManager(new GridLayoutManager(getContext(),7));
            recyclerViewCategoryProduct.addItemDecoration(new MediaSpaceDecoration(10,5));
        }
        else {
            recyclerViewCategoryProduct.setLayoutManager(new GridLayoutManager(getContext(),7));
            recyclerViewCategoryProduct.addItemDecoration(new MediaSpaceDecoration(10,10));
        }
    }

    void showSearchProduct(){
        if(productName.isEmpty()){
            recyclerViewCategory.setVisibility(View.VISIBLE);
            orderCreate.setCategory("all");
            displayCategoryRecyclerView();
        }else{
            orderCreate.setCategory("all");
            recyclerViewCategory.setVisibility(View.GONE);
            productList = new ArrayList<>();
            allProducts.forEach((a)->{
                if(a.shortName.toUpperCase().contains(productName.toUpperCase()))
                    productList.add(a);
            });
            productListAdapter = new ProductListAdapter(getContext(), productList,true, this);
            recyclerViewCategoryProduct.setAdapter(productListAdapter);
        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerViewCategory.setAdapter(null);
        recyclerViewCategoryProduct.setAdapter(null);
        topCategoryRecyclerView.setAdapter(null);

    }
    void loadTopCategory(){
//        if(propertyList.size() ==0){
//            loadProperty(getContext());
//        }else{
//        }
        displayProperty();
    }
    public void displayCategoryRecyclerView(){
        selectedProperty.items.sort((a,b)->Integer.compare(a.sort_order,b.sort_order));
        categoryListAdapter = new CategoryListAdapter(getContext(), selectedProperty.items,category);
        recyclerViewCategory.setAdapter(categoryListAdapter);
        getCategoryProduct(category);
    }
    void displayProperty(){
        propertyList.sort((a,b)->{
            a.key_name=a.key_name.replaceAll("category_","").replaceAll("category","All Category");
            b.key_name=b.key_name.replaceAll("category_","").replaceAll("category","All Category");
            return a.key_name.compareTo(b.key_name);
        });

        propertyList.removeIf((obj)->obj.key_name.toLowerCase().equals("all category"));
        if(propertyList.size()!=0){
            if(orderCreate.topCategory == null)
                selectedProperty = propertyList.get(0);
            else
                selectedProperty = orderCreate.topCategory;
        }
        propertyAdapter = new TopCategoryListAdapter(getContext(), propertyList,this);
        topCategoryRecyclerView.setAdapter(propertyAdapter);
        if(selectedProperty != null){
            displayCategoryRecyclerView();
        }
    }

    public void openComponentPage(Product product) {
        Bundle bundle = new Bundle();
        String _productData = GsonParser.getGsonParser().toJson(product);
        bundle.putString("product", _productData);
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, Components.class, bundle)
                .commit();

    }

    public void getCategoryProduct(String catName){
        category = catName;
        if(catName.equals("all")){
            if(selectedProperty.items.size()>0){
                catName = selectedProperty.items.get(0).value;
            }
        }
        if(orderCreate!=null){
            orderCreate.setCategory(catName);
        }

//        if(allProducts.size() ==0){
//            loadProductsFromServer(getContext());
//        }
        List<Product> _products = new ArrayList<>();
        for (Product prod: allProducts) {
            if(prod.properties != null){
                if(prod.properties.category != null){
                    if(prod.properties.category.contains(catName)){
                        _products.add(prod);
                    }
                }
            }
        }
        if(!catName.equals("all")){
            productList = _products;
        }
        else{
            productList = new ArrayList<>();
        }

//        productList.sort((a,b)-> {
//            if(a.properties != null && b.properties!=null){
//                return Integer.compare(a.properties.sort_order,b.properties.sort_order);
//            }else if(a.properties == null){
//                return -1;
//            }else
//                return 1;
//        });
        productList.sort((a,b)->a.shortName.compareTo(b.shortName));

        productListAdapter = new ProductListAdapter(getContext(), productList,false, this);
        recyclerViewCategoryProduct.setAdapter(productListAdapter);
    }
    void loadProductsFromServer(Context context) {
        if (context == null) {
            return;
        }
        Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
        ProductApi api = retrofit.create(ProductApi.class);
        Call call = api.getProducts();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int responseCode = response.code();
                List<ProductTb> _productList = new ArrayList<ProductTb>();
                if(responseCode == 200){
                    try {
                        JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                        for (int i = 0;i<_data.length();i++){
                            Product product = Product.fromJSON(_data.getJSONObject(i));
                            ProductTb productTb = Product.toProductTable(product);
                            _productList.add(productTb);
                            try {
                                if (product.productType.equals("DISCOUNT")) {
                                    if (product.productUUid.equals("discount_on_amount_collection") && product.properties != null) {
                                        SharedPrefManager.setDiscountCollection(context, Integer.parseInt(product.properties.percentage));
                                    } else if (product.productUUid.equals("discount_on_amount_delivery") && product.properties != null) {
                                        SharedPrefManager.setDiscountDelivery(context, Integer.parseInt(product.properties.percentage));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MMMMMMMM", "onResponse: error "+e.getMessage() );
                    }
                }
                SharedPrefManager.setAllProduct(context,_productList);
                allProducts = SharedPrefManager.getAvailableProduct(context);
                getCategoryProduct(category);
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
            }
        });
    }
    void loadProperty(Context context) {
        if (context == null) {
            return;
        }
        Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        Call call = api.getProperty();
        call.enqueue(new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call call, Response response) {
                int responseCode = response.code();
                propertyList = new ArrayList<>();
                List<Category> proList = new ArrayList<Category>();
                if(responseCode == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        jsonObject.keys().forEachRemaining(str->{
                            JSONArray jsonArray = jsonObject.optJSONArray(str);
                            List<PropertyItem> propertyItems = new ArrayList<>();
                            for (int i = 0;i<jsonArray.length();i++){
                                PropertyItem _proItem = GsonParser.getGsonParser().fromJson(jsonArray.optJSONObject(i).toString(),PropertyItem.class);
                                propertyItems.add(_proItem);
                            }
                            Property prop = new Property(str,propertyItems);
                            Category category = new Category();
                            category.keyName = prop.key_name;
                            category.items = prop.items;
                            proList.add(category);
                            propertyList.add(prop);
                        });
                        Log.e("mmmm", "onResponse: si  "+proList.size() );
                    } catch (JSONException e) {
                        Log.e("mmmm", "onResponse: "+e.getMessage() );
                        e.printStackTrace();
                    }
                }
                SharedPrefManager.setAllProperty(context,proList);
                displayProperty();
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
            }
        });
    }

    @Override
    public void onSelect(Property property) {
        selectedProperty = property;
        category = "all";
        displayCategoryRecyclerView();
    }

    @Override
    public void onClickAction() {
        listener.onUpdate();
    }

    public interface OnProductUpdateListener {
        void onUpdate();
    }
}