package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.novo.tech.redmangopos.view.activity.Login;
import com.novo.tech.redmangopos.R;

public class LoginKeyboardAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    String[] keyList;

    public LoginKeyboardAdapter(Context applicationContext, String[] keyList) {
        this.context = applicationContext;
        this.keyList = keyList;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return keyList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.model_key, null);
        TextView key = view.findViewById(R.id.modelKeyText);
        if(keyList[position].equals("")){
            key.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_back_black_24, 0, 0, 0);
            key.setText(null);

        }else{
            key.setText(keyList[position]);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);


        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280)
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 92));
        else
            view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 135));


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Login)context).onInput(keyList[position]);
                //v.setBackgroundColor(Color.parseColor("#E60026"));

            }
        });
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    v.setBackgroundColor(Color.parseColor("#1E000000"));
                    key.setTextColor(Color.parseColor("#ffffff"));
                    key.setTextSize(35);

                }
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v.setBackgroundColor(Color.parseColor("#ffffff"));
                    key.setTextColor(Color.parseColor("#000000"));
                    key.setTextSize(20);

                }
                return false;
            }
        });
        return view;
    }
}
