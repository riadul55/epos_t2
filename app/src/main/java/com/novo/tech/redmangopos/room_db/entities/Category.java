package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.room_db.DataConverter;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "category")
public class Category implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "key_name")
    @Nullable
    public String keyName;

    @TypeConverters(DataConverter.class)
    @ColumnInfo(name = "items")
    @Nullable
    public List<PropertyItem> items;
}
