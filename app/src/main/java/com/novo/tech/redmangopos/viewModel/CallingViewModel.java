package com.novo.tech.redmangopos.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CallingViewModel extends ViewModel {
    public int callingStatus; //0=idle, 1=incoming,
    String callingNumber;

    void CallNow(String callingNumber){
        callingStatus = 1;
        this.callingNumber = callingNumber;
    }
    void disconnectNow(){
        callingStatus = 0;
        this.callingNumber = "";
    }
}
