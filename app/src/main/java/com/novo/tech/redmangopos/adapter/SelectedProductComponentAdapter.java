package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.ComponentsModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SelectedProductComponentAdapter extends RecyclerView.Adapter<SelectedProductComponentAdapter.ViewHolder>{
    ArrayList<ComponentsModel> componentsModelArrayList = new ArrayList<>();
    ComponentsModel componentsModel;
    Context context;


    public SelectedProductComponentAdapter(ArrayList<ComponentsModel> componentsModelArrayList, Context context) {
        this.componentsModelArrayList = componentsModelArrayList;
        this.context = context;

    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.component_item, parent, false);;
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        componentsModel = componentsModelArrayList.get(position);
        if (componentsModel != null){
            holder.componentName.setText(componentsModel.getShort_name());
        }
    }

    @Override
    public int getItemCount() {
        return componentsModelArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView componentName;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            componentName = itemView.findViewById(R.id.componentName);
        }
    }
}
