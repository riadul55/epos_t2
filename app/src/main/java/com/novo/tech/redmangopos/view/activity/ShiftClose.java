package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.room_db.repositories.ShiftRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.DBShiftManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

public class ShiftClose extends AppCompatActivity {
    List<OrderModel> orderModelList = new ArrayList<>();
    AppCompatButton orderCloseButton,cashOutButton;
    CardView card_shiftCloseButtonUncloseOrders,card_shiftCloseButtonCashOut;
    LinearLayout back;
    Button shiftEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shift_close);
        orderCloseButton = findViewById(R.id.shiftCloseButtonUncloseOrders);
        cashOutButton = findViewById(R.id.shiftCloseButtonCashOut);
        back = findViewById(R.id.shiftCloseBack);
        shiftEnd = findViewById(R.id.shiftCloseButton);

        card_shiftCloseButtonUncloseOrders = findViewById(R.id.card_shiftCloseButtonUncloseOrders);
        card_shiftCloseButtonUncloseOrders.setAlpha(0f);
        card_shiftCloseButtonUncloseOrders.setTranslationX(50);
        card_shiftCloseButtonUncloseOrders.animate().alpha(1f).translationXBy(-50).setDuration(900);

        card_shiftCloseButtonCashOut = findViewById(R.id.card_shiftCloseButtonCashOut);
        card_shiftCloseButtonCashOut.setAlpha(0f);
        card_shiftCloseButtonCashOut.setTranslationY(50);
        card_shiftCloseButtonCashOut.animate().alpha(1f).translationYBy(-50).setDuration(900);


        loadOrders();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cashOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCashPage("CASH OUT");
            }
        });
        shiftEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeShiftWarning();
            }
        });
        orderCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadOrders() {
        orderModelList = SharedPrefManager.getAllOrders(getApplicationContext());
        if(orderModelList.size()==0){
            orderCloseButton.setEnabled(false);
        }
    }

    void openCashPage(String optionType){
        cashOutButton.setEnabled(false);
        Intent intent = new Intent(ShiftClose.this, CashActivity.class);
        intent.putExtra("optionType",optionType);
        startActivity(intent);
    }
    void logOut(){
        Intent intent = new Intent(ShiftClose.this, Login.class);
        intent.putExtra("finish", true); // if you are checking for this in your other Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    void closeShiftWarning(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ShiftClose.this);
        builder.setTitle("Are you sure you want to close this shift ?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Thread(() -> closeShift()).start();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }


    void closeShift(){
//        DBOrderManager dbOrderManager = new DBOrderManager(ShiftClose.this);
      //  CashMemoHelper.printShiftReport(getApplicationContext());
        ShiftRepository dbShiftManager = new ShiftRepository(getApplicationContext());
        CashRepository dbCashManager = new CashRepository(getApplicationContext());
        dbShiftManager.closeShift(SharedPrefManager.getCurrentShift(getApplicationContext()));
        dbCashManager.deleteLastDaysTransaction();

        SharedPrefManager.removeAllCall(ShiftClose.this);
        OrderRepository repository = new OrderRepository(getApplicationContext());
        repository.dayCloseNow();
        repository.deleteAllCustomerOrder(SharedPrefManager.getCustomerOrderCount(getApplicationContext()));
        repository.deleteAllOrder();
        SharedPrefManager.clearUpdatedData(getApplicationContext());
        dbShiftManager.vacuumDb();
        repository.vacuumDb();
        logOut();
    }
}