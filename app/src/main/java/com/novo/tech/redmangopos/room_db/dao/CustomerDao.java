package com.novo.tech.redmangopos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.novo.tech.redmangopos.room_db.entities.Address;
import com.novo.tech.redmangopos.room_db.entities.Customer;

import java.util.List;

@Dao
public interface CustomerDao {
    @Query("SELECT * FROM customer WHERE id=:id")
    Customer getById(int id);

    @Query("SELECT * FROM customer WHERE consumer_uuid=:id")
    Customer getByConsumerId(String id);

    @Query("SELECT * FROM customer ORDER BY id DESC LIMIT 5000")
    List<Customer> getAllCustomer();

    @Query("SELECT * FROM customer WHERE phone LIKE :query OR first_name LIKE :query OR last_name LIKE :query OR email LIKE :query LIMIT :offset,20")
    List<Customer> getCustomersBySearch(String query, int offset);

    @Query("SELECT * FROM customer WHERE phone =:phone OR consumer_uuid=:phone LIMIT 1")
    Customer getCustomerByPhone(String phone);

    @Query("SELECT IFNULL(MAX(id), 0) AS max FROM customer WHERE email =:email")
    int existCustomerByEmail(String email);

    @Query("SELECT IFNULL(MAX(id), 0) AS max FROM customer WHERE phone =:phone OR consumer_uuid=:phone")
    int existCustomerByPhone(String phone);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCustomer(Customer customer);

    @Update
    int updateCustomer(Customer customer);

    //------------- address
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertAddress(Address address);

    @Update
    int updateAddress(Address address);

    @Query("UPDATE address SET isPrimary=:isPrimary WHERE customer_id=:customerId AND id!=:id")
    int updateWithoutId(int customerId, int id, int isPrimary);

    @Query("SELECT * FROM address WHERE customer_id=:customerId AND isPrimary=1")
    Address getPrimaryCustomerAddress(String customerId);

    @Query("SELECT * FROM address WHERE customer_id =:customerId ORDER BY isPrimary DESC LIMIT 5000")
    List<Address> getAllCustomerAddress(String customerId);
}
