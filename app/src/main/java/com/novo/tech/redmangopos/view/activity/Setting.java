package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.view.fragment.CachedFragment;
import com.novo.tech.redmangopos.view.fragment.ServerSetting;
import com.novo.tech.redmangopos.view.fragment.BusinessSetting;
import com.novo.tech.redmangopos.view.fragment.TransactionDelete;

public class Setting extends AppCompatActivity {
    Button serverButton,shiftTimeButton,products,deleteData,deleteCustomerData, cacheDataShow;
    LinearLayout settingBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        serverButton = findViewById(R.id.settingButtonServer);
        shiftTimeButton = findViewById(R.id.settingButtonShiftTime);
        settingBack = findViewById(R.id.settingBAck);
        deleteData = findViewById(R.id.transactionDelete);
        products = findViewById(R.id.products);
        deleteCustomerData = findViewById(R.id.customerDataDelete);
        cacheDataShow = findViewById(R.id.cacheDataShow);
        settingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this,Dashboard.class);
                intent.putExtra("shift","EXISTING");
                startActivity(intent);
                finish();
            }
        });
        products.setOnClickListener(v -> {
            Intent intent  = new Intent(Setting.this,ProductActivity.class);
            startActivity(intent);
        });

        serverButton.setOnClickListener(v -> openServerFragment());

        shiftTimeButton.setOnClickListener(v -> openBusinessFragment());

        deleteData.setOnClickListener(v -> openDeleteFragment());
        deleteCustomerData.setOnClickListener(view -> {
            dataDeleteWarning();
        });

        cacheDataShow.setOnClickListener(v -> openCacheFragment());

        openBusinessFragment();

    }


    void dataDeleteWarning(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Setting.this);
        builder.setTitle("Are you sure you want to delete customer data?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OrderRepository repository = new OrderRepository(getApplicationContext());
                repository.deleteAllCustomerOrder(SharedPrefManager.getCustomerOrderCount(getApplicationContext()));
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void openServerFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.settingFragmentView, ServerSetting.class,null)
                .commit();
    }
    void openBusinessFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.settingFragmentView, BusinessSetting.class,null)
                .commit();
    }
    void openDeleteFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.settingFragmentView, TransactionDelete.class,null)
                .commit();
    }

    void openCacheFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.settingFragmentView, CachedFragment.class,null)
                .commit();
    }
}