package com.novo.tech.redmangopos.room_db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ProductFile;
import com.novo.tech.redmangopos.model.ProductProperties;
import com.novo.tech.redmangopos.model.PropertyItem;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverter implements Serializable {
    public List<Component> componentList;

    @TypeConverter // note this annotation
    public String fromProductProperties(ProductProperties value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<ProductProperties>() {}.getType();
        String json = gson.toJson(value, type);
        return json;
    }

    @TypeConverter // note this annotation
    public ProductProperties toProductProperties(String value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<ProductProperties>() {
        }.getType();
        ProductProperties properties = gson.fromJson(value, type);
        return properties;
    }


    @TypeConverter // note this annotation
    public String fromComponents(List<Component> value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Component>>() {
        }.getType();
        String json = gson.toJson(value, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<Component> toComponents(String value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Component>>() {
        }.getType();
        List<Component> componentList = gson.fromJson(value, type);
        return componentList;
    }

    @TypeConverter // note this annotation
    public String fromProductFiles(List<ProductFile> value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductFile>>() {}.getType();
        String json = gson.toJson(value, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<ProductFile> toProductFiles(String value) {
        if (value == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductFile>>() {
        }.getType();
        List<ProductFile> productFiles = gson.fromJson(value, type);
        return productFiles;
    }


    @TypeConverter // note this annotation
    public String fromPropertyItemList(List<PropertyItem> optionValues) {
        if (optionValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<PropertyItem>>() {}.getType();
        String json = gson.toJson(optionValues, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<PropertyItem> toPropertyItemList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<PropertyItem>>() {
        }.getType();
        List<PropertyItem> propertyItems = gson.fromJson(optionValuesString, type);
        return propertyItems;
    }
}
