package com.novo.tech.redmangopos.room_db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.novo.tech.redmangopos.room_db.entities.Inventory;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.room_db.entities.Category;

import java.util.List;

@Dao
public interface ProductDao {
    // category dao
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProperty(Category property);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProperties(List<Category> properties);

    @Query("SELECT * FROM Category")
    List<Category> getAllProperty();

    @Query("SELECT * FROM Category")
    LiveData<List<Category>> getPropertiesLive();

    @Query("DELETE FROM category")
    void deleteAllCategories();


    // product dao
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProduct(ProductTb product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProducts(List<ProductTb> products);

    @Query("SELECT * FROM product_tb")
    List<ProductTb> getAllProduct();

    @Query("SELECT * FROM product_tb")
    LiveData<List<ProductTb>> getProductsLive();

    @Query("DELETE FROM product_tb")
    void deleteAllProducts();

    // inventory
    @Query("SELECT * FROM inventory WHERE product_uuid =:productUuid")
    Inventory getStockByUuId(String productUuid);
}
