package com.novo.tech.redmangopos.sync.sync_service;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.adapter.ComponentListAdapter;
import com.novo.tech.redmangopos.adapter.ProductListAdapter;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.view.activity.Dashboard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class UpdateItemAsync extends AsyncTask<Void, Void, String> {
    private Context context;
    private String productUUId;
    private String productType;
    private String productShortName;
    private String productDescription;
    private String productAvailable;
    private String priceUUID;
    private double productPrice;
    private boolean isPriceChange;
    ProgressDialog dialog2;
    ProductListAdapter.OnItemUpdateListener listener;
    ComponentListAdapter.OnItemUpdateListener cListener;

    public UpdateItemAsync(Context context, String productUUId, String productType, String productShortName, String productDescription,
                           String productAvailable, String priceUUID, double productPrice, boolean isPriceChange,
                           ProductListAdapter.OnItemUpdateListener listener,ComponentListAdapter.OnItemUpdateListener cListener) {
        this.context = context;
        this.productUUId = productUUId;
        this.productType = productType;
        this.productShortName = productShortName;
        this.productDescription = productDescription;
        this.productAvailable = productAvailable;
        this.priceUUID = priceUUID;
        this.productPrice = productPrice;
        this.isPriceChange = isPriceChange;
        this.listener = listener;
        this.cListener = cListener;
        dialog2 = ProgressDialog.show(context, "", "Updating data. Please wait...", true);
    }


    @Override
    protected String doInBackground(Void... voids) {
        dialog2.show();
        JSONObject requestJson = new JSONObject();
        JSONObject price = new JSONObject();
        JSONObject properties = new JSONObject();
        try {
            requestJson.put("product_type", productType);
            requestJson.put("short_name", productShortName);
            requestJson.put("description", productDescription);

            if (productType.equals("ITEM")) {
                properties.put("available", productAvailable);
                requestJson.put("properties", properties);
            }
        } catch (Exception e) {
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, requestJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "product/" + productUUId)
                .patch(requestBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response != null) {
            if (response.code() == 202) {
                Log.e("Update info status ===>", "" + response.code());
                if (isPriceChange) {
                    checkPrice();
                }else {
                    updateInfo();
                }
//                dialog2.dismiss();
                return "success";
            } else {
                String body = "update item  status code ==> :" + response.code() + " | failed to update order status | " + response.message() + " | " + requestJson.toString() + " | url: " + BASE_URL + "product/" + productUUId;
                Log.e("Error==>", body);
                return "error";
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null && s.equals("success")) {
            Log.e("Item updating==>", s);
        }
//        if(dialog2!=null && dialog2.isShowing()){
//            dialog2.dismiss();
//        }
    }

    void checkPrice() {
        JSONObject requestJson = new JSONObject();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
//        RequestBody requestBody = RequestBody.create(JSON, requestJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "product/" + productUUId + "/price")
                .addHeader("Content-Type", "application/json")
                .addHeader("providersession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response != null) {
            if (response.code() == 200) {
                Log.e("Check Price", "" + response.code());
                modifyPrice();

            } else if (response.code() == 204) {
                Log.e("Check Price", "" + response.code());
                createNewPrice();

            } else {
                String body = "Check Price status code:" + response.code() + " | failed to check price | " + response.message() + " | " + requestJson.toString() + " | url: " + BASE_URL + "product/" + productUUId + "/price";
                Log.e("Error==>", body);

            }
        }
    }

    void modifyPrice() {
        JSONObject requestJson = new JSONObject();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            requestJson.put("end_date", "" + dateFormat.format(date.getTime()-(24 * 3600000)));
//
        } catch (Exception e) {
        }

        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, requestJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "product/" + productUUId + "/price/"+priceUUID)
                .patch(requestBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("providersession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response != null) {
            if (response.code() == 200) {
                Log.e("Modify status ", "" + response.code());
                createNewPrice();
            } else {
                String body = "Modify price code===>" + response.code() + " | failed to modify price | " + response.message() + " | " + requestJson.toString() + " | url: " + BASE_URL + "product/" + productUUId+"/price/"+priceUUID;
                Log.e("Error==>", body);

            }
        }

    }

    void createNewPrice() {
        JSONObject requestJson = new JSONObject();
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            requestJson.put("VAT", 0);
            if (productType.equals("ITEM")) {
                requestJson.put("price", productPrice);
            } else if (productType.equals("COMPONENT")) {
                requestJson.put("extra_price", productPrice);
            }
            requestJson.put("currency", "GBP");
            requestJson.put("start_date", "" + dateFormat.format(date));


        } catch (Exception e) {
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, requestJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "product/" + productUUId + "/price")
                .post(requestBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("providersession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response != null) {
            if (response.code() == 201) {
                Log.e("New price create status ", "" + response.code());
                updateInfo();
            } else {
                String body = "New price create status :" + response.code() + " | failed to update order status | " + response.message() + " | " + requestJson.toString() + " | url: " + BASE_URL + "product/" + productUUId + "/price";
                Log.e("Error==>", body);

            }
        }
    }

    void updateInfo(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(AppConstant.SITE_URL+"cache/forget")
                .get()
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (response != null) {
            if (response.code() == 200) {
                Log.e("cache-clear==>", "success");
                loadProductsFromServer();
            }
        }
    }


    private void loadProductsFromServer() {
        Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
        ProductApi api = retrofit.create(ProductApi.class);
        Call call = api.getProducts ();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
                int responseCode = response.code();
                List<ProductTb> _productList = new ArrayList<ProductTb>();
                if(responseCode == 200){
                    try {
                        JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                        for (int i = 0;i<_data.length();i++){
                            Product product = Product.fromJSON(_data.getJSONObject(i));
                            ProductTb productTb = Product.toProductTable(product);
                            _productList.add(productTb);
                            try {
                                if (product.productType.equals("DISCOUNT")) {
                                    if (product.productUUid.equals("discount_on_amount_collection") && product.properties != null) {
                                        SharedPrefManager.setDiscountCollection(context, Integer.parseInt(product.properties.percentage));
                                        } else if (product.productUUid.equals("discount_on_amount_delivery") && product.properties != null) {
                                        SharedPrefManager.setDiscountDelivery(context, Integer.parseInt(product.properties.percentage));
                                        }
                                    }
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                        }
                        if(dialog2!=null && dialog2.isShowing()){
                            dialog2.dismiss();
                        }
                        showAlertDialog("Successfully updated");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MMMMMMMM", "onResponse: error "+e.getMessage() );
                    }
                }
                SharedPrefManager.setAllProduct(context,_productList);
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("loadProduct==>", t.getMessage());
            }
        });
    }
    void  showAlertDialog(String message){
        new AlertDialog.Builder(context)
                .setTitle(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if (listener != null) {
                            listener.onClickAction();
                        }
                        if(cListener!=null){
                            cListener.onClickAction();
                        }
                    }
                }).create()
                .show();
    }
}
