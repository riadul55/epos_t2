package com.novo.tech.redmangopos.socket;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;

import org.json.JSONObject;

import java.util.List;

public class MergedItemsWorker extends AsyncTask<Void, Void, JSONObject> {
    Context context;
    JSONObject jsonObject;

    public MergedItemsWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        JSONObject newJson = new JSONObject();
        try {
            String bookingId = jsonObject.optString("bookingId");
            List<CartItem> mergedItems = BookingOrdersHelper.getMergedItems(context, Integer.parseInt(bookingId), "");
            String items = GsonParser.getGsonParser().toJson(mergedItems);

            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            newJson.put("order_items", items);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newJson;
    }

    @Override
    protected void onPostExecute(JSONObject emitObject) {
        super.onPostExecute(emitObject);
        try {
            TheApp.webSocket.emit(SocketHandler.EMIT_MERGED_ITEMS, emitObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
