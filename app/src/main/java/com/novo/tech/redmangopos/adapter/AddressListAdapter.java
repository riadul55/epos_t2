package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.AddressSelectCallBack;
import com.novo.tech.redmangopos.model.AddressHelperModel;
import com.novo.tech.redmangopos.model.JsonAddress;

import java.util.ArrayList;
import java.util.List;

public class AddressListAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> addressHelperModelArrayList;
    LayoutInflater inflater;

    public AddressListAdapter(Context context, int textViewResourceId, List<String> addressHelperModelArrayList) {
        super(context, textViewResourceId, addressHelperModelArrayList);
        this.context = context;
        this.addressHelperModelArrayList = addressHelperModelArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        TextView label = row.findViewById(android.R.id.text1);
        if (position == 0) {
            label.setText("Please select house number");
        } else {
            label.setText(addressHelperModelArrayList.get(position));
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280) {
            label.setTextSize(12);
        }
        return row;
    }
}
