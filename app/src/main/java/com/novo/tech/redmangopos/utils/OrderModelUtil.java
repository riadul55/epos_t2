package com.novo.tech.redmangopos.utils;

import android.content.Context;
import android.util.Log;

import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;

import java.util.ArrayList;
import java.util.List;

public class OrderModelUtil {
    public static OrderModel getOrderModel(Context context, OrderModel model, List<CartItem> items) {
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        CustomerAddress shippingAddress = null;
        TableBookingModel tableBookingModel = null;
        String order_type;

        try {
            customerModel = model.customer;
            shippingAddress = model.shippingAddress;

            if (model.tableBookingModel != null) {
                tableBookingModel = model.tableBookingModel;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cartItemList = items;


        double cashPayment = 0;
        double cardPayment = 0;
        CashRepository cashManager = new CashRepository(context);
//        if (model.bookingId != 0) {
//            List<OrderModel> ordersByBookingId = manager.getOrdersByBookingId(model.bookingId, "");
//            for (OrderModel orderData: ordersByBookingId) {
//                List<TransactionModel> transactions = cashManager.getOrderTransaction(orderData.db_id);
//                allTransactions.addAll(transactions);
//            }
//        } else {
//            List<TransactionModel> transactions = cashManager.getOrderTransaction(model.db_id);
//            allTransactions.addAll(transactions);
//        }

        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(model.db_id);

        for (TransactionModel transac : allTransactions) {
            Log.e("inCash", transac.inCash + "");
            Log.e("type", transac.type + "");
            Log.e("amount", transac.amount + "");
            if (transac.inCash == 0) {
                if (transac.type == 4) {
                    cardPayment -= transac.amount;
                } else if (transac.type == 2) {
                } else if (transac.type == 3) {
//                    cardPayment -= model.amount;
                } else {
                    cardPayment += transac.amount;
                }
            } else {
                if (transac.type == 4) {
                    cashPayment -= transac.amount;
                } else if (transac.type == 2) {
                } else if (transac.type == 3) {
//                    cashPayment -= model.amount;
                } else {
                    cashPayment += transac.amount;
                }
            }
        }

        double discountAmount = (model.discountAmount / model.items.size()) * cartItemList.size();

        OrderModel orderModel = new OrderModel(model.order_id, model.order_channel, model.order_type, model.order_status, model.paymentMethod, model.subTotal, model.total, model.discountableTotal, cardPayment + cashPayment, model.dueAmount, discountAmount, model.discountCode, model.tips, cartItemList, model.orderDateTime, model.currentDeliveryTime, model.requestedDeliveryTime, model.deliveryType, model.paymentStatus,
                model.receive, model.db_id, model.orderDate, model.comments, model.shift, model.cloudSubmitted, model.fixedDiscount, model.isDiscountOverride, model.serverId, customerModel, model.cashId, model.paymentUid, model.deliveryCharge, model.discountPercentage, model.refundAmount, model.adjustmentAmount, model.adjustmentNote,
                model.requester_type, new ArrayList<>(), model.plasticBagCost, model.containerBagCost, cashPayment, cardPayment, model.bookingId, tableBookingModel, shippingAddress);
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        double subTotalPaid = 0;
        for (CartItem item : orderModel.items) {
            if (item.uuid.equals("plastic-bag")) {
                orderModel.plasticBagCost = item.quantity * item.price;
            } else if (item.uuid.equals("container")) {
                orderModel.containerBagCost = item.quantity * item.price;
            } else {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else {
                    price = item.total;
                }

                subTotalAmount += price;

                if (item.discountable) {
                    _discountableTotal += (item.subTotal * item.quantity);
                }

                if (item.paymentMethod.equals("CARD")) {
                    subTotalPaid += price;
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.discountableTotal = _discountableTotal;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips + orderModel.plasticBagCost + orderModel.containerBagCost;
        orderModel.totalPaid = subTotalPaid;
        orderModel.dueAmount = orderModel.total - orderModel.totalPaid;
        return orderModel;
    }
}
