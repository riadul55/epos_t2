package com.novo.tech.redmangopos.viewModel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.novo.tech.redmangopos.model.CategoryModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;

public class OrderCreateViewModel extends ViewModel {
    private MutableLiveData<List<Property>> topCategory;
    private MutableLiveData<Property> selectedTopCategory;
    private MutableLiveData<List<CategoryModel>> productCategory;
    private MutableLiveData<CategoryModel> selectedProductCategory;
    private MutableLiveData<List<Product>> products;
    private MutableLiveData<Boolean> isLoading;

    LiveData<List<Property>> getTopCategories(Context context){
        topCategory.setValue(SharedPrefManager.getAllProperty(context));
        return topCategory;
    }


}
