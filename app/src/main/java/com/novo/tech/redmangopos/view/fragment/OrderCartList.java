package com.novo.tech.redmangopos.view.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.databinding.ViewDynamicItemBinding;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBProductManager;
import com.novo.tech.redmangopos.sync.ui_threads.OrderCartListWorker;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.activity.CustomerSelect;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.view.activity.OrderDetails;
import com.novo.tech.redmangopos.view.activity.Payment;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CartListAdapter;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;

public class OrderCartList extends Fragment {
    TextView type,orderId,orderCreateFragmentOrderTableNo;
    TextView totalPayable;
    RecyclerView recyclerViewList;
    CartListAdapter adapter;
    public OrderModel orderModel = new OrderModel();
    RelativeLayout btnCustomerSelect,tableNoContainer;
    TextView customerName;
    CategoryAndProduct categoryAndProduct;
    String orderType;

    OnCartReadyListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnCartReadyListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement " + CategoryAndProduct.OnProductUpdateListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_cart_list, container, false);
        type = v.findViewById(R.id.orderCreateFragmentOrderType);
        orderId = v.findViewById(R.id.orderCreateFragmentOrderNumber);
        totalPayable = v.findViewById(R.id.orderCreateFragmentTotalAmount);
        recyclerViewList = v.findViewById(R.id.orderCreateCartRecyclerView);
        btnCustomerSelect = v.findViewById(R.id.orderCreateProfileButton);
        customerName = v.findViewById(R.id.orderCreateFragmentSelectCustomer);
        tableNoContainer = v.findViewById(R.id.tableNoContainer);
        orderCreateFragmentOrderTableNo = v.findViewById(R.id.orderCreateFragmentOrderTableNo);

        orderType = requireArguments().getString("orderType");
        String bookingId = requireArguments().getString("bookingId");
        TableBookingModel table = (TableBookingModel) requireArguments().getSerializable("table");
        CustomerModel customerModel = (CustomerModel) requireArguments().getSerializable("customerModel");
        CustomerAddress shippingAddress = (CustomerAddress) requireArguments().getSerializable("shippingAddress");
        int orderModelId =  requireArguments().getInt("OrderModel",-1);

        new OrderCartListWorker(requireContext(),
                orderType,
                orderModelId,
                customerModel,
                shippingAddress,
                new OrderCartListWorker.OnChangeListener() {
                    @Override
                    public void onChange(OrderModel model) {
                        orderModel = model;
                        orderId.setText("Order : # "+String.valueOf(orderModel.order_id));
                        type.setText(orderModel.order_type);

                        try {
                            loadData();
                        } catch (Exception e) {}

                        btnCustomerSelect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putString("customerPhone","");
                                Intent i = new Intent(getContext(), CustomerSelect.class);
                                i.putExtras(bundle);
                                startActivityForResult(i, 1);
                            }
                        });

                        if(table!=null){
                            tableNoContainer.setVisibility(View.VISIBLE);
                            orderCreateFragmentOrderTableNo.setText(String.valueOf(table.table_no));
                            if (bookingId != null) {
                                orderModel.bookingId = Integer.parseInt(bookingId);
                            }
                            orderModel.tableBookingModel = table;
                        }

                        if (listener != null) {
                            listener.onCartReady();
                        }
                    }
                }).execute();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1){
            if(resultCode == RESULT_OK) {
                orderModel.customer = (CustomerModel) data.getSerializableExtra("customer");
                orderModel.shippingAddress = (CustomerAddress) data.getSerializableExtra("shippingAddress");
                displayCustomer();
            }
        }else if(requestCode==2){
            Log.d("xxxxxxx", "onActivityResult: "+resultCode);
            if(resultCode == RESULT_OK){
                OrderModel od = (OrderModel) data.getSerializableExtra("orderModel");
                orderModel.discountAmount = od.discountAmount;
                orderModel.fixedDiscount = od.fixedDiscount;
                orderModel.discountPercentage = od.discountPercentage;
                orderModel.comments = od.comments;
                orderModel.deliveryCharge = od.deliveryCharge;
                orderModel.tips = od.tips;
                orderModel.adjustmentAmount = od.adjustmentAmount;
                orderModel.adjustmentNote = od.adjustmentNote;
                orderModel.totalPaid = od.totalPaid;
                orderModel.items.clear();
                orderModel.items = od.items;
                loadData();
            }

        }
    }
    void displayCustomer(){
        if(orderModel.customer != null){
            if(orderModel.customer.profile!= null){
                customerName.setText((orderModel.customer.profile.first_name+" "+orderModel.customer.profile.last_name).toString().replaceAll("null",""));
            }
        }
    }

    void loadData(){
        recyclerViewList.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(requireContext(), LinearLayout.VERTICAL));
        if(orderModel.items == null)
            orderModel.items = new ArrayList<>();
        List<CartItem> _itms =  orderModel.items;
        _itms.removeIf((obj)->obj.type.equals("DELIVERY"));
        adapter = new CartListAdapter(getContext(), _itms);
        recyclerViewList.setAdapter(adapter);
        updateCartData();
    }
    public void updateCartData(){
        orderModel.subTotal = 0.0;
        if(orderModel.items == null){
            orderModel.items = new ArrayList<>();
        }

        orderModel.items.sort(Comparator.comparingInt(a -> a.localId));
        Collections.reverse(orderModel.items);
        for (CartItem item : orderModel.items){
            if(!item.offered){
                orderModel.subTotal+=(item.subTotal*item.quantity);

                if (item.extra != null) {
                    for (CartItem extraItem: item.extra) {
                        orderModel.subTotal+=extraItem.price;
                    }
                }

            }
            else {
                orderModel.subTotal += (item.total*item.quantity);
            }
        }
        orderModel.total = orderModel.subTotal;
        totalPayable.setText("£ "+String.format(Locale.getDefault(),"%.2f",orderModel.total));
        adapter.notifyDataSetChanged();
        displayCustomer();

    }

    void updateCategoryAndProduct(){
        if(categoryAndProduct==null){
            try{
                categoryAndProduct = (CategoryAndProduct)(getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);

            }catch (Exception e){

            }
        }
        if(categoryAndProduct != null){
            categoryAndProduct.searchBox.setText("");
            try {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                // TODO: handle exception
            }
            categoryAndProduct.getCategoryProduct(categoryAndProduct.category);
        }
    }

    public void clearCart(){
        orderModel.items = new ArrayList<>();
        orderModel.total = 0;
        orderModel.subTotal = 0;
        orderModel.discountAmount = 0;
        orderModel.adjustmentAmount = 0;
        orderModel.deliveryCharge = 0;
        orderModel.tips = 0;
        adapter = new CartListAdapter(getContext(), orderModel.items);
        recyclerViewList.setAdapter(adapter);
        updateCartData();
    }
    public void addToCart(CartItem item){
        boolean exist = alreadyExist(item);
        if(!exist){
            item.localId = getMaxId()+1;
            if(offerExist()){
                if(item.offerAble){
                    item.total = 0;
                    item.type = "DYNAMIC";
                    item.offered = true;
                }
            }

            orderModel.items.add(item);
            adapter.active = item;
            updateCategoryAndProduct();
        }
        updateCartData();
    }
    int getMaxId(){
        int max = 0;
        for (CartItem item : orderModel.items){
            if(item.localId > max)
                max = item.localId;
        }
        return max;
    }
    public void setComment(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    orderModel.items.get(i).comment = item.comment;
            }
        }
        try {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
        updateCartData();
    }
    public void setPrice(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                orderModel.items.get(i).type = "DYNAMIC";
                orderModel.items.get(i).offered = true;
                orderModel.items.get(i).total = item.total;
                break;
            }
        }
        updateCartData();
    }
    public void inCreaseQuantity(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    orderModel.items.get(i).quantity++;
            }
        }
        updateCartData();
    }
    public void deCreaseQuantity(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    if(orderModel.items.get(i).quantity > 1)
                        orderModel.items.get(i).quantity--;

            }
        }
        updateCartData();
    }
    public void removeFromCart(CartItem item){
        for (int i=0;i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                deleteItemFromCart(i);
                boolean offer = offerExist();
                if(!offer){
                    for (CartItem cart: orderModel.items) {
                        if(cart.offered){
                            cart.offered = false;
                        }
                    }
                }

                break;

            }
        }
        updateCartData();
    }
    public void updateCartItem(CartItem item){
        boolean exist = alreadyExist(item);
        if(!exist){
            for (int i = 0; i<orderModel.items.size();i++){
                if(orderModel.items.get(i).localId == item.localId) {
                    orderModel.items.set(i, item);
                    adapter.active = null;
                    break;
                }
            }
        }

        updateCartData();
    }

     void deleteItemFromCart(int index){
        FragmentManager fm = getFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.frameLayoutOrderCreateCategory);
        if(currentFragment instanceof ComponentsEdit){
            Toast.makeText(getContext(),"You Cart item in Edit Mode, Please Save or Cancel to delete an item",Toast.LENGTH_LONG).show();
        }else{
            orderModel.items.remove(index);
            updateCategoryAndProduct();
        }
    }

    private boolean offerExist (){
        boolean result = false;
        for(CartItem item : orderModel.items){
            if(item.category.equals("offer")){
                result = true;
                break;
            }
        }
        return result;
    }


    public void openCheckoutPage(){
        orderModel.items.removeIf((a) -> a.paymentMethod.equals("CARD"));
        if(orderModel.items==null){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            orderModel.items = new ArrayList<>();
            return;
        }else if(orderModel.items.size()==0){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            return;
        }else if(orderModel.subTotal==0){
            Toast.makeText(getContext(),"Cart amount is too low",Toast.LENGTH_SHORT).show();
            return;
        }
        Log.e("OrderCart====>", orderModel.order_type);
        if (orderModel.order_type != null && orderModel.order_type.equals("DELIVERY")) {
            if (orderModel.customer == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setTitle("Warning!");
                builder.setMessage("Please provide customer information");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle bundle = new Bundle();
                        bundle.putString("customerPhone","");
                        Intent intent = new Intent(getContext(), CustomerSelect.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, 1);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create();
                builder.show();
                return;
            }
        }
        if(orderModel.paymentStatus==null){
            orderModel.paymentStatus = "UNPAID";
        }
        orderModel.total = orderModel.subTotal;
        if (orderModel.orderDate == null)
            orderModel.orderDateTime = DateTimeHelper.getTime();

        if (orderModel.order_type != null && orderModel.order_type.equals("DELIVERY") && orderModel.deliveryCharge == 0.0) {
            orderModel.deliveryCharge = SharedPrefManager.getDeliveryCharge(getContext());
        }

//        int id = SharedPrefManager.createUpdateOrder(getContext(),orderModel);

        int id = SharedPrefManager.createUpdateOrder(getContext(), orderModel);
        if(id < 0){
            Toast.makeText(getContext(),"Failed to create order . error : Db Id Conflicting",Toast.LENGTH_SHORT).show();
            return;
        }
        orderModel.db_id = id;
        if(SharedPrefManager.getPlasticBagMandatory(getContext())){
            Product pro = SharedPrefManager.getPlasticBagProduct(getContext());
            if(pro != null){
                CartItem cartItem = new CartItem(
                        0,
                        pro.productUUid,
                        pro.description,
                        pro.description,
                        new ArrayList<>(),
                        pro.price,
                        pro.offerPrice,
                        pro.price,
                        pro.price,
                        0,
                        0,
                        "",
                        "",
                        0,
                        false,false,false, pro.properties.kitchenItem,"",new ArrayList<>(), true,
                        "CASH"
                );

                if(orderModel.order_type.equals("DELIVERY") || orderModel.order_type.equals("COLLECTION") || orderModel.order_type.equals("TAKEOUT")){
                    cartItem.quantity = 1;
                }
                if(!alreadyExist(cartItem)){
                    addToCart(cartItem);
//                    SharedPrefManager.createUpdateOrder(getContext(),orderModel);
                    SharedPrefManager.createUpdateOrder2(getContext(), orderModel);
                }
            }
        }
        if(SharedPrefManager.getContainerBagEnable(getContext())){
            Product pro = SharedPrefManager.getContainerBagProduct(getContext());
            if(pro != null){
                CartItem cartItem = new CartItem(
                        0,
                        pro.productUUid,
                        pro.description,
                        pro.description,
                        new ArrayList<>(),
                        pro.price,
                        pro.offerPrice,
                        pro.price,
                        pro.price,
                        0,
                        0,
                        "",
                        "",
                        0,
                        false,false,false, pro.properties.kitchenItem, "",new ArrayList<>(), true,
                        "CASH"
                );
                if(!alreadyExist(cartItem)){
                    addToCart(cartItem);
//                    SharedPrefManager.createUpdateOrder(getContext(),orderModel);

                    SharedPrefManager.createUpdateOrder2(getContext(), orderModel);
                }
            }
        }
        int discount = SharedPrefManager.getDiscountType(getContext());
        int amount = 0;
        if (orderModel.order_type.equals("COLLECTION")) {
            amount = SharedPrefManager.getDiscountCollection(getContext());
        } else if (orderModel.order_type.equals("DELIVERY")) {
            amount = SharedPrefManager.getDiscountDelivery(getContext());
        }
        int limitAmount = 0;
        if (orderModel.order_type.equals("COLLECTION")) {
            limitAmount = SharedPrefManager.getMinAmountCollection(getContext());
        } else if (orderModel.order_type.equals("DELIVERY")) {
            limitAmount = SharedPrefManager.getMinAmountDelivery(getContext());
        }
        if(orderModel.total >= limitAmount) {
            if (amount > 0) {
                double percentage = ((float) amount) / 100;
                orderModel.discountAmount = orderModel.total * percentage;
                orderModel.discountPercentage = amount;

            }
            SharedPrefManager.createUpdateOrder2(getContext(),orderModel);

        }


      gotToCheckoutPage();
    }

    public void saveTheOrder(boolean print){
        orderModel.items.removeIf((a) -> a.paymentMethod.equals("CARD"));
        if(orderModel.items==null){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            orderModel.items = new ArrayList<>();
            return;
        }else if(orderModel.items.size()==0){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            return;
        }else if(orderModel.subTotal==0){
            Toast.makeText(getContext(),"Cart amount is too low",Toast.LENGTH_SHORT).show();
            return;
        }
        if(orderModel.paymentStatus == null){
            orderModel.paymentStatus = "UNPAID";
        }
        orderModel.total = orderModel.subTotal;
        if (orderModel.orderDate == null)
            orderModel.orderDateTime = DateTimeHelper.getTime();

        if (orderModel.order_type.equals("DELIVERY") && orderModel.deliveryCharge == 0.0) {
            orderModel.deliveryCharge = SharedPrefManager.getDeliveryCharge(getContext());
        }

        int id = SharedPrefManager.createUpdateOrder2(getContext(),orderModel);

        if(id < 0){
            Toast.makeText(getContext(),"Failed to create order . error : Db Id Conflicting",Toast.LENGTH_SHORT).show();
            return;
        }
        orderModel.db_id = id;
        if(SharedPrefManager.getPlasticBagMandatory(getContext())){
            Product pro = SharedPrefManager.getPlasticBagProduct(getContext());
            if(pro != null){
                CartItem cartItem = new CartItem(
                        0,
                        pro.productUUid,
                        pro.description,
                        pro.description,
                        new ArrayList<>(),
                        pro.price,
                        pro.offerPrice,
                        pro.price,
                        pro.price,
                        0,
                        0,
                        "",
                        "",
                        0,
                        false,false,false, pro.properties.kitchenItem,"",new ArrayList<>(), true,
                        "CASH"
                );

                if(orderModel.order_type.equals("DELIVERY") || orderModel.order_type.equals("COLLECTION") || orderModel.order_type.equals("TAKEOUT")){
                    cartItem.quantity = 1;
                }
                if(!alreadyExist(cartItem)){
                    addToCart(cartItem);
                    SharedPrefManager.createUpdateOrder2(getContext(),orderModel);
                }
            }
        }
        if(SharedPrefManager.getContainerBagEnable(getContext())){
            Product pro = SharedPrefManager.getContainerBagProduct(getContext());
            if(pro != null){
                CartItem cartItem = new CartItem(
                        0,
                        pro.productUUid,
                        pro.description,
                        pro.description,
                        new ArrayList<>(),
                        pro.price,
                        pro.offerPrice,
                        pro.price,
                        pro.price,
                        0,
                        0,
                        "",
                        "",
                        0,
                        false,false,false, pro.properties.kitchenItem, "",new ArrayList<>(), true,
                        "CASH"
                );
                if(!alreadyExist(cartItem)){
                    addToCart(cartItem);
                    SharedPrefManager.createUpdateOrder2(getContext(),orderModel);
                }
            }
        }
        int discount = SharedPrefManager.getDiscountType(getContext());
        int amount = 0;
        if (orderModel.order_type.equals("COLLECTION")) {
            amount = SharedPrefManager.getDiscountCollection(getContext());
        } else if (orderModel.order_type.equals("DELIVERY")) {
            amount = SharedPrefManager.getDiscountDelivery(getContext());
        }
        int limitAmount = 0;
        if (orderModel.order_type.equals("COLLECTION")) {
            limitAmount = SharedPrefManager.getMinAmountCollection(getContext());
        } else if (orderModel.order_type.equals("DELIVERY")) {
            limitAmount = SharedPrefManager.getMinAmountDelivery(getContext());
        }
        if(orderModel.total >= limitAmount){
            if(amount > 0){
                double percentage = ((float)amount) /100;
                orderModel.discountAmount = orderModel.total*percentage;
                orderModel.discountPercentage = amount;
            }
            SharedPrefManager.createUpdateOrder2(getContext(),orderModel);
        }
        if(print){
            try {
                CashMemoHelper.printCustomerMemo(orderModel,getContext(),false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Toast.makeText(getContext(), "Successfully Saved", Toast.LENGTH_SHORT).show();
    }

    private void gotToCheckoutPage(){
        Intent myIntent = new Intent(getActivity(), Payment.class);
        myIntent.putExtra("orderData", orderModel.db_id);
        myIntent.putExtra("isCheckout", true);
        startActivity(myIntent);
    }

    public void voidOrderAlert(){
        String[] options = { "Some item are not available ", "Customer request", "Technical issue ", "Recall- Opened", "Recall-Unopened ", "Other",};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Void order cause of");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                voidOrder(options[which]);
            }
        });
        builder.create().show();
    }
    void voidOrder(String message){
        OrderRepository orderManager = new OrderRepository(getContext());
        orderManager.delete(orderModel);

        Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//        gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        gotoScreenVar.putExtra("shift","EXISTING");
        startActivity(gotoScreenVar);
    }

    public boolean alreadyExist(CartItem item){
        List<Boolean> anyExist = new ArrayList<>();
        int j = 0;
        for(CartItem cartItem:orderModel.items){
            anyExist.add(false);
            if(cartItem.uuid.equals(item.uuid)){
                if(item.componentSections.size() == 0){
                    anyExist.set(j,true);
                    break;
                }else if(item.componentSections.size()!=cartItem.componentSections.size()){
                    anyExist.set(j,false);
                    break;
                }else{
                    List<Boolean> data = new ArrayList<>();
                    for(int i = 0;i<cartItem.componentSections.size();i++){
                        data.add(false);
                        ComponentSection section1 = cartItem.componentSections.get(i);
                        ComponentSection section2 = item.componentSections.get(i);
                        if(section1.sectionValue.equals(section2.sectionValue)){
                            if(section1.selectedItem!= null && section2.selectedItem != null){
                                if(section1.selectedItem.productUUid.equals(section2.selectedItem.productUUid)){
                                    if (section1.selectedItem.subComponents.size() != 0) {
                                        if (section1.selectedItem.subComponentSelected.productUUid.equals(section2.selectedItem.subComponentSelected.productUUid)) {
                                            data.set(i,true);
                                        }
                                    } else {
                                        data.set(i,true);
                                    }
                                }
                            }
                        }

                    }
                    anyExist.set(j,true);
                    for(boolean b : data){
                        if(!b){
                            anyExist.set(j,false);
                            break;
                        }
                    }
                }
            }
            j++;
        }
        boolean finalExist = false;
        for(boolean b : anyExist){
            if(b){
                finalExist = true;
                break;
            }
        }
        return finalExist;
    }

    public void getDynamicProduct(){
        int[] printOrder = {2};
        LayoutInflater inflater = LayoutInflater.from(getContext());
        ViewDynamicItemBinding binding = ViewDynamicItemBinding.inflate(inflater);
        binding.radioStarter.setChecked(true);
        binding.categoryRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioStarter:
                        printOrder[0] = 2;
                        break;
                    case R.id.radioMainDish:
                        printOrder[0] = 3;
                        break;
                    case R.id.radioSideDish:
                        printOrder[0] = 4;
                        break;
                    case R.id.radioSundries:
                        printOrder[0] = 5;
                        break;

                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Dynamic Product");
        builder.setView(binding.getRoot())
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = binding.productName.getText().toString();
                String strPrice = binding.productPrice.getText().toString();
                double price = strPrice.isEmpty()?0.0:Double.parseDouble(strPrice);
                if (name.isEmpty()) {
                    binding.productName.setError("Required");
                } else if (strPrice.isEmpty()) {
                    binding.productPrice.setError("Required");
                } else {
                    CartItem item = new CartItem(-1,name.replaceAll(" ","_"),"",name,new ArrayList<>(),price,0.0,price,price,1, 1,"","DYNAMIC",printOrder[0],true,false,false, "1","",new ArrayList<>(), true, "CASH");
                    addToCart(item);
                    DBProductManager manager = new DBProductManager(getContext());
                    manager.addProduct(item);
                    dialog.dismiss();
                }
            }
        });
    }

    public interface OnCartReadyListener {
        void onCartReady();
    }
}