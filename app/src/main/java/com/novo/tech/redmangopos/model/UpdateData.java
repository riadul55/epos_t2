package com.novo.tech.redmangopos.model;

import com.google.protobuf.Any;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UpdateData implements Serializable {
    int orderId;
    String serverId;
    String orderChannel;
    String status;
    String dateTime;

    public UpdateData() {
    }

    public UpdateData(int orderId, String serverId, String orderChannel, String status, String dateTime) {
        this.orderId = orderId;
        this.serverId = serverId;
        this.orderChannel = orderChannel;
        this.status = status;
        this.dateTime = dateTime;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(String orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        map.put("serverId", serverId);
        map.put("channel", orderChannel);
        map.put("status", status);
        map.put("dateTime", dateTime);
        return map;
    }

    public static UpdateData fromMap(Map<String, Object> data) {
        UpdateData updateData = new UpdateData();
        if (data.get("orderId") != null) updateData.setOrderId((Integer) data.get("orderId"));
        if (data.get("serverId") != null) updateData.setServerId((String) data.get("serverId"));
        if (data.get("channel") != null) updateData.setOrderChannel((String) data.get("channel"));
        if (data.get("status") != null) updateData.setStatus((String) data.get("status"));
        if (data.get("dateTime") != null) updateData.setDateTime((String) data.get("dateTime"));
        return updateData;
    }
}
