package com.novo.tech.redmangopos.sync.ui_threads;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CategoryProductWorker extends AsyncTask<Void, Void, Void> {
    Context context;
    OnGetProductsListener listener;
    List<Product> allProducts = new ArrayList<>();
    List<Property> propertyList = new ArrayList<>();

    public CategoryProductWorker(Context context, OnGetProductsListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        allProducts = SharedPrefManager.getAvailableProduct(context);
        propertyList = SharedPrefManager.getAllProperty(context);
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        super.onPostExecute(unused);
        listener.onGetProduct(allProducts, propertyList);
        if (propertyList.size() == 0) {
            Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
            DashboardApi api = retrofit.create(DashboardApi.class);
            Call<Object> call = api.getProperty();
            call.enqueue(new Callback() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call call, Response response) {
                    int responseCode = response.code();
                    propertyList = new ArrayList<>();
                    List<Category> proList = new ArrayList<Category>();
                    if(responseCode == 200){
                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                            jsonObject.keys().forEachRemaining(str->{
                                JSONArray jsonArray = jsonObject.optJSONArray(str);
                                List<PropertyItem> propertyItems = new ArrayList<>();
                                for (int i = 0;i<jsonArray.length();i++){
                                    PropertyItem _proItem = GsonParser.getGsonParser().fromJson(jsonArray.optJSONObject(i).toString(),PropertyItem.class);
                                    propertyItems.add(_proItem);
                                }
                                Property prop = new Property(str,propertyItems);
                                Category category = new Category();
                                category.keyName = prop.key_name;
                                category.items = prop.items;
                                proList.add(category);
                                propertyList.add(prop);
                            });
                            Log.e("mmmm", "onResponse: si  "+proList.size() );
                        } catch (JSONException e) {
                            Log.e("mmmm", "onResponse: "+e.getMessage() );
                            e.printStackTrace();
                        }
                    }
                    SharedPrefManager.setAllProperty(context,proList);


                    listener.onGetProduct(allProducts, propertyList);
                }
                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
                }
            });
        }

        if (allProducts.size() == 0) {
            Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
            ProductApi api = retrofit.create(ProductApi.class);
            Call call = api.getProducts();
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    int responseCode = response.code();
                    List<ProductTb> _productList = new ArrayList<ProductTb>();
                    if(responseCode == 200){
                        try {
                            JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                            for (int i = 0;i<_data.length();i++){
                                Product product = Product.fromJSON(_data.getJSONObject(i));
                                ProductTb productTb = Product.toProductTable(product);
                                _productList.add(productTb);
                                allProducts.add(product);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("MMMMMMMM", "onResponse: error "+e.getMessage() );
                        }
                    }
                    SharedPrefManager.setAllProduct(context,_productList);
                    listener.onGetProduct(allProducts, propertyList);
                }
                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
                }
            });
        }
    }

    public interface OnGetProductsListener {
        void onGetProduct(List<Product> products, List<Property> properties);
    }
}
