package com.novo.tech.redmangopos.view.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.PaymentKeyboardAdapter;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.util.Locale;

public class PaymentCalculator extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    EditText subTotal,tips,receive;
    TextView total,dueAmount,changeAmount;
    PaymentCart cartListFragment;
    PaymentMethod paymentMethodFragment;
    RelativeLayout totalLayout,dueLayout;
    GridView gridView;
    PaymentKeyboardAdapter adapter;
    EditText activeEditText;
    RelativeLayout payButton,continueButton;
    TextView textPayNowBtn;
    double receiveAmount = 0;
    boolean firstTime = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_payment_calculator, container, false);

        subTotal = v.findViewById(R.id.calculatorFragmentSubTotal);
        total = v.findViewById(R.id.calculatorFragmentTotal);
        dueAmount = v.findViewById(R.id.calculatorFragmentDue);
        changeAmount = v.findViewById(R.id.calculatorFragmentChangeGivenAmount);
        receive = v.findViewById(R.id.calculatorFragmentReceived);
        tips = v.findViewById(R.id.calculatorFragmentTips);
        totalLayout = v.findViewById(R.id.calculatorTotalLayout);
        dueLayout = v.findViewById(R.id.calculatorDueLayout);
        gridView = v.findViewById(R.id.calculatorButtonGridVIew);
        payButton = v.findViewById(R.id.calculatorFragmentPayButton);
        textPayNowBtn = v.findViewById(R.id.textPayNowBtn);
        continueButton = v.findViewById(R.id.continueButton);
        receive.setOnFocusChangeListener(this);
        tips.setOnFocusChangeListener(this);
        subTotal.setEnabled(false);
        subTotal.setShowSoftInputOnFocus(false);
        receive.setShowSoftInputOnFocus(false);
        tips.setShowSoftInputOnFocus(false);
        totalLayout.setOnClickListener(this);
        dueLayout.setOnClickListener(this);
        payButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);
        changeAmount.setOnClickListener(this);
        cartListFragment = (PaymentCart)(getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCart);
        paymentMethodFragment = (PaymentMethod) (getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentRight);

        activeEditText = receive;
        dueAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_selector) );
        setData();
        setButton();

        return  v;
    }
    void setButton(){
        String[] data = {"1", "2", "3","4", "5", "6","7", "8", "9","","0","."};
        adapter = new PaymentKeyboardAdapter(getContext(),data);
        gridView.setAdapter(adapter);
    }
    @SuppressLint("DefaultLocale")
    void setData(){
        subTotal.setText(String.format("%.2f",cartListFragment.orderModel.subTotal));
        total.setText(String.format("%.2f",cartListFragment.orderModel.total));
        receive.setText("0");
        tips.setText(String.format("%.2f",cartListFragment.orderModel.tips));
        dueAmount.setText(String.format("%.2f",cartListFragment.orderModel.dueAmount));

        if(cartListFragment.orderModel.paymentStatus == null){
            cartListFragment.orderModel.paymentStatus ="UNPAID";
        }
        if(cartListFragment.orderModel.paymentStatus.equals("PAID")){
            textPayNowBtn.setText("SAVE");
        }else{
            textPayNowBtn.setText("PAY Now");
        }

    }
    public void onInput(String value){
        if(firstTime){
            if(activeEditText.getId()==receive.getId()){
                receive.setText("");
                firstTime = false;
            }
        }

        if(!value.equals("") && !value.equals("-")){
            if(activeEditText != null){
                String str = activeEditText.getText().toString().replaceAll("-","");
                str+=value;
                int len = str.length();
                if(len > 1 && str.charAt(0)=='0'){
                    str = str.substring(1,len);
                }
                activeEditText.setText(str);
            }
        }else{
            String str = activeEditText.getText().toString().replaceAll("-","");

            if(str.length()<=1){
                str = "0";
            }else{
                str = str.substring(0,str.length()-1);
            }
            activeEditText.setText(str);
        }
        double _tipsAmount = 0;
        try {
            _tipsAmount = Double.parseDouble(tips.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        cartListFragment.orderModel.tips = _tipsAmount;
        cartListFragment.calculateTotal();
        total.setText(String.format("%.2f",cartListFragment.orderModel.total));
        dueAmount.setText(String.format("%.2f",cartListFragment.orderModel.total-cartListFragment.orderModel.totalPaid));
        try{
            receiveAmount = Double.parseDouble(receive.getText().toString());
        }catch (Exception e){
            receiveAmount = 0;
        }
        changeAmount.setText(String.format("%.2f",receiveAmount - Double.parseDouble(dueAmount.getText().toString())));
    }
    boolean checkPayAmount(){
        receiveAmount = Double.parseDouble(receive.getText().toString());
        changeAmount.setText(String.format(Locale.getDefault(),"%.2f",receiveAmount-Double.parseDouble(dueAmount.getText().toString())));
        dueAmount.setText(String.format(Locale.getDefault(),"%.2f",cartListFragment.orderModel.total-cartListFragment.orderModel.totalPaid));
        DecimalFormat df = new DecimalFormat("###.##");
        return df.format(cartListFragment.orderModel.dueAmount).equals(df.format(receiveAmount));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onClick(View v) {
        double due = 0;
        try{
            receiveAmount = Double.parseDouble(receive.getText().toString().replaceAll("-",""));
        }catch (Exception e){
            receiveAmount = 0;
            e.printStackTrace();
        }

        try{
             due = Double.parseDouble(dueAmount.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }

        cartListFragment.orderModel.comments = paymentMethodFragment.comments.getText().toString();

        if(v.getId() == dueLayout.getId()){
            receive.setText(String.format("%.2f",cartListFragment.orderModel.dueAmount));
            receiveAmount = cartListFragment.orderModel.dueAmount;
            changeAmount.setText(String.format("%.2f",receiveAmount-due));
            dueAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_selector) );
            receive.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
            tips.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
            changeAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );

            activeEditText = receive;
            firstTime = true;
            // checkPayAmount();
        }
        if(v.getId() == R.id.continueButton){
            Log.e("Pay later", "clicked");
            if(SharedPrefManager.getPrintOnPayLaterButtonClick(getContext())){
                try {
                    OrderRepository manager = new OrderRepository(requireContext());
                    OrderModel orderData1 = manager.getOrderData(cartListFragment.orderModel.db_id);
                    CashMemoHelper.printCustomerMemo(orderData1,getContext(),true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            setData();

//            DBOrderManager dbOrderManager = new DBOrderManager(getContext());
            OrderRepository repository = new OrderRepository(getContext());
            OrderModel orderData = repository.getOrderData(cartListFragment.orderModel.db_id);
            orderData.comments = cartListFragment.orderModel.comments;
            SharedPrefManager.createUpdateOrder2(getContext(), orderData);

            Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//                    gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            gotoScreenVar.putExtra("shift","EXISTING");
            startActivity(gotoScreenVar);
        }else if(v.getId() == R.id.calculatorFragmentPayButton){
            if(!checkPayAmount()){
                if(receiveAmount==0){
                    Toast.makeText(getContext(),"Receive Amount can't be 0",Toast.LENGTH_LONG).show();
                    return;
                }
                if(receiveAmount >= cartListFragment.orderModel.dueAmount){
                    cartListFragment.orderModel.receive = cartListFragment.orderModel.dueAmount;
                    cartListFragment.payNow(false,cartListFragment.orderModel.dueAmount, paymentMethodFragment.paymentMethod);
                    cartListFragment.calculateTotal();
                    checkPayAmount();
                    setData();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Due Amount doesn't match with Receive Amount. You want to pay partial Amount ?")
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cartListFragment.orderModel.receive = receiveAmount;
                                    cartListFragment.orderModel.comments = paymentMethodFragment.comments.getText().toString();
                                    cartListFragment.payNow(false,receiveAmount, paymentMethodFragment.paymentMethod);
                                    receive.setText("0");
                                    setData();
                                    checkPayAmount();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }
            }else{
                cartListFragment.orderModel.receive = receiveAmount;
                cartListFragment.orderModel.comments = paymentMethodFragment.comments.getText().toString();
                cartListFragment.payNow(true,receiveAmount, paymentMethodFragment.paymentMethod);
                checkPayAmount();
            }

//            DBOrderManager dbOrderManager = new DBOrderManager(getContext());
//            OrderModel orderData = dbOrderManager.getOrderData(cartListFragment.orderModel.db_id);
//            orderData.comments = cartListFragment.orderModel.comments;
//            dbOrderManager.open();
//            dbOrderManager.addOrUpdateOrder2(orderData);
//            dbOrderManager.close();
        }else if (v.getId() == R.id.calculatorFragmentChangeGivenAmount){
            changeAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_selector) );
            tips.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
            receive.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();
        if(hasFocus){
            if(id == R.id.calculatorFragmentReceived){
                activeEditText = receive;
                receive.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_selector) );
                tips.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
                changeAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
                dueAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
            }else if(id == R.id.calculatorFragmentTips){
                activeEditText = tips;
                activeEditText.setText("");
                tips.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_selector) );
                receive.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
                changeAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );
                dueAmount.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_background) );

            }
        }else{
            activeEditText = null;
        }
    }
}