package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.databinding.ModelPrint2Binding;
import com.novo.tech.redmangopos.databinding.ViewPrint2Binding;
import com.novo.tech.redmangopos.escpos.OnDeviceConnect;
import com.novo.tech.redmangopos.escpos.OnPrintProcess;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.utils.AlertUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class KitchenPrintService extends AsyncTask<String, Integer, List<Bitmap>> {
    final Context context;
    OrderModel orderModel;
    boolean multiplePrint;
    String printerIp;
    CashMemoHelper.printType printType;

    public KitchenPrintService(Context context, OrderModel orderModel,boolean multiplePrint, String printerIp, CashMemoHelper.printType printType){
        this.context = context;
        this.orderModel= orderModel;
        this.multiplePrint = multiplePrint;
        this.printerIp = printerIp;
        this.printType = printType;
    }
    @Override
    protected List<Bitmap> doInBackground(String... strings) {
        orderModel.items.sort(Comparator.comparingInt((CartItem a) -> a.printOrder));
        orderModel.items.removeIf((a) -> a.uuid.equals("plastic-bag"));
        orderModel.items.removeIf((a) -> a.uuid.equals("container"));
        switch (printType) {
            case KITCHEN:
                orderModel.items.removeIf((a) -> a.kitchenItem == null || !a.kitchenItem.equals("1"));
                break;
        }
        Log.e("ItemSize===>", orderModel.items.size() + "");

        /*****Print unctionality*****/
        ViewPrint2Binding bind = ViewPrint2Binding.inflate(LayoutInflater.from(context));
        int style  = SharedPrefManager.getPrintStyle(context);
        boolean orderPrint = SharedPrefManager.getEnablePrintOrderNo(context);


        int noOfPrint = 1;
        /*
         * 0 = single print
         * 1 = double print
         * 2 = triple print
         * */
        if (orderModel.order_type.equals("DELIVERY") || orderModel.order_type.equals("COLLECTION")) {
            switch (SharedPrefManager.getPrintCount(context)) {
                case 0:
                    noOfPrint = 1;
                    break;
                case 1:
                    if (multiplePrint)
                        noOfPrint = 2;
                    break;
                case 2:
                    if (orderModel.paymentStatus.equals("PAID")) {
                        noOfPrint = 3;
                    } else if (orderModel.paymentMethod.equals("CASH")){
                        noOfPrint = 2;
                    }
                    break;
            }
        } else if(SharedPrefManager.getDoublePrintTakeout(context) && orderModel.order_type.equals("TAKEOUT")){
            noOfPrint = 2;
        }

        List<Bitmap> bitmapList = new ArrayList<>();
        for (int i = 0;i<noOfPrint;i++) {
            double cashPayment = 0;
            double cardPayment = 0;
            CashRepository cashManager = new CashRepository(context);
            List<TransactionModel> allTransactions = cashManager.getOrderTransaction(orderModel.db_id);

            for (TransactionModel model : allTransactions) {
                if (model.inCash == 0) {
                    cardPayment += model.amount;
                } else {
                    cashPayment += model.amount;
                }
            }
            if (orderModel.order_channel == null) {
                orderModel.order_channel = "EPOS";
            }
            if (orderModel.paymentStatus == null) {
                orderModel.paymentStatus = "UNPAID";
            }

            String orderType = orderModel.order_type;
            if (orderModel.order_type.equalsIgnoreCase("TAKEOUT") && orderModel.order_channel.equalsIgnoreCase("ONLINE")) {
                orderType = "COLLECTION"+" ( ONLINE )";
            }else if(orderModel.order_type.equalsIgnoreCase("LOCAL")){
                orderType = "TABLE #";
            }else if(orderModel.order_channel.equalsIgnoreCase("ONLINE")){
                orderType+=" ( ONLINE )";
            }


            bind.businessName.setText(SharedPrefManager.getBusinessName(context));
            bind.businessLocation.setText(SharedPrefManager.getBusinessLocation(context));
            bind.businessPhone.setText(SharedPrefManager.getBusinessPhone(context));
            bind.paymentMethod.setText(cardPayment != 0 && cashPayment != 0 ? "CASH and CARD" : orderModel.paymentMethod);
            bind.orderFrom.setText(orderModel.order_channel.replaceAll("EPOS", "LOCAL"));
            bind.orderNo.setText(String.valueOf(orderModel.order_id));

            bind.containerPaymentMoethod.setVisibility(View.GONE);
            bind.containerOrderFrom.setVisibility(View.GONE);

            String _time = "";
            if (orderModel.deliveryType == null) {
                _time += orderModel.order_type.toLowerCase() + " at : " + "ASAP";
            } else if (orderModel.deliveryType.equals("ASAP")) {
                _time += orderModel.order_type.toLowerCase() + " at : " + "ASAP";
            } else {
                _time += orderModel.order_type.toLowerCase() + " at : " + orderModel.currentDeliveryTime;
            }
            if(_time.length()>1){
                _time =  _time.substring(0, 1).toUpperCase()+_time.substring(1);
            }
            bind.orderTime.setText("Order at : "+orderModel.orderDateTime);
            bind.deliveryTime.setText(_time);
            bind.orderType.setText(orderType);

            if (SharedPrefManager.getWaiterApp(context)) {
                if (orderModel.order_type.equals("LOCAL")) {
                    String tableStr = String.valueOf(orderModel.tableBookingModel == null ? "N/A" : orderModel.tableBookingModel.table_no);
                    String guestStr = String.valueOf(orderModel.tableBookingModel == null ? "N/A" : orderModel.tableBookingModel.adult + orderModel.tableBookingModel.child);
                    bind.tableNo.setText(tableStr);
                    bind.noOfGuest.setText(guestStr);
                }else {
                    bind.containerTableNo.setVisibility(View.GONE);
                }

            } else {
                bind.containerTableNo.setVisibility(View.GONE);
            }
            bind.dueTotalContainer.setVisibility(View.GONE);
            String paidOrNot = "ORDER IS PAID "+(cardPayment != 0 && cashPayment != 0 ? "CASH and CARD" : orderModel.paymentMethod);
            if (orderModel.paymentStatus.equals("PAID") || orderModel.totalPaid == orderModel.total) {
                paidOrNot = "ORDER IS PAID "+(cardPayment != 0 && cashPayment != 0 ? "CASH and CARD" : orderModel.paymentMethod);
            } else if ((cardPayment + cashPayment) < orderModel.total) {
                paidOrNot = "ORDER NOT PAID";
                bind.dueTotalContainer.setVisibility(View.VISIBLE);
                if (orderModel.bookingId != 0){
                    bind.dueTotal.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.dueAmount));
                } else {
                    bind.dueTotal.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.total - (cardPayment + cashPayment)));
                }
            }

            bind.orderPaidMessage.setText(paidOrNot);

            if (orderModel.refundAmount != 0) {
                bind.refunded.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.refundAmount));
            } else {
                bind.refundContainer.setVisibility(View.GONE);
            }
            bind.subTotal.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.subTotal));

            if (orderModel.order_type.toUpperCase().equals("DELIVERY")) {
                bind.deliveryCharge.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.deliveryCharge));
            } else {
                bind.deliveryChargeContainer.setVisibility(View.GONE);
            }

            if (orderModel.order_channel.toUpperCase().equals("ONLINE")) {
                if (orderModel.discountAmount > 0) {
                    if (orderModel.discountCode == null)
                        orderModel.discountCode = "";
                    bind.discountTitle.setText("Discount (" + orderModel.discountCode + ")");
                    bind.discount.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.discountAmount));
                } else
                    bind.discount.setText("£ " + String.format(Locale.getDefault(), "%.2f", 0.0));

            } else {
                String discountStr = "Discount";
                if(orderModel.discountPercentage>0){
                    discountStr = "Discount (" + orderModel.discountPercentage + "%)";
                }
                bind.discountTitle.setText(discountStr);
                bind.discount.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.discountAmount));
            }

            if (orderModel.plasticBagCost > 0)
                bind.plasticBag.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.plasticBagCost));
            else
                bind.plasticBagContainer.setVisibility(View.GONE);


            if (orderModel.containerBagCost > 0)
                bind.containerBag.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.containerBagCost));
            else
                bind.containerBagContainer.setVisibility(View.GONE);


            if (orderModel.adjustmentAmount > 0)
                bind.adjustment.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.adjustmentAmount));
            else
                bind.adjustmentContainer.setVisibility(View.GONE);

            if(!orderPrint){
                bind.containerOrderNo.setVisibility(View.GONE);
            }else{
                bind.containerOrderNo.setVisibility(View.VISIBLE);
            }


            bind.total.setText("£ " + String.format(Locale.getDefault(), "%.2f", orderModel.total));

//            if (style == 0) {
//                if (cardPayment != 0)
//                    bind.cardPay.setText("£ " + String.format(Locale.getDefault(), "%.2f", cardPayment));
//                else
//                    bind.cardPayContainer.setVisibility(View.GONE);
//
//
//                if (cashPayment != 0)
//                    bind.cashPay.setText("£ " + String.format(Locale.getDefault(), "%.2f", cashPayment));
//                else
//                    bind.cashPayContainer.setVisibility(View.GONE);
//
//            }

            bind.cardPayContainer.setVisibility(View.GONE);
            bind.cashPayContainer.setVisibility(View.GONE);


            String dlAddress = "";
            if (SharedPrefManager.getServiceChargeIncluded(context)) {
                dlAddress += "Service charge is not included\n";
            }


            if (orderModel.order_type.equalsIgnoreCase("DELIVERY") || orderModel.order_type.equalsIgnoreCase("COLLECTION")) {
                if (orderModel.customer != null) {
                    CustomerModel customerModel = orderModel.customer;
                    if (customerModel.profile != null) {
                        dlAddress += "\nName : " + customerModel.profile.first_name + " " + customerModel.profile.last_name;
                        if(!orderType.equalsIgnoreCase("COLLECTION"))
                            if (orderModel.shippingAddress != null) {
                                CustomerAddress address =  orderModel.shippingAddress;
                                if(address.properties!=null){
                                    CustomerAddressProperties pro = address.properties;
//                                    CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
                                    dlAddress += "\nAddress : " + pro.building + " " + pro.street_number + " " + pro.street_name + "\n" + pro.city + " " + pro.state + " " + pro.zip;

                                }
                            }
                        dlAddress += "\nPhone : " + customerModel.profile.phone;
                    }
                }
            }

            bind.comments.setText("Comments : " + orderModel.comments);
            bind.address.setText(dlAddress);
            Log.e("PrintService", dlAddress);
            Log.d("mmmmm", "printImageMemo: 2");


            List<Bitmap> bmps    = new ArrayList<Bitmap>();
            int allitemsheight   = 0;
            bind.items.removeAllViews();
            for (int j = 0; j < orderModel.items.size(); j++) {
                Log.e(orderModel.items.get(j).uuid, orderModel.items.get(j).kitchenItem + " ");
                View childView = getView(j, context,style);
                bind.items.addView(childView);
//                bmps.add(getBitmapFromItemView(childView));
                allitemsheight+=childView.getMeasuredHeight();
            }

//            Bitmap bigbitmap    = Bitmap.createBitmap(600, allitemsheight, Bitmap.Config.ARGB_8888);
//            Canvas bigcanvas    = new Canvas(bigbitmap);
//            Paint paint = new Paint();
//            int iHeight = 0;
//            for (int j = 0; j < bmps.size(); j++) {
//                Bitmap bmp = bmps.get(j);
//                bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
//                iHeight+=bmp.getHeight();
//                bmp.recycle();
//                bmp=null;
//            }

//            bind.items.setImageBitmap(bigbitmap);


            Log.d("mmmmm", "printImageMemo: 3");


            Bitmap bitmap = getBitmapFromView(bind.getRoot());
            Log.d("mmmmm", "printImageMemo: 4");

            bitmapList.add(bitmap);
        }

        return bitmapList;
    }


    @Override
    protected void onPostExecute(List<Bitmap> bitmaps) {
        super.onPostExecute(bitmaps);

        if (bitmaps != null && bitmaps.size() > 0) {
            for (int i=0;i<bitmaps.size();i++) {
                int index = i;

                TheApp.serviceBinding.connetNet(printerIp, new OnDeviceConnect() {
                    @Override
                    public void onConnect(boolean isConnect) {
                        if (isConnect) {
                            Log.e("Printing", "connected");
                            TheApp.serviceBinding.printBitmap(bitmaps.get(index), new OnPrintProcess() {
                                @Override
                                public void onSuccess() {
                                    Log.d("mmmmm", "printImageMemo: 666");
                                }

                                @Override
                                public void onError(String msg) {
                                    Log.d("mmmmm", "printImageMemo: " + msg);
                                }
                            });
                        } else {
                            Intent intent = new Intent(TheApp.PRINTER_ALERT_ACTION);
                            context.sendBroadcast(intent);
//                            Toast.makeText(context, "Could not connect to printer!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
//            Tiny.BitmapCompressOptions options = new Tiny.BitmapCompressOptions();
//            Tiny.getInstance().source(bitmaps.get(0)).asBitmap().withOptions(options).compress(new BitmapCallback() {
//                @Override
//                public void callback(boolean isSuccess, Bitmap bitmap) {
//                    if (isSuccess){
////                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
////                        ImageView imageView = new ImageView(context);
////                        imageView.setImageBitmap(bitmap);
////                        builder.setView(imageView);
////                        builder.create();
////                        builder.show();
//
//                    }
//                }
//            });
        }
    }
    private static Bitmap getBitmapFromView(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);

        //create resized image and display
        float maxImageSize = 570;
        float ratio = (float) maxImageSize / returnedBitmap.getWidth();
        int width = Math.round((float) ratio * returnedBitmap.getWidth());
        int height = Math.round((float) ratio * returnedBitmap.getHeight());
        Bitmap resizedImage = Bitmap.createScaledBitmap(returnedBitmap, width, height, true);
        //return the bitmap
        return resizedImage;
    }
    private static Bitmap getBitmapFromItemView(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, 600, view.getMeasuredHeight());

        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(600, view.getMeasuredHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
    View getView(int position, Context mCtx,int style) {
        ModelPrint2Binding binding = ModelPrint2Binding.inflate(LayoutInflater.from(mCtx));
        CartItem item = orderModel.items.get(position);
        String str3="";

        if(position <orderModel.items.size()-1){
            if(orderModel.items.get(position).printOrder < orderModel.items.get(position+1).printOrder){
                binding.underLine.setVisibility(View.VISIBLE);
            }
        }

        if(style == 0){
            if(item.componentSections.size()>0){
                str3+=String.valueOf(item.quantity)+" x "+item.shortName;
                for (ComponentSection section : item.componentSections){
                    String _comName = section.selectedItem.shortName;
                    if(section.selectedItem.subComponentSelected != null){
                        if (!section.selectedItem.subComponentSelected.shortName.equalsIgnoreCase("NONE"))
                            _comName+=" -> "+section.selectedItem.subComponentSelected.shortName;
                    }
                    str3+="\n"+_comName;
                }
            }else{
                str3+=String.valueOf(item.quantity)+" x "+item.shortName;
            }


        }else{
            if(item.componentSections.size()>0){
                for (ComponentSection section : item.componentSections){
                    String _comName = section.selectedItem.shortName;
                    if(section.selectedItem.subComponentSelected != null){
                        if (!section.selectedItem.subComponentSelected.shortName.equalsIgnoreCase("NONE"))
                            _comName+=" -> "+section.selectedItem.subComponentSelected.shortName;
                    }
                    str3+=String.valueOf(item.quantity)+" x "+item.shortName +" : "+_comName;

                }
            }else{
                str3+=String.valueOf(item.quantity)+" x "+item.shortName;
            }
        }

        if(item.extra != null && item.extra.size() > 0){
            StringBuilder str = new StringBuilder("\nExtra :");
            for (CartItem extraItem :item.extra) {
                str.append("  *").append(extraItem.shortName);
            }
            str3+=str.toString();
        }




        double price = 0;

        if(!item.offered){
            price=(item.subTotal*item.quantity);

            if (item.extra != null) {
                for (CartItem extraItem: item.extra) {
                    price+=extraItem.price;
                }
            }

        }
        else
            price = item.total;

        if(!item.comment.isEmpty())
            str3+="\nNote : "+item.comment;
        binding.itemText.setText(str3);
        binding.itemPrice.setText("£ "+String.format(Locale.getDefault(),"%.2f",price));
        binding.getRoot().buildDrawingCache(true);

        Log.d("mmmmm", "getView: "+position);

        return binding.getRoot();
    }
}
