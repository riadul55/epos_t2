package com.novo.tech.redmangopos.storage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.Log;

import androidx.work.ArrayCreatingInputMerger;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.type.DateTime;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.ApiCall;
import com.novo.tech.redmangopos.model.AppConfig;
import com.novo.tech.redmangopos.model.CallerHistoryModel;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.ProductProperties;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.model.kitchenPref.PreparedOrder;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.Inventory;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.room_db.repositories.ProductRepository;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.sync.TableService;
import com.novo.tech.redmangopos.sync.sync_service.ApiSyncService;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.view.activity.Dashboard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SharedPrefManager {
    private static final String SOME_STRING_VALUE = "ORDER_DATA";
    private static final ProductProperties defaultProperties = new ProductProperties(false, false, false, false, false, false, "", "", 0, 0, "","", 0, "0","0");

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SOME_STRING_VALUE, Context.MODE_PRIVATE);
    }

    public static void setOnlineIds(Context context, String serverId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();

        try {
            JSONObject onlineIds = getOnlineIds(context);

            if (onlineIds == null) {
                createNewIds(editor, serverId);
            } else {
                long timestamp = Long.parseLong(onlineIds.getString("timestamp"));
                if (DateUtils.isToday(timestamp)) {
                    String ids = onlineIds.getString("ids") + "," + serverId;
                    onlineIds.put("ids", ids);

                    //update ids
                    editor.putString("onlineIds", onlineIds.toString());
                    editor.apply();
                } else {
                    createNewIds(editor, serverId);
                }
            }
        } catch (Exception e) {
            createNewIds(editor, serverId);
        }
    }

    public static void createNewIds(SharedPreferences.Editor editor, String serverId) {
        try {
            JSONObject object = new JSONObject();
            object.put("timestamp", System.currentTimeMillis());
            object.put("ids", serverId);

            //create new
            editor.putString("onlineIds", object.toString());
            editor.apply();
            Log.e("createdIds", object.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getOnlineIds(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String json = prefs.getString("onlineIds", null);
        try {
            return new JSONObject(json);
        } catch (Exception e) {
            return null;
        }
    }

    public static void setAppConfig(Context context, AppConfig body) {
        try {
            SharedPreferences.Editor editor = getSharedPreferences(context).edit();
            Gson gson = new Gson();
            String json = gson.toJson(body);
            editor.putString("appConfig", json);
            editor.apply();
        } catch (Exception e) {}
    }

    public static AppConfig getAppConfig(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("appConfig", null);
        return gson.fromJson(json, AppConfig.class);
    }

    public static void setUpdateData(Context context, UpdateData data) {
        Log.e("Status===>", data.getStatus());
        Data.Builder builder = new Data.Builder();
        builder.putAll(data.toMap());
        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(ApiSyncService.class)
                .setInputData(builder.build())
                .build();
        TheApp.mWorkManager.beginUniqueWork("updatingToServer", ExistingWorkPolicy.APPEND, request).enqueue();
//        Intent intent = new Intent(context, ApiSyncService.class);
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("data", data);
//        intent.putExtras(bundle);
//        context.startService(intent);
//        ApiSyncService.enqueueWork(context, intent);
    }

    public static void saveForLaterUpdate(Context context, UpdateData data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        Gson gson = new Gson();
        List<UpdateData> updateDataList = getUpdatedData(context) != null ? getUpdatedData(context) : new ArrayList<>();
//        if (updateDataList.size() > 0) {
//            for (UpdateData d : updateDataList) {
//                if (d.getOrderId() == data.getOrderId()) {
//                    updateDataList.remove(d);
//                }
//            }
//        }

        updateDataList.add(data);
        String json = gson.toJson(updateDataList);
        editor.putString("updatedDatas", json);
        editor.apply();
    }

    public static List<UpdateData> getUpdatedData(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("updatedDatas", String.valueOf(new ArrayList<UpdateData>()));
        Type type = new TypeToken<ArrayList<UpdateData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void clearUpdatedData(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        prefs.edit().remove("updatedDatas").apply();
    }

    public static void removeUpdatedOrder(Context context, int db_id, String cacheStatus) {
        List<UpdateData> removeList = new ArrayList<>();
        List<UpdateData> updateDataList = getUpdatedData(context);
        if (updateDataList != null && updateDataList.size() > 0) {
            for (UpdateData d : updateDataList) {
                if (d.getOrderId() == db_id && d.getStatus().equals(cacheStatus)) {
                    removeList.add(d);
                }
            }

            updateDataList.removeAll(removeList);
            SharedPreferences.Editor editor = SharedPrefManager.getSharedPreferences(context).edit();
            Gson gson = new Gson();
            String json = gson.toJson(updateDataList);
            Log.e("PrefJson", json);
            editor.putString("updatedDatas", json);
            editor.apply();
        }
    }

    //pref for kitchen app
    public static void savePrefForKitchen(Context context, PreparedOrder data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        Gson gson = new Gson();
        List<PreparedOrder> updateDataList = getKitchenPrefData(context) != null ? getKitchenPrefData(context) : new ArrayList<>();
        Log.e("updateDataList==>", updateDataList.size() + "");
        List<PreparedOrder> newDataList = new ArrayList<>();
        if (isKitchenDataExist(updateDataList, data)) {
            for (int i=0;i<updateDataList.size();i++) {
                PreparedOrder order = updateDataList.get(i);
                if (order.getOrderId() != null && data.getOrderId() != null && order.getOrderId().equals(data.getOrderId())) {
                    updateDataList.set(i, data);
                }
            }
            newDataList.addAll(updateDataList);
        } else {
            updateDataList.add(data);
            newDataList.addAll(updateDataList);
        }
        String json = gson.toJson(newDataList);
        Log.e("prefKitchenJson==>", json);
        editor.putString("prefForKitchen", json);
        editor.apply();
    }
    public static List<PreparedOrder> getKitchenPrefData(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("prefForKitchen", String.valueOf(new ArrayList<PreparedOrder>()));
        Type type = new TypeToken<ArrayList<PreparedOrder>>() {}.getType();
        return gson.fromJson(json, type);
    }
    public static boolean isKitchenDataExist(List<PreparedOrder> orders, PreparedOrder order) {
        boolean exist = false;
        for (PreparedOrder model: orders) {
            if (model.getOrderId() != null && order.getOrderId() != null) {
                if (model.getOrderId().equals(order.getOrderId())) {
                    exist = true;
                    break;
                }
            }
        }
        return exist;
    }



    public static ArrayList<ApiCall> getPullBy(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("pullBy", String.valueOf(new ArrayList<ApiCall>()));
        Type type = new TypeToken<ArrayList<ApiCall>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void setPullBy(Context context, String from) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        ArrayList<ApiCall> callList = getPullBy(context);
        callList.add(new ApiCall(from, System.currentTimeMillis() + ""));
        ArrayList<ApiCall> newList = new ArrayList<ApiCall>();
        for (int i = 0; i < callList.size(); i++) {
            if (callList.get(i).getTime() != null && DateUtils.isToday(Long.parseLong(callList.get(i).getTime()))) {
                newList.add(callList.get(i));
            }
        }
        Gson gson = new Gson();
        String json = gson.toJson(newList);
        editor.putString("pullBy", json);
        editor.apply();
    }

    public static void clearPullBy(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove("pullBy");
        editor.apply();
    }

    public static void setNotifyDate(Context context, String date) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("notifyDate", date);
        editor.apply();
    }

    public static String getNotifyDate(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString("notifyDate", null);
    }

    public static void setEmailSendingTime(Context context, long date) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("emailSending", date + "");
        editor.apply();
    }

    public static Long getEmailSendingTime(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String time = prefs.getString("emailSending", null);
        return Long.parseLong(time);
    }

    public static void setCustomerLoaded(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("loadedCustomer", true);
        editor.apply();
    }

    public static boolean getCustomerLoaded(Context context) {
        boolean result = getSharedPreferences(context).getBoolean("loadedCustomer", false);
        return result;
    }

    public static void setShiftOpenTime(Context context, String closeTIme) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("shiftOpenTime", closeTIme);
        editor.apply();
    }

    public static Date getShiftOpenTime(Context context) {
        String time = getSharedPreferences(context).getString("shiftOpenTime", "10:00");
        String[] splitTime = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
        return calendar.getTime();
    }

    public static void setShiftCloseTime(Context context, String closeTIme) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("shiftCloseTime", closeTIme);
        editor.apply();
    }

    public static Date getShiftCloseTime(Context context) {
        String time = getSharedPreferences(context).getString("shiftCloseTime", null);
        Date closeTime = null;
        try {
            if (time != null)
                closeTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return closeTime;
    }

    public static void setBusinessName(Context context, String name) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("BusinessName", name);
        editor.apply();
    }

    public static String getBusinessName(Context context) {
        return getSharedPreferences(context).getString("BusinessName", "");
    }

    public static void setBusinessPhone(Context context, String name) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("BusinessPhone", name);
        editor.apply();
    }

    public static String getBusinessPhone(Context context) {
        return getSharedPreferences(context).getString("BusinessPhone", "0");
    }

    public static void setCookDuration(Context context, int duration) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("cookDuration", duration);
        editor.apply();
    }

    public static Integer getCookDuration(Context context) {
        return getSharedPreferences(context).getInt("cookDuration", 40);
    }

    public static void setBusinessLocation(Context context, String location) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("BusinessLocation", location);
        editor.apply();
    }

    public static String getBusinessLocation(Context context) {
        return getSharedPreferences(context).getString("BusinessLocation", "United Kingdom");
    }

//    public static void setDiscountAmount(Context context, int amount) {
//        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
//        editor.putInt("discountAmount", amount);
//        editor.apply();
//    }
//
//    public static int getDiscountAmount(Context context) {
//        return getSharedPreferences(context).getInt("discountAmount", 0);
//    }


    public static void setDiscountCollection(Context context, int amount) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("discountAmountCollection", amount);
        editor.apply();
    }

    public static int getDiscountCollection(Context context) {
        return getSharedPreferences(context).getInt("discountAmountCollection", 0);
    }

    public static void setDiscountDelivery(Context context, int amount) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("discountAmountDelivery", amount);
        editor.apply();
    }

    public static int getDiscountDelivery(Context context) {
        return getSharedPreferences(context).getInt("discountAmountDelivery", 0);
    }

    public static void setDaysCount(Context context, int days) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("daysCount", days);
        editor.apply();
    }

    public static int getDaysCount(Context context) {
        return getSharedPreferences(context).getInt("daysCount", 0);
    }

    public static void setCustomerOrderCount(Context context, int days) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("CustomerOrderCount", days);
        editor.apply();
    }

    public static int getCustomerOrderCount(Context context) {
        return getSharedPreferences(context).getInt("CustomerOrderCount", 0);
    }

    public static void setDeliveryCharge(Context context, double amount) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("deliveryCharge", amount + "");
        editor.apply();
    }

    public static double getDeliveryCharge(Context context) {
        String deliveryCharge = getSharedPreferences(context).getString("deliveryCharge", "0.0");
        return Double.parseDouble(deliveryCharge);
    }

    public static void setMinAmountCollection(Context context, int amount) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("minAmountCollection", amount);
        editor.apply();
    }

    public static int getMinAmountCollection(Context context) {
        return getSharedPreferences(context).getInt("minAmountCollection", 0);
    }

    public static void setMinAmountDelivery(Context context, int amount) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("minAmountDelivery", amount);
        editor.apply();
    }

    public static int getMinAmountDelivery(Context context) {
        return getSharedPreferences(context).getInt("minAmountDelivery", 0);
    }

    public static void setPlasticBagMandatory(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("PlasticBagMandatory", status);
        editor.apply();
    }

    public static boolean getPlasticBagMandatory(Context context) {
        return getSharedPreferences(context).getBoolean("PlasticBagMandatory", false);
    }

    public static void setOnlineOrderStatus(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("OnlineOrderStatus", status);
        editor.apply();
    }

    public static boolean getOnlineOrderStatus(Context context) {
        return getSharedPreferences(context).getBoolean("OnlineOrderStatus", true);
    }

    public static void setContainerBagEnable(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("containerBag", status);
        editor.apply();
    }

    public static boolean getContainerBagEnable(Context context) {
        return getSharedPreferences(context).getBoolean("containerBag", false);
    }


    public static void setEnableTakeoutOrder(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("TakeoutOrder", status);
        editor.apply();
    }

    public static boolean getEnableTakeoutOrder(Context context) {
        return getSharedPreferences(context).getBoolean("TakeoutOrder", false);
    }

    public static void setEnablePrintOrderNo(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("printOrderNo", status);
        editor.apply();
    }

    public static boolean getEnablePrintOrderNo(Context context) {
        return getSharedPreferences(context).getBoolean("printOrderNo", false);
    }

    public static void setEnableOrderPull(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("orderPull", status);
        editor.apply();
    }

    public static boolean getEnableOrderPull(Context context) {
        return getSharedPreferences(context).getBoolean("orderPull", true);
    }

    public static void setEnableAutoRefresh(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("autoRefresh", status);
        editor.apply();
    }

    public static boolean getEnableAutoRefresh(Context context) {
        return getSharedPreferences(context).getBoolean("autoRefresh", true);
    }

    public static void setEnableTableOrder(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("TableOrder", status);
        editor.apply();
    }

    public static boolean getEnableTableOrder(Context context) {
        return getSharedPreferences(context).getBoolean("TableOrder", false);
    }

    public static void setEnableWaiterApp(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("waiterApp", status);
        editor.apply();
    }

    public static boolean getWaiterApp(Context context) {
        return getSharedPreferences(context).getBoolean("waiterApp", false);
    }

    public static void setPrintOnPayButtonClick(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("payButtonCLick", status);
        editor.apply();
    }

    public static boolean getPrintOnPayButtonClick(Context context) {
        return getSharedPreferences(context).getBoolean("payButtonCLick", false);
    }

    public static void setPrintOnPayLaterButtonClick(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("payLaterButtonCLick", status);
        editor.apply();
    }

    public static boolean getPrintOnPayLaterButtonClick(Context context) {
        return getSharedPreferences(context).getBoolean("payLaterButtonCLick", false);
    }

    public static void setServiceChargeIncluded(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("ServiceChargeIncluded", status);
        editor.apply();
    }

    public static boolean getServiceChargeIncluded(Context context) {
        return getSharedPreferences(context).getBoolean("ServiceChargeIncluded", false);
    }


    public static void setPrintStyle(Context context, int type) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("printStyle", type);
        editor.apply();
    }

    public static int getPrintStyle(Context context) {
        return getSharedPreferences(context).getInt("printStyle", 0);
    }

    public static void setPrintFontSize(Context context, int size) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("fontSize", size);
        editor.apply();
    }

    public static int getPrintSize(Context context) {
        return getSharedPreferences(context).getInt("fontSize", 35);
    }

    public static void setPrintOnOnlineOrder(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("printOnOnline", status);
        editor.apply();
    }

    public static boolean getPrintOnOnlineOrder(Context context) {
        return getSharedPreferences(context).getBoolean("printOnOnline", true);
    }

    public static void setCurrentShift(Context context, int currentShift) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("currentShift", currentShift);
        editor.apply();
    }

    public static int getCurrentShift(Context context) {
        return getSharedPreferences(context).getInt("currentShift", -1);
    }

    public static void setOrderCloseOnPayment(Context context, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("setOrderCloseOnPaymentClose", value);
        editor.apply();
    }

    public static boolean getOrderCloseOnPayment(Context context) {
        return getSharedPreferences(context).getBoolean("setOrderCloseOnPaymentClose", false);
    }

    public static void setDiscountType(Context context, int value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("discountOn25", value);
        editor.apply();
    }

    public static int getDiscountType(Context context) {
        return getSharedPreferences(context).getInt("discountOn25", 0);
    }


    public static void setPrinterIndex(Context context, int index) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("printerIndex", index);
        editor.apply();
    }

    public static int getPrinterIndex(Context context) {
        return getSharedPreferences(context).getInt("printerIndex", Conts.PRINTER_XPRINTER);
    }

    public static void setConnectionIndex(Context context, int index) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("printerConnIndex", index);
        editor.apply();
    }

    public static int getConnectionIndex(Context context) {
        return getSharedPreferences(context).getInt("printerConnIndex", Conts.CONNECTION_USB);
    }

    public static void setPrinterIp(Context context, String ip) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("printerIp", ip);
        editor.apply();
    }

    public static String getPrinterIp(Context context) {
        return getSharedPreferences(context).getString("printerIp", "");
    }


    public static void setPrintCount(Context context, int value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("printCount", value);
        editor.apply();
    }

    public static int getPrintCount(Context context) {
        return getSharedPreferences(context).getInt("printCount", 0);
    }

    public static void setDoublePrintTakeout(Context context, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("doublePrintTakeout", value);
        editor.apply();
    }

    public static boolean getDoublePrintTakeout(Context context) {
        return getSharedPreferences(context).getBoolean("doublePrintTakeout", false);
    }

    public static void setTableBooked(Context context, int tableNo, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("tableBooked" + String.valueOf(tableNo), !value);
        Log.d("TAG", "tableBooked: "+(value?"booked":"free"));
        editor.apply();

        TableBookingModel tableBookingModel = new TableBookingModel(
                1, 1, tableNo, 1, value, 1, 1, ""
        );
        new TableService(context, tableBookingModel).execute();
    }

    public static boolean getTableBooked(Context context, int tableNo) {
        return getSharedPreferences(context).getBoolean("tableBooked" + String.valueOf(tableNo), false);
    }

    public static List<CustomerModel> getAllCustomer(Context context) {
        List<CustomerModel> allCustomer = new ArrayList<>();
        Set<String> allData = getSharedPreferences(context).getStringSet("customer", new HashSet<String>());
        for (String data : allData) {
            allCustomer.add(GsonParser.getGsonParser().fromJson(data, CustomerModel.class));
        }
        return allCustomer;
    }

    public static void addNewCustomer(Context context, CustomerModel customerModel) {
        Set<String> allData = getSharedPreferences(context).getStringSet("customer", new HashSet<String>());
        allData.add(GsonParser.getGsonParser().toJson(customerModel));
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet("customer", new HashSet<String>(allData));
        editor.apply();
    }


    public static List<CallerHistoryModel> getCallHistory(Context context) {
        List<CallerHistoryModel> callerHistoryModels = new ArrayList<>();
        Set<String> history = getSharedPreferences(context).getStringSet("callHistory", new HashSet<String>());
        history.forEach((a) -> {
            callerHistoryModels.add(
                    GsonParser.getGsonParser().fromJson(a, CallerHistoryModel.class)
            );
        });

        callerHistoryModels.forEach((a) -> {
            CustomerRepository dbCustomerManager = new CustomerRepository(context);
            a.customerModel = dbCustomerManager.getCustomerDataByPhone(a.phoneNumber);
        });

        return callerHistoryModels;
    }

    public static void addNewCall(Context context, CallerHistoryModel callerHistoryModel) {
        Set<String> allData = getSharedPreferences(context).getStringSet("callHistory", new HashSet<String>());
        allData.add(GsonParser.getGsonParser().toJson(callerHistoryModel, CallerHistoryModel.class));
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet("callHistory", new HashSet<String>(allData));
        editor.apply();
    }

    public static void removeCall(Context context, CallerHistoryModel callerHistoryModel) {
        Set<String> allData = getSharedPreferences(context).getStringSet("callHistory", new HashSet<String>());
        Set<String> newData = new HashSet<>();
        for (String data : allData) {
            CallerHistoryModel model = GsonParser.getGsonParser().fromJson(data, CallerHistoryModel.class);
            if (!callerHistoryModel.phoneNumber.equals(model.phoneNumber)) {
                newData.add(GsonParser.getGsonParser().toJson(model, CallerHistoryModel.class));
            }
        }
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet("callHistory", new HashSet<String>(newData));
        editor.apply();
    }

    public static void removeAllCall(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet("callHistory", new HashSet<String>());
        editor.apply();
    }


    public static List<Property> getAllProperty(Context context) {
        ProductRepository repository = new ProductRepository(context);
        return repository.getAllProperties();
//        List<Property> allProperty = new ArrayList<>();
//        Set<String> allData = getSharedPreferences(context).getStringSet("property", new HashSet<String>());
//        for (String data : allData) {
//            try {
//                allProperty.add(GsonParser.getGsonParser().fromJson(data, Property.class));
//            } catch (Exception e) {
//                Log.e("mmmmmmmmmm", "getAllProperty: " + e + "  String value = " + data);
//            }
//        }
//        return allProperty;
    }

    public static void setAllProperty(Context context, List<Category> allProperty) {
        ProductRepository repository = new ProductRepository(context);
        repository.deleteAllProperties();
        repository.addProperties(allProperty);
//        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
//        Set<String> allData = new HashSet<String>();
//        for (Property pro : allProperty) {
//            try {
//                allData.add(GsonParser.getGsonParser().toJson(pro));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        editor.putStringSet("property", new HashSet<String>(allData));
//        editor.apply();

    }


    public static List<Product> getAvailableProduct(Context context) {
        ProductRepository repository = new ProductRepository(context);
        List<Product> allProduct = new ArrayList<>();
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.properties == null) {
                pro.properties = defaultProperties;
            }
            if (pro.visible && (pro.properties.available_on.equals("both") || pro.properties.available_on.equals("epos"))) {
                if (pro.componentList.size() > 0) {
                    List<Component> components = new ArrayList<>(pro.componentList);
                    pro.componentList = new ArrayList<>();
                    components.forEach((a) -> {
                        if (a.visible) {
                            if (a.properties != null) {
                                if (a.properties.available_on.equals("both") || a.properties.available_on.equals("epos")) {
                                    pro.componentList.add(a);
                                }
                            }
                        }
                    });
                }
                allProduct.add(pro);
            }
        }
        return allProduct;
    }

    public static List<Product> getAvailableExtraProduct(Context context) {
        ProductRepository repository = new ProductRepository(context);
        List<Product> allProduct = new ArrayList<>();
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.properties == null) {
                pro.properties = defaultProperties;
            }
            if (pro.visible && (pro.properties.available_on.equals("both") || pro.properties.available_on.equals("epos")) && pro.properties.category.equals("extra")) {
                if (pro.componentList.size() > 0) {
                    List<Component> components = new ArrayList<>(pro.componentList);
                    pro.componentList = new ArrayList<>();
                    components.forEach((a) -> {
                        if (a.visible) {
                            if (a.properties != null) {
                                if (a.properties.available_on.equals("both") || a.properties.available_on.equals("epos")) {
                                    pro.componentList.add(a);
                                }
                            }
                        }
                    });
                }
                allProduct.add(pro);
            }
        }
        return allProduct;
    }

    public static Product getProductDetails(Context context, String product_uuid) {
        ProductRepository repository = new ProductRepository(context);
        Product product = null;
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.productUUid.equals(product_uuid)) {

                if (pro.properties == null) {
                    pro.properties = defaultProperties;
                }
                if (pro.visible && (pro.properties.available_on.equals("both") || pro.properties.available_on.equals("epos"))) {
                    if (pro.componentList.size() > 0) {
                        List<Component> components = new ArrayList<>(pro.componentList);
                        pro.componentList = new ArrayList<>();
                        components.forEach((a) -> {
                            if (a.visible) {
                                if (a.properties != null) {
                                    if (a.properties.available_on.equals("both") || a.properties.available_on.equals("epos")) {
                                        pro.componentList.add(a);
                                    }
                                }
                            }
                        });
                    }
                }
                product = pro;
            }
        }
        return product;
    }

    public static Product getProductDetailsByComponent(Context context, String component_uuid) {
        ProductRepository repository = new ProductRepository(context);
        Product product = null;
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            for (Component component : pro.componentList) {
                if (component.productUUid.equals(component_uuid)) {
                    product = pro;
                    break;
                }
            }
        }
        return product;
    }

    public static Product getDeliveryProduct(Context context) {
        ProductRepository repository = new ProductRepository(context);
        Product delivery = null;
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.productType.equals("DELIVERY")) {
                pro.properties = defaultProperties;
            }
            delivery = pro;
        }
        return delivery;
    }

    public static Product getPlasticBagProduct(Context context) {
        ProductRepository repository = new ProductRepository(context);
        Product plasticBag = null;
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.productUUid.equals("plastic-bag")) {
                pro.properties = defaultProperties;
                plasticBag = pro;
            }
        }
        return plasticBag;
    }

    public static Product getContainerBagProduct(Context context) {
        ProductRepository repository = new ProductRepository(context);
        Product plasticBag = null;
        List<Product> allData = repository.getAllProducts();
        for (Product pro : allData) {
            if (pro.productUUid.equals("container")) {
                pro.properties = defaultProperties;
                plasticBag = pro;
            }
        }
        return plasticBag;
    }

    public static void setAllProduct(Context context, List<ProductTb> allProduct) {
        ProductRepository repository = new ProductRepository(context);
        repository.deleteAllProducts();
        repository.addAllProducts(allProduct);
        
//        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
//        Set<String> allData = new HashSet<String>();
//        for (Product pro : allProduct) {
//            try {
//                allData.add(GsonParser.getGsonParser().toJson(pro));
//            } catch (Exception e) {
//                Log.e("mmmmmm", "setAllProduct: " + pro.shortName + " error = " + e.getMessage());
//                e.printStackTrace();
//            }
//        }
//
//        editor.putStringSet("product", new HashSet<String>(allData));
//        editor.apply();

    }

    public static int getLastOrderId(Context context) {
        OrderRepository repository = new OrderRepository(context);
        return repository.getOrderMax();
    }

    public static int createUpdateOrder(Context context, OrderModel orderModel) {
        int dbId = 0;
        try {
            OrderRepository repository = new OrderRepository(context);
            dbId = repository.addOrUpdateOrder(orderModel);
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return dbId;
    }

    public static int createUpdateOrder2(Context context, OrderModel orderModel) {
        int dbId = 0;
        try {
            OrderRepository repository = new OrderRepository(context);
            dbId = repository.addOrUpdateOrder2(orderModel);
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return dbId;
    }

    public static OrderModel getOrderData(Context context, int id) {
        OrderRepository repository = new OrderRepository(context);
        return repository.getOrderData(id);
//        OrderModel orderModel = new OrderModel();
//        OrderRepository dbOrderManager = new OrderRepository(context);
//        dbOrderManager.open();
//        orderModel = dbOrderManager.getOrderData(id);
//        dbOrderManager.close();
    }

    public static List<OrderModel> getAllOrders(Context context) {
        OrderRepository repository = new OrderRepository(context);
        return repository.fetchDashboardData();
    }

}