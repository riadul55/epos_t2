package com.novo.tech.redmangopos.view.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static android.view.View.GONE;

public class OrderDetailsConsumerInfo extends Fragment implements View.OnClickListener {
    LinearLayout btnPrint,refund,voidOrder;
    TextView firstName,lastName,email,phone,houseNumber,streetName,town,postCode,comments,deliveryTimeTextView;
    CustomerModel customerModel;
    OrderDetailsAction actionFragment;
    OrderDetailsCart cartFragment;
    String deliveryTime;
    RelativeLayout deliveryTimeContainer;
    LinearLayout customerInfo;
    boolean customerOrder = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_order_details_customer_info, container, false);

        OrderModel model = (OrderModel) requireArguments().getSerializable("orderModel");
        customerOrder = requireArguments().getBoolean("customerOrder", false);

        actionFragment = (OrderDetailsAction)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
        cartFragment = (OrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);

        customerModel = model.customer;

        btnPrint = v.findViewById(R.id.orderDetailsConsumerPrint);
        refund = v.findViewById(R.id.orderDetailsConsumerRefund);
        voidOrder = v.findViewById(R.id.orderDetailsConsumerVoid);
        deliveryTimeTextView = v.findViewById(R.id.paymentMethodDeliveryTimeValue);
        deliveryTimeContainer = v.findViewById(R.id.DeliveryTimeSelect);
        customerInfo = v.findViewById(R.id.customerInfoContainer);
        btnPrint.setOnClickListener(this);
        refund.setOnClickListener(this);
        voidOrder.setOnClickListener(this);
        deliveryTimeContainer.setOnClickListener(this);


        if (!model.order_status.equals("CANCELLED") && model.paymentStatus.equals("PAID") && model.refundAmount == 0){
            refund.setVisibility(View.VISIBLE);
        }else{
            refund.setVisibility(View.INVISIBLE);
        }
        if(model.paymentStatus.equals("PAID")){
            voidOrder.setVisibility(View.INVISIBLE);
        }else{
            voidOrder.setVisibility(View.VISIBLE);
        }

        firstName = v.findViewById(R.id.orderDetailsConsumerFirstName);
        lastName = v.findViewById(R.id.orderDetailsConsumerLastName);
        email = v.findViewById(R.id.orderDetailsConsumerEmail);
        phone = v.findViewById(R.id.orderDetailsConsumerPhone);
        houseNumber = v.findViewById(R.id.orderDetailsConsumerBuilding);
        streetName = v.findViewById(R.id.orderDetailsConsumerStreet);
        town = v.findViewById(R.id.orderDetailsConsumerCity);
        postCode = v.findViewById(R.id.orderDetailsConsumerZip);
        comments = v.findViewById(R.id.commentsText);
        if(customerModel != null){
            if(customerModel.profile!= null){
                firstName.setText(": "+customerModel.profile.first_name);
                lastName.setText(": "+customerModel.profile.last_name);
                email.setText(": "+customerModel.profile.email);
                phone.setText(": "+customerModel.profile.phone);
            }
//            if(customerModel.addresses!=null){
//                if(customerModel.addresses.size()!=0){
//                    if(customerModel.addresses.get(0).properties!=null) {
//                        CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
//                        houseNumber.setText(": "+pro.building);
//                        streetName.setText(": "+pro.street_number+" "+pro.street_name);
//                        town.setText(": "+pro.city);
//                        postCode.setText(": "+pro.zip);
//                    }
//                }
//            }
            if(model.shippingAddress!=null){
                CustomerAddress address =  model.shippingAddress;
                if(address.properties!=null){
                    CustomerAddressProperties pro = address.properties;
                    houseNumber.setText(": "+pro.building);
                    streetName.setText(": "+pro.street_number+" "+pro.street_name);
                    town.setText(": "+pro.city);
                    postCode.setText(": "+pro.zip);
                }
            }
        }
        comments.setText(model.comments);
        if(model.currentDeliveryTime==null){
            deliveryTimeTextView.setText("ASAP");
        }else{
            deliveryTimeTextView.setText(model.currentDeliveryTime);
        }


        if (customerOrder) {
            refund.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    public void handleRefund(){
        if(cartFragment != null){
            if(!cartFragment.orderModel.paymentStatus.equals("PAID") || cartFragment.orderModel.refundAmount!=0){
                refund.setVisibility(View.INVISIBLE);
            }else{
                refund.setVisibility(View.VISIBLE);
            }
            if(!cartFragment.orderModel.paymentStatus.equals("PAID") && !cartFragment.orderModel.order_status.equals("CANCELLED")){
                voidOrder.setVisibility(View.VISIBLE);
            }
        }

    }
    @Override
    public void onClick(View v) {
        OrderModel orderModel = ((OrderDetails)getContext()).orderModel;
        if(v.getId() == btnPrint.getId()){
            try {
                if(cartFragment != null){
                    if (cartFragment.orderModel.bookingId != 0){
                        OrderRepository manager = new OrderRepository(getContext());
                        OrderModel orderData = manager.getOrderData(orderModel.db_id);
                        cartFragment.orderModel.dueAmount = orderData.total - orderData.totalPaid;
                    }
                    CashMemoHelper.printCustomerMemo(cartFragment.orderModel,getContext(),true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(v.getId() == refund.getId()){
            refundAlert();
        }else if(v.getId() == voidOrder.getId()){
            voidAlert();
        }else if(v.getId() == deliveryTimeContainer.getId()){
           // getDateTimeInput();
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            } else {
                getTimeInput(deliveryTimeTextView);
            }
        }
    }
    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }




    void voidAlert(){
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Void Order")
                .setMessage("Are you sure ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        actionFragment.orderModel.order_status="CANCELLED";
                        cartFragment.paySeal.setText("CANCELLED");
                        cartFragment.status.setText("CANCELLED");
                        voidOrder.setVisibility(View.INVISIBLE);
                        actionFragment.payNow.setVisibility(GONE);
                        SharedPrefManager.createUpdateOrder(getContext(),actionFragment.orderModel);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }

    void refundAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL);

        inputField.setText(String.format(Locale.getDefault(),"%.2f",(cartFragment.orderModel.total-cartFragment.orderModel.refundAmount)));

        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Refund Order")
                .setView(inputField)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String str = inputField.getText().toString();
                        if(str.equals("")){
                            str = "0";
                        }
                        double amount = Double.parseDouble(str);
                        refund(amount);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void refund(double amount){
        cartFragment.orderModel.refundAmount = amount;
        actionFragment.orderModel.refundAmount = amount;

        CashRepository dbCashManager = new CashRepository(getContext());
        dbCashManager.addTransaction(getContext(),amount,4,actionFragment.orderModel.db_id,true);
        Log.e("Refund==>", "set");

//        if(actionFragment.orderModel.order_status.equals("CLOSED")){
//        }
        DecimalFormat df = new DecimalFormat("###.##");
        if(df.format(actionFragment.orderModel.total).equals(df.format(amount))){
            actionFragment.orderModel.order_status="REFUNDED";
            cartFragment.orderModel.order_status="REFUNDED";
            cartFragment.paySeal.setText("REFUNDED");
            cartFragment.status.setText("REFUNDED");
        } else {
            actionFragment.orderModel.order_status="CLOSED";
            cartFragment.orderModel.order_status="CLOSED";
            cartFragment.paySeal.setText("CLOSED");
            cartFragment.status.setText("CLOSED");
        }
        refund.setVisibility(GONE);
        SharedPrefManager.createUpdateOrder(getContext(),actionFragment.orderModel);
        actionFragment.orderModel.isSetUpdate = true;
        cartFragment.calculateTotal();
        actionFragment.setActionView();

        // save status
        if (!actionFragment.orderModel.order_status.equals("CLOSED")) {
            actionFragment.orderModel.isSetUpdate = false;
            long dateTime = System.currentTimeMillis();
            SharedPrefManager.setUpdateData(getActivity(), new UpdateData(actionFragment.orderModel.db_id, actionFragment.orderModel.serverId + "", actionFragment.orderModel.order_channel, "CLOSED", "" + dateTime));
        }
    }
    void getOptionInput(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String[] animals = {"Set Time", "As soon as possible"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 1){
                    cartFragment.orderModel.deliveryType="ASAP";
                    deliveryTimeTextView.setText("ASAP");
                }else{
                  //  getDateTimeInput();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void getDateTimeInput(){
        DecimalFormat fm = new DecimalFormat("00");
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.date_time_picker, (ViewGroup) getView(), false);
        RelativeLayout getDate,getTime;
        TextView dateView,timeView;
        getDate = viewInflated.findViewById(R.id.datePickerDate);
        getTime = viewInflated.findViewById(R.id.datePickerTime);
        dateView = viewInflated.findViewById(R.id.datePickerDateValue);
        timeView = viewInflated.findViewById(R.id.datePickerTimeValue);
        Date date = new java.util.Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
            date = format.parse(cartFragment.orderModel.currentDeliveryTime);
        }catch (Exception e){
            e.printStackTrace();
        }

        dateView.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(date));
        timeView.setText(new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(date));
        getDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDateInput(dateView);
            }
        });
        getTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTimeInput(timeView);
            }
        });
        builder.setTitle("Select Delivery Time");
        builder.setView(viewInflated);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deliveryTime = dateView.getText()+" "+timeView.getText();
                deliveryTimeTextView.setText(deliveryTime);
                cartFragment.orderModel.currentDeliveryTime = deliveryTime;
                cartFragment.orderModel.deliveryType=null;
                actionFragment.orderModel.currentDeliveryTime = deliveryTime;
                SharedPrefManager.createUpdateOrder(getContext(),cartFragment.orderModel);
                deliveryTimeTextView.setText(cartFragment.orderModel.currentDeliveryTime);
            }
        });
        builder.show();

    }


    void getTimeInput(TextView timeView){
        Calendar mcurrentTime = Calendar.getInstance();
        DecimalFormat fm = new DecimalFormat("00");
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        Date currentDate = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());
            currentDate = format.parse(cartFragment.orderModel.currentDeliveryTime);
            hour = currentDate.getHours();
            minute = currentDate.getMinutes();
        }catch (Exception e){
            e.printStackTrace();
        }

        MaterialTimePicker picker =
                new MaterialTimePicker.Builder()
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .setHour(12)
                        .setMinute(10)
                        .setTitleText("Select Time")
                        .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                        .setHour(hour)
                        .setMinute(minute)
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .build();
        Date finalCurrentDate = currentDate;
        picker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedHour = picker.getHour();
                int selectedMinute = picker.getMinute();
                String AM_PM ;
                if(selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    if(selectedHour>12){
                        selectedHour-=12;
                    }
                }
                deliveryTime = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(finalCurrentDate)+" "+String.valueOf(selectedHour)+":"+String.valueOf(selectedMinute)+" "+AM_PM;
                timeView.setText(deliveryTime);
                cartFragment.orderModel.currentDeliveryTime = deliveryTime;
                cartFragment.orderModel.isSetUpdate = false;
                actionFragment.orderModel.currentDeliveryTime = deliveryTime;
                SharedPrefManager.createUpdateOrder2(getContext(), cartFragment.orderModel);
                cartFragment.orderModel.isSetUpdate = true;
            }
        });
        picker.show(getChildFragmentManager(), picker.toString());
    }
    void getDateInput(TextView dateView){
        DecimalFormat fm = new DecimalFormat("00");
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dateView.setText(fm.format(dayOfMonth)+"/"+fm.format(month+1)+"/"+year);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }
}