package com.novo.tech.redmangopos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppConfig implements Serializable {
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("minimum_delivery")
    @Expose
    private String minimumDelivery;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    @SerializedName("business_phone")
    @Expose
    private String businessPhone;
    @SerializedName("min_amount_for_discount_collection")
    @Expose
    private String minDiscountCollection;
    @SerializedName("min_amount_for_discount_delivery")
    @Expose
    private String minDiscountDelivery;
    @SerializedName("business_status")
    @Expose
    private String businessStatus;
    @SerializedName("business_off_date")
    @Expose
    private String businessOffDate;

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getMinimumDelivery() {
        return minimumDelivery;
    }

    public void setMinimumDelivery(String minimumDelivery) {
        this.minimumDelivery = minimumDelivery;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getMinDiscountCollection() {
        return minDiscountCollection;
    }

    public void setMinDiscountCollection(String minDiscountCollection) {
        this.minDiscountCollection = minDiscountCollection;
    }

    public String getMinDiscountDelivery() {
        return minDiscountDelivery;
    }

    public void setMinDiscountDelivery(String minDiscountDelivery) {
        this.minDiscountDelivery = minDiscountDelivery;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getBusinessOffDate() {return businessOffDate;}

    public void setBusinessOffDate(String businessOffDate) {this.businessOffDate = businessOffDate;}
}
