package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.view.activity.Payment;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {

    Context mCtx;
    List<OrderModel> orderList;
    List<OrderModel> selected = new ArrayList<>();

    public OrderListAdapter(Context mCtx, List<OrderModel> orderList) {
        this.mCtx = mCtx;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_dashborad_item_view, parent, false);

        return new OrderListViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        OrderModel order = orderList.get(position);
        holder.orderId.setText(String.valueOf(order.order_id));
        holder.orderType.setText(order.order_type.toUpperCase());
        if(order.serverId != 0)
            holder.serverId.setText(String.valueOf(order.serverId));
        if(order.customer!=null){
            if(order.customer.profile != null){
                holder.customerName.setText(order.customer.profile.first_name+"");
                holder.customerPhone.setText(order.customer.profile.phone);
            }

        }
        try {
            if(DateTimeHelper.getDBTime().equals(DateTimeHelper.formatDBDate(order.orderDateTime)))
                holder.orderTime.setText("Today " + order.orderDateTime.substring(11) );
            else
                holder.orderTime.setText(order.orderDateTime);
        } catch (Exception e) {
            if (order.orderDateTime != null)
                holder.orderTime.setText(order.orderDateTime);
        }

        if(order.deliveryType == null){
            holder.deliveryTime.setText("ASAP");
        }else if(order.deliveryType.equals("ASAP")){
            holder.deliveryTime.setText("ASAP");
        }else if(!order.currentDeliveryTime.equals("")){
            holder.deliveryTime.setText(order.currentDeliveryTime);
        }

        if(order.order_channel==null)
            order.order_channel = "EPOS";
        if(order.paymentStatus == null){
            order.paymentStatus = "UNPAID";
        }

        if(order.totalPaid > 0){
            holder.editButton.setVisibility(View.INVISIBLE);
        }else if (!order.order_channel.equals("EPOS")) {
            if(order.order_channel.equals("WAITERAPP")){
                holder.editButton.setVisibility(View.VISIBLE);
            }else
                holder.editButton.setVisibility(View.INVISIBLE);
        }else{
            if(!order.order_status.equals("CLOSED")){
                holder.editButton.setVisibility(View.VISIBLE);
            }
            else{
                holder.editButton.setVisibility(View.INVISIBLE);
            }
        }

        if (order.paymentStatus != null){
            if(order.paymentStatus.equals("PAID")){
                holder.paymentStatus.setImageDrawable(ContextCompat.getDrawable(mCtx, R.drawable.payment_complete));
                holder.editButton.setVisibility(View.INVISIBLE);
            }else{
                holder.paymentStatus.setImageDrawable(ContextCompat.getDrawable(mCtx, R.drawable.payment_not_complete));
            }
        }

        holder.paymentStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                if(order.paymentStatus.equals("PAID")){
                    Toast.makeText(mCtx,"Your Order Already Paid",Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent myIntent = new Intent(mCtx, Payment.class);
                myIntent.putExtra("orderData", order.db_id);
                mCtx.startActivity(myIntent);
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                ((Dashboard)mCtx).openOrderPage(order);
            }
        });


        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                if(order.order_channel !=null) {
                    if (order.order_channel.toUpperCase().equals("ONLINE")) {
                        ((Dashboard) mCtx).openOnlineOrderDetailsPage(order.db_id);
                    } else {
                        ((Dashboard) mCtx).openOrderDetailsPage(order.db_id);
                    }
                }
                else
                    ((Dashboard)mCtx).openOrderDetailsPage(order.db_id);

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                boolean isSelected = false;


                for (OrderModel orderModel: selected) {
                    if (orderModel.db_id == order.db_id) {
                        isSelected = true;
                        break;
                    }
                }
                if(isSelected)
                    selected.removeIf((orderModel -> orderModel.db_id == order.db_id));
                else
                    selected.add(order);

                notifyDataSetChanged();


                ((Dashboard)mCtx).onOrderSelect(selected);




            }
        });
        boolean isSelected = false;
        for (OrderModel orderModel: selected) {
            if (orderModel.db_id == order.db_id) {
                isSelected = true;
                break;
            }
        }

        if(isSelected){
           // holder.itemView.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.Beige));
            holder.parent_card.setCardBackgroundColor(ContextCompat.getColor(mCtx, R.color.LightCoral));
        }else if(order.order_channel != null){
            if (!order.order_channel.equals("EPOS")){
                if (order.order_channel.equals("WAITERAPP")) {
                    if(order.order_status.equals("NEW")){
                        holder.parent_card.setBackgroundColor(Color.parseColor("#e5f595"));
                    }else
                        holder.parent_card.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.white));
                } else {
                    if(order.order_status.equals("REVIEW")){
                        holder.parent_card.setBackgroundColor(Color.parseColor("#e5f595"));
                    }else
                        holder.parent_card.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.white));
                }
            }
        } else{
            holder.parent_card.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.white));
        }

        if (order.order_status.equals("REFUNDED")){
            holder.editButton.setVisibility(View.INVISIBLE);
        }

//        if (order.bookingId != 0 && !order.order_status.equals("CANCELLED") && !order.order_status.equals("CLOSED") && !order.order_status.equals("REFUNDED")) {
//            OrderRepository orderManager = new OrderRepository(mCtx);
//            List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(order.bookingId, order.order_status);
//            double total = 0.0;
//            double cashPaid = 0.0;
//            double cardPaid = 0.0;
//            for (OrderModel model: ordersByBookingId) {
//                total += model.total;
//                cashPaid += model.cashPaid;
//                cardPaid += model.cardPaid;
//            }
//            holder.amount.setText("£ "+String.format("%.2f", total));
//            holder.cashPaid.setText("£ "+String.format("%.2f", cashPaid));
//            holder.cardPaid.setText("£ "+String.format("%.2f", cardPaid));
//        } else {
//            holder.amount.setText("£ "+String.format("%.2f",order.total));
//            holder.cashPaid.setText("£ "+String.format("%.2f",order.cashPaid));
//            holder.cardPaid.setText("£ "+String.format("%.2f",order.cardPaid));
//        }
        holder.amount.setText("£ "+String.format("%.2f",order.total));
        holder.cashPaid.setText("£ "+String.format("%.2f",order.cashPaid));
        holder.cardPaid.setText("£ "+String.format("%.2f",order.cardPaid));

        holder.status.setText(order.order_status);
        holder.terminal.setText(order.order_channel);

        holder.tableBookId.setText(String.valueOf(order.tableBookingModel==null?"N/A":order.tableBookingModel.table_no));

        if(SharedPrefManager.getWaiterApp(mCtx)){
            holder.tableBookId.setVisibility(View.VISIBLE);
        }else{
            holder.tableBookId.setVisibility(View.GONE);
        }

        holder.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                if(order.order_channel !=null) {
                    if (order.order_channel.toUpperCase().equals("ONLINE")) {
                        ((Dashboard) mCtx).openOnlineOrderDetailsPage(order.db_id);
                    } else {
                        ((Dashboard) mCtx).openOrderDetailsPage(order.db_id);
                    }
                }
                else
                    ((Dashboard)mCtx).openOrderDetailsPage(order.db_id);
            }
        });
        holder.statusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_STOP);
                mCtx.sendBroadcast(intent);
                if(order.order_channel !=null) {
                    if (order.order_channel.toUpperCase().equals("ONLINE")) {
                        ((Dashboard) mCtx).openOnlineOrderDetailsPage(order.db_id);
                    } else {
                        ((Dashboard) mCtx).openOrderDetailsPage(order.db_id);
                    }
                }
                else
                    ((Dashboard)mCtx).openOrderDetailsPage(order.db_id);
            }
        });


        /*** adjustment with order status and icon 'a'***/
        if (order.order_status.equals("REVIEW"))
            holder.statusImage.setImageResource(R.drawable.ic_review);
        if (order.order_status.equals("PROCESSING"))
            holder.statusImage.setImageResource(R.drawable.ic_processing);
        if (order.order_status.equals("READY"))
            holder.statusImage.setImageResource(R.drawable.ic_ready);
        if (order.order_status.equals("SENT"))
            holder.statusImage.setImageResource(R.drawable.ic_send);
        if (order.order_status.equals("DELIVERING"))
            holder.statusImage.setImageResource(R.drawable.ic_delivering);
        if (order.order_status.equals("DELIVERED"))
            holder.statusImage.setImageResource(R.drawable.ic_delivered);
        if (order.order_status.equals("CLOSED"))
            holder.statusImage.setImageResource(R.drawable.ic_closed);
        if (order.order_status.equals("CANCELLED"))
            holder.statusImage.setImageResource(R.drawable.ic_cancelled);
        if (order.order_status.equals("NEW"))
            holder.statusImage.setImageResource(R.drawable.ic_baseline_star_outline_24);



        if (order.order_type != null && order.order_type.equals("DELIVERY")) {
            if (order.customer == null) {
                holder.paymentStatus.setVisibility(View.INVISIBLE);
                holder.detailsButton.setVisibility(View.INVISIBLE);
                holder.editButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView orderId,orderType,orderTime,deliveryTime,status,amount,serverId,cardPaid,cashPaid,tableBookId;
        ImageView statusImage,syncStatus,profileImage;
        ImageButton detailsButton,editButton,paymentStatus;
        TextView customerName,customerPhone,terminal;

        CardView parent_card;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            orderId = itemView.findViewById(R.id.orderListSL);
            orderType = itemView.findViewById(R.id.orderListOrderType);
            orderTime = itemView.findViewById(R.id.orderListItemOrderTime);
            deliveryTime = itemView.findViewById(R.id.orderListItemDeliveryTime);
            status = itemView.findViewById(R.id.orderListItemStatus);
            statusImage = itemView.findViewById(R.id.orderListItemStatusImage);
            paymentStatus =(ImageButton)itemView.findViewById(R.id.orderListItemPaymentButton);
            amount = itemView.findViewById(R.id.orderListItemAmount);
            detailsButton = (ImageButton)itemView.findViewById(R.id.orderListItemDetailsButton);
            syncStatus = itemView.findViewById(R.id.orderListItemSyncIcon);
            customerName = itemView.findViewById(R.id.orderListItemUserName);
            customerPhone = itemView.findViewById(R.id.orderListItemUserPhone);
            terminal = itemView.findViewById(R.id.orderListItemTerminal);
            serverId = itemView.findViewById(R.id.serverId);
            tableBookId = itemView.findViewById(R.id.tableNo);
            editButton = itemView.findViewById(R.id.orderListItemEditButton);
            parent_card = itemView.findViewById(R.id.parent_card);
            cashPaid = itemView.findViewById(R.id.orderListItemCashPaid);
            cardPaid = itemView.findViewById(R.id.orderListItemCardPaid);
        }
    }
}