package com.novo.tech.redmangopos.socket;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;

import com.novo.tech.redmangopos.callerId.app.TheApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import io.socket.emitter.Emitter;

public class SocketListeners {
    private Context context;

    public SocketListeners(Context context) {
        this.context = context;
    }

    public final Emitter.Listener onOrderCreate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            CreateOrderWorker worker = new CreateOrderWorker(context, jsonObject);
            worker.execute();

        }
    };

    public final Emitter.Listener onOrderUpdate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("jsonObject", jsonObject.toString());
            try {
                if (jsonObject.get(SocketHandler.ORDER).equals("null")) {
                    OrderListWorker worker = new OrderListWorker(context);
                    worker.execute();
                } else {
                    UpdateOrderWorker worker = new UpdateOrderWorker(context, jsonObject);
                    worker.execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    public final Emitter.Listener onOrderList = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("orderList==>", jsonObject.toString());
            OrderListWorker worker = new OrderListWorker(context);
            worker.execute();
        }
    };

    public final Emitter.Listener onOrderSuccess = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
        }
    };

    public final Emitter.Listener onListenGetItems = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("GET_Items==>", jsonObject.toString());
            new MergedItemsWorker(context, jsonObject).execute();
        }
    };


    public final Emitter.Listener onListenOrderPrint = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            new PrintOrdersWorker(context, jsonObject).execute();
        }
    };


    public final Emitter.Listener onListenTableBooked = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            new TableBookingWorker(context, jsonObject).execute();
        }
    };

    public final Emitter.Listener onListenSetOrderPref = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            new PreferenceWorker(context, jsonObject).execute();
        }
    };
}
