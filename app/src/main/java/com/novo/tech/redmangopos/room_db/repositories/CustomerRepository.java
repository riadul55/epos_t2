package com.novo.tech.redmangopos.room_db.repositories;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.dao.CustomerDao;
import com.novo.tech.redmangopos.room_db.entities.Address;
import com.novo.tech.redmangopos.room_db.entities.Customer;
import com.novo.tech.redmangopos.room_db.entities.Order;

import java.util.ArrayList;
import java.util.List;

public class CustomerRepository {
    private Context context;
    private CustomerDao customerDao;

    public CustomerRepository(Context context) {
        this.context = context;
        AppDatabase database = AppDatabase.getInstance(context);
        customerDao = database.CustomerDao();
    }

    public CustomerModel getCustomerData(int id) {
        Customer customer = customerDao.getById(id);
        return getCustomerDataFromEntity(customer);
    }

    public CustomerModel getCustomerData(String customer_id) {
        Customer customer = customerDao.getByConsumerId(customer_id);
        return getCustomerDataFromEntity(customer);
    }

    public List<CustomerModel> getAllCustomerData() {
        List<CustomerModel> customers = new ArrayList<>();
        List<Customer> allCustomer = customerDao.getAllCustomer();
        for (Customer customer: allCustomer) {
            customers.add(getCustomerDataFromEntity(customer));
        }
        return customers;
    }

    public List<CustomerModel> getCustomersBySearch(String query, int offset) {
        List<CustomerModel> customers = new ArrayList<>();
        List<Customer> customersBySearch = customerDao.getCustomersBySearch("%" + query + "%", offset);
        for (Customer customer: customersBySearch) {
            customers.add(getCustomerDataFromEntity(customer));
        }
        return customers;
    }

    public int addNewCustomer(CustomerModel model,boolean cloudSync) {
        int id = 0;
        if(model.profile==null){
            return -1;
        }
        Customer customer = new Customer();
        customer.firstName = model.profile.first_name;
        customer.lastName = model.profile.last_name;
        customer.phone = model.profile.phone;
        customer.email = model.profile.email;
        customer.userName = model.username;
        customer.consumer_uuid = model.consumer_uuid;
        customer.cloudSubmitted = cloudSync;
        customer.address = GsonParser.getGsonParser().toJson(model.addresses);
        id = (int) customerDao.insertCustomer(customer);
        return id;
    }

    public void updateCustomer(CustomerModel model,boolean submitted) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                Customer customer = new Customer();
                customer.id = model.dbId;
                customer.firstName = model.profile.first_name;
                customer.lastName = model.profile.last_name;
                customer.phone = model.profile.phone;
                customer.email = model.profile.email;
                customer.userName = model.username;
                customer.consumer_uuid = model.consumer_uuid;
                customer.cloudSubmitted = submitted;
                customer.address = GsonParser.getGsonParser().toJson(model.addresses);
                customerDao.updateCustomer(customer);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public CustomerModel getCustomerDataByPhone(String phone) {
        Customer customerByPhone = customerDao.getCustomerByPhone(phone);
        return getCustomerDataFromEntity(customerByPhone);
    }

    public int customerExist (String phone) {
        return customerDao.existCustomerByPhone(phone);
    }

    public int customerExistWithMail(String email) {
        return customerDao.existCustomerByEmail(email);
    }

    public int addNewAddress(CustomerAddress model,boolean isPrimary) {
        List<Address> allAddress = customerDao.getAllCustomerAddress(String.valueOf(model.customerDbId));
        if(allAddress.size()==0){
            isPrimary = true;
        }
        int id = 0;
        Address address = new Address();
        address.customerId = model.customerDbId;
        address.uuid = model.address_uuid;
        address.type = model.type;
        address.building = model.properties.building;
        address.streetName = model.properties.street_number + " " + model.properties.street_name;
        address.city = model.properties.city;
        address.state = model.properties.state;
        address.zip = model.properties.zip;
        address.isPrimary = isPrimary;
        id = (int) customerDao.insertAddress(address);
        if (isPrimary) {
            customerDao.updateWithoutId(model.customerDbId, model.dbId, 0);
        }
        return id;
    }

    public CustomerAddress getPrimaryCustomerAddress(String id){
        Address address = customerDao.getPrimaryCustomerAddress(id);
        return getCustomerAddressFromEntity(address);
    }

    public void updateAddress(CustomerAddress model) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                Address address = customerDao.getPrimaryCustomerAddress(String.valueOf(model.customerDbId));
                if(address == null && !model.primary){
                    model.primary = true;
                }

                Address data = new Address();
                data.id = model.dbId;
                data.uuid = model.address_uuid;
                data.type = model.type;
                data.building = model.properties.building;
                data.streetName = model.properties.street_number + " " + model.properties.street_name;
                data.city = model.properties.city;
                data.state = model.properties.state;
                data.zip = model.properties.zip;
                data.isPrimary = model.primary;
                customerDao.updateAddress(data);

                if (model.primary) {
                    customerDao.updateWithoutId(model.customerDbId, model.dbId, 0);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public List<CustomerAddress> getAllCustomerAddress(String customerId) {
        List<CustomerAddress> customerAddressList = new ArrayList<>();
        List<Address> allCustomerAddress = customerDao.getAllCustomerAddress(customerId);
        for (Address address: allCustomerAddress) {
            customerAddressList.add(getCustomerAddressFromEntity(address));
        }
        return customerAddressList;
    }

    private CustomerModel getCustomerDataFromEntity(Customer customer) {
        int id = customer.id;
        String  consumer_uuid = customer.consumer_uuid;
        String  userName = customer.userName;
        String  firstName = customer.firstName;
        String  lastName = customer.lastName;
        String  email = customer.email;
        String  phone = customer.phone;
        boolean cloudSubmitted = customer.cloudSubmitted;
        CustomerProfile profile = new CustomerProfile(phone,lastName,firstName,email);
        List<CustomerAddress> addresses = new ArrayList<>();
        List<Address> allCustomerAddress = customerDao.getAllCustomerAddress(String.valueOf(id));
        for (Address address: allCustomerAddress) {
            addresses.add(getCustomerAddressFromEntity(address));
        }
        return new  CustomerModel(id,consumer_uuid,email,userName,profile,addresses,cloudSubmitted);
    }

    private CustomerAddress getCustomerAddressFromEntity(Address address) {
        if (address == null) {
            return null;
        }
        CustomerAddressProperties addressProperties = new CustomerAddressProperties(
                address.zip != null ? address.zip : "",
                "United Kingdom",
                address.city != null ? address.city : "",
                "",
                address.state != null ? address.state : "",
                address.building != null ? address.building : "",
                address.streetName != null ? address.streetName : ""
        );
        return new CustomerAddress(
                address.id,
                address.customerId,
                address.uuid,
                address.type,
                address.isPrimary,addressProperties);
    }


    private static class BackgroundTask extends AsyncTask<Order, Void, Void> {
        private BackgroundTask.OnListener listener;
        private BackgroundTask(BackgroundTask.OnListener listener) {
            this.listener = listener;
        }
        @Override
        protected Void doInBackground(Order... model) {
            listener.doInBackground();
            return null;
        }

        interface OnListener {
            void doInBackground();
        }
    }
}
