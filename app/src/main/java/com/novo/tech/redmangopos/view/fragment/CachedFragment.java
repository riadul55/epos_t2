package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CacheListAdapter;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;

public class CachedFragment extends Fragment {
    RecyclerView cacheList;
    CacheListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cached, container, false);
        cacheList = view.findViewById(R.id.cacheList);

        List<UpdateData> updatedData = SharedPrefManager.getUpdatedData(getContext());
        adapter = new CacheListAdapter(updatedData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        cacheList.setLayoutManager(linearLayoutManager);
        cacheList.setAdapter(adapter);
        return view;
    }
}