package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.addressCreator;
import static com.novo.tech.redmangopos.util.AppConstant.adminSession;
import static com.novo.tech.redmangopos.util.AppConstant.business;

import android.content.Context;
import android.util.Log;

import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class CustomerApi {
    Context context;
    CustomerModel customerModel;
    public CustomerApi(Context context,CustomerModel customerModel){
        this.context = context;
        this.customerModel = customerModel;
    }

    public static  List<CustomerModel> getCustomerList(Context context){
        OkHttpClient client = new OkHttpClient();
        List<CustomerModel> modelList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url(BASE_URL+"admin/consumer/list")
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();


        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    String consumerId = jsonArray.optJSONObject(i).optString("consumer_uuid","");
                    String phoneNum = jsonArray.optJSONObject(i).optString("phone_no","");
                    if(!consumerId.isEmpty() && !phoneNum.isEmpty()){
                        CustomerModel model = getCustomerData(consumerId,context);
                        if(model.profile.phone != null)
                            if(!model.profile.phone.equals("null") && !model.profile.phone.isEmpty())
                                modelList.add(model);
                    }
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return modelList;
    }


    public static CustomerModel getCustomerData(String consumerId,Context context){
        OkHttpClient client = new OkHttpClient();
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"consumer/"+consumerId)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();
        Response response = null;
        Log.d("mmmmmm", "getCustomerData: "+consumerId);
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONObject jsonObject = new JSONObject(jsonData);
                model = CustomerModel.fromJSON(jsonObject);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }

    public static  List<CustomerModel> pulCustomerList(Context context){
        OkHttpClient client = new OkHttpClient();
        List<CustomerModel> modelList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url("https://cust.tech-novo.uk/api/consumer?business="+business)
                .addHeader("Content-Type","application/json")
                .addHeader("Authorization", "Bearer 12|ptw64uDtUTyZ7gawKX6FTgVcYzLp6VPMsdJwNw6g")
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.e("Response Import", response.body().toString());
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    CustomerModel customerModel = CustomerModel.fromBackupJSON(object);
                    modelList.add(customerModel);
                }
            }
        }catch (Exception e){
            Log.e("Error=>", e.getMessage());
            e.printStackTrace();
        }
        return modelList;
    }

    public boolean pushCustomer(){
        JSONObject customerJson = new JSONObject();
        JSONArray addresses = new JSONArray();
//        ArrayList<JSONObject> addresses = new ArrayList<>();

        try{
            customerJson.put("business",business);

            if(customerModel.profile!=null){
                customerJson.put("first_name",customerModel.profile.first_name);
                customerJson.put("last_name",customerModel.profile.last_name);
                customerJson.put("phone",customerModel.profile.phone);
                customerJson.put("email",customerModel.profile.email);
            }else{
                customerJson.put("first_name","");
                customerJson.put("last_name","");
                customerJson.put("phone","");
                customerJson.put("email","");
            }

            if(customerModel.addresses.size() > 0){
                for (int i=0;i< customerModel.addresses.size();i++) {
                    JSONObject addressItem = new JSONObject();
                    CustomerAddress addressModel = customerModel.addresses.get(i);
                    if(addressModel.properties == null){
                        addressModel.properties = new CustomerAddressProperties(
                                "","","","","","",""
                        );
                    }
                    addressItem.put("type", addressModel.primary ? "PRIMARY" : "SECONDARY");
                    addressItem.put("street_name",String.valueOf(addressModel.properties.street_number)+" "+addressModel.properties.street_name);
                    addressItem.put("building",addressModel.properties.building);
                    addressItem.put("city",addressModel.properties.city);
                    addressItem.put("state",addressModel.properties.state);
                    addressItem.put("zip",addressModel.properties.zip);

                    addresses.put(addressItem);
                }
            }
            customerJson.put("addresses", addresses);
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.e("ExportBody", customerJson.toString());

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://cust.tech-novo.uk/api/consumer")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("Authorization", "Bearer 12|ptw64uDtUTyZ7gawKX6FTgVcYzLp6VPMsdJwNw6g")
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "createCustomer: called 2 "+response.code());
            if(response.code()==201){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public CustomerModel createCustomer(){
        JSONObject customerJson = new JSONObject();
        JSONObject profile = new JSONObject();
        Log.d("mmmmm", "createCustomer: called 1");
        try{
            if(customerModel.profile!=null){
                profile.put("first_name",customerModel.profile.first_name);
                profile.put("last_name",customerModel.profile.last_name);
                profile.put("phone",customerModel.profile.phone);
                profile.put("email",customerModel.profile.email);
            }

            customerJson.put("username",customerModel.username);
            customerJson.put("password","test1234");
            customerJson.put("language","en_GB");
            customerJson.put("email",customerModel.email);
            customerJson.put("profile",profile);

        }catch (Exception e){
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(BASE_URL+"consumer/register")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "createCustomer: called 2 "+response.code());
            if(response.code()==201){
                Log.d("mmmmm", "createCustomer: STEP 1");
                String customerId = loginCustomer();
                if(customerId!=null){
                    Log.d("mmmmm", "createCustomer: STEP 2");
                    customerModel.consumer_uuid = customerId;
                    if(customerModel.addresses.size()!=0){
                        String addressUuId = createAddress(customerModel.addresses.get(0));
                        if(addressUuId != null){
                            customerModel.addresses.get(0).address_uuid = addressUuId;
                            CustomerRepository customerManager = new CustomerRepository(context);
                            customerManager.updateCustomer(customerModel,true);
                            Log.d("mmmmm", "createCustomer: COMPLETED");
                        }else{
                            CustomerRepository customerManager = new CustomerRepository(context);
                            customerManager.updateCustomer(customerModel,false);
                        }
                    }
                }

            }else if(response.code()==409){
                String customerId = loginCustomer();
                CustomerRepository customerManager = new CustomerRepository(context);
                customerManager.updateCustomer(customerModel,true);
                if(customerId!=null){
                    Log.d("mmmmm", "createCustomer: STEP 2");
                    customerModel.consumer_uuid = customerId;
                    customerManager.updateCustomer(customerModel,true);
                }
                Log.d("mmmmm", "createCustomer: "+customerJson.toString());
            }else{
                Log.d("mmmmm", "createCustomer: "+customerJson.toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return customerModel;
    }
    public CustomerModel updateCustomer(){
        JSONObject customerJson = new JSONObject();
        JSONObject profile = new JSONObject();
        Log.d("mmmmm", "Update Customer: called 1");
        try{
            if(customerModel.profile!=null){
                profile.put("first_name",customerModel.profile.first_name);
                profile.put("last_name",customerModel.profile.last_name);
                profile.put("phone",customerModel.profile.phone);
                profile.put("email",customerModel.profile.email);
            }

            customerJson.put("username",customerModel.username);
            customerJson.put("password","test1234");
            customerJson.put("language","en_GB");
            customerJson.put("email",customerModel.email);
            customerJson.put("profile",profile);

        }catch (Exception e){
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        OkHttpClient client = new OkHttpClient();
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"consumer/"+customerModel.consumer_uuid)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "update Customer: called 2 "+response.code());
            if(response.code()==202){
                    Log.d("mmmmm", "update Customer: STEP 2");
                if(customerModel.addresses.size()!=0){
                    if(customerModel.addresses.get(0).address_uuid==null){
                        String _address = createAddress(customerModel.addresses.get(0));
                        if(_address!= null){
                            customerModel.addresses.get(0).address_uuid = _address;
                            CustomerRepository customerManager = new CustomerRepository(context);
                            customerManager.updateCustomer(customerModel,true);
                        }
                    }else{
                        boolean success = updateAddress(customerModel.addresses.get(0));
                        CustomerRepository customerManager = new CustomerRepository(context);
                        customerManager.updateCustomer(customerModel,success);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }
    public Boolean changeCustomer(int orderId,String consumerId){
        boolean updated = false;
        JSONObject customerJson = new JSONObject();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        OkHttpClient client = new OkHttpClient();
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+orderId+"/consumer_uuid/"+consumerId)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "update Customer: called 2 "+BASE_URL+"order/"+orderId+"/consumer_uuid/"+consumerId);
            Log.d("mmmmm", "update Customer: called 2 "+response.code());
            if(response.code()==202){
                updated = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return updated;
    }
    public String createAddress(CustomerAddress address){
        JSONObject addressJSON = new JSONObject();
        JSONObject properties = new JSONObject();
        try{
            if(address.properties!=null){
                properties = CustomerAddressProperties.toJSON(address.properties);
            }
            addressJSON.put("name","DELIVERY");
            addressJSON.put("type","INVOICE");
            addressJSON.put("creator_type","CONSUMER");
            addressJSON.put("creator_uuid",addressCreator);
            addressJSON.putOpt("properties",properties);
        }catch (Exception e){
            e.printStackTrace();
        }
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, addressJSON.toString());
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"address")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();
        Response response = null;
        String address_uuid=null;
        try{
            Log.d("mmmmmm", "createAddress: "+BASE_URL+"address");
            response = client.newCall(request).execute();
            Log.d("mmmmm", "createCustomer: called 3 "+response.code()+"  "+customerModel.email+" "+addressJSON.toString());
            if(response.code()==201){
                Log.d("mmmmm", "createCustomer Address: STEP 3");
                boolean approved = approveAddress(response.header("Location"));
                if(approved){
                    address_uuid = response.header("Location");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return address_uuid;
    }
    public boolean updateAddress(CustomerAddress address){
        JSONObject addressJSON = new JSONObject();
        JSONObject properties = new JSONObject();
        try{
            if(address.properties!=null){
                properties = CustomerAddressProperties.toJSON(address.properties);
            }

            addressJSON.put("name","DELIVERY");
            addressJSON.put("type","INVOICE");
            addressJSON.put("creator_type","CONSUMER");
            addressJSON.put("creator_uuid",addressCreator);
            addressJSON.putOpt("properties",properties);

        }catch (Exception e){
            e.printStackTrace();
        }
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, addressJSON.toString());
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"address/"+address.address_uuid)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        boolean success=false;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "createCustomer: called 3 "+response.code());
            if(response.code()==202){
                success = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }
    public boolean approveAddress(String addressUuid){
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, (new JSONObject()).toString());
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"consumer/"+customerModel.consumer_uuid+"/address/"+addressUuid)
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        boolean success=false;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmm", "approveAddress: "+BASE_URL+"consumer/"+customerModel.consumer_uuid+"/address/"+addressUuid);
            Log.d("mmmmm", "createCustomer: called 4 "+response.code());
            if(response.code()==202){
                success = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }

    public String loginCustomer(){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("username",customerModel.username);
            jsonObject.put("password","test1234");

        }catch (Exception e){
            e.printStackTrace();
        }
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        CustomerModel model  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"consumer/login")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();

        Response response = null;
        String consumer_uuid=null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmmm", "createCustomer: called 5 "+response.code());

            if(response.code()==200){
                String jsonData = response.body().string();
                JSONObject body = new JSONObject(jsonData);
                consumer_uuid = body.optString("consumer_uuid",null);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return consumer_uuid;
    }
    public static  ArrayList<String> getZipSuggestion(Context context,String str){
        Log.d("mmmm", "getZipSuggestion: "+str);
        OkHttpClient client = new OkHttpClient();
        ArrayList<String> zipList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/connector/postal_code/"+str)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmm", "getZipSuggestion: "+response.code());
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    zipList.add(jsonArray.optString(i));
                }
            }
        }catch (Exception e){
            Log.e("mmmmm", "getZipSuggestion: "+e );
            e.printStackTrace();
        }
        return zipList;
    }



}
