package com.novo.tech.redmangopos.retrofit2;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.content.Context;

import com.google.gson.Gson;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    Context context;
    public static RetrofitClient instance;
    public RetrofitClient(Context context){
        if(instance == null){
            instance = this;
        }
        this.context = context;
    }
    public Retrofit getRetrofit(Gson gson){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(160, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("ProviderSession",providerSession)
                        .build();
                return chain.proceed(request);
            }
        });
        return new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
    public Retrofit getRetrofitLogin(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .build();
                return chain.proceed(request);
            }
        });
        return new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }
}
