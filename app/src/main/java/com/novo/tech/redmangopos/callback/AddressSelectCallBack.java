package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.AddressHelperModel;

public interface AddressSelectCallBack {
    void onAddressSelect(AddressHelperModel model);
}
