package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.OrderModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SelectionItemAdapter extends RecyclerView.Adapter<SelectionItemAdapter.ViewHolder> {
    private Context context;
    private List<CartItem> cartItems;
    private List<CartItem> selectedItems;
    private OnSectionListener listener;

    public SelectionItemAdapter(Context context, List<CartItem> cartItems, OnSectionListener listener) {
        this.context = context;
        this.cartItems = cartItems;
        this.listener = listener;
        selectedItems = new ArrayList<CartItem>();
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public List<CartItem> getSelectedItems() {
        return selectedItems;
    }

    @NonNull
    @NotNull
    @Override
    public SelectionItemAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.model_selection_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull SelectionItemAdapter.ViewHolder holder, int position) {
        CartItem cartItem = cartItems.get(position);
        holder.itemName.setText(cartItem.shortName);
        holder.quantity.setText("x"+cartItem.quantity);


        double price = 0;

        if(!cartItem.offered){
            price=(cartItem.subTotal*cartItem.quantity);

            if (cartItem.extra != null) {
                for (CartItem extraItem: cartItem.extra) {
                    price+=extraItem.price;
                }
            }

        }
        else {
            price = cartItem.total;
        }
        holder.price.setText("£ "+String.format(Locale.getDefault(),"%.2f", price));


        String componentName = "";
        if(cartItem.componentSections.size()>0){

            for (ComponentSection section : cartItem.componentSections){
                String _comName = section.selectedItem.shortName;
                if(section.selectedItem.subComponentSelected != null){
                    if (!section.selectedItem.subComponentSelected.shortName.toUpperCase().equals("NONE"))
                        _comName+=" -> "+section.selectedItem.subComponentSelected.shortName;
                }
                componentName+=(section.sectionValue+" : "+_comName+"\n");

            }
        }
        if(cartItem.extra != null && cartItem.extra.size() > 0){
            StringBuilder str = new StringBuilder("Extra :");
            for (CartItem extraItem :cartItem.extra) {
                str.append("  *").append(extraItem.shortName);
            }
            componentName+=str.toString()+"\n";
        }
        if (componentName.isEmpty()) {
            holder.extraItem.setVisibility(View.GONE);
        } else {
            holder.extraItem.setVisibility(View.VISIBLE);
            holder.extraItem.setText(componentName);
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    selectedItems.add(cartItem);
                } else {
                    selectedItems.remove(cartItem);
                }
                listener.onItemSelect(selectedItems);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.checkBox.isChecked()) {
                    holder.checkBox.setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatCheckBox checkBox;
        TextView itemName;
        TextView extraItem;
        TextView quantity;
        TextView price;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.check_box);
            itemName = itemView.findViewById(R.id.item_name);
            extraItem = itemView.findViewById(R.id.extra_items);

            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
        }
    }

    public interface OnSectionListener {
        void onItemSelect(List<CartItem> items);
    }
}
