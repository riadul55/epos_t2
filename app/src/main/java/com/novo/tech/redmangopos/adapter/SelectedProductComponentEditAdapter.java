package com.novo.tech.redmangopos.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.view.activity.ComponentEditActivity;
import com.novo.tech.redmangopos.view.activity.ProductDetailsEditActivity;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.CategoryDetailsCallBack;
import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.model.ComponentsModel;
import com.novo.tech.redmangopos.sync.ComponentDeleteHelper;
import com.novo.tech.redmangopos.sync.GetSelectComponentProductHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SelectedProductComponentEditAdapter extends RecyclerView.Adapter<SelectedProductComponentEditAdapter.ViewHolder>{
    ArrayList<ComponentsModel> componentsModelArrayList = new ArrayList<>();
    ComponentsModel componentsModel;
    Context context;
    ArrayList<CategoryDetailsModel> componentArrayListH = new ArrayList<CategoryDetailsModel>();
    String imgUrl="";


    EditText component_name;
    public SelectedProductComponentEditAdapter(ArrayList<ComponentsModel> componentsModelArrayList, Context context) {
        this.componentsModelArrayList = componentsModelArrayList;
        this.context = context;

    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.component_item_edit, parent, false);;
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        componentsModel = componentsModelArrayList.get(position);
        if (componentsModel != null){
            holder.componentName.setText(componentsModel.getShort_name());



           /* holder.component_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.componentName.setFocusable(true);
                    holder.componentName.setFocusableInTouchMode(true);
                    holder.componentName.setClickable(true);


                }
            });*/

            holder.component_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, ComponentEditActivity.class);



                    String productuid = ProductDetailsEditActivity.product_uuid;
                    Log.d("product-----------uid : ",productuid);
                    String componentuid = componentsModelArrayList.get(position).getProduct_uuid();
                    Log.d("component-------------------uid : ",componentuid);
                    //OpenDialog(componentsModelArrayList.get(position));

                    new GetSelectComponentProductHelper(componentArrayListH, new CategoryDetailsCallBack() {
                        @Override
                        public void onCategoryDetailsResponse(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList) {
                            Log.d("uuuuuuuuuuuuuuuuuuuu",String.valueOf(categoryDetailsModelArrayList.size()));
                            if (categoryDetailsModelArrayList.size()>0)
                                ComponentEditActivity.componentData(categoryDetailsModelArrayList.get(0));
                            context.startActivity(intent);
                        }
                    },componentuid).execute();


                }
            });
        }
    }

    public void setComponent(CategoryDetailsModel componentData) {

        if (componentData != null){

            /*if (componentData.getPrice()!=null)
                if (componentData.getPrice().getPrice()>0)
                    ComponentEditActivity.et_price.setText(String.valueOf(componentData.getPrice().getPrice()));
            if (componentData.getProperties() != null)
                if (componentData.getProperties().getSort_order() != null)
                    ComponentEditActivity.et_short.setText("alamin");
                    //ComponentEditActivity.et_short.setText(String.valueOf(componentData.getProperties().getSort_order()));

            ComponentEditActivity.et_title.setText(String.valueOf(componentData.getShort_name()));
            ComponentEditActivity.et_description.setText(String.valueOf(componentData.getDescription()));
            if (componentData.isDiscountable())
                ComponentEditActivity.cb_discountable.setChecked(true);
            imgUrl = "https://labapi.yuma-technology.co.uk:8443/delivery/product/" + componentData.files.get(0).itemId + "/file/" + componentData.files.get(0).fileUid;
            Glide.with(context).load(imgUrl).into( ComponentEditActivity.iv_product);*/



        }

    }

    @Override
    public int getItemCount() {
        return componentsModelArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView componentName;
        ImageView component_edit;
        ImageView component_delete;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            componentName = itemView.findViewById(R.id.componentName);
            component_edit = itemView.findViewById(R.id.component_edit);
            component_delete = itemView.findViewById(R.id.component_delete);
        }
    }

    private void OpenDialog(ComponentsModel componentModel) {
        //final Dialog dialog = new Dialog((FragmentActivity) getContext());
        Dialog dialog = new Dialog((FragmentActivity) context);
        dialog.setContentView(R.layout.component_edit_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        component_name = dialog.findViewById(R.id.component_name);
        component_name.setText(String.valueOf(componentsModel.getShort_name()));



        final Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
        final Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        final ImageButton ib_close = (ImageButton) dialog.findViewById(R.id.ib_close);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String componentName = component_name.getText().toString().trim();
                if(componentName.length()>2){
                    new ComponentDeleteHelper(ProductDetailsEditActivity.product_uuid, componentModel.getProduct_uuid(), new PriceUpdateResponseCallBack() {
                        @Override
                        public void onPriceUpdateResponse(String response) {
                            Log.d("Response",response);

                        }
                    }).execute();

                }else {
                    Toast.makeText(context, "Component Can't be Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
