package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "inventory", indices = @Index(value = {"product_uuid"}))
public class Inventory {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "product_uuid")
    @Nullable
    public String productUuId;

    @ColumnInfo(name = "stock")
    public int stock;
}
