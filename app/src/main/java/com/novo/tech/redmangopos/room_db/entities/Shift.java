package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "shift")
public class Shift {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "open_time")
    @Nullable
    public String openTime;

    @ColumnInfo(name = "close_time")
    @Nullable
    public String closeTime;

    @ColumnInfo(name = "cloudSubmitted")
    @Nullable
    public boolean cloudSubmitted;
}
