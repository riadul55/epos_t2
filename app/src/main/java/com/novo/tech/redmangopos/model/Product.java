package com.novo.tech.redmangopos.model;


import android.util.Log;

import com.novo.tech.redmangopos.room_db.entities.ProductTb;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Product implements Serializable {
    public String productType,productUUid,shortName,description,priceUUID;
    public double price,offerPrice;
    public boolean discountable;
    public boolean visible;
    public ProductProperties properties;
    public List<ProductFile> files;
    public List<Component> componentList;

    public Product(String productType, String productUUid, String shortName, String description, String priceUUID, double price,double offerPrice,boolean visible,boolean discountable, ProductProperties properties,List<ProductFile> files,List<Component> componentList) {
        this.productType = productType;
        this.productUUid = productUUid;
        this.shortName = shortName;
        this.description = description;
        this.priceUUID = priceUUID;
        this.price = price;
        this.offerPrice = offerPrice;
        this.properties = properties;
        this.files = files;
        this.componentList = componentList;
        this.discountable = discountable;
        this.visible = visible;
    }
    public static Product fromJSON(JSONObject data){
        double _price = 0;
        double offerPrice  = 0;
        String _priceUUID="";
        ProductProperties properties = null;
        List<ProductFile> files = new ArrayList<>();
        List<Component> components  = new ArrayList<Component>();

        if(data.optJSONObject("price")!=null){
            _price = data.optJSONObject("price").optDouble("price",0);
            if(_price==0){
                _price = data.optJSONObject("price").optDouble("extra_price",0);
            }
            _priceUUID=data.optJSONObject("price").optString("price_uuid","");
        }
        if(data.optJSONObject("properties") !=null){
            try{
                properties = ProductProperties.fromJSON(data.optJSONObject("properties"));
                offerPrice = properties.offerPrice;
            }catch(Exception e){
                Log.e("mmmmm", "fromJSON: "+e.getMessage());
            }
        }
        if(data.optJSONArray("files") !=null){
            JSONArray jsonArray = data.optJSONArray("files");
            for(int i =0 ; i < jsonArray.length();i++){
                files.add(ProductFile.fromJSON(jsonArray.optJSONObject(i)));
            }
        }
        if(data.optJSONArray("components") !=null){
            JSONArray jsonArray = data.optJSONArray("components");
            for (int i = 0; i < jsonArray.length(); i++) {
                components.add(Component.fromJSON(jsonArray.optJSONObject(i),null));
            }

        }else if(data.optString("product_type","ITEM").equals("BUNDLE")){
            JSONArray jsonArray = data.optJSONArray("items");
            if(jsonArray==null)
                jsonArray = new JSONArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                components.add(Component.fromJSON(jsonArray.optJSONObject(i),null));
            }
        }
        return new Product(
                data.optString("product_type"),
                data.optString("product_uuid"),
                data.optString("short_name"),
                data.optString("description"),
                _priceUUID,
                _price,
                offerPrice,
                data.optBoolean("visible",false),
                data.optBoolean("discountable",false),
                properties,
                files,
                components
        );
    }

    public static ProductTb toProductTable(Product product) {
        ProductTb productTb = new ProductTb();
        productTb.productType = product.productType;
        productTb.productUUid = product.productUUid;
        productTb.shortName = product.shortName;
        productTb.description = product.description;
        productTb.priceUUID = product.priceUUID;
        productTb.price = product.price;
        productTb.offerPrice = product.offerPrice;
        productTb.discountable = product.discountable;
        productTb.visible = product.visible;
        productTb.properties = product.properties;
        productTb.files = product.files;
        productTb.componentList = product.componentList;
        return productTb;
    }
}

