package com.novo.tech.redmangopos.model;

import androidx.annotation.Nullable;

import java.util.Date;

public class ShiftModel {
    public int id,cloudSubmitted;
    public Date openTime,closeTime;

    public ShiftModel(int id, int cloudSubmitted, Date openTime,@Nullable Date closeTime) {
        this.id = id;
        this.cloudSubmitted = cloudSubmitted;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }
}
