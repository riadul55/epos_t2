package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.SubCartListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.Product;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderComponentSubCartList extends Fragment {
    TextView productName,price,totalPrice;
    Product product;
    String data;
    double totalPriceAmount = 0;
    RecyclerView recyclerView;
    public List<ComponentSection> sectionList;
    SubCartListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_component_sub_cart, container, false);
        data = requireArguments().getString("product");
        sectionList = (List<ComponentSection>) requireArguments().getSerializable("sections");
        sectionList = new ArrayList<>(sectionList);
        product = GsonParser.getGsonParser().fromJson(data, Product.class);
        recyclerView = v.findViewById(R.id.subCartRecyclerView);
        recyclerView.setAlpha(0f);
        recyclerView.setTranslationY(50);
        recyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);
        productName = v.findViewById(R.id.subCartItemName);
        price = v.findViewById(R.id.subCartItemPrice);
        totalPrice = v.findViewById(R.id.subCartTotalAmount);
        productName.setText(product.shortName);
        price.setText("£ "+new DecimalFormat("##.##").format(product.price));
        totalPriceAmount = product.price;
        totalPrice.setText("£ "+new DecimalFormat("##.##").format(totalPriceAmount));
        loadSubCart();
        return v;
    }
    public void loadSubCart(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        adapter = new SubCartListAdapter(getContext(), new ArrayList<>(sectionList));
        recyclerView.setAdapter(adapter);
        showPrice();
    }
    public void onComponentChange(ComponentSection section){
        for (int i = 0;i<sectionList.size();i++){
            if(sectionList.get(i).sectionValue.equals(section.sectionValue)){
                if(section.selectedItem.subComponentSelected != null){
                    section.selectedItem.subTotal = section.selectedItem.mainPrice+section.selectedItem.subComponentSelected.price;
                }
                sectionList.get(i).selectedItem = section.selectedItem;

            }
        }
        adapter.notifyDataSetChanged();
        showPrice();

    }

    void showPrice(){
        totalPriceAmount = product.price;
        for (ComponentSection item : sectionList){
              totalPriceAmount+=item.selectedItem.subTotal;
        }
        totalPrice.setText("£ "+new DecimalFormat("##.##").format(totalPriceAmount));
    }

}