package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CashApi {
    Context context;
    public CashApi(Context context){
        this.context = context;
    }

    void checkCashTransaction(){
        CashRepository manager = new CashRepository(context);
        List<TransactionModel> transactionModelList = manager.getAllCashTransaction();
        Log.d("mmmmm", "checkCashTransaction: "+transactionModelList.size());
        for (TransactionModel model: transactionModelList) {
            if(model.cashSubmitted==0 && model.serverId !=0){
                switch (model.type){
                    case 0:
                        String entryType = "CASH_ORDER";
                        if(model.inCash == 0){
                            entryType = "CARD_ORDER";
                        }
                        submitCashOrder(model.cashId,model.amount,model.serverId,entryType,"");
                        break;
                    case 2:
                        submitCashOrder(model.cashId,model.amount,model.serverId,"DISCOUNT","");
                        break;
                    case 3:
                        submitCashOrder(model.cashId,model.amount,model.serverId,"TIP","");
                        break;
                    case 4:
                        submitCashOrder(model.cashId,model.amount,model.serverId,"REFUND","");
                        break;
                    case 5:
                        submitCashOrder(model.cashId,model.amount,model.serverId,"ADJUSTMENT",model.note);
                        break;
                    default:
                }

            }
        }
    }

/*    void checkGeneralCashTransaction(){
        DBCashManager manager = new DBCashManager(context);
        List<TransactionModel> transactionModelList = manager.getAllGeneralTransaction();
        for (TransactionModel model: transactionModelList) {
            if(model.cashSubmitted==0){
                switch (model.type){
                    case 1:
                        submitCashGeneral(model.cashId,model.amount,"CASH_IN");
                        break;
                    case -1:
                        submitCashGeneral(model.cashId,model.amount*-1,"CASH_OUT");
                    default:

                }

            }
        }
    }*/

    void checkGeneralCashTransaction(){
        CashRepository manager = new CashRepository(context);
        List<TransactionModel> transactionModelList = manager.getAllGeneralTransaction();
        for (TransactionModel model: transactionModelList) {
            if(model.cashSubmitted==0){
                switch (model.type){
                    case 1:
                        submitCashGeneral(model.cashId,model.amount,"CASH_IN");
                        break;
                    case -1:
                        submitCashGeneral(model.cashId,model.amount*-1,"CASH_OUT");
                    default:

                }

            }
        }
    }

    void submitCashOrder(int dbID,double amount,int orderId,String entryType,String comment){
        Log.d("mmmmmm", "submitCashOrder: "+orderId+" => "+entryType+" => "+amount);
        OkHttpClient client = new OkHttpClient();
        String token = providerSession;
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("entry_type", entryType);
            jsonObject.put("amount",amount);
            jsonObject.put("comment",comment);
            jsonObject.put("payment_uuid","");
            jsonObject.put("provider_uuid", providerID);
        }catch (Exception e){
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());

        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+orderId+"/cash_entry")
                .addHeader("Content-Type","application/json")
                .post(requestBody)
                .addHeader("ProviderSession",token==null?"":token)
                .build();
        try{
            Response response = client.newCall(request).execute();
            if(response!=null){
                if(response.code() == 201){
                    CashRepository cashManager = new CashRepository(context);
                    cashManager.updateTransaction(dbID,true);
                }else if(response.code() == 409){
                    CashRepository cashManager = new CashRepository(context);
                    cashManager.updateTransaction(dbID,true);
                }
                else{
                    CashRepository cashManager = new CashRepository(context);
                    cashManager.updateTransaction(dbID,false);
                    Log.d("mmmmm", "submitCashOrder: "+orderId+"  "+response.body().string()+"  => "+jsonObject);
                }

                Log.d("mmmmmm", "submitCashOrder: result code = "+response.code());
            }

        }catch (Exception e){
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }
    void submitCashGeneral(int dbID,double amount,String entryType){
        Log.d("mmmmm", "submitCashGeneral: submitting cah in cahout   "+entryType+" => "+amount);
        OkHttpClient client = new OkHttpClient();
        String token = providerSession;
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("order_id",-1);
            jsonObject.put("entry_type", entryType);
            jsonObject.put("amount",amount);
            jsonObject.put("comment","");
            jsonObject.put("payment_uuid","");
            jsonObject.put("provider_uuid", providerID);

        }catch (Exception e){
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());

        Request request = new Request.Builder()
                .url(BASE_URL+"cash/entry")
                .addHeader("Content-Type","application/json")
                .post(requestBody)
                .addHeader("ProviderSession",token==null?"":token)
                .build();
        try{
            Response response = client.newCall(request).execute();
            int code = response.code();
            Log.d("mmmm", "submitCashGeneral: "+code);
            if(code == 201){
                CashRepository cashManager = new CashRepository(context);
                cashManager.updateTransaction(dbID,true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
