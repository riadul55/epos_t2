package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import com.novo.tech.redmangopos.callback.ZipCallBack;
import com.novo.tech.redmangopos.model.JsonAddress;
import com.novo.tech.redmangopos.storage.AddressDbHelper;

import java.util.ArrayList;
import java.util.List;

public class ZipCodeHelper extends AsyncTask<String, Void, List<JsonAddress>> {
    Context context;
    ZipCallBack callBack;
    String str;
    public ZipCodeHelper(Context context, String str, ZipCallBack callBack){
        this.context = context;
        this.str = str;
        this.callBack = callBack;
    }
    @Override
    protected List<JsonAddress> doInBackground(String... strings) {
        return getZipSuggestion(context,str);
    }

    public static  List<JsonAddress> getZipSuggestion(Context context, String str){
        try {
            AddressDbHelper addressDbHelper = new AddressDbHelper(context);
            return addressDbHelper.getSearchResult(str);
        } catch (Exception e) {
            return new ArrayList<JsonAddress>();
        }
    }

    @Override
    protected void onPostExecute(List<JsonAddress> arrayList) {
        ArrayList<String> zipList = new ArrayList<>();
        for (int i=0;i<arrayList.size();i++) {
            zipList.add(arrayList.get(i).getPostcode());
        }
        callBack.onResponseFromServer(zipList);
        super.onPostExecute(arrayList);
    }
}