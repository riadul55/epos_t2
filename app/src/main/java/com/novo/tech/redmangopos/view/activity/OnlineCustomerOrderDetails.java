package com.novo.tech.redmangopos.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsAction;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsCart;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsConsumerInfo;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.sync.CustomerOrderDetailsService;

public class OnlineCustomerOrderDetails extends AppCompatActivity implements MyCallBack {
    int orderID;
    public OrderModel orderModel;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_order_details);
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        orderID = getIntent().getIntExtra("orderData",0);

        loadDataFromServer();
    }
    void loadDataFromServer(){
        new CustomerOrderDetailsService(getApplicationContext(),String.valueOf(orderID),this).execute();
    }
    void loadFragments(){
        openCartFragment();
        openActionFragment();
        openConsumerFragment();
    }
    void openCartFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCart, OnlineCustomerOrderDetailsCart.class,bundle)
                .commit();
    }
    void openActionFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCenter, OnlineCustomerOrderDetailsAction.class,bundle)
                .commit();
    }
    void openConsumerFragment(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel",orderModel);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsRight, OnlineCustomerOrderDetailsConsumerInfo.class,bundle)
                .commit();
    }

    void backToHome(){
        finish();
    }
    @Override
    public void onBackPressed() {
        backToHome();
    }

    @Override
    public void voidCallBack() {

    }

    @Override
    public void OrderCallBack(OrderModel order) {
        progressDialog.dismiss();
        if(order != null){
            orderModel = order;
            loadFragments();
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    getApplicationContext()
            );
            builder.setMessage("No Data found!");
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }
}