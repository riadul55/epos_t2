package com.novo.tech.redmangopos.receivers;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.novo.tech.redmangopos.escpos.PosServiceBinding;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import net.posprinter.posprinterface.UiExecute;
import net.posprinter.service.PosprinterService;

import java.util.Timer;
import java.util.TimerTask;

public class PrinterBgService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent in = new Intent("PRINTER_STATE_CHANGE");
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (SharedPrefManager.getPrinterIndex(getApplicationContext()) == Conts.PRINTER_XPRINTER) {
                    PosServiceBinding.binder.checkLinkedState(new UiExecute() {
                        @Override
                        public void onsucess() {
                        }

                        @Override
                        public void onfailed() {
                            PosServiceBinding.IS_CONNECTED = false;
                            in.putExtra("state",false);
                            sendBroadcast(in);
                        }
                    });
                }
            }
        }, 1000, 5000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
