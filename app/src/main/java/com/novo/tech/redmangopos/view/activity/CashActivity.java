package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.customerdisplay.CustomerDisplayBlank;
import com.novo.tech.redmangopos.view.fragment.CashCalculator;
import com.novo.tech.redmangopos.view.fragment.CashOptions;
import com.novo.tech.redmangopos.view.fragment.CashOptionsExtra;
import com.novo.tech.redmangopos.utils.SunmiPrintHelper;

public class CashActivity extends AppCompatActivity {
    String cashType = "CASH IN";
    Display presentationDisplay;
    CustomerDisplayBlank customerDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash);
        cashType = getIntent().getStringExtra("optionType");
        openCalculatorFragment();
        openOptionsFragment();
        openLeftFragment();

        if(presentationDisplay != null){

            if(customerDisplay != null){
                if(customerDisplay.isShowing()){
                    customerDisplay.dismiss();
                }
            }

            customerDisplay = new CustomerDisplayBlank(CashActivity.this, presentationDisplay);
            customerDisplay.show();
        }

        SunmiPrintHelper.getInstance().openCashBox();


        Intent intent = new Intent("ACTION_TIMER_CALL");
        sendBroadcast(intent);
    }
    void openCalculatorFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("optionType",cashType);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.cashActivityFrameLayoutCenter, CashCalculator.class,bundle)
                .commit();
    }
    void openOptionsFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("optionType",cashType);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.cashActivityFrameLayoutRight, CashOptions.class,bundle)
                .commit();
    }
    void openLeftFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.cashActivityFrameLayoutLeft, CashOptionsExtra.class,null)
                .commit();
    }


}