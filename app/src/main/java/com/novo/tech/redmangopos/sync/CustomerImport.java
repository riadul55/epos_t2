package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.CustomerExportCallBack;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;

import java.util.ArrayList;
import java.util.List;

public class CustomerImport extends AsyncTask<String, Integer, List<Boolean>> {
    private Context context;
    CustomerExportCallBack callBack;
    public CustomerImport(Context context, CustomerExportCallBack callBack){
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected List<Boolean> doInBackground(String... strings) {
        List<Boolean> result = checkCustomerDataInServer();
        return result;
    }

    List<Boolean> checkCustomerDataInServer(){
        List<CustomerModel> customerModelList =  CustomerApi.pulCustomerList(context);
        List<Boolean> success = new ArrayList<>();
        CustomerRepository manager = new CustomerRepository(context);
        for (CustomerModel model:customerModelList) {
            boolean done = false;
            if(model.profile != null){
                if(!model.profile.phone.isEmpty()){
                    int id = manager.customerExist(model.profile.phone);
                    Log.e("id======>", "" + id);
                    if(id==0){
                        int dbId = manager.addNewCustomer(model,true);

                        if(dbId != 0){
                            done = true;
                            model.addresses.forEach((e)->{
                                e.customerDbId = dbId;
                                manager.addNewAddress(e,false);
                            });
                        }
                    }
                }
            }
            success.add(done);
        }
        return success;
    }


    @Override
    protected void onPostExecute(List<Boolean> success) {
        callBack.onSuccess(success);

        super.onPostExecute(success);
    }
}
