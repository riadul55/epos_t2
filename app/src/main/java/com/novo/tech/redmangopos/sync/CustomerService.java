package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.List;

public class CustomerService extends AsyncTask<String, Integer, String> {
    Context context;

    public CustomerService(Context context){
        this.context = context;
    }
    @Override
    protected String doInBackground(String... strings) {
        checkCustomerDataInLocal();
        getCustomerDataFromServer();
        return null;
    }
    void checkCustomerDataInLocal(){
        CustomerRepository manager = new CustomerRepository(context);
        List<CustomerModel> customerModelList = manager.getAllCustomerData();
        for (CustomerModel model:customerModelList) {
            if(!model.cloudSubmitted){
                if(model.consumer_uuid==null){
                    //create consumer
                    CustomerModel customerModel = new CustomerApi(context,model).createCustomer();
                }else{
                    //update Consumer
                    CustomerModel customerModel = new CustomerApi(context,model).updateCustomer();
                }
            }
        }
    }
    void getCustomerDataFromServer(){
       List<CustomerModel> customerModelList =  CustomerApi.getCustomerList(context);
          customerModelList.forEach((customer)->{
              if(customer!=null){
                  CustomerRepository manager = new CustomerRepository(context);
                  int id = manager.customerExist(customer.consumer_uuid);
                  if(id == 0){
                      id = manager.addNewCustomer(customer,true);
                  }
              }
          });
          if(customerModelList.size() > 0){
              SharedPrefManager.setCustomerLoaded(context);
          }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
