package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "cash")
public class Cash {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "date_time")
    @Nullable
    public String dateTime;

    @ColumnInfo(name = "amount")
    @Nullable
    public Double amount;

    @ColumnInfo(name = "user")
    @Nullable
    public String user;

    @ColumnInfo(name = "type")
    @Nullable
    public int type;

    @ColumnInfo(name = "shift")
    @Nullable
    public int shift;

    @ColumnInfo(name = "order_id")
    @Nullable
    public int orderId;

    @ColumnInfo(name = "cloud_submitted")
    @Nullable
    public boolean cloudSubmitted;

    @ColumnInfo(name = "in_cash")
    @Nullable
    public boolean inCash;

    @ColumnInfo(name = "local_payment")
    @Nullable
    public int localPayment;
}
