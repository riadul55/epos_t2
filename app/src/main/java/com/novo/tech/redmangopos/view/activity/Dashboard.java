package com.novo.tech.redmangopos.view.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.OrderListAdapter;


import com.novo.tech.redmangopos.adapter.TableListAdapter;
import com.novo.tech.redmangopos.callback.NetworkCallBack;


import com.novo.tech.redmangopos.callback.TableSelectCallBack;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.customerdisplay.CustomerDisplayBlank;
import com.novo.tech.redmangopos.customerdisplay.CustomerDisplayHelper;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.AppConfig;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.model.ShiftInfoModel;
import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;


import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.receivers.NetworkChangeReceiver;

import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.room_db.view_models.OrderViewModel;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.socket.TableBookingWorker;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.CustomerService;
import com.novo.tech.redmangopos.sync.ui_threads.GetActiveOrders;
import com.novo.tech.redmangopos.sync.PrintShiftReport;
import com.novo.tech.redmangopos.sync.ShiftInfoHelper;
import com.novo.tech.redmangopos.sync.ShiftInfoOrderHelper;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.utils.SunmiPrintHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Dashboard extends BaseActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, NetworkCallBack {

    LinearLayout drawerButton;
    DrawerLayout drawerLayout;
    NavigationView navigationViewDrawer;
    RecyclerView recyclerView;
    OrderListAdapter adapter;
    BottomNavigationView bottomAppBar;
    List<OrderModel> orderModelList = new ArrayList<>();
    List<OrderModel> selectedOrders = new ArrayList<>();
    Display presentationDisplay;
    CustomerDisplayBlank customerDisplay;
    TextView noDataFound,dashBoardType,totalOrder,totalOnlineOrder, noInternet;
    EditText searchBox;
    BottomNavigationItemView takeOutOrder,tableOrder,voidButton;
    AppCompatButton btnAll,btnOnline,btnCollection,btnDelivery,btnDineIN;
    ImageButton callHistory;

    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver mNetworkReceiver;

    /** refunded pages heading edit now use so i off this heading with visibility off "a"*/
    TextView tv_top_edit_heading;
    TextView bookingNo;
    View support_top_edit_heading;

    //calling view
    View callingPanel;
    TextView phoneNo;
    AppCompatButton accept,decline;
    //end calling view

    int displayType = 0;
    int filterType = 0;
    int tableNo = 0;

    private OrderViewModel orderViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        drawerButton = findViewById(R.id.drawerButton);
        recyclerView = findViewById(R.id.dashboardOrderRecyclerView);
        noDataFound = findViewById(R.id.dashboardNotDataFound);
        takeOutOrder = findViewById(R.id.menuItemCreateTakeWay);
        tableOrder = findViewById(R.id.menuItemCreateTable);
        voidButton = findViewById(R.id.menuItemVoid);
        btnAll = findViewById(R.id.filterButtonALL);
        btnCollection = findViewById(R.id.filterButtonCollection);
        btnOnline = findViewById(R.id.filterButtonOnline);
        btnDelivery = findViewById(R.id.filterButtonDelivery);
        btnDineIN = findViewById(R.id.filterButtonDineIN);
        searchBox = findViewById(R.id.dashboardTopAppBarSearchBox);
        totalOrder = findViewById(R.id.dashboardOrderTotal);
        totalOnlineOrder = findViewById(R.id.dashboardOrderTotalOnline);
        bookingNo = findViewById(R.id.tableNo);
        dashBoardType = findViewById(R.id.dashboardOrderType);
        noInternet = findViewById(R.id.noInternet);
        callHistory = findViewById(R.id.callHistory);

        //calling view
        callingPanel = findViewById(R.id.CallingView);
        phoneNo = findViewById(R.id.callNumber);
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });
        //end calling view

        tv_top_edit_heading = findViewById(R.id.tv_top_edit_heading);
        support_top_edit_heading = findViewById(R.id.support_top_edit_heading);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        recyclerView.setAlpha(0f);
        recyclerView.setTranslationY(50);
        recyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);

        drawerButton.setOnClickListener(this);
        btnAll.setOnClickListener(this);
        btnOnline.setOnClickListener(this);
        btnCollection.setOnClickListener(this);
        btnDelivery.setOnClickListener(this);
        btnDineIN.setOnClickListener(this);

        callHistory.setOnClickListener(view -> {
            openCallHistoryPage();
        });

        voidButton.setVisibility(View.INVISIBLE);


        if(!SharedPrefManager.getWaiterApp(Dashboard.this)){
            btnDineIN.setVisibility(View.INVISIBLE);
        }



        clearActive();
        activeThis(btnAll);

        bottomAppBar = findViewById(R.id.dashboardMenu_bottom);
        drawerLayout = findViewById(R.id.dashboardDrawerLayout);
        navigationViewDrawer = findViewById(R.id.navigation_view_left);
        presentationDisplay = CustomerDisplayHelper.getPresentationDisplays(Dashboard.this);
        searchBox.setEnabled(false);
        bottomAppBar.setOnNavigationItemSelectedListener(item -> {
                    int menuItem = item.getItemId();
                    if(menuItem == R.id.menuItemCreateDelivery){
                        openOrderPage("DELIVERY");
                    }else if(menuItem == R.id.menuItemCreateTakeWay){
                        openOrderPage("TAKEOUT");
                    }else if(menuItem == R.id.menuItemCreateCollection){
                        openOrderPage("COLLECTION");
                    }else if(menuItem == R.id.menuItemCreateTable){
//                        getTableBookings();
                        getTableNoInput();
                    }else if(menuItem == voidButton.getId()){
                        voidAlert();
                    }
                    return false;
                }
        );

        searchBox.setEnabled(true);
        navigationViewDrawer.setNavigationItemSelectedListener(this);
        //init Printer
        SunmiPrintHelper.getInstance().initSunmiPrinterService(Dashboard.this);

        String shiftType = getIntent().getStringExtra("shift");
        if(!shiftType.equals("EXISTING")){
//            pullData("Manual");
            openCashPage("CASH IN");
        }

        FirebaseMessaging.getInstance().subscribeToTopic(AppConstant.providerID);

        //  checkCustomerOnline();

        Log.d("mmmmm", "onCreate: "+AppConstant.providerSession);

        searchBox.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                displayOrderList();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();


//        for (int i=0;i<10;i++) {
//            try {
//                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
//            } catch (InterruptedException e) {
//                Thread.currentThread().interrupt();
//                Log.e("Thread interrupted", e.toString());
//            }
//            Log.e("loop ===>", i + "");
//        }

        displayOrderList();

        TheApp.webSocket.off(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business);
        TheApp.webSocket.on(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayOrderList();
                    }
                });
            }
        });

        orderViewModel = new ViewModelProvider(this).get(OrderViewModel.class);
        orderViewModel.getAllOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(List<Order> orders) {
                for (Order order: orders) {
                    Log.e("OverberDashboard==>", order.orderStatus + "");
                }
                displayOrderList();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }

    //    private void getTableBookings() {
//        ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
//        dialog.show();
//        new TableBookingApiHelper(data -> {
//            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }
//            if (data.size() > 0) {
//                Log.e("TableBookings==>", "data found");
//                getTableNoInput();
//            }
//        }).execute();
//    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }


    void getTableFilterInput(){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(Dashboard.this).setTitle("Select Table").create();
        RecyclerView recyclerView = new RecyclerView(Dashboard.this);
        recyclerView.setLayoutManager(new GridLayoutManager(Dashboard.this,5));


        TableListAdapter adapter = new TableListAdapter(Dashboard.this, new TableSelectCallBack() {
            @Override
            public void onTableSelect(int index) {
                alertDialog.dismiss();
                TableBookingModel table = new TableBookingModel(index, 1, index, 100, true, 1, 0, DateTimeHelper.getTime());
//                getNoOfGuestInput(tableBookingModel);
                tableNo = table.table_no;
                displayType = 0;
                clearActive();
                activeThis(btnDineIN);
                displayOrderList();
                alertDialog.dismiss();

            }
        });
        recyclerView.setAdapter(adapter);
        alertDialog.setView(recyclerView);
        alertDialog.show();
        alertDialog.getWindow().setLayout(1000, 850);
    }

    void getTableNoInput(){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(Dashboard.this).setTitle("Select Table").create();
        RecyclerView recyclerView = new RecyclerView(Dashboard.this);
        recyclerView.setLayoutManager(new GridLayoutManager(Dashboard.this,5));

        TableListAdapter adapter = new TableListAdapter(Dashboard.this, new TableSelectCallBack() {
            @Override
            public void onTableSelect(int index) {
                alertDialog.dismiss();
                TableBookingModel tableBookingModel = new TableBookingModel(index, 1, index, 100, true, 1, 0, DateTimeHelper.getTime());
                getNoOfGuestInput(tableBookingModel);
            }
        });

        TheApp.webSocket.on(SocketHandler.LISTEN_TABLE_BOOKED + AppConstant.business, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
//                JSONObject jsonObject = (JSONObject) args[0];
//                new TableBookingWorker(context, jsonObject).execute();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        });
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("UPDATE_BOOKING_TABLES")) {
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        };
        registerReceiver(receiver, new IntentFilter("UPDATE_BOOKING_TABLES"));

        recyclerView.setAdapter(adapter);
        alertDialog.setView(recyclerView);
        alertDialog.show();
        alertDialog.getWindow().setLayout(1000, 850);
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                unregisterReceiver(receiver);
            }
        });
    }

    public void getNoOfGuestInput(TableBookingModel tableNo){
        EditText inputField = new EditText(Dashboard.this);
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputField.setText("");
        AlertDialog builder = new AlertDialog.Builder(Dashboard.this)
                .setTitle("No of Guest")
                .setView(inputField)
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        int noOfGuest = 1;
                        String str = inputField.getText().toString();
                        if(!str.isEmpty()){
                            try {
                                noOfGuest = Integer.parseInt(str);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        tableNo.adult  = noOfGuest;

                        openOrderPageWithTable(tableNo);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }


//    void bookTableNow(TableBookingModel tableBookingModel){
//        ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
//        dialog.show();
//
//        JSONObject customerJson = new JSONObject();
//        try {
//            {
//                customerJson.put("local_no",tableBookingModel.local_no);
//                customerJson.put("floor_no",tableBookingModel.floor_no);
//                customerJson.put("table_no",tableBookingModel.table_no);
//                customerJson.put("num_adults",tableBookingModel.adult);
//                customerJson.put("num_children",tableBookingModel.child);
//                customerJson.put("booking_start",DateTimeHelper.formatLocalDateTimeForServer(DateTimeHelper.getTime()));
//                customerJson.put("creator_type","PROVIDER");
//                customerJson.put("creator_uuid",providerID);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        Log.e("TableBooking==>", customerJson.toString());
//
//        OkHttpClient client = new OkHttpClient();
//        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
//        Request request = new Request.Builder()
//                .url(BASE_URL+"booking/table?ignore_booking_logic=true")
//                .post(requestBody)
//                .addHeader("Content-Type","application/json")
//                .addHeader("ProviderSession", providerSession)
//                .build();
//
//        client.newCall(request).enqueue(new okhttp3.Callback() {
//
//            @Override
//            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
//                dialog.dismiss();
//                failedToConnect();
//                Log.e("BookingError==>", e.getMessage());
//            }
//
//            @Override
//            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response){
//                dialog.dismiss();
//                if(response.code()!=201){
//                    failedToConnect();
//                    Log.e("BookingError==>", response.code() + "");
//                }else{
//                    String generatedId = response.header("Location");
//                    openOrderPageWithTable(tableBookingModel);
//                }
//            }
//        });
//    }

    void voidAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        builder.setTitle("Are you sure , you want to void selected orders?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        voidOrder("");
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                }).create().show();
    }



    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "INTERNET_STATE_CHANGE":
                        boolean connected = intent.getBooleanExtra("state", false);
                        if (connected) {
                            noInternet.setVisibility(View.GONE);
                        } else {
                            noInternet.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_STATE_CHANGE"));
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }



    void deleteDataAlert(){
        EditText inputField = new EditText(Dashboard.this);
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputField.setText("2");
        AlertDialog builder = new AlertDialog.Builder(Dashboard.this)
                .setTitle("Order order day")
                .setView(inputField)
                .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String str = inputField.getText().toString();
                        if(str.equals("")){
                            str = "0";
                        }
                        int day  = Integer.parseInt(str);
                        OrderRepository orderManager = new OrderRepository(getApplicationContext());
                        orderManager.deleteOldOrder(day);

                    }
                })
                .setNegativeButton("BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SharedPrefManager.getEnableTakeoutOrder(Dashboard.this)){
            takeOutOrder.setVisibility(View.VISIBLE);
        }else{
            takeOutOrder.setVisibility(View.GONE);
        }

        if(SharedPrefManager.getEnableTableOrder(Dashboard.this)){
            tableOrder.setVisibility(View.VISIBLE);
        }else{
            tableOrder.setVisibility(View.GONE);
        }




        if(SharedPrefManager.getWaiterApp(Dashboard.this)){
            bookingNo.setVisibility(View.VISIBLE);
        }else{
            bookingNo.setVisibility(View.GONE);
        }


        bottomAppBar.refreshDrawableState();

        if(presentationDisplay != null){

            if(customerDisplay != null){
                if(customerDisplay.isShowing()){
                    customerDisplay.dismiss();
                }
            }

            customerDisplay = new CustomerDisplayBlank(Dashboard.this, presentationDisplay);
            customerDisplay.show();
        }
//        displayOrderList();
        refreshCallingView(null);
    }

    public void displayOrderList(){
        new GetActiveOrders(Dashboard.this,
                searchBox,
                displayType,
                tableNo,
                filterType,
                support_top_edit_heading,
                tv_top_edit_heading,
                dashBoardType,
                new GetActiveOrders.OnActiveOrderListener() {
            @Override
            public void onActiveOrders(List<OrderModel> orders) {
                orderModelList = orders;
                if (orderModelList != null && orderModelList.size() != 0) {
                    orderModelList.sort((a,b)->{
                        long aDate  = Long.parseLong(a.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(a.orderDate):a.orderDate.replaceAll("/","").replaceAll("-",""));
                        long bDate  = Long.parseLong(b.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(b.orderDate):b.orderDate.replaceAll("/","").replaceAll("-",""));
                        if(aDate == bDate){
                            return Integer.compare(b.order_id,a.order_id);
                        }else{
                            return Long.compare(bDate,aDate);
                        }
                    });

                    noDataFound.setVisibility(View.GONE);
                } else {
                    orderModelList = new ArrayList<>();
                    noDataFound.setVisibility(View.VISIBLE);
                    if(displayType==-1){
                        noDataFound.setText("No Closed Order Found!");
                    }else if(displayType==-2){
                        noDataFound.setText("No Refunded Order Found!");
                    }else if(displayType==-3){
                        noDataFound.setText("No Cancelled Order Found!");
                    }else{
                        noDataFound.setText("No Active Order Found!");
                    }
                }
                adapter = new OrderListAdapter(Dashboard.this, orderModelList);
                recyclerView.setAdapter(adapter);

                CashRepository cash = new CashRepository(Dashboard.this);
                totalOrder.setText("Total Order\n"+String.valueOf(cash.getTotalOrders()));
                totalOnlineOrder.setText("Online Order\n"+String.valueOf(cash.getOnlineOrders()));
            }
        }).execute();
//        try{
//            DBOrderManager dbOrderManager = new DBOrderManager(Dashboard.this);
//            if(searchBox.getText().toString().isEmpty()){
//                if(displayType==-1){
//                    /**heading edit hide "a"*/
//                    support_top_edit_heading.setVisibility(View.VISIBLE);
//                    tv_top_edit_heading.setVisibility(View.GONE);
//                    dashBoardType.setText("CLOSED ORDER LIST");
//                }else if(displayType == -2){
//                    /**heading edit hide "a"*/
//                    support_top_edit_heading.setVisibility(View.VISIBLE);
//                    tv_top_edit_heading.setVisibility(View.GONE);
//                    dashBoardType.setText("REFUNDED ORDER LIST");
//                }else if(displayType == -3){
//                    dashBoardType.setText("CANCELLED ORDER LIST");
//                }else{
//                    /**heading edit active "a"*/
//                    support_top_edit_heading.setVisibility(View.GONE);
//                    tv_top_edit_heading.setVisibility(View.VISIBLE);
//                    dashBoardType.setText("ACTIVE ORDERS");
//                }
//                if(displayType == -1)
//                    orderModelList = dbOrderManager.getCloseOrder();
//                else if(displayType==-2)
//                    orderModelList = dbOrderManager.getRefundedOrder();
//                else if(displayType==-3)
//                    orderModelList = dbOrderManager.getCanceledOrder();
//                else{
//                    orderModelList.clear();
//                    List<OrderModel> _list= dbOrderManager.getActiveOrder();
//                    _list.forEach((obj)->{
//                        if(tableNo!=0){
//                            if(obj.tableBookingModel!= null)
//                                if(obj.tableBookingModel.table_no == tableNo)
//                                    orderModelList.add(obj);
//                        }
//                        else if(filterType==0){
//                            orderModelList.add(obj);
//                        }else if(filterType==1){
//                            if(obj.order_channel!=null)
//                                if(obj.order_channel.equals("ONLINE")){
//                                    orderModelList.add(obj);
//                                }
//                        }else if(filterType==2){
//                            if(obj.order_type.equals("COLLECTION")){
//                                orderModelList.add(obj);
//                            }
//                        }else if(filterType==3){
//                            if(obj.order_type.equals("DELIVERY")){
//                                orderModelList.add(obj);
//                            }
//                        }
//                    });
//                }
//            }else{
//                dashBoardType.setText("Search Order");
//                orderModelList = dbOrderManager.getSearchOrder(searchBox.getText().toString());
//            }
//            orderModelList.sort((a,b)->{
//                long aDate  = Long.parseLong(a.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(a.orderDate):a.orderDate.replaceAll("/","").replaceAll("-",""));
//                long bDate  = Long.parseLong(b.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(b.orderDate):b.orderDate.replaceAll("/","").replaceAll("-",""));
//                if(aDate == bDate){
//                    return Integer.compare(b.order_id,a.order_id);
//                }else{
//                    return Long.compare(bDate,aDate);
//                }
//            });
//
//            adapter = new OrderListAdapter(Dashboard.this, orderModelList);
//            recyclerView.setAdapter(adapter);
//
//
//            if(orderModelList.size()==0){
//                noDataFound.setVisibility(View.VISIBLE);
//                if(displayType==-1){
//                    noDataFound.setText("No Closed Order Found!");
//                }else if(displayType==-2){
//                    noDataFound.setText("No Refunded Order Found!");
//                }else if(displayType==-3){
//                    noDataFound.setText("No Cancelled Order Found!");
//                }else{
//                    noDataFound.setText("No Active Order Found!");
//                }
//            }else{
//                noDataFound.setVisibility(View.GONE);
//            }
//
//            DBCashManager cash = new DBCashManager(Dashboard.this);
//            totalOrder.setText("Total Order\n"+String.valueOf(cash.getTotalOrders()));
//            totalOnlineOrder.setText("Online Order\n"+String.valueOf(cash.getOnlineOrders()));
//        }catch (Exception e){
//            Log.e("Dashboard Error===>", e.toString());
//            FirebaseCrashlytics.getInstance().recordException(e);
//        }
    }
    void clearActive(){
        btnAll.setBackgroundColor(getResources().getColor(R.color._grey));
        btnOnline.setBackgroundColor(getResources().getColor(R.color._grey));
        btnCollection.setBackgroundColor(getResources().getColor(R.color._grey));
        btnDelivery.setBackgroundColor(getResources().getColor(R.color._grey));
        btnDineIN.setBackgroundColor(getResources().getColor(R.color._grey));
    }

    void activeThis(View v){
        v.setBackgroundColor(getResources().getColor(R.color.DarkOrange));
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if( id== R.id.drawerButton){
            drawerLayout.openDrawer(Gravity.LEFT);
        }else if(id == btnAll.getId()){
            filterType = 0;
            tableNo = 0;
            displayType = 0;
            clearActive();
            activeThis(btnAll);
            displayOrderList();
        }else if(id == btnOnline.getId()){
            filterType = 1;
            tableNo = 0;
            displayType = 0;
            clearActive();
            activeThis(btnOnline);
            displayOrderList();
        }else if(id == btnCollection.getId()){
            filterType = 2;
            tableNo = 0;
            displayType = 0;
            clearActive();
            activeThis(btnCollection);
            displayOrderList();
        }else if(id == btnDelivery.getId()){
            filterType = 3;
            tableNo = 0;
            displayType = 0;
            clearActive();
            activeThis(btnDelivery);
            displayOrderList();
        }else if(id == btnDineIN.getId()){
//            ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
//            dialog.show();
//            new TableBookingApiHelper(data -> {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//                if (data.size() > 0) {
//                    Log.e("TableBookings==>", "data found");
//                    getTableFilterInput(data);
//                } else {
//                    Toast.makeText(this, "No table found!", Toast.LENGTH_LONG).show();
//                }
//            }).execute();

            getTableFilterInput();
        }
    }
    void openOrderPage(String orderType){
        Intent in=new Intent(Dashboard.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType",orderType);
        in.putExtras(b);
        startActivity(in);
    }
    public void openOrderPage(OrderModel orderModel){
        Intent in=new Intent(Dashboard.this,OrderCreate.class);
        Bundle b = new Bundle();
        b.putInt("orderModel", orderModel.db_id);
        in.putExtras(b);
        startActivity(in);
    }
    private void openOrderPageWithTable(TableBookingModel tableNo){
        Intent in=new Intent(Dashboard.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType","LOCAL");
        b.putSerializable("table",tableNo);
        in.putExtras(b);
        startActivity(in);
    }
    public void openOrderDetailsPage(int orderId){
        Intent in=new Intent(Dashboard.this, OrderDetails.class);
        Bundle b = new Bundle();
        b.putInt("orderData",orderId);
        in.putExtras(b);
        startActivity(in);
    }
    public void openOnlineOrderDetailsPage(int orderId){
        Intent in=new Intent(Dashboard.this, OnlineOrderDetails.class);
        Bundle b = new Bundle();
        b.putInt("orderData",orderId);
        in.putExtras(b);
        startActivity(in);
    }
    private void openCallHistoryPage() {
        Intent in=new Intent(Dashboard.this, CallHistory.class);
        startActivity(in);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();
        if(item.getItemId() == R.id.drawerMenuLogOut){
            logOut();

        }else if(item.getItemId() == R.id.drawerMenuRefreshData){
            refreshDataFromServer();
        }else if(item.getItemId() == R.id.drawerMenuActiveOrder){
            displayType = 0;
            displayOrderList();
        }else if(item.getItemId() == R.id.drawerMenuClosedOrder){
            displayType = -1;
            clearActive();
            displayOrderList();
        }else if(item.getItemId() == R.id.drawerMenuRefundedOrder){
            displayType = -2;
            clearActive();
            displayOrderList();
        }else if(item.getItemId() == R.id.drawerMenuShiftInfo){
            showShiftInfo();
        }else if(item.getItemId() == R.id.drawerMenuVoidOrder){
            displayType = -3;
            displayOrderList();
        }else if(item.getItemId() == R.id.drawerMenuShiftOutOrder){
            openShiftClosePage();

        }else if(item.getItemId() == R.id.drawerMenuDeleteData){
            deleteDataAlert();

        }else if(item.getItemId() == R.id.drawerMenuCashOption){
            openCashPage("CASH OUT");

        }else if(item.getItemId() == R.id.drawerMenuSetting){
            Intent intent = new Intent(Dashboard.this, Setting.class);
            startActivity(intent);
        }
        return false;
    }


    private void showShiftInfo() {

        final Calendar myCalendar = Calendar.getInstance();
        ArrayList<ShiftInfoOrderModel> shiftInfoOrderModelArrayList = new ArrayList<>();
        ArrayList<ShiftInfoModel> shiftInfoModelArrayList = new ArrayList<>();

        CashRepository cash = new CashRepository(Dashboard.this);
        final Dialog dialog = new Dialog(Dashboard.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.ship_info_layout);
        LinearLayout ll_order_quantity = (LinearLayout) dialog.findViewById(R.id.ll_order_quantity);
        LinearLayout ll_order_carPayment = (LinearLayout) dialog.findViewById(R.id.ll_card_payment);
        LinearLayout ll_shiftINfo = (LinearLayout) dialog.findViewById(R.id.ll_shiftInfo);

        CheckBox cb_order_quantity = (CheckBox) dialog.findViewById(R.id.cb_order_quantity);
        CheckBox cb_shift = (CheckBox) dialog.findViewById(R.id.cb_tip);
        CheckBox cb_card = (CheckBox) dialog.findViewById(R.id.cb_card);

        cb_order_quantity.setChecked(true);
        cb_shift.setChecked(true);
        cb_card.setChecked(true);

        TextView local_balance = (TextView) dialog.findViewById(R.id.local_balance);
        TextView total_order = (TextView) dialog.findViewById(R.id.total_order);
        TextView total_local_order = (TextView) dialog.findViewById(R.id.total_local_order);

        TextView online_order = (TextView) dialog.findViewById(R.id.online_order);

        TextView total_local_card = (TextView) dialog.findViewById(R.id.local_card_total);
        TextView total_local_cash = (TextView) dialog.findViewById(R.id.local_cash_total);

        TextView online_card_total = (TextView) dialog.findViewById(R.id.online_card_total);
        TextView total_cart = (TextView) dialog.findViewById(R.id.total_cart);
        // TextView manual_discount = (TextView) dialog.findViewById(R.id.manual_discount);
        TextView total_cash_in = (TextView) dialog.findViewById(R.id.total_cash_in);
        TextView total_cash_out = (TextView) dialog.findViewById(R.id.total_cash_out);
        TextView refunded_amount = (TextView) dialog.findViewById(R.id.refunded_amount);
        // TextView balance = (TextView) dialog.findViewById(R.id.balance);
        TextView date_tv = (TextView) dialog.findViewById(R.id.date_tv);
        EditText et_date = (EditText) dialog.findViewById(R.id.et_date);
        ImageView calenderOpen = (ImageView) dialog.findViewById(R.id.calenderOpen);
        Button btn_search_info = (Button) dialog.findViewById(R.id.btn_search_info);


        cb_order_quantity.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                ll_order_quantity.setVisibility(View.VISIBLE);
            else
                ll_order_quantity.setVisibility(View.GONE);

        });
        cb_card.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                ll_order_carPayment.setVisibility(View.VISIBLE);
            else
                ll_order_carPayment.setVisibility(View.GONE);

        });
        cb_shift.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)
                ll_shiftINfo.setVisibility(View.VISIBLE);
            else
                ll_shiftINfo.setVisibility(View.GONE);

        });


        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        date_tv.setText(thisDate);
        et_date.setText(thisDate);
        et_date.setFocusable(false);
        et_date.setFocusable(true);



        // this is for local data
        total_order.setText(String.valueOf(cash.getTotalOrders()));
        total_local_order.setText(String.valueOf(cash.getTotalOrders()- cash.getOnlineOrders()));
        online_order.setText(String.valueOf(cash.getOnlineOrders()));
        online_card_total.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCardAmount()-cash.getTotalCardAmountLocal()));
        total_local_cash.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashAmount()));
        total_local_card.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCardAmountLocal()));
        total_cart.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashAmount() + cash.getTotalCardAmount()));
        total_cash_in.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashIn()));
        total_cash_out.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashOut()));
        refunded_amount.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getTotalRefund()));
        local_balance.setText("£ "+String.format(Locale.getDefault(),"%.2f",cash.getBalance()).replace("-",""));


        Button closeButton = (Button) dialog.findViewById(R.id.close_button);
        Button printButton = (Button) dialog.findViewById(R.id.print_button);
        final DatePickerDialog[] datePickerDialog = new DatePickerDialog[1];
        calenderOpen.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();

            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            datePickerDialog[0] = new DatePickerDialog(Dashboard.this,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        if ((monthOfYear + 1)<10 && dayOfMonth<10){
                            et_date.setText(""+year + "-0"+ (monthOfYear + 1) + "-0" + dayOfMonth);
                        }else if ((monthOfYear + 1)<10){
                            et_date.setText(""+year + "-0"+ (monthOfYear + 1) + "-" + dayOfMonth);
                        }else if (dayOfMonth<10){
                            et_date.setText(""+year + "-"+ (monthOfYear + 1) + "-0" + dayOfMonth);
                        }else {
                            et_date.setText(""+year + "-"+ (monthOfYear + 1) + "-" + dayOfMonth);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog[0].show();

        });
        btn_search_info.setOnClickListener(v -> {

            if (TextUtils.isEmpty(et_date.toString().trim())){
                Toast.makeText(Dashboard.this, "Date is Empty!", Toast.LENGTH_SHORT).show();
            }else{
                new ShiftInfoOrderHelper((et_date.getText().toString().trim()), data -> {
                    total_order.setText(String.valueOf(data.localOrderTotal+data.onlineOrderTotal));
                    total_local_order.setText(String.valueOf(data.localOrderTotal));
                    online_order.setText(String.valueOf(data.onlineOrderTotal));

                }).execute();

                new ShiftInfoHelper(et_date.getText().toString().trim(), data -> {
                    total_cash_in.setText("£"+String.format(Locale.getDefault(),"%.2f",data.cashInAmount));
                    total_cash_out.setText("£"+String.format(Locale.getDefault(),"%.2f",data.cashOutAmount).replace("-",""));
                    refunded_amount.setText("£"+String.format(Locale.getDefault(),"%.2f",data.cashOutAmount));
                }).execute();

            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getPrinterIndex(Dashboard.this) == Conts.PRINTER_SUNMI) {
                    CashMemoHelper.printShiftReport(Dashboard.this);
                } else {
                    Log.e("Printing", "processing");
                    new PrintShiftReport(Dashboard.this).execute();
                }
            }
        });

        dialog.show();
    }

    public void onOrderSelect(List<OrderModel> _orderModels){
        selectedOrders = _orderModels;
        if(!selectedOrders.isEmpty()){
            voidButton.setVisibility(View.VISIBLE);
        }else{
            voidButton.setVisibility(View.INVISIBLE);
        }
    }
    void openCashPage(String optionType){
        Intent intent = new Intent(Dashboard.this, CashActivity.class);
        intent.putExtra("optionType",optionType);
        startActivity(intent);
    }
    void openShiftClosePage(){
        Intent intent = new Intent(Dashboard.this, ShiftClose.class);
        startActivity(intent);
    }
    void logOut(){
        confirmLogOut();
       /* Intent intent = new Intent(Dashboard.this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
    }

    private void confirmLogOut() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Logout..!!!")
                .setIcon(R.drawable.ic_logout_red                    )
                .setMessage("Are you sure,You want to logout ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // logout
                        Intent intent = new Intent(Dashboard.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to logout
                    }
                })
                .show();
    }

    void voidOrder(String message){
        OrderRepository orderManager = new OrderRepository(Dashboard.this);
        for (OrderModel orderModel:selectedOrders){
            orderManager.delete(orderModel);
        }
        voidButton.setVisibility(View.INVISIBLE);
        displayOrderList();
    }


    void checkCustomerOnline(){
        if(!SharedPrefManager.getCustomerLoaded(Dashboard.this)){
            CustomerService sync = new CustomerService(Dashboard.this);
            sync.execute();
        }
    }

    @Override
    public void onChanged() {
        displayOrderList();
    }

    void refreshDataFromServer(){
        getGlobalConfigs();
//        DBOrderManager orderManager = new DBOrderManager(this);
//        DBOrderHistoryManager historyManager = new DBOrderHistoryManager(this);
//
//        List<OrderModel> orderModelList = orderManager.getAllOrder();
//        List<OrderModel> historyList = historyManager.getAllOrder();
//
//        Log.e("order_list_count==>", orderModelList.size() + "");
//        Log.e("history_order_list_count==>", historyList.size() + "");
    }

    void getGlobalConfigs() {
        ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
        dialog.show();
//        AppConfig appConfig = SharedPrefManager.getAppConfig(Dashboard.this);
        Retrofit retrofit = new RetrofitClient(Dashboard.this).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        Call<AppConfig> call = api.getAppConfig();
        call.enqueue(new Callback<AppConfig>() {
            @Override
            public void onResponse(Call<AppConfig> call, Response<AppConfig> response) {
                dialog.dismiss();
                if (response.code() == 200 && response.body() != null) {
                    SharedPrefManager.setAppConfig(Dashboard.this, response.body());
                    AppConfig appConfig = response.body();
                    if (appConfig.getBusinessName() != null) {
                        SharedPrefManager.setBusinessName(Dashboard.this, response.body().getBusinessName());
                    }
                    if (appConfig.getBusinessPhone() != null) {
                        SharedPrefManager.setBusinessPhone(Dashboard.this, response.body().getBusinessPhone());
                    }
                    if (appConfig.getBusinessAddress() != null) {
                        SharedPrefManager.setBusinessLocation(Dashboard.this, response.body().getBusinessAddress());
                    }
                    if (appConfig.getDeliveryCharge() != null && !appConfig.getDeliveryCharge().isEmpty()) {
                        SharedPrefManager.setDeliveryCharge(Dashboard.this, Double.parseDouble(response.body().getDeliveryCharge()));
                    }
                    if (appConfig.getMinDiscountCollection() != null && !appConfig.getMinDiscountCollection().isEmpty()) {
                        SharedPrefManager.setMinAmountCollection(Dashboard.this, Integer.parseInt(response.body().getMinDiscountCollection()));
                    }
                    if (appConfig.getMinDiscountDelivery() != null && !appConfig.getMinDiscountDelivery().isEmpty()) {
                        SharedPrefManager.setMinAmountDelivery(Dashboard.this, Integer.parseInt(response.body().getMinDiscountDelivery()));
                    }
                    if(appConfig.getBusinessStatus().equals("open")){
                        SharedPrefManager.setOnlineOrderStatus(Dashboard.this, true);
                    }else if(appConfig.getBusinessStatus().equals("closed")){
                        SharedPrefManager.setOnlineOrderStatus(Dashboard.this, false);
                    }
                }
                loadProperty();
            }

            @Override
            public void onFailure(Call<AppConfig> call, Throwable t) {
                dialog.dismiss();
                loadProperty();
            }
        });
    }

    void loadProperty() {
        ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
        dialog.show();
        Retrofit retrofit = new RetrofitClient(Dashboard.this).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        Call call = api.getProperty();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();
                int responseCode = response.code();
                List<Category> proList = new ArrayList<Category>();
                if(responseCode == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        jsonObject.keys().forEachRemaining(str->{
                            JSONArray jsonArray = jsonObject.optJSONArray(str);
                            List<PropertyItem> propertyItems = new ArrayList<>();
                            for (int i = 0;i<jsonArray.length();i++){
                                PropertyItem _proItem = GsonParser.getGsonParser().fromJson(jsonArray.optJSONObject(i).toString(),PropertyItem.class);
                                propertyItems.add(_proItem);
                            }
                            Property prop = new Property(str,propertyItems);
                            Category category = new Category();
                            category.keyName = prop.key_name;
                            category.items = prop.items;

                            proList.add(category);

                        });
                        Log.e("mmmm", "onResponse: si  "+proList.size() );
                    } catch (JSONException e) {
                        Log.e("mmmm", "onResponse: "+e.getMessage() );
                        e.printStackTrace();
                    }
                }
                SharedPrefManager.setAllProperty(Dashboard.this,proList);
                loadProductsFromServer();
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();
                failedToConnect();
            }
        });
    }
    private void loadProductsFromServer() {
        ProgressDialog dialog = ProgressDialog.show(Dashboard.this, "", "Loading. Please wait...", true);
        dialog.show();
        Retrofit retrofit = new RetrofitClient(Dashboard.this).getRetrofit(new GsonBuilder().create());
        ProductApi api = retrofit.create(ProductApi.class);
        Call call = api.getProducts ();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();
                int responseCode = response.code();
                List<ProductTb> _productList = new ArrayList<ProductTb>();
                if(responseCode == 200){
                    try {
                        JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                        for (int i = 0;i<_data.length();i++){
                            Product product = Product.fromJSON(_data.getJSONObject(i));
                            ProductTb productTb = Product.toProductTable(product);
                            _productList.add(productTb);
                            if (product.productType.equals("DISCOUNT")) {
                                if (product.productUUid.equals("discount_on_amount_collection") && product.properties != null) {
                                    SharedPrefManager.setDiscountCollection(getApplicationContext(), Integer.parseInt(product.properties.percentage));
                                } else if (product.productUUid.equals("discount_on_amount_delivery") && product.properties != null) {
                                    SharedPrefManager.setDiscountDelivery(getApplicationContext(), Integer.parseInt(product.properties.percentage));
                                }
                            }
                        }
                        Log.e("MMMMMMMM", "onResponse: success "+_data.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MMMMMMMM", "onResponse: error "+e.getMessage() );
                    }
                }
                SharedPrefManager.setAllProduct(Dashboard.this,_productList);
                successMessage();
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();
                failedToConnect();
            }
        });
    }

    public void failedToConnect(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog builder = new AlertDialog.Builder(Dashboard.this)
                        .setTitle("Connection failed!")
                        .setMessage("Please check your connection and try again")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        }).create();
                builder.show();
            }
        });
    }

    void successMessage(){
        AlertDialog builder = new AlertDialog.Builder(Dashboard.this)
                .setTitle("Success")
                .setMessage("Updating Data from server Success")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }

    @Override
    public void onNetworkStateChange(boolean connected) {
        Log.d("mmmmm", "onNetworkStateChange: "+connected);
    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}