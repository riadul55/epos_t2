package com.novo.tech.redmangopos.model;

public class ShiftSummeryModel {
    public double tipsAmount,refundedAmount,cashInAmount,cashOutAmount;
    public double totalCashPaid ,totalCardPaid ,totalOnlineCardPaid ,totalLocalCardPaid;

    public ShiftSummeryModel(double tipsAmount, double refundedAmount, double cashInAmount, double cashOutAmount, double totalCashPaid, double totalCardPaid, double totalOnlineCardPaid, double totalLocalCardPaid) {
        this.tipsAmount = tipsAmount;
        this.refundedAmount = refundedAmount;
        this.cashInAmount = cashInAmount;
        this.cashOutAmount = cashOutAmount;
        this.totalCashPaid = totalCashPaid;
        this.totalCardPaid = totalCardPaid;
        this.totalOnlineCardPaid = totalOnlineCardPaid;
        this.totalLocalCardPaid = totalLocalCardPaid;
    }
}
