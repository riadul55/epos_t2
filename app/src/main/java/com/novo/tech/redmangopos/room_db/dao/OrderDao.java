package com.novo.tech.redmangopos.room_db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.entities.OrderBackup;

import java.util.List;

@Dao
public interface OrderDao {
    @Query("SELECT * FROM `orders`")
    LiveData<List<Order>> getAll();

    @RawQuery
    List<Order> getOrders(SupportSQLiteQuery query);

    @Query("SELECT IFNULL(MAX(order_id), 0) AS max FROM orders WHERE date BETWEEN :minDate AND :maxDate")
    int getOrderMax(String minDate, String maxDate);

    @Query("SELECT * FROM orders WHERE id=:id")
    Order getById(int id);

    @RawQuery
    List<Order> getByBookingId(SupportSQLiteQuery query);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOne(Order order);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertAll(Order... orders);

    @Update
    int updateOne(Order order);

    @Update
    void updateAll(Order... orders);

    @Query("DELETE FROM orders WHERE date < :lastDate")
    void deleteOldOrder(String lastDate);

    @Delete
    void delete(Order model);

    @Query("DELETE FROM orders")
    void deleteAllOrders();

    @Query("SELECT id FROM orders WHERE server_id=:serverId AND tableBookingId != '0'")
    int existOnlineTableOrder(String serverId);

    @Query("SELECT id FROM orders WHERE server_id=:serverId")
    int existOnlineOrder(String serverId);

    @Query("SELECT id FROM orders WHERE id=:id")
    int existOrder(String id);

    @Query("DELETE FROM orders WHERE id=:id")
    void deleteById(int id);

    @RawQuery
    int vacumDb(SupportSQLiteQuery supportSQLiteQuery);


    //Backup orders dao
    @Query("SELECT * FROM orders_backup WHERE customerId=:customerId ORDER BY date LIMIT 0,:limit")
    List<OrderBackup> getBackupOrdersByCustomer(String customerId, String limit);

    @Query("SELECT * FROM orders_backup WHERE customerId = :dbId ORDER BY date LIMIT 0,:limit")
    LiveData<List<OrderBackup>> getHistoryOrdersByCustomer(int dbId, int limit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOne(OrderBackup order);

    @Query("SELECT IFNULL(MAX(order_id), 0) AS max FROM orders_backup WHERE date =:date")
    int getOrderHistoryMax(String date);

    @Query("SELECT id FROM orders_backup")
    List<Integer> getBackupOrderIds();

    @Query("SELECT id FROM orders_backup WHERE customerId = :cId ORDER BY date DESC LIMIT :limit")
    List<Integer> getBackupOrderIdsByCustomer(int cId, int limit);

    @Query("SELECT * FROM orders_backup WHERE id = :id")
    OrderBackup getHistoryOrderData(int id);

    @Query("DELETE FROM orders_backup WHERE id = :id")
    void deleteBackupOrderById(int id);
}
