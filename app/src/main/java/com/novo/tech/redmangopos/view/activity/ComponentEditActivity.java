package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.PriceAddResponseCallBack;
import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;
import com.novo.tech.redmangopos.callback.ProductUpdateResponseCallBack;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.sync.ComponentDetailsUpdateHelper;
import com.novo.tech.redmangopos.sync.ImageDeleteHelper;
import com.novo.tech.redmangopos.sync.PriceAddHelper;
import com.novo.tech.redmangopos.sync.PriceUpdateHelper;
import com.novo.tech.redmangopos.utils.CountingRequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ComponentEditActivity extends AppCompatActivity {
    LinearLayout productBAck;
    public static EditText et_price;
    public static EditText et_short;
    public static EditText et_title;
    public static Spinner sp_property;
    public static Spinner sp_category;
    public static Spinner sp_availability;
    public static EditText et_description;
    public static CheckBox cb_discountable;
    public static ImageView iv_product;
    static CategoryDetailsModel categoryDetailsModelArrayList;
    String imgUrl = "";
    Button price_save,details_save_button,upload;
    int REQUEST_GET_SINGLE_FILE = 1;
    Uri selectedImageURI;
    ProgressBar progressBar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component_edit);
        productBAck = findViewById(R.id.productBAck);
        productBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComponentEditActivity.this,ProductDetailsEditActivity.class);
                startActivity(intent);
                finish();
            }
        });

        et_price = findViewById(R.id.et_price);
        et_short = findViewById(R.id.et_short_order_cm);
        et_title = findViewById(R.id.et_title);
        sp_category = findViewById(R.id.sp_category);
        sp_availability = findViewById(R.id.sp_availability);
        et_description = findViewById(R.id.et_description);
        cb_discountable = findViewById(R.id.cb_discountable);
        iv_product = findViewById(R.id.iv_product);
        price_save = findViewById(R.id.price_save);
        details_save_button = findViewById(R.id.details_save_button);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        upload = findViewById(R.id.img_btn_save);

        if (categoryDetailsModelArrayList.getPrice()!=null)
            if (categoryDetailsModelArrayList.getPrice().getPrice()>0)
                et_price.setText(String.valueOf(categoryDetailsModelArrayList.getPrice().getPrice()));
        if (categoryDetailsModelArrayList.getProperties() != null)
            if (categoryDetailsModelArrayList.getProperties().getSort_order() != null)
                et_short.setText(String.valueOf(categoryDetailsModelArrayList.getProperties().getSort_order()));
        et_title.setText(String.valueOf(categoryDetailsModelArrayList.getShort_name()));
        et_description.setText(String.valueOf(categoryDetailsModelArrayList.getDescription()));
        if (categoryDetailsModelArrayList.isDiscountable())
            cb_discountable.setChecked(true);
        imgUrl = "https://labapi.yuma-technology.co.uk:8443/delivery/product/" + categoryDetailsModelArrayList.files.get(0).itemId + "/file/" + categoryDetailsModelArrayList.files.get(0).fileUid;
        Glide.with(ComponentEditActivity.this).load(imgUrl).into( ComponentEditActivity.iv_product);

        price_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryDetailsModelArrayList.getPrice()==null){
                    if (et_price.getText().toString().trim() != null){
                        new PriceAddHelper(
                                Double.valueOf(et_price.getText().toString().trim()), categoryDetailsModelArrayList.getProduct_uuid(), new PriceAddResponseCallBack() {
                            @Override
                            public void onPriceAddResponse(String response) {
                                if (response.equals("success")){
                                    Toast.makeText(ComponentEditActivity.this, "Price Added Successfully", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        ).execute();
                    }
                }else {
                    if (et_price.getText().toString().trim() != null) {
                        new PriceUpdateHelper(
                                categoryDetailsModelArrayList.getProduct_uuid(),
                                categoryDetailsModelArrayList.getPrice().getPrice_uuid(),
                                Double.valueOf(et_price.getText().toString().trim()),
                                new PriceUpdateResponseCallBack() {
                                    @Override
                                    public void onPriceUpdateResponse(String response) {
                                        if (response.equals("success")) {

                                            new PriceAddHelper(
                                                    Double.valueOf(et_price.getText().toString().trim()), categoryDetailsModelArrayList.getProduct_uuid(), new PriceAddResponseCallBack() {
                                                @Override
                                                public void onPriceAddResponse(String response) {
                                                    if (response.equals("success")){
                                                        Toast.makeText(ComponentEditActivity.this, "Price Update Successfully", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                                            ).execute();
                                        }
                                    }
                                }
                        ).execute();
                    }
                }
            }
        });
        details_save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String product_uuid = categoryDetailsModelArrayList.getProduct_uuid();
                String sort_order = et_short.getText().toString().trim();
                String short_name = et_title.getText().toString().trim();
                boolean discountable = cb_discountable.isChecked();
                String description = et_description.getText().toString().trim();

                if (!TextUtils.isEmpty(sort_order) && !TextUtils.isEmpty(short_name)){
                    new ComponentDetailsUpdateHelper(product_uuid, sort_order, short_name, discountable, description, new ProductUpdateResponseCallBack() {
                        @Override
                        public void onProductUpdateResponse(String response) {
                            if (response.equals("success")){
                                Toast.makeText(ComponentEditActivity.this, "Details Update Successfully", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).execute();
                }
            }
        });
        iv_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),REQUEST_GET_SINGLE_FILE);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                if (categoryDetailsModelArrayList.files.get(0).fileUid !=null){
                    new ImageDeleteHelper(
                            categoryDetailsModelArrayList.getProduct_uuid(),
                            categoryDetailsModelArrayList.files.get(0).fileUid,
                            new PriceUpdateResponseCallBack() {
                                @Override
                                public void onPriceUpdateResponse(String response) {
                                    if (response.equals("success")){
                                        uploadImage(categoryDetailsModelArrayList.getProduct_uuid());
                                    }
                                }
                            }
                    ).execute();
                }else {
                    uploadImage(categoryDetailsModelArrayList.getProduct_uuid());
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                selectedImageURI = data.getData();
                Glide.with(ComponentEditActivity.this).load(selectedImageURI).into(iv_product);
            }
        }
    }
    public static CategoryDetailsModel componentData(CategoryDetailsModel categoryArrayList){
        categoryDetailsModelArrayList = categoryArrayList;
        return categoryDetailsModelArrayList;
    }

    private String uriToFilename(Uri uri) {
        String path = null;

        if ((Build.VERSION.SDK_INT < 19) && (Build.VERSION.SDK_INT > 11)) {
            path = getRealPathFromURI_API11to18(this, uri);
        } else {
            path = getFilePath(this, uri);
        }

        return path;
    }

    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;
        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    public String getFilePath(Context context, Uri uri) {
        String filePath = "";
        if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);
            String[] splits = wholeID.split(":");
            if (splits.length == 2) {
                String id = splits[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        } else {
            filePath = uri.getPath();
        }
        return filePath;
    }
    public void uploadImage(String product_uid){
        if(selectedImageURI == null){
            return;
        }
        final File imageFile = new File(uriToFilename(selectedImageURI));
        Uri uris = Uri.fromFile(imageFile);
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uris.toString());
        String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
        String imageName = imageFile.getName();

        JSONObject fileJson = new JSONObject();
        try {
            fileJson.put("key_name","item_image");
            fileJson.put("file_name",imageName);
            fileJson.put("content_type","image/png");
            fileJson.put("scope","public");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)

                .addFormDataPart("request", String.valueOf(fileJson))
                .addFormDataPart("file", String.valueOf(uris),
                        RequestBody.create(imageFile, MediaType.parse(mime)))
                .build();

        final CountingRequestBody.Listener progressListener = new CountingRequestBody.Listener() {
            @Override
            public void onRequestProgress(long bytesRead, long contentLength) {
                if (bytesRead >= contentLength) {
                    if (progressBar != null)
                        ComponentEditActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                } else {
                    if (contentLength > 0) {
                        final int progress = (int) (((double) bytesRead / contentLength) * 100);
                        if (progressBar != null)
                            ComponentEditActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressBar.setProgress(progress);
                                }
                            });

                        if(progress >= 100){
                            progressBar.setVisibility(View.GONE);
                        }
                        Log.e("uploadProgress called", progress+" ");
                    }
                }
            }
        };

        OkHttpClient imageUploadClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        if (originalRequest.body() == null) {
                            return chain.proceed(originalRequest);
                        }
                        Request progressRequest = originalRequest.newBuilder()
                                .method(originalRequest.method(),
                                        new CountingRequestBody(originalRequest.body(), progressListener))
                                .build();
                        return chain.proceed(progressRequest);
                    }
                })
                .build();

        RequestBody formBody = new FormBody.Builder()
                .add("message", "Your message")
                .build();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uid+"/file")
                .header("ProviderSession", "0b09064c-82e1-4548-b693-b6081df85b39")
                .header("Content-Type", "application/json")
                .post(requestBody)
                .build();


        imageUploadClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                //Toast.makeText(ChatScreen.this, "Error uploading file", Toast.LENGTH_LONG).show();
                Log.e("failure Response", mMessage);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String mMessage = response.body().string();

                ComponentEditActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("TAG", mMessage);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }


}