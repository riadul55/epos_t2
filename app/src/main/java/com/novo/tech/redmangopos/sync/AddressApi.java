package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.ADDRESS_URL;
import static com.novo.tech.redmangopos.util.AppConstant.adminSession;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.novo.tech.redmangopos.callback.ZipCallBack;
import com.novo.tech.redmangopos.model.AddressHelperModel;
import com.novo.tech.redmangopos.model.JsonAddress;
import com.novo.tech.redmangopos.storage.AddressDbHelper;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.utils.Utils;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class AddressApi  extends AsyncTask<String, Void, List<JsonAddress>> {
    Context context;
    ZipCallBack callBack;
    String str;
    public AddressApi(Context context, String str, ZipCallBack callBack){
        this.context = context;
        this.str = str;
        this.callBack = callBack;
    }
    @Override
    protected List<JsonAddress> doInBackground(String... strings) {
        return getHouseByZip(str);
    }

    public  List<JsonAddress> getHouseByZip(String str){
        try {
            AddressDbHelper addressDbHelper = new AddressDbHelper(context);
            return addressDbHelper.getSelectedResult(str);
        } catch (Exception e) {
            return new ArrayList<JsonAddress>();
        }
    }

    @Override
    protected void onPostExecute(List<JsonAddress> arrayList) {
        callBack.onAddressResponse(arrayList);
        super.onPostExecute(arrayList);
    }
}

