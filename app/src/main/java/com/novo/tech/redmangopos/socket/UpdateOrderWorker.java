package com.novo.tech.redmangopos.socket;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UpdateOrderWorker extends AsyncTask<Void, Void, JSONObject> {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private JSONObject jsonObject;

    public UpdateOrderWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        JSONObject newJson = new JSONObject();
        try {
            JSONObject orderModelJson = (JSONObject) jsonObject.get("order");
            OrderModel orderModel = JsonToOrderModel.toModel(orderModelJson, context);

            OrderRepository manager = new OrderRepository(context);
            int id = manager.orderExist(orderModel.db_id);
            if(id < 1){
                orderModel.order_type = orderModel.order_type.toUpperCase().replaceAll("TAKEOUT","COLLECTION");
                try {
                    SharedPrefManager.createUpdateOrder(context, orderModel);

                    // play sound
//                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                    Ringtone r = RingtoneManager.getRingtone(context, notification);
//                    r.play();
                    Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_PLAY);
                    context.sendBroadcast(intent);
                    if (SharedPrefManager.getPrintOnOnlineOrder(context)) {
                        CashMemoHelper.printKitchenMemo(orderModel, context, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                OrderModel orderData = manager.getOrderData(id);
                manager.update(orderModel);

                List<CartItem> oldItems = orderData.items;
                List<CartItem> newItems = orderModel.items;
                List<CartItem> extraItems = new ArrayList<>();
                for (int i=0;i<newItems.size();i++) {
                    boolean flag = false;
                    for (int j=0;j<oldItems.size();j++) {
                        if (newItems.get(i).uuid.equals(oldItems.get(j).uuid) && newItems.get(i).quantity == oldItems.get(j).quantity) {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        extraItems.add(newItems.get(i));
                    }
                }
                Log.e("ExtraItems===>", extraItems.size() + "");
                if (extraItems.size() > 0) {
                    orderData.items = extraItems;
                    CashMemoHelper.printKitchenMemo(orderData, context, true);
                }
            }

            OrderRepository repository = new OrderRepository(context);
            List<OrderModel> activeOrders = repository.getActiveOrder();
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<activeOrders.size();i++) {
                jsonArray.put(OrderModelToJson.fromModel(activeOrders.get(i), context));
            }

            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            newJson.put(SocketHandler.ORDER_LIST, jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newJson;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        try {
            TheApp.webSocket.emit(SocketHandler.EMIT_ORDER_LIST, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("UpdateOrderWorker", "called");

        try {
            JSONObject newJson = new JSONObject();
            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<20;i++) {
                boolean isBooked = SharedPrefManager.getTableBooked(context, i+1);
                TableBookingModel tableBookingModel = new TableBookingModel(
                        1, 1, i+1, 1, isBooked, 1, 1, ""
                );
                jsonArray.put(TableBookingModel.toJson(tableBookingModel));
            }
            newJson.put("booking_list", jsonArray);
            TheApp.webSocket.emit(SocketHandler.EMIT_BOOKING_LIST, newJson);
        } catch (Exception e) {
            e.printStackTrace();
        }


        JSONObject emitGetOrders = new JSONObject();
        try {
            emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            emitGetOrders.put("data", "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);
    }
}
