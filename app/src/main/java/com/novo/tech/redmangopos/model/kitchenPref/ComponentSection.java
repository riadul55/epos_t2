package com.novo.tech.redmangopos.model.kitchenPref;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ComponentSection implements Serializable {
    @SerializedName("sectionName")
    @Expose
    private String sectionName;
    @SerializedName("sectionValue")
    @Expose
    private String sectionValue;
    @SerializedName("components")
    @Expose
    private List<Component> components = null;
    @SerializedName("selectedItem")
    @Expose
    private Component selectedItem;
    private final static long serialVersionUID = -4177325413347515251L;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionValue() {
        return sectionValue;
    }

    public void setSectionValue(String sectionValue) {
        this.sectionValue = sectionValue;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public Component getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Component selectedItem) {
        this.selectedItem = selectedItem;
    }

}
