package com.novo.tech.redmangopos.view.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.novo.tech.redmangopos.adapter.ComponentListAdapter;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.SectionListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Components extends Fragment implements View.OnClickListener, ComponentListAdapter.OnItemUpdateListener{
    List<ComponentSection> sections = new ArrayList<>();
    RecyclerView recyclerViewSegment;
    SectionListAdapter sectionListAdapter;
    Button cancel,add;
    Product product;
    String productData;

    OnProductUpdateListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnProductUpdateListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement " + OnProductUpdateListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sub_product, container, false);
        cancel = v.findViewById(R.id.orderCreateComponentsSubCancel);
        add = v.findViewById(R.id.orderCreateComponentsSubAdd);
        cancel.setOnClickListener(this);
        add.setOnClickListener(this);
        productData = requireArguments().getString("product");
        product = GsonParser.getGsonParser().fromJson(productData, Product.class);
        List<Component> componentArrayList = product.componentList;
        componentArrayList.sort((a,b)->{
            if(a.properties != null && b.properties!=null){
                return Integer.compare(a.properties.sort_order,b.properties.sort_order);
            }else if(a.properties == null){
                return -1;
            }else
                return 1;
        });
        for (Component component : componentArrayList){
            ComponentSection group = null;
            for(ComponentSection section : sections){
               if(section.sectionValue.equals(component.relationGroup)){
                   group = section;
               }
            }
            if(group != null){
                group.components.add(component);
            }else{
                group = new ComponentSection(component.relationGroup,component.relationGroup,new ArrayList<>(),null);
                group.components.add(component);
                sections.add(group);
            }
        }
        for (ComponentSection section: sections){
            section.selectedItem = section.components.get(0);
        }

        loadSegment(v);
        loadSubCartFragment();
        return v;
    }

    private void loadSubCartFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("product",  productData);
        bundle.putSerializable("sections", (Serializable) sections);
        getFragmentManager().beginTransaction()
                .replace(R.id.orderCreateComponentsSubCart, OrderComponentSubCartList.class, bundle)
                .commit();
    }

    void loadSegment(View v){
        recyclerViewSegment = v.findViewById(R.id.orderCreateComponentsSubSectionRecyclerView);
        recyclerViewSegment.setLayoutManager(new LinearLayoutManager(getContext()));
        sectionListAdapter = new SectionListAdapter(getContext(),new ArrayList<>(sections),this);
        recyclerViewSegment.setAdapter(sectionListAdapter);
    }

    @Override
    public void onClick(View v) {
        OrderCreate orderCreate = ((OrderCreate) getActivity());
        if(v.getId() == R.id.orderCreateComponentsSubCancel){
            orderCreate.backFragment();
        }else if(v.getId() == R.id.orderCreateComponentsSubAdd){
            OrderCartList cartListFragment = (OrderCartList)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
            OrderComponentSubCartList subCartFragment = (OrderComponentSubCartList)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);

            double totalPayable = product.price;
            for(ComponentSection section:sections){
                totalPayable+=section.selectedItem.subTotal;
            }
            String category = "";
            boolean offer = false;
            if(product.properties != null){
                category = product.properties.category;
                offer = product.properties.offer;

            }

            CartItem item = new CartItem(0,product.productUUid,product.description,product.shortName,subCartFragment.sectionList,product.price,product.offerPrice,totalPayable,totalPayable,1, 1,"",product.productType,product.properties.print_order,product.discountable,offer,false, product.properties.kitchenItem,category,new ArrayList<>(), true, "CASH");

            cartListFragment.addToCart(item);
            orderCreate.setCategory(category);


            /*This line to back after add button click*/
            orderCreate.backFragment();

            /* THis is for staying in component page after add button click*/
//            Bundle bundle = new Bundle();
//            String _productData = GsonParser.getGsonParser().toJson(product);
//            bundle.putString("product", _productData);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.frameLayoutOrderCreateCategory, Components.class, bundle)
//                    .commit();


        }
    }
    @Override
    public void onClickAction() {
        listener.onUpdate();
    }

    public interface OnProductUpdateListener {
        void onUpdate();
    }

}