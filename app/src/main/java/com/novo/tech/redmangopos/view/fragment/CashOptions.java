package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;

import java.text.DecimalFormat;

public class CashOptions extends Fragment implements View.OnClickListener {

    AppCompatButton cashIn,cashOut;
    TextView balance,dateText;
    public double balanceAmount = 0.0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_cash_options, container, false);

        String optionType = requireArguments().getString("optionType");
        cashIn = v.findViewById(R.id.cashOptionsCashIn);
        cashOut = v.findViewById(R.id.cashOptionsCashOut);
        balance = v.findViewById(R.id.cashOptionsBalance);
        dateText = v.findViewById(R.id.cashOptionsTodayDate);
        cashIn.setOnClickListener(this);
        cashOut.setOnClickListener(this);
        dateText.setText(DateTimeHelper.getTime().substring(0,10));
        if(optionType.equals("CASH IN")){
            setEnable(cashIn);
            setDisable(cashOut);
        }else {
            setEnable(cashOut);
            setDisable(cashIn);
        }

        loadBalance();
        return  v;
    }
    void setEnable(Button button){
        button.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.LightGrey));
    }
    void setDisable(Button button){
        button.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.white));
    }
    public void loadBalance(){
        CashRepository dbCashManager = new CashRepository(getContext());
        balanceAmount = dbCashManager.getBalance();
        DecimalFormat df = new DecimalFormat("#,###.##");
        balance.setText("£ "+df.format(balanceAmount));
    }
    @Override
    public void onClick(View v) {
        if(v.getId() == cashIn.getId()){
            CashCalculator cashCalculator = (CashCalculator)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.cashActivityFrameLayoutCenter);
            cashCalculator.changeSubmitType("CASH IN");
            setEnable(cashIn);
            setDisable(cashOut);
        }else if(v.getId() == cashOut.getId()){
            CashCalculator cashCalculator = (CashCalculator)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.cashActivityFrameLayoutCenter);
            cashCalculator.changeSubmitType("CASH OUT");
            setEnable(cashOut);
            setDisable(cashIn);
        }
    }
}