package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.Address;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;
import com.novo.tech.redmangopos.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBCustomerManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBCustomerManager(Context c) {
        context = c;
    }

    public DBCustomerManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    public CustomerModel getCustomerData(int id) {
        open();
        CustomerModel customerModel = null;
        Cursor cursor = database.rawQuery("SELECT * from CUSTOMER WHERE id=?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                customerModel = getCustomerDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customerModel;
    }
    public CustomerModel getCustomerData(String customer_id) {
        open();
        CustomerModel customerModel = null;
        Cursor cursor = database.rawQuery("SELECT * from CUSTOMER WHERE consumer_uuid=?", new String[]{String.valueOf(customer_id)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                customerModel = getCustomerDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        customerModel.addresses = getAllCustomerAddress(String.valueOf(customerModel.dbId));
        return customerModel;
    }
    public List<CustomerModel> getAllCustomerData() {
        open();
        List<CustomerModel> customers = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" ORDER BY id DESC LIMIT 5000",null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    CustomerModel customerModel = getCustomerDataFromCursor(cursor);
                    customers.add(customerModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customers;
    }

    public List<CustomerModel> getCustomersBySearch(String query, int offset) {
        open();
        List<CustomerModel> customers = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE phone LIKE ? OR firstName LIKE ? OR lastName LIKE ? OR email LIKE ? LIMIT ?,20",new String[] {"%" + query + "%", "%" + query + "%", "%" + query + "%", "%" + query + "%", String.valueOf(offset)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    CustomerModel customerModel = getCustomerDataFromCursor(cursor);
                    customers.add(customerModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customers;
    }

    private CustomerModel getCustomerDataFromCursor(Cursor cursor){
        int id = cursor.getInt(cursor.getColumnIndex("id"));
        String  consumer_uuid = cursor.getString(cursor.getColumnIndex("consumer_uuid"));
        String  userName = cursor.getString(cursor.getColumnIndex("userName"));
        String  firstName = cursor.getString(cursor.getColumnIndex("firstName"));
        String  lastName = cursor.getString(cursor.getColumnIndex("lastName"));
        String  email = cursor.getString(cursor.getColumnIndex("email"));
        String  phone = cursor.getString(cursor.getColumnIndex("phone"));
        boolean cloudSubmitted = cursor.getInt(cursor.getColumnIndex("cloudSubmitted")) != 0;
        CustomerProfile profile = new CustomerProfile(phone,lastName,firstName,email);
        List<CustomerAddress> addresses = getAllCustomerAddress(String.valueOf(id));

        return new  CustomerModel(id,consumer_uuid,email,userName,profile,addresses,cloudSubmitted);
    }

    public int addNewCustomer(CustomerModel model,boolean cloudSync) {
        int id = 0;
        if(model.profile==null){
            return -1;
        }
        ContentValues data = new ContentValues();
        data.put("firstName",model.profile.first_name);
        data.put("lastName",model.profile.last_name);
        data.put("phone",model.profile.phone);
        data.put("email",model.profile.email);
        data.put("userName",model.username);
        data.put("consumer_uuid",model.consumer_uuid);
        data.put("cloudSubmitted",cloudSync);
        data.put("address",GsonParser.getGsonParser().toJson(model.addresses));
        open();
        id = (int)database.insert(DatabaseHelper.CUSTOMER_TABLE_NAME, null,data);
        close();
        return id;
    }
    public void updateCustomer(CustomerModel model,boolean submitted) {
        ContentValues data = new ContentValues();
        data.put("firstName",model.profile.first_name);
        data.put("lastName",model.profile.last_name);
        data.put("phone",model.profile.phone);
        data.put("email",model.profile.email);
        data.put("userName",model.username);
        data.put("consumer_uuid",model.consumer_uuid);
        data.put("cloudSubmitted",submitted);
        data.put("address",GsonParser.getGsonParser().toJson(model.addresses));
        open();
        database.update(DatabaseHelper.CUSTOMER_TABLE_NAME, data,"id=?",new String[]{String.valueOf(model.dbId)});
        close();
    }




    public CustomerModel getCustomerDataByPhone(String phone) {
        open();
        CustomerModel customerModel = null;
        Cursor cursor = database.rawQuery("SELECT *  from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE phone = ? OR consumer_uuid=?",new String[]{String.valueOf(phone),String.valueOf(phone)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                customerModel = getCustomerDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customerModel;
    }
    public int customerExist (String phone) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE phone = ? OR consumer_uuid=?",new String[]{String.valueOf(phone),String.valueOf(phone)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }
    
    public int customerExistWithMail(String email) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE email = ? ",new String[]{email});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }

    public int addNewAddress(CustomerAddress model,boolean isPrimary) {
        List<CustomerAddress> allAddress = getAllCustomerAddress(String.valueOf(model.customerDbId));
        if(allAddress.size()==0){
            isPrimary = true;
        }
        int id = 0;
        ContentValues data = new ContentValues();
        data.put("customerDbId",model.customerDbId);
        data.put("uuid",model.address_uuid);
        data.put("type",model.type);
        data.put("building",model.properties.building);
        data.put("streetName",model.properties.street_number+" "+model.properties.street_name);
        data.put("city",model.properties.city);
        data.put("state",model.properties.state);
        data.put("zip",model.properties.zip);
        data.put("isPrimary",isPrimary?1:0);
        open();
        id = (int)database.insert(DatabaseHelper.ADDRESS_TABLE_NAME, null,data);
        close();
        if(isPrimary){
            ContentValues _data = new ContentValues();

            _data.put("isPrimary",0);
            open();
            database.update(DatabaseHelper.ADDRESS_TABLE_NAME, _data,"customerDbId=? and id !=?",new String[]{String.valueOf(model.customerDbId),String.valueOf(id)});
            close();
        }

        return id;
    }
    public void updateAddress(CustomerAddress model) {
        CustomerAddress address = getPrimaryCustomerAddress(String.valueOf(model.customerDbId));
        if(address == null && !model.primary){
            model.primary = true;
        }

        ContentValues data = new ContentValues();
        data.put("uuid",model.address_uuid);
        data.put("type",model.type);
        data.put("building",model.properties.building);
        data.put("streetName",model.properties.street_number+" "+model.properties.street_name);
        data.put("city",model.properties.city);
        data.put("state",model.properties.state);
        data.put("zip",model.properties.zip);
        data.put("isPrimary",model.primary?1:0);
        open();
        database.update(DatabaseHelper.ADDRESS_TABLE_NAME, data,"id=?",new String[]{String.valueOf(model.dbId)});
        close();

        if(model.primary = true){
            ContentValues _data = new ContentValues();
            _data.put("isPrimary",0);
            open();
            database.update(DatabaseHelper.ADDRESS_TABLE_NAME, _data,"customerDbId=? and id !=?",new String[]{String.valueOf(model.customerDbId),String.valueOf(model.dbId)});
            close();
        }

    }

    public void setPrimaryAddress(int customerDbId,CustomerAddress model) {
        ContentValues data = new ContentValues();
        data.put("isPrimary",0);
        open();
        database.update(DatabaseHelper.ADDRESS_TABLE_NAME, data,"customerDbId=?",new String[]{String.valueOf(customerDbId)});

        data.put("isPrimary",1);
        database.update(DatabaseHelper.ADDRESS_TABLE_NAME, data,"id=?",new String[]{String.valueOf(model.dbId)});
        close();

    }


    public List<CustomerAddress> getAllCustomerAddress(String customerId) {
        open();
        List<CustomerAddress> customerAddressList = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * from "+DatabaseHelper.ADDRESS_TABLE_NAME +" WHERE customerDbId = ? ORDER BY isPrimary DESC LIMIT 5000", new String[]{customerId});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    CustomerAddress customerAddress = getCustomerAddressFromCursor(cursor);
                    customerAddressList.add(customerAddress);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customerAddressList;
    }
    public CustomerAddress getPrimaryCustomerAddress(String customerId) {
        open();
        CustomerAddress primaryAddress = null;
        Cursor cursor = database.rawQuery("SELECT * from "+DatabaseHelper.ADDRESS_TABLE_NAME +" WHERE customerDbId = ? and isPrimary=1", new String[]{customerId});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    primaryAddress = getCustomerAddressFromCursor(cursor);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return primaryAddress;
    }


    private CustomerAddress getCustomerAddressFromCursor(Cursor cursor){
        int customerDb = cursor.getInt(cursor.getColumnIndex("customerDbId"));
        int id = cursor.getInt(cursor.getColumnIndex("id"));
        String  address_uuid = cursor.getString(cursor.getColumnIndex("uuid"));
        String  type = cursor.getString(cursor.getColumnIndex("type"));
        String  building = cursor.getString(cursor.getColumnIndex("building"));
        String  streetName = cursor.getString(cursor.getColumnIndex("streetName"));
        String  city = cursor.getString(cursor.getColumnIndex("city"));
        String  state = cursor.getString(cursor.getColumnIndex("state"));
        String  zip = cursor.getString(cursor.getColumnIndex("zip"));
        int isPrimary = cursor.getInt(cursor.getColumnIndex("isPrimary"));
        CustomerAddressProperties addressProperties = new CustomerAddressProperties(
            zip,"United Kingdom",city,"",state,building,streetName
        );
        return new CustomerAddress(id,customerDb,address_uuid,type, isPrimary != 0,addressProperties);
    }
}
