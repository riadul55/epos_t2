package com.novo.tech.redmangopos.sync.sync_service;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.adminSession;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.view.activity.Setting;
import com.novo.tech.redmangopos.view.fragment.BusinessSetting;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UpdateOnlineOrderStatusAsync extends AsyncTask<Void, Void, String> {
    private Context context;
    private String status;
    private boolean isChecked;
    UpdateStatusListener listener;

    public UpdateOnlineOrderStatusAsync(Context context, String status,boolean isChecked, UpdateStatusListener listener) {
        this.context = context;
        this.status = status;
        this.isChecked = isChecked;
        this.listener = listener;
    }

    @Override


    protected String doInBackground(Void... voids) {
        JSONObject requestJson = new JSONObject();
        try {
//            "business_off_date": "2022-08-07",
//            "business_status": "open",
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = sdf.format(new Date());
            String previousDate = sdf.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24)));
            Log.e("currentDate", "" + currentDate);
            Log.e("previousDate", "" + previousDate);
            if(Objects.equals(status, "open")){
                requestJson.put("business_off_date", ""+previousDate);
            }else if(Objects.equals(status, "closed")){
                requestJson.put("business_off_date", ""+currentDate);
            }
//            requestJson.put("business_status", ""+status);
        } catch (Exception e) {
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, requestJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "config/global/parameter")
                .post(requestBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("providersession", providerSession)
                .addHeader("adminsession", adminSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (response != null) {
            if (response.code() == 200) {
                Log.e("Online order status code:", "" + response.code());
                Log.e("business status", "" + status);
                Log.e("business status", "" + isChecked);
                SharedPrefManager.setOnlineOrderStatus(context, isChecked);
                cacheClear();
            } else {
                String body = "Online order status code:" + response.code() + " | failed to update Online order status | " + response.message() + " | " + requestJson.toString() + " | url: " + BASE_URL +"config/global/parameter";
                Log.e("Error==>", body);
            }
        }else {
            Log.e("Error==>","response not found.");
        }
        return null;
    }
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null && s.equals("success")) {
            Log.e("updating==>", s);
        }
        listener.onUpdate();
    }

    void cacheClear(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(AppConstant.SITE_URL+"cache/forget")
                .get()
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (response != null) {
            if (response.code() == 200) {
                Log.e("cache-clear==>", "success");
            }
        }
    }


    public interface UpdateStatusListener {
        void onUpdate();
    }
}

