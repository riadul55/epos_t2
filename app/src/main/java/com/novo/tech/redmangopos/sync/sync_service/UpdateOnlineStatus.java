package com.novo.tech.redmangopos.sync.sync_service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.ViewModelProviders;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.room_db.view_models.OrderViewModel;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.DatabaseHelper;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.view.activity.Dashboard;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Timer;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class UpdateOnlineStatus extends AsyncTask<String, Void, OrderModel> {
    private Context context;
    private UpdateData data;

    public UpdateOnlineStatus(Context context, UpdateData data) {
        this.context = context;
        this.data = data;
    }

    @SuppressLint("WrongThread")
    @Override
    protected OrderModel doInBackground(String... strings) {


        try {
           DBOrderManager dbOrderManager = new DBOrderManager(context);
           dbOrderManager.open();
           OrderModel orderData = dbOrderManager.getOrderData(data.getOrderId());
           dbOrderManager.close();

        int serverId = orderData.serverId != 0 ? orderData.serverId : data.getServerId().isEmpty()? 0 : Integer.parseInt(data.getServerId());
        if (serverId != 0) {
            JSONObject jsonObject = new JSONObject();

            try {
                if (data.getStatus().equals("TIME_CHANGE")) {
                    if (orderData.currentDeliveryTime == null) {
                        orderData.currentDeliveryTime = DateTimeHelper.getTime();
                    }
                    Log.d("mmmmm", "Delivery time updateOrder: " + orderData.currentDeliveryTime);
                    if (orderData.currentDeliveryTime != null) {
                        jsonObject.put("current_delivery_timestamp", DateTimeHelper.formatLocalDateTimeForServer(orderData.currentDeliveryTime));
                    }
                    jsonObject.put("driver_provider_uuid", providerID);
                } else {
                    String orderStatus;
                    if (data.getStatus().equals("REVIEW")) {
                        orderStatus = data.getStatus();
                    } else if (orderData.order_status.equals("CLOSED")) {
                        orderStatus = "CLOSED";
                    } else if (orderData.order_status.equals("REFUNDED")) {
                        orderStatus = "CANCELLED";
                    } else {
                        orderStatus = data.getStatus();
                    }
                    jsonObject.put("order_status", orderStatus);
                    jsonObject.put("driver_provider_uuid", providerID);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("mmmmm", "updateOrder: " + jsonObject.toString());


            if (orderData.order_channel.equals("ONLINE") &&
                    orderData.order_status.equals("CLOSED") &&
                    orderData.bookingId != 0
            ) {
                for (CartItem item : orderData.items) {
                    JSONObject object = new JSONObject();
                    boolean added = false;
                    if (item.isLocalItem) {
                        try {
                            object.put("product_uuid", item.uuid);
                            object.put("product_type", item.type);
                            object.put("units", item.quantity);
                            object.put("comment", item.comment);
                            added = true;
                        } catch (Exception e) {}
                    } else {
                        if (item.quantity > item.quantityOrg) {
                            try {
                                object.put("product_uuid", item.uuid);
                                object.put("product_type", item.type);
                                object.put("units", item.quantity - item.quantityOrg);
                                object.put("comment", item.comment);
                                added = true;
                            } catch (Exception e) {
                            }
                        }
                    }

                    if (added && item.paymentMethod.equals("CASH")) {
                        addItemToOnline(serverId, object);
                    }
                }
            }

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
            Request request = new Request.Builder()
                    .url(BASE_URL + "order/" + serverId)
                    .patch(requestBody)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("ProviderSession", providerSession)
//                .addHeader("ProviderSession", "49cfb910-7c1e-4261-aed6-57afa4fd2e76")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response != null) {
                if (response.code() == 202) {
                    if (!data.getStatus().equals("TIME_CHANGE") && !data.getStatus().equals("REFUNDED")) {
                        new SendEmail(context, orderData, data.getStatus(), null).execute();
                    }
                    return orderData;
                } else if (response.code() != 400) {
                    String body = "updateOrder: code:" + response.code() + " | failed to update order status | " + response.message() + " | " + jsonObject.toString() + " | url: " + BASE_URL + "order/" + serverId;
                    new SendEmail(context, orderData, "ERROR", body).execute();
                }
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void addItemToOnline(int serverId, JSONObject object) {
        Log.e("itemObject==>", object.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, object.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+serverId+"/order_item")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
//                .addHeader("ProviderSession", "49cfb910-7c1e-4261-aed6-57afa4fd2e76")
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            if(response.code() == 201) {
            }
        }
    }

    @Override
    protected void onPostExecute(OrderModel orderModel) {
        super.onPostExecute(orderModel);
        if (orderModel != null && data.getStatus().equals("REVIEW") && !orderModel.order_channel.equals("ONLINE")) {
            long dateTime = System.currentTimeMillis();
            SharedPrefManager.setUpdateData(context, new UpdateData(data.getOrderId(), data.getServerId() + "", "ONLINE", "CLOSED", "" + dateTime));
        }
        if (orderModel != null && orderModel.order_channel != null &&
                (data.getStatus().equals("TIME_CHANGE") || data.getStatus().equals("REFUNDED"))) {
            new SendEmail(context, orderModel, data.getStatus(), null).execute();
        }
        if (data.getStatus().equals("CLOSED") || data.getStatus().equals("REFUNDED")) {

            new UpdateOnlineTransaction(context, data).execute();
        } else {
            SharedPrefManager.removeUpdatedOrder(context, data.getOrderId(), data.getStatus());
        }
    }
}
