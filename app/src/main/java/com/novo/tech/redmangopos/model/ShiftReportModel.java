package com.novo.tech.redmangopos.model;

public class ShiftReportModel {
    public int onlineOrderTotal,localOrderTotal;
    public double diningTotalAmount,deliveredTotalAmount,takeOutOrderTotal;

    public ShiftReportModel(int onlineOrderTotal, int localOrderTotal, double diningTotalAmount, double deliveredTotalAmount, double takeOutOrderTotal) {
        this.onlineOrderTotal = onlineOrderTotal;
        this.localOrderTotal = localOrderTotal;
        this.diningTotalAmount = diningTotalAmount;
        this.deliveredTotalAmount = deliveredTotalAmount;
        this.takeOutOrderTotal = takeOutOrderTotal;

    }
}
