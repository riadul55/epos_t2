package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.view.fragment.CustomerInput;
import com.novo.tech.redmangopos.view.fragment.CustomerOrderList;
import com.novo.tech.redmangopos.view.fragment.CustomerSearch;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;

import java.util.ArrayList;
import java.util.List;

public class CustomerSearchAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<CustomerModel> customerModelList;
    CustomerRepository customerManager;

    public CustomerSearchAdapter(Context applicationContext, List<CustomerModel> customerModelList) {
        this.context = applicationContext;
        this.customerModelList = customerModelList;
        inflater = (LayoutInflater.from(applicationContext));
        customerManager = new CustomerRepository(applicationContext);
    }

    public void updateList(List<CustomerModel> data) {
        if (customerModelList == null) {
            customerModelList = new ArrayList<>();
        }
        customerModelList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return customerModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.item_customer_select, null);
        CustomerModel model = customerModelList.get(position);
        TextView customerName,customerPhone;
        AppCompatImageButton edit,select;
        customerName = view.findViewById(R.id.customerItemName);
        customerPhone = view.findViewById(R.id.customerItemPhone);
        edit = view.findViewById(R.id.edit);
        select = view.findViewById(R.id.select);
        if(model.profile != null){
            customerName.setText((model.profile.first_name+" "+model.profile.last_name).replaceAll("null",""));
            customerPhone.setText((model.profile.phone).replaceAll("null",""));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerOrderList customerOrderList = (CustomerOrderList)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentRight);
                customerOrderList.getCustomerOrdersFromServer(String.valueOf(model.dbId));
              //  displayCustomer(model);
            }
        });
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerSearch customerSearch = (CustomerSearch)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerSelect);
                CustomerInput customerInput = (CustomerInput)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
                customerSearch.backToOrder(model,customerInput.orderType,customerManager.getPrimaryCustomerAddress(String.valueOf(model.dbId)));
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayCustomer(model);

            }
        });
        return view;
    }


    void displayCustomer(CustomerModel model){
        CustomerInput customerInput = (CustomerInput) ((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
        if(customerInput!=null){
            customerInput.customerModel = model;
            // customerInput.addNewCustomerFromDialog(model);
            customerInput.selectButton.setVisibility(View.VISIBLE);
            if(model.profile!=null){
                customerInput.firstName.setText(model.profile.first_name);
                customerInput.lastName.setText(model.profile.last_name);
                customerInput.phone.setText(model.profile.phone);
                customerInput.email.setText(model.email);
//
//                       customerInput.et_first_name.setText(model.profile.first_name);
//                       customerInput.et_last_name.setText(model.profile.last_name);
//                       customerInput.et_phn_number.setText(model.profile.phone);
//                       customerInput.et_email_address.setText(model.email);
            }

            CustomerAddress address = customerManager.getPrimaryCustomerAddress(String.valueOf(model.dbId));
            if(address != null){
                customerInput.primaryCheckbox.setChecked(true);
                customerInput.selectedAddress = address;
                customerInput.primaryCheckbox.setChecked(address.primary);
                if(address.properties != null){
                    customerInput.street.setText(address.properties.street_number+" "+address.properties.street_name);
                    customerInput.town.setText(address.properties.city);
                    customerInput.zip.setText(address.properties.zip);
                    customerInput.houseNumber.setText(address.properties.building);

                }
            }else{
                customerInput.primaryCheckbox.setChecked(false);
                customerInput.selectedAddress.dbId = 0;
                customerInput.street.setText("");
                customerInput.town.setText("");
                customerInput.zip.setText("");
                customerInput.houseNumber.setText("");
            }
            customerInput.selectedAddress.customerDbId = model.dbId;
            customerInput.loadCustomerAddress(String.valueOf(model.dbId));
        }
    }
}
