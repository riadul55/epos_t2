package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.CategoryDetailsCallBack;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.model.CategoryModel;

import org.json.JSONArray;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

/**
 * This class created by alamin
 */
public class CategoryDetailsHelper extends AsyncTask<String,Void, ArrayList> {
    ArrayList<CategoryDetailsModel> categoryDetailsModelArrayListH = new ArrayList<CategoryDetailsModel>();
    CategoryDetailsCallBack callBack;
    String category_value;


    public CategoryDetailsHelper(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList, CategoryDetailsCallBack callBack, String category_value) {
        this.categoryDetailsModelArrayListH = categoryDetailsModelArrayList;
        this.callBack = callBack;
        this.category_value = category_value;
    }

    @Override
    protected ArrayList<CategoryDetailsModel> doInBackground(String... strings) {
        return getCategoryDetails(category_value);
    }

    private ArrayList<CategoryDetailsModel> getCategoryDetails(String category_value) {

        OkHttpClient client = new OkHttpClient();
        ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url(BASE_URL+"product?product_type=ITEM&show_hierarchy=true&provider_uuid=a7033019-3dee-457b-a9da-50df238b3c9f&categories="+category_value+"&show_files=true&page_items=100&current_page=1")
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    CategoryDetailsModel obj = CategoryDetailsModel.fromJSON(jsonArray.optJSONObject(i));
                    categoryDetailsModelArrayList.add(obj);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        this.categoryDetailsModelArrayListH = categoryDetailsModelArrayList;
        return categoryDetailsModelArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList arrayList) {
        callBack.onCategoryDetailsResponse(categoryDetailsModelArrayListH);
        super.onPostExecute(arrayList);
    }
}
