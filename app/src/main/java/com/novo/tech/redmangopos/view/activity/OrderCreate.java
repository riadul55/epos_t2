package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;
import com.novo.tech.redmangopos.view.fragment.Components;
import com.novo.tech.redmangopos.view.fragment.ComponentsEdit;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

public class OrderCreate extends BaseActivity implements View.OnClickListener, OrderCartList.OnCartReadyListener, CategoryAndProduct.OnProductUpdateListener,Components.OnProductUpdateListener{
    LinearLayout btnBack,btnCheckout,btnVoid,btnClear,btnDynamic,btnSave,btnPrint;
    public String category = "all";
    public Property topCategory;
    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_create);
        setView();
        openCartFragment();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void setView(){
        btnBack = findViewById(R.id.orderCreateBack);
        btnCheckout = findViewById(R.id.orderCreateCheckout);
        btnVoid = findViewById(R.id.orderCreateVoid);
        btnClear = findViewById(R.id.orderCreateClear);
        btnDynamic = findViewById(R.id.orderCreateNewItemDynamic);
        btnPrint = findViewById(R.id.orderCreatePrint);
        btnSave = findViewById(R.id.orderCreateSave);
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);
        callingPanel = findViewById(R.id.CallingView);


        btnBack.setOnClickListener(this);
        btnCheckout.setOnClickListener(this);
        btnVoid.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnDynamic.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnPrint.setOnClickListener(this);
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });
    }

    void openCartFragment(){
        Bundle b = getIntent().getExtras();
        int orderModelId = -1;
        String orderType = null; // or other values
        String bookingId = b.getString("bookingId");
        TableBookingModel table = (TableBookingModel) b.getSerializable("table");
        CustomerModel customer = (CustomerModel) b.getSerializable("customerModel");
        CustomerAddress shippingAddress = (CustomerAddress) b.getSerializable("shippingAddress");

        orderType = b.getString("orderType");
        orderModelId = b.getInt("orderModel");

        if(orderType != null ){
            //new order
            btnVoid.setClickable(false);
            btnVoid.setAlpha(0.5f);
        }
        Bundle bundle = new Bundle();
        bundle.putString("orderType", orderType);
        bundle.putInt("OrderModel",orderModelId);
        bundle.putString("bookingId", bookingId);
        bundle.putSerializable("table",table);
        bundle.putSerializable("customerModel",customer);
        bundle.putSerializable("shippingAddress",shippingAddress);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderCreateLeft, OrderCartList.class, bundle)
                .commit();
    }
    public void openProductFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putSerializable("topCategory", topCategory);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class,bundle)
                .commit();
    }
    public void replaceWithProductFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putSerializable("topCategory", topCategory);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class,bundle)
                .commit();
    }
    public void openComponentEditPage(CartItem cartItem) {
        Bundle bundle = new Bundle();
        Product pro = SharedPrefManager.getProductDetails(getApplicationContext(),cartItem.uuid);
        String _productData = GsonParser.getGsonParser().toJson(pro);
        bundle.putString("product", _productData);
        bundle.putSerializable("cartItem", cartItem);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, ComponentsEdit.class, bundle)
                .commit();
    }

    @Override
    public void onClick(View v) {
        OrderCartList cartListFragment = (OrderCartList)getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
        int id = v.getId();
        if(id == R.id.orderCreateBack){
            new Thread(this::backFragment).start();
        }else if(id == R.id.orderCreateCheckout){
            cartListFragment.openCheckoutPage();
        }else if(id == R.id.orderCreateVoid){
            cartListFragment.voidOrderAlert();
        }else if(id == R.id.orderCreateClear){
            cartListFragment.clearCart();
            replaceWithProductFragment();
        }else if(id == R.id.orderCreateSave){
            cartListFragment.saveTheOrder(false);
        }else if(id == R.id.orderCreatePrint){
            cartListFragment.saveTheOrder(true);
        }else if(id == btnDynamic.getId()){
            cartListFragment.getDynamicProduct();
        }
    }
    public void backFragment(){
//        Trace myTrace = FirebasePerformance.getInstance().newTrace("OrderCreateBackPress");
//        myTrace.start();

        FragmentManager fm = getSupportFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.frameLayoutOrderCreateCategory);
        if(currentFragment instanceof Components){
            backToCategory();
        }else{
            OrderCartList cartListFragment = (OrderCartList)getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
            OrderModel orderModel = cartListFragment.orderModel;
            if(orderModel.items!=null){
                if(orderModel.items.size()!=0 && orderModel.subTotal!=0){
                  //  exitAlert(orderModel);
                    createUpdateOnBackPress(orderModel);
                }else{
                    //finish();
                    backToHomeClean();
                }
            }else {
                //finish();
                backToHomeClean();
            }
        }

//        myTrace.stop();
    }

    public void backToCategory() {
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class, bundle)
                .commit();
    }
    public void setCategory(String catName) {
        category = catName;
        Log.e("Category", category);
    }

    void backToHomeClean(){
        finish();
//        Intent gotoScreenVar = new Intent(this, Dashboard.class);
//        gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        gotoScreenVar.putExtra("shift","EXISTING");
//        startActivity(gotoScreenVar);
    }
/*
    void exitAlert(OrderModel orderModel){
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderCreate.this);
        builder.setTitle("You have unsaved item in your cert, Do you want to save order?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createUpdateOnBackPress(orderModel);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }

 */
    public void createUpdateOnBackPress(OrderModel orderModel){
        orderModel.items.removeIf((a) -> a.paymentMethod.equals("CARD"));
        orderModel.total = orderModel.subTotal;
        orderModel.total = (orderModel.subTotal-orderModel.discountAmount)+orderModel.deliveryCharge+orderModel.tips;
        if (orderModel.orderDate == null)
            orderModel.orderDateTime = DateTimeHelper.getTime();

        orderModel.db_id = SharedPrefManager.createUpdateOrder2(OrderCreate.this, orderModel);;
//        orderModel.db_id = SharedPrefManager.createUpdateOrder(OrderCreate.this,orderModel);
        backToHomeClean();
    }


    @Override
    public void onChanged() {

    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }

    @Override
    public void onUpdate() {
        Log.e("onUpdate", "updating...");
//        recreate();
        replaceWithProductFragment();
    }

    @Override
    public void onCartReady() {
        openProductFragment();
//        try {
//            CategoryAndProduct fragment = (CategoryAndProduct) getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);
//            if (fragment != null){
//                fragment.loadAfterCartReady();
//            }
//        } catch (Exception e) {
//            Log.e("Error==>", e.getMessage());
//        }
    }
}