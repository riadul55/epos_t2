package com.novo.tech.redmangopos.adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.UpdateData;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CacheListAdapter extends RecyclerView.Adapter<CacheListAdapter.ViewHolder> {
    List<UpdateData> dataList;

    public CacheListAdapter(List<UpdateData> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @NotNull
    @Override
    public CacheListAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cache_data, parent, false);
        return new CacheListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CacheListAdapter.ViewHolder holder, int position) {
        holder.bindData(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textId;
        TextView textServerId;
        TextView textChannel;
        TextView textStatus;
        TextView textTime;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textId = itemView.findViewById(R.id.textId);
            textServerId = itemView.findViewById(R.id.textServerId);
            textChannel = itemView.findViewById(R.id.textChannel);
            textStatus = itemView.findViewById(R.id.textStatus);
            textTime = itemView.findViewById(R.id.textTime);
        }

        void bindData(UpdateData data) {
            textId.setText(String.valueOf(data.getOrderId()));
            textServerId.setText(data.getServerId());
            textChannel.setText(data.getOrderChannel());
            textStatus.setText(data.getStatus());
            textTime.setText(getDate(Long.parseLong(data.getDateTime())));
        }

        private String getDate(long time) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            sdf.setTimeZone(cal.getTimeZone());
            return sdf.format(new Date(time));
        }
    }
}
