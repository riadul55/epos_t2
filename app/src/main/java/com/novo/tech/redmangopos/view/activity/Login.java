package com.novo.tech.redmangopos.view.activity;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.LoginKeyboardAdapter;
import com.novo.tech.redmangopos.customerdisplay.CustomerDisplayBlank;
import com.novo.tech.redmangopos.customerdisplay.CustomerDisplayHelper;
import com.novo.tech.redmangopos.databinding.ActivityLoginBinding;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.ShiftModel;
import com.novo.tech.redmangopos.room_db.repositories.ShiftRepository;
import com.novo.tech.redmangopos.storage.DBShiftManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Login extends AppCompatActivity{
    EditText inputFiled;
    public Display presentationDisplay;
    public CustomerDisplayBlank customerDisplay;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    final int WRITE_REQUEST_CODE = 100;
    private ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        presentationDisplay = CustomerDisplayHelper.getPresentationDisplays(Login.this);
        if(presentationDisplay != null){
            customerDisplay = new CustomerDisplayBlank(Login.this, presentationDisplay);
            customerDisplay.show();
        }

        inputFiled = findViewById(R.id.loginInputField);
        inputFiled.setShowSoftInputOnFocus(false);
        inputFiled.setFocusable(false);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        setUpGridView();
       // requestPermissions(permissions, WRITE_REQUEST_CODE);

        binding.loginButton.setOnClickListener(v -> {
            if(providerSession == null){
                login();
            }else if(inputFiled.getText().toString().equals("0000")){
                checkShift();
            }else{
                Toast.makeText(Login.this,"Login Failed",Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
    private void setUpGridView() {
        String[] data = {"1", "2", "3","4", "5", "6","7", "8", "9","","0","."};
        LoginKeyboardAdapter adapter = new LoginKeyboardAdapter(Login.this,data);
        binding.loginInputGridVIew.setAdapter(adapter);
    }
    public void onInput(String value){
        String str = inputFiled.getText().toString();
        if(!value.equals("")){
                str+=value;
        }else{
            if(str.length() != 0){
                str = str.substring(0,str.length()-1);
            }
        }
        inputFiled.setText(str);
    }



    void login(){
        checkShift();
    }
    void checkShift(){

        ShiftRepository dbShiftManager =  new ShiftRepository(getApplicationContext());
        ShiftModel shift = dbShiftManager.getCurrentShift();
        if(shift == null){
            dbShiftManager.vacuumDb();
            showShiftOpeningMessage(dbShiftManager.getLastShift());
        }else{
            boolean today = false;
            long _now = Integer.parseInt(new SimpleDateFormat("yyyMMdd",Locale.getDefault()).format(new java.util.Date()));
            long _open = Integer.parseInt(new SimpleDateFormat("yyyMMdd",Locale.getDefault()).format(shift.openTime));
            if(_now == _open){
                today = true;
            }
            int _closeTime = 2400;
            if(SharedPrefManager.getShiftCloseTime(Login.this) != null){
                _closeTime = Integer.parseInt(new SimpleDateFormat("HHmm",Locale.getDefault()).format(SharedPrefManager.getShiftCloseTime(Login.this)));
            }
            int nowTime = Integer.parseInt(new SimpleDateFormat("HHmm",Locale.getDefault()).format(new java.util.Date())); //1
            long openTIme = Integer.parseInt(new SimpleDateFormat("HHmm",Locale.getDefault()).format(shift.openTime));

            if(today){
                if(openTIme < _closeTime && _closeTime < nowTime){
                    dbShiftManager.closeShift(shift.id);
                    dbShiftManager.vacuumDb();
                    showShiftOpeningMessage(dbShiftManager.getLastShift());
                }else{
                    openDashboard("EXISTING");
                }
            }else{
                if(_now-_open>1 || nowTime > _closeTime){
                    dbShiftManager.closeShift(shift.id);
                    dbShiftManager.vacuumDb();
                    showShiftOpeningMessage(dbShiftManager.getLastShift());
                }else{
                    openDashboard("EXISTING");
                }
            }
        }
    }
    void openNewShift(){
        ShiftRepository dbShiftManager =  new ShiftRepository(getApplicationContext());
        int shift = dbShiftManager.openShift();
        Log.d("mmmmm", "openNewShift: "+shift);
        if(shift!=-1)
            SharedPrefManager.setCurrentShift(getApplicationContext(),shift);
        openDashboard("NEW");

    }

    void openDashboard(String newShift){
        Intent intent = new Intent(Login.this, Dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("shift",newShift);
        intent.putExtra("orders",0);
        startActivity(intent);
        finish();
    }

    void showShiftOpeningMessage(String lastTime){
        String str = "Would you like to start new shift ?";
        String lastCloseDate = lastTime;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyy hh:mm a",Locale.getDefault());
        if(lastTime != null){
            try {
                lastCloseDate =  newFormat.format(format.parse(lastTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(lastCloseDate.substring(0,10).equals(DateTimeHelper.getTime().substring(0,10))){
                lastCloseDate = " Today "+lastCloseDate.substring(11);
            }
            str = "Previous shift was ended at "+lastCloseDate+". "+str;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setMessage(str);
        builder.setTitle("Confirmation");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openNewShift();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

}