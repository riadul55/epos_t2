package com.novo.tech.redmangopos.view.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.AddressListAdapter;
import com.novo.tech.redmangopos.adapter.CustomerAddressListAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperNameAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperPhoneAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperZipAdapter;
import com.novo.tech.redmangopos.callback.CustomerSelectCallBack;
import com.novo.tech.redmangopos.callback.ZipCallBack;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;
import com.novo.tech.redmangopos.model.JsonAddress;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.sync.AddressApi;
import com.novo.tech.redmangopos.sync.ZipCodeHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CustomerInput extends Fragment implements ZipCallBack {

    public EditText lastName,email,city,houseNumber,town,street;
    public AutoCompleteTextView phone,firstName;
    public AutoCompleteTextView addressSelect;
    public Button saveBtn,selectButton, addressSearch;
    public ImageButton addressSearchImg;
    public CustomerModel customerModel;
    public AutoCompleteTextView zip;
    CustomerSearch customerSearch;
    ArrayAdapter houseAdapter;
    CustomerHelperPhoneAdapter customerHelperPhoneAdapter;
    CustomerHelperNameAdapter customerHelperNameAdapter;
    CustomerHelperZipAdapter customerHelperZipAdapter;
    List<String> zipList = new ArrayList<String>();
    JsEvaluator jsEvaluator;
    String preStr="";
    Context context;
    CustomerRepository customerManager = new CustomerRepository(getContext());
    CustomerOrderList customerOrderList;
    LinearLayout buttonOrderType;
    RadioGroup orderTypeRadio;
    public String orderType="";
    String customerPhoneNo;
    RecyclerView addressRecyclerView;
    CustomerAddressListAdapter addressListAdapter;
    AppCompatButton addNewButton;
    List<CustomerAddress> addressList = new ArrayList<>();
    public CheckBox primaryCheckbox;
    boolean adding = false;
    public CustomerAddress selectedAddress = new CustomerAddress(
            0,0,"","",false,
            new CustomerAddressProperties(
                    "","","","","","",""
            )
    );
    CustomerRepository dbCustomerManager;
    private List<CustomerModel> customerData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_customer_input, container, false);
        dbCustomerManager = new CustomerRepository(getContext());
        customerPhoneNo = requireArguments().getString("customerPhone","");
        adding = false;
        context = getActivity();
        firstName = v.findViewById(R.id.customerInputFirstName);
        lastName = v.findViewById(R.id.customerInputLastName);
        phone = v.findViewById(R.id.customerInputPhone);
        email = v.findViewById(R.id.customerInputEmail);
        street = v.findViewById(R.id.customerInputStreetName);
        city = v.findViewById(R.id.customerInputCity);
        zip = v.findViewById(R.id.customerInputZip);
        town = v.findViewById(R.id.customerInputCity);
        houseNumber = v.findViewById(R.id.customerInputHouseNumber);
        saveBtn = v.findViewById(R.id.customerSave);
        addNewButton = v.findViewById(R.id.addNewButton);
        buttonOrderType = v.findViewById(R.id.buttonOrderType);
        selectButton = v.findViewById(R.id.customerSelect);
        orderTypeRadio = v.findViewById(R.id.orderTypeRadio);
        addressSearch = v.findViewById(R.id.addressSearch);
        addressSearchImg = v.findViewById(R.id.addressSearchImg);
        addressSelect = v.findViewById(R.id.addressSelect);
        addressRecyclerView = v.findViewById(R.id.addressRecyclerView);
        primaryCheckbox = v.findViewById(R.id.primaryCheckBox);
        customerSearch = (CustomerSearch) (getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerSelect);
        customerOrderList = (CustomerOrderList) (getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentRight);
        addressListAdapter = new CustomerAddressListAdapter(getContext(),addressList);
        addressRecyclerView.setAdapter(addressListAdapter);
        addressRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if(customerModel!=null){
            loadCustomerAddress(String.valueOf(customerModel.dbId));
        }else{
            addNewButton.setVisibility(View.GONE);
        }
        jsEvaluator = new JsEvaluator(getContext());

        firstName.setEnabled(true);
        buttonOrderType.setOnClickListener(view -> { });

        if(customerPhoneNo.isEmpty()){
            buttonOrderType.setVisibility(View.GONE);
        }else{
            orderTypeRadio.findViewById(R.id.deliveryRadio).setSelected(true);
            orderType = "DELIVERY";
            phone.setText(customerPhoneNo);
            selectButton.setVisibility(View.GONE);
        }

        orderTypeRadio.setOnCheckedChangeListener((group, checkedId) -> {
            if(checkedId == R.id.deliveryRadio){
                orderType = "DELIVERY";
            }else if(checkedId == R.id.collectionRadio){
                orderType = "COLLECTION";
            }else if(checkedId == R.id.takeOutRadio){
                orderType = "TAKEOUT";
            }
        });
        addNewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedAddress.dbId = 0;
                selectedAddress.customerDbId = 0;
                primaryCheckbox.setChecked(false);

                street.setText("");
                town.setText("");
                zip.setText("");
                houseNumber.setText("");
            }
        });
        saveBtn.setOnClickListener(v1 -> {
                    try {
                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    addCustomer();

                }
        );
        selectButton.setOnClickListener(view -> {
            if(customerModel != null){
                CustomerAddressProperties addressProperties = new CustomerAddressProperties(
                        zip.getText().toString(),
                        "United Kingdom",
                        city.getText().toString(),
                        "",
                        "",
                        houseNumber.getText().toString(),
                        street.getText().toString()
                );
                selectedAddress.properties = addressProperties;
                customerSearch.backToOrder(customerModel,orderType,selectedAddress);
            }
        });
        email.setOnFocusChangeListener((v12, hasFocus) -> {
            if(!hasFocus && !email.getText().toString().isEmpty()){
                CustomerRepository manager = new CustomerRepository(getContext());
                int id  = manager.customerExistWithMail(email.getText().toString());
                if(id > 0){
                    email.setError("Email Already Exist");
                }
            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customerManager = new CustomerRepository(getContext());
                List<CustomerModel> customerModelList =  customerManager.getCustomersBySearch(s.toString(), 0);
                customerModelList.removeIf((a)->{
                    if(a.profile==null){
                        return true;
                    } else if(a.profile.first_name.isEmpty()){
                        return true;
                    }else if(a.profile.phone.isEmpty()){
                        return true;
                    }else return false;
                });
                customerData = customerModelList;
                customerHelperPhoneAdapter = new CustomerHelperPhoneAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, customerModelList, new CustomerSelectCallBack() {
                    @Override
                    public void onCustomerSelect(CustomerModel customerModel) {
                        onCustomerSelectFromSuggestion(customerModel);
                        phone.setSelection(phone.getText().toString().length());
                        phone.dismissDropDown();
                    }
                });
                phone.setAdapter(customerHelperPhoneAdapter);
                phone.setThreshold(3);


                customerHelperNameAdapter = new CustomerHelperNameAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, customerModelList, new CustomerSelectCallBack() {
                    @Override
                    public void onCustomerSelect(CustomerModel customerModel) {
                        onCustomerSelectFromSuggestion(customerModel);
                        firstName.setSelection(firstName.getText().toString().length());
                        firstName.dismissDropDown();
                    }
                });
                firstName.setAdapter(customerHelperNameAdapter);
                firstName.setThreshold(100);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        addressSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadHouse();
            }
        });
        addressSearchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadHouse();
            }
        });


        zip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() > 1 && !preStr.equals(s.toString())){
                    preStr = s.toString();
                    loadData(s.toString());

                    Log.e(preStr, s.toString());
                    //callFormatPostalCode(s.toString());
//                    loadData(s.toString());
                }
                if (s.length()>=3){
                  //  callFormatPostalCode(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("mmmmm", "afterTextChanged: called");
            }
        });

        zip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loadHouse();
            }
        });

//        primaryCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if(b){
//                    if(customerModel!=null && selectedAddress != null){
//                        if(selectedAddress.dbId != 0){
//                            dbCustomerManager.setPrimaryAddress(customerModel.dbId,selectedAddress);
//                            selectedAddress.primary = true;
//                        }
//
//                    }
//                }else{
//                    selectedAddress.primary = false;
//                    Toast.makeText(getContext(),"User must have a primary address, this will be primary address if user don't have any primary address",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        return  v;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public CustomerModel getOrdersByPhone(String phone, List<CustomerModel> data) {
        for (CustomerModel item : data) {
            if (item.profile.phone.equals(phone)) {
                return item;
            }
        }
        return null;
    }

    public void loadCustomerAddress(String dbID){
        addressList.clear();
        try{
            addressList.addAll(customerManager.getAllCustomerAddress(dbID));
        }catch (Exception e){
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        addressListAdapter = new CustomerAddressListAdapter(getContext(),addressList);
        addressRecyclerView.setAdapter(addressListAdapter);
        if(customerModel!=null){
            addNewButton.setVisibility(View.VISIBLE);
        }else{
            addNewButton.setVisibility(View.GONE);
        }

    }
    private void loadHouse(){
        Log.d("ppppp", "loadHouse: calling");

        jsEvaluator.callFunction("function myFunction(a) { return a.replace(/(\\S*)\\s*(\\d)/, \"$1 $2\"); }",
                new JsCallback() {
                    @Override
                    public void onResult(String result) {
                        zip.setText(result.toUpperCase());
                        zip.setSelection(zip.getText().length());

                        zip.dismissDropDown();
                        zip.clearFocus();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        // Process JavaScript error here.
                        // This method is called in the UI thread.
                    }
                }, "myFunction", zip.getText().toString());

        new AddressApi(context, zip.getText().toString(),this).execute();
    }

    void onCustomerSelectFromSuggestion(CustomerModel model){
        customerModel = model;
        selectedAddress.customerDbId = model.dbId;
        if(model.profile!=null){
            firstName.setText(model.profile.first_name);
            lastName.setText(model.profile.last_name);
            phone.setText(model.profile.phone);
            email.setText(model.email);
        }

        CustomerAddress address = dbCustomerManager.getPrimaryCustomerAddress(String.valueOf(model.dbId));
        if(address != null){
            selectedAddress = address;
            if(address.properties != null){
                street.setText(address.properties.street_number+" "+address.properties.street_name);
                town.setText(address.properties.city);
                zip.setText(address.properties.zip);
                houseNumber.setText(address.properties.building);
            }
        }
        customerOrderList.getCustomerOrdersFromServer(String.valueOf(customerModel.dbId));
        selectButton.setVisibility(View.VISIBLE);
        if(customerPhoneNo.isEmpty()){
            buttonOrderType.setVisibility(View.GONE);

        }else{
            buttonOrderType.setVisibility(View.VISIBLE);
        }
        loadCustomerAddress(String.valueOf(model.dbId));

    }

    void loadData(String str){
        new ZipCodeHelper(getContext(), str,this).execute();;
    }

    void addCustomer(){
        if (!phone.getText().toString().isEmpty() || !firstName.getText().toString().isEmpty()) {

            CustomerRepository manager = new CustomerRepository(getContext());

            CustomerRepository dbCustomerManager = new CustomerRepository(getContext());
            CustomerProfile profile = new CustomerProfile(
                    phone.getText().toString(),
                    lastName.getText().toString(),
                    firstName.getText().toString(),
                    email.getText().toString()
            );
            CustomerAddressProperties addressProperties = new CustomerAddressProperties(
                    zip.getText().toString(),
                    "United Kingdom",
                    city.getText().toString(),
                    "",
                    "",
                    houseNumber.getText().toString(),
                    street.getText().toString()
            );
            selectedAddress.properties = addressProperties;
            selectedAddress.primary = primaryCheckbox.isChecked();

            CustomerModel customer = new CustomerModel(
                    -1,
                    null,
                    email.getText().toString(),
                    email.getText().toString(),
                    profile,
                    addressList,
                    false
            );


            int id = 0;
            if(customerModel == null){

                if(!phone.getText().toString().isEmpty()) {
                    id = dbCustomerManager.customerExist(phone.getText().toString());
                }

                if(id < 1){
                    if(!email.getText().toString().isEmpty()){
                        int idEmail  = manager.customerExistWithMail(email.getText().toString());
                        if(idEmail > 0){
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Email Already Exist. Please use existing customer profile or remove email address from the field for guest checkout")
                                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .create()
                                    .show();
                            return;
                        }else{
                            customer.dbId = dbCustomerManager.addNewCustomer(customer,false);
                            backToOrder(customer,selectedAddress);
                            clear();                        }
                    }else{
                        customer.dbId = dbCustomerManager.addNewCustomer(customer,false);
                        backToOrder(customer,selectedAddress);
                        clear();
                    }
                }else{
                    customerSearch.customerNamePhone.setText(phone.getText().toString());
                    customerSearch.offset = 0;
                    customerSearch.getCustomerList(phone.getText().toString());
                    Toast.makeText(getContext(),"Phone Number Already Exist",Toast.LENGTH_LONG).show();
                }

            }else{
                customer.dbId = customerModel.dbId;
                if(customerModel.consumer_uuid != null){
                    customer.consumer_uuid = customerModel.consumer_uuid;
                }
                customerModel = customer;
                dbCustomerManager.updateCustomer(customer,false);
                //clear();
                if(!customerPhoneNo.isEmpty()){

                    backToOrder(customer,selectedAddress);
                }

            }

            if(selectedAddress.dbId == 0){
                selectedAddress.customerDbId = customer.dbId;
                dbCustomerManager.addNewAddress(selectedAddress,primaryCheckbox.isChecked());
            }else {
                dbCustomerManager.updateAddress(selectedAddress);
            }

            customerSearch.customerNamePhone.setText("");
            customerSearch.offset = 0;
            customerSearch.setCustomerListView();
            loadCustomerAddress(String.valueOf(customer.dbId));
        } else {
            Toast.makeText(getContext(),"Phone Number Or Name Can't be empty",Toast.LENGTH_LONG).show();
        }
    }
    public void backToOrder(CustomerModel customerModel,CustomerAddress _selectedAddress){
        customerSearch.backToOrder(customerModel,orderType,_selectedAddress);
//        if(customerModel.profile != null){
//            CustomerProfile profile = customerModel.profile;
//            if((!profile.first_name.isEmpty() || !profile.last_name.isEmpty() ) && !profile.phone.isEmpty()){
//
//            }
//        }
    }
    public void clear(){
        customerModel = null;
        firstName.setText("");
        lastName.setText("");
        phone.setText("");
        email.setText("");
        street.setText("");
        city.setText("");
        zip.setText("");
        houseNumber.setText("");
    }

    @Override
    public void onResponseFromServer(List<String> str) {
        customerHelperZipAdapter = new CustomerHelperZipAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, str);
        zip.setAdapter(customerHelperZipAdapter);
        if(getContext() !=null && str != null && str.size() > 0) {
            if (zipList.size() == 0) {
                zip.dismissDropDown();
            } else {
                zip.showDropDown();
            }
            zipList = str;
        }
    }

    @Override
    public void onAddressResponse(List<JsonAddress> data) {
        List<String> addressList = new ArrayList<>();
        for (int i=0;i<data.size();i++) {
            JsonAddress address = data.get(i);
            StringBuilder builder = new StringBuilder();
            if (address.getBuildingNumber() != null) {
                builder.append(address.getBuildingNumber()).append(" ");
            }
            if (address.getBuildingName() != null) {
                builder.append(address.getBuildingName()).append(" ");
            }
            if (address.getSubBuildingName() != null) {
                builder.append(address.getSubBuildingName()).append(" ");
            }
            if (address.getDepartmentName() != null) {
                builder.append(address.getDepartmentName()).append(" ");
            }
            if (address.getOrganisationName() != null) {
                builder.append(address.getOrganisationName()).append(" ");
            }
            if (address.getThoroughfare() != null) {
                builder.append(address.getThoroughfare()).append(" ");
            }
            if (address.getDependentThoroughfare() != null) {
                builder.append(address.getDependentThoroughfare()).append(" ");
            }
            if (address.getDependentLocality() != null) {
                builder.append(address.getDependentLocality()).append(" ");
            }
            if (address.getPosttown() != null) {
                builder.append(address.getPosttown()).append(" ");
            }
            addressList.add(builder.toString());
        }
        try{
            houseAdapter = new AddressListAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, addressList);
            addressSelect.setAdapter(houseAdapter);
            addressSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    JsonAddress model =  data.get(position);
                    street.setText(model.getThoroughfare()+" "+model.getDependentThoroughfare());
                    // et_street_name.setText(model.streetName1+" "+model.streetName2);
                    city.setText(model.getPosttown());
                    //et_city.setText(model.town);
                    houseNumber.setText(model.getBuildingNumber()+" "+model.getBuildingName());
                    //et_building_number.setText(model.buildingNumber);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}