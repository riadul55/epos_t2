package com.novo.tech.redmangopos.view.activity;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CategoryAdapter;
import com.novo.tech.redmangopos.adapter.SelectedProductComponentEditAdapter;
import com.novo.tech.redmangopos.adapter.SelectedProductDetailsAdapter;
import com.novo.tech.redmangopos.callback.CategoryCallBack;
import com.novo.tech.redmangopos.callback.PriceAddResponseCallBack;
import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;
import com.novo.tech.redmangopos.callback.ProductUpdateResponseCallBack;
import com.novo.tech.redmangopos.model.CategoryDetailsModel;
import com.novo.tech.redmangopos.model.CategoryModel;
import com.novo.tech.redmangopos.sync.CategoryCodeHelper;
import com.novo.tech.redmangopos.sync.ImageDeleteHelper;
import com.novo.tech.redmangopos.sync.PriceAddHelper;
import com.novo.tech.redmangopos.sync.PriceUpdateHelper;
import com.novo.tech.redmangopos.sync.ProductDetailsUpdateHelper;
import com.novo.tech.redmangopos.utils.CountingRequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProductDetailsEditActivity extends AppCompatActivity {
    //dialog variable

    public static EditText et_price;
    public static EditText et_short;
    public static EditText et_title;
    public static Spinner sp_property;
    public static Spinner sp_category;
    public static Spinner sp_availability;
    public static EditText et_description;
    public static CheckBox cb_discountable;
    public static ImageView iv_product;
    Button img_btn_save;
    ProgressBar progressBar;
    public Button price_save,details_save_button;
    String imgUrl,category;
    LinearLayout productBAck;
    RecyclerView recycler_component_edit;
    String[] available = { "No", "Yes"};
    CategoryDetailsModel categoryDetailsModel;
    PriceUpdateResponseCallBack callBack;
    String selectedCategoryValue = "";
    String categoryData = "";
    String categoryDataVal;
    String selectedAvailableValue = "yes";
    String availabilityData = "0";
    boolean discountable = false;
    int REQUEST_GET_SINGLE_FILE = 1;
    Uri selectedImageURI;

    public static String product_uuid;

    int pos = 0;


    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_edit);
        productBAck = findViewById(R.id.productBAck);
        productBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailsEditActivity.this,ProductActivity.class);
                startActivity(intent);
                finish();
            }
        });
        categoryDetailsModel=SelectedProductDetailsAdapter.categoryDetailsModel2;
        product_uuid = categoryDetailsModel.getProduct_uuid();
        imgUrl = BASE_URL+"product/" + categoryDetailsModel.files.get(0).itemId + "/file/" + categoryDetailsModel.files.get(0).fileUid;
        et_price = findViewById(R.id.et_price);
        et_short = findViewById(R.id.et_short_order);
        et_title = findViewById(R.id.et_title);
        sp_category = findViewById(R.id.sp_category);
        sp_availability = findViewById(R.id.sp_availability);
        et_description = findViewById(R.id.et_description);
        cb_discountable = findViewById(R.id.cb_discountable);
        iv_product = findViewById(R.id.iv_product);
        recycler_component_edit = findViewById(R.id.recycler_component_edit);
        price_save = findViewById(R.id. price_save);
        details_save_button = findViewById(R.id. details_save_button);
        progressBar = findViewById(R.id. progressBar);
        img_btn_save = findViewById(R.id. img_btn_save);

        //previous data
        if (categoryDetailsModel != null) {
            if (categoryDetailsModel.getPrice() != null)
                if (categoryDetailsModel.getPrice().getPrice() >= 0)
                    et_price.setText(String.valueOf(categoryDetailsModel.getPrice().getPrice()));
            et_short.setText(categoryDetailsModel.getProperties().getSort_order());
            et_title.setText(categoryDetailsModel.getShort_name());
            et_description.setText(categoryDetailsModel.getDescription());
            Glide.with(ProductDetailsEditActivity.this).load(imgUrl).into(iv_product);

            category = categoryDetailsModel.getProperties().getCategories();
            String  abc = "";
            for (CategoryModel categoryModelll : ProductActivity.categoryModelArrayList){
                if (categoryModelll.getValue().equals(category)){
                    abc = categoryModelll.getDisplay_name();
                }
            }

            ArrayAdapter categoryAdapter = new ArrayAdapter(ProductDetailsEditActivity.this, android.R.layout.simple_spinner_item, ProductActivity.categoryNameList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_category.setAdapter(categoryAdapter);
            sp_category.setSelection(CategoryAdapter.categoryPositionSelected);

            int available_position = 1;
            if (categoryDetailsModel.getProperties().getAvailable().equals(String.valueOf(0))){
                available_position=0;
            }

            ArrayAdapter availabilityAdapter = new ArrayAdapter(ProductDetailsEditActivity.this, android.R.layout.simple_spinner_item, available);
            availabilityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_availability.setAdapter(availabilityAdapter);
            sp_availability.setSelection(available_position);

            if (categoryDetailsModel.isDiscountable())
                cb_discountable.setChecked(true);

            LinearLayoutManager layoutManager= new LinearLayoutManager(recycler_component_edit.getContext(),
                    LinearLayoutManager.VERTICAL,
                    false);
            layoutManager.setInitialPrefetchItemCount(categoryDetailsModel.getComponents().size());
            SelectedProductComponentEditAdapter selectedProductComponentAdapter = new SelectedProductComponentEditAdapter(categoryDetailsModel.getComponents(),ProductDetailsEditActivity.this);
            recycler_component_edit.setLayoutManager(layoutManager);
            recycler_component_edit.setAdapter(selectedProductComponentAdapter);
            recycler_component_edit.setRecycledViewPool(viewPool);
        }
        price_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double newPrice ;
                if (TextUtils.isEmpty(et_price.getText().toString().trim())){
                    et_price.setError("Price Empty");
                    et_price.requestFocus();
                    return;
                }else {
                    newPrice = Double.parseDouble(et_price.getText().toString().trim());
                    if (newPrice <0){
                        et_price.setError("Price can't be 0");
                        et_price.requestFocus();
                        return;
                    }else {
                        new PriceUpdateHelper(categoryDetailsModel.getProduct_uuid(), categoryDetailsModel.getPrice().getPrice_uuid(), newPrice, new PriceUpdateResponseCallBack() {
                            @Override
                            public void onPriceUpdateResponse(String response) {
                                if (response.equals("success")){
                                    new PriceAddHelper(newPrice, categoryDetailsModel.getProduct_uuid(), new PriceAddResponseCallBack() {
                                        @Override
                                        public void onPriceAddResponse(String response) {
                                            if (response.equals("success")){
                                                Toast.makeText(ProductDetailsEditActivity.this, "Price Update Successfully", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }).execute();
                                }
                            }
                        }).execute();
                    }
                }
            }
        });
        sp_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryValue = parent.getItemAtPosition(position).toString();

                categoryInfo(position);

               /* pos = position;
                if (pos>0)
                    categoryData = ProductActivity.categoryModelArrayList.get(pos).getValue();*/
               // Toast.makeText(ProductDetailsEditActivity.this, "aaaa :"+categoryDataVal, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_availability.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedAvailableValue = parent.getItemAtPosition(position).toString();
                // Toast.makeText(ProductDetailsEditActivity.this, "bbbb :"+selectedAvailableValue, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (selectedAvailableValue.equals("No")){
            availabilityData = "1";
        }
        details_save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String product_uuid = categoryDetailsModel.getProduct_uuid();
                String sort_order = et_short.getText().toString().trim();
                String short_name = et_title.getText().toString().trim();

                discountable = cb_discountable.isChecked();
                String description = et_description.getText().toString().trim();
                if ((categoryDataVal.length()>0) && (!TextUtils.isEmpty(short_name)) && (!TextUtils.isEmpty(sort_order))){
                    new ProductDetailsUpdateHelper(product_uuid, sort_order, short_name, categoryDataVal, availabilityData, discountable, description, new ProductUpdateResponseCallBack() {
                        @Override
                        public void onProductUpdateResponse(String response) {
                            Toast.makeText(ProductDetailsEditActivity.this, "Update : "+response, Toast.LENGTH_SHORT).show();
                            //Log.d("msg",response);
                        }
                    }).execute();
                }
            }
        });
        iv_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),REQUEST_GET_SINGLE_FILE);
            }
        });


        img_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                if (categoryDetailsModel.files.get(0).fileUid !=null){
                    new ImageDeleteHelper(
                            categoryDetailsModel.getProduct_uuid(),
                            categoryDetailsModel.files.get(0).fileUid,
                            new PriceUpdateResponseCallBack() {
                                @Override
                                public void onPriceUpdateResponse(String response) {
                                    if (response.equals("success")){
                                        uploadImage(categoryDetailsModel.getProduct_uuid());
                                    }
                                }
                            }
                    ).execute();
                }else {
                    uploadImage(categoryDetailsModel.getProduct_uuid());
                }
            }
        });
    }
    public void categoryInfo(int categoryPosition) {
        final String[] val = {null};
        ArrayList<CategoryModel> categoryModels = new ArrayList<>();
        new CategoryCodeHelper(categoryModels, new CategoryCallBack() {
            @Override
            public void onCategoryResponse(ArrayList<CategoryModel> data) {
                if (data.size()>0){
                    if (data.size() > categoryPosition){
                        categoryDataVal = data.get(categoryPosition).getValue();
                    }
                }
            }
        }).execute();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                selectedImageURI = data.getData();
                Glide.with(ProductDetailsEditActivity.this).load(selectedImageURI).into(iv_product);
            }
        }
    }

    private String uriToFilename(Uri uri) {
        String path = null;

        if ((Build.VERSION.SDK_INT < 19) && (Build.VERSION.SDK_INT > 11)) {
            path = getRealPathFromURI_API11to18(this, uri);
        } else {
            path = getFilePath(this, uri);
        }

        return path;
    }

    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;
        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    public String getFilePath(Context context, Uri uri) {
        String filePath = "";
        if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);
            String[] splits = wholeID.split(":");
            if (splits.length == 2) {
                String id = splits[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        } else {
            filePath = uri.getPath();
        }
        return filePath;
    }
    public void uploadImage(String product_uid){
        if(selectedImageURI == null){
            return;
        }
        final File imageFile = new File(uriToFilename(selectedImageURI));
        Uri uris = Uri.fromFile(imageFile);
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uris.toString());
        String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
        String imageName = imageFile.getName();

        JSONObject fileJson = new JSONObject();
        try {
            fileJson.put("key_name","item_image");
            fileJson.put("file_name",imageName);
            fileJson.put("content_type","image/png");
            fileJson.put("scope","public");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)

                .addFormDataPart("request", String.valueOf(fileJson))
                .addFormDataPart("file", String.valueOf(uris),
                        RequestBody.create(imageFile, MediaType.parse(mime)))
                .build();

        final CountingRequestBody.Listener progressListener = new CountingRequestBody.Listener() {
            @Override
            public void onRequestProgress(long bytesRead, long contentLength) {
                if (bytesRead >= contentLength) {
                    if (progressBar != null)
                        ProductDetailsEditActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                } else {
                    if (contentLength > 0) {
                        final int progress = (int) (((double) bytesRead / contentLength) * 100);
                        if (progressBar != null)
                            ProductDetailsEditActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressBar.setProgress(progress);
                                }
                            });

                        if(progress >= 100){
                            progressBar.setVisibility(View.GONE);
                        }
                        Log.e("uploadProgress called", progress+" ");
                    }
                }
            }
        };

        OkHttpClient imageUploadClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        if (originalRequest.body() == null) {
                            return chain.proceed(originalRequest);
                        }
                        Request progressRequest = originalRequest.newBuilder()
                                .method(originalRequest.method(),
                                        new CountingRequestBody(originalRequest.body(), progressListener))
                                .build();
                        return chain.proceed(progressRequest);
                    }
                })
                .build();

        RequestBody formBody = new FormBody.Builder()
                .add("message", "Your message")
                .build();
        Request request = new Request.Builder()
                .url(BASE_URL+"product/"+product_uid+"/file")
                .header("ProviderSession", "0b09064c-82e1-4548-b693-b6081df85b39")
                .header("Content-Type", "application/json")
                .post(requestBody)
                .build();


        imageUploadClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                //Toast.makeText(ChatScreen.this, "Error uploading file", Toast.LENGTH_LONG).show();
                Log.e("failure Response", mMessage);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String mMessage = response.body().string();

                ProductDetailsEditActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("TAG", mMessage);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

}