package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.TransactionOrderListAdapter;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;

import java.util.ArrayList;
import java.util.List;

public class TransactionDelete extends Fragment {
    RecyclerView recyclerView;
    TransactionOrderListAdapter adapter;
    List<OrderModel> orderModelList = new ArrayList<>();
    TextView noDataFound;
    public MaterialButton delete;
    MaterialCheckBox checkAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_transaction_delete, container, false);
        recyclerView = v.findViewById(R.id.dashboardOrderRecyclerView);
        noDataFound = v.findViewById(R.id.dashboardNotDataFound);
        delete = v.findViewById(R.id.transactionButtonDelete);
        checkAll = v.findViewById(R.id.itemCheckBox);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        recyclerView.setAlpha(0f);
        recyclerView.setTranslationY(50);
        recyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);
        displayOrderList();
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CashRepository cashManager = new CashRepository(getContext());
                OrderRepository orderManager = new OrderRepository(getContext());
                for (OrderModel order: adapter.selected) {
                    cashManager.deleteOrderTransaction(order.db_id);
                    orderManager.delete(order);
                }
                displayOrderList();
            }
        });
        checkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    adapter.selected = adapter.orderList;
                }else{
                    adapter.selected = new ArrayList<>();
                }
                adapter.notifyDataSetChanged();
            }
        });
        checkAll.setVisibility(View.INVISIBLE);
        return  v;
    }


    public void displayOrderList(){
        OrderRepository repository = new OrderRepository(getContext());
        orderModelList = repository.getCloseOrder();
        orderModelList.sort((a,b)->{
            long aDate  = Long.parseLong(a.orderDate.length()>12? DateTimeHelper.formatOnlyDBDate(a.orderDate):a.orderDate.replaceAll("/","").replaceAll("-",""));
            long bDate  = Long.parseLong(b.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(b.orderDate):b.orderDate.replaceAll("/","").replaceAll("-",""));
            if(aDate == bDate){
                return Integer.compare(b.order_id,a.order_id);
            }else{
                return Long.compare(bDate,aDate);
            }

        });
        adapter = new TransactionOrderListAdapter(getContext(), orderModelList);
        recyclerView.setAdapter(adapter);

        if(adapter.selected.size()==0){
            delete.setVisibility(View.INVISIBLE);
        }else{
            delete.setVisibility(View.VISIBLE);
        }
        if(orderModelList.size()==0){
            noDataFound.setVisibility(View.VISIBLE);
            noDataFound.setText("No Closed Order Found!");
        }else{
            noDataFound.setVisibility(View.GONE);
        }
    }

//    public void onOrderSelect(List<OrderModel> _orderModels){
//        selectedOrders = _orderModels;
//    }




}