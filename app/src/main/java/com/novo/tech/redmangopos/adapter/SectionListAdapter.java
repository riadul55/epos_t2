package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.ComponentSection;

import java.util.List;

public class SectionListAdapter extends RecyclerView.Adapter<SectionListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<ComponentSection> sectionList;
    ComponentListAdapter.OnItemUpdateListener listener;

    public SectionListAdapter(Context mCtx, List<ComponentSection> sectionList, ComponentListAdapter.OnItemUpdateListener listener) {
        this.mCtx = mCtx;
        this.sectionList = sectionList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_section_view, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        //do something
        holder.sectionTitle.setText(sectionList.get(position).sectionName);
        ComponentListAdapter adapter = new ComponentListAdapter(mCtx,sectionList.get(position),listener);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) mCtx.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);


        if (displayMetrics.heightPixels<=800 && displayMetrics.widthPixels<=1280)
            holder.recyclerView.setLayoutManager(new GridLayoutManager(mCtx,4));
        else
            holder.recyclerView.setLayoutManager(new GridLayoutManager(mCtx,6));

        holder.recyclerView.addItemDecoration(new MediaSpaceDecoration(10,10));
        holder.recyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {
        TextView sectionTitle;
        RecyclerView recyclerView;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            sectionTitle = itemView.findViewById(R.id.sectionViewTitle);
            recyclerView = itemView.findViewById(R.id.sectionViewComponentRecyclerView);
        }
    }
    public interface OnItemUpdateListener {
        void onClickAction();
    }
}
