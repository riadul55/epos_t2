package com.novo.tech.redmangopos.model.kitchenPref;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreparedItem implements Serializable {
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("localId")
    @Expose
    private Integer localId;
    @SerializedName("isPreparing")
    @Expose
    private Boolean isPreparing;
    @SerializedName("isCooked")
    @Expose
    private Boolean isCooked;
    @SerializedName("isPacked")
    @Expose
    private Boolean isPacked;
    @SerializedName("cartItem")
    @Expose
    private CartItem cartItem;
    private final static long serialVersionUID = 5006954164986536236L;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Boolean getIsPreparing() {
        return isPreparing;
    }

    public void setIsPreparing(Boolean isPreparing) {
        this.isPreparing = isPreparing;
    }

    public Boolean getIsCooked() {
        return isCooked;
    }

    public void setIsCooked(Boolean isCooked) {
        this.isCooked = isCooked;
    }

    public Boolean getIsPacked() {
        return isPacked;
    }

    public void setIsPacked(Boolean isPacked) {
        this.isPacked = isPacked;
    }

    public CartItem getCartItem() {
        return cartItem;
    }

    public void setCartItem(CartItem cartItem) {
        this.cartItem = cartItem;
    }
}
