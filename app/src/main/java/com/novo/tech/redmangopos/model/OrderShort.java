package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class OrderShort {
    public int serverId;
    public String order_type,order_status,order_channel;
    public double deliveryCharge,discountableAmount,notDiscountableAmount,discountedAmount;

    public OrderShort(int serverId, String order_type, String order_status, String order_channel, double deliveryCharge, double discountableAmount, double notDiscountableAmount, double discountedAmount) {
        this.serverId = serverId;
        this.order_type = order_type;
        this.order_status = order_status;
        this.order_channel = order_channel;
        this.deliveryCharge = deliveryCharge;
        this.discountableAmount = discountableAmount;
        this.notDiscountableAmount = notDiscountableAmount;
        this.discountedAmount = discountedAmount;
    }

    public static OrderShort fromJSON(JSONObject data){

        return new OrderShort(
                data.optInt("order_id"),
                data.optString("order_type"),
                data.optString("order_status"),
                data.optString("order_channel"),
                data.optDouble("delivery_charges",0),
                data.optDouble("discountable_amount",0),
                data.optDouble("none_discountable_amount",0),
                data.optDouble("discounted_amount",0)
        );
    }
}
