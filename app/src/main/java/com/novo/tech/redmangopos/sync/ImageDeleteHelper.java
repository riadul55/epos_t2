package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

public class ImageDeleteHelper extends AsyncTask<String, Void,String> {
    String product_uid;
    String file_uid;
    PriceUpdateResponseCallBack callBack;
    String apiResponseMsg="";

    public ImageDeleteHelper(String product_uid, String file_uid, PriceUpdateResponseCallBack callBack) {
        this.product_uid = product_uid;
        this.file_uid = file_uid;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... strings) {

        return deleteImg(product_uid,file_uid);
    }
    public String deleteImg(String product_uuid, String file_uuid){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(BASE_URL+"product/"+product_uuid+"/file/"+file_uuid)
                .header("ProviderSession", "0b09064c-82e1-4548-b693-b6081df85b39")
                .delete()
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.d("response<<<<<<<<< : ",response.body().toString());
            if (response.code() == 200){
                apiResponseMsg = "success";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiResponseMsg;
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onPriceUpdateResponse(apiResponseMsg);
        super.onPostExecute(s);
    }
}
