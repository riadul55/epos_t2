package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kotlin.jvm.Throws;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServerPusher extends AsyncTask<String, Integer, String>{
    private Context context;
    String[] deliveryStates = {"NEW","REVIEW","PROCESSING","READY","SENT","DELIVERING","DELIVERED","CLOSED"};
    String[] takeOutStates = {"NEW","REVIEW","PROCESSING","READY","CLOSED"};
    String[] currentStatesList = {"NEW","REVIEW","PROCESSING","READY","CLOSED"};
    String[] waiterStatesList = {"NEW","REVIEW","PROCESSING","READY","DELIVERING","CLOSED"};
    RefreshCallBack callBack;
    public ServerPusher(Context context,RefreshCallBack callBack){
     this.context = context;   
     this.callBack = callBack;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
//            checkCustomerDataInLocal();
            OrderRepository repository = new OrderRepository(context);
            List<OrderModel> orderModelList = repository.fetchAllOrderData();
            Log.d("mmmmm", "doInBackground: Running" + orderModelList.size());

            for (OrderModel order : orderModelList) {
                if (!order.cloudSubmitted) {
                    if (order.order_channel == null) {
                        order.order_channel = "EPOS";
                    }
                    if (order.order_status != null) {
                        order.order_status = order.order_status.replaceAll("REFUNDED", "CLOSED");
                        if (order.order_channel.toUpperCase().equals("ONLINE")) {
                            createOrUpdate(order);
                        }else if (order.order_status.toUpperCase().equals("CLOSED")) {
                            createOrUpdate(order);
                        }else if (order.order_status.toUpperCase().equals("CANCELLED")) {
                            updateOrder(order,false);
                        }
                    }

                }
            }

            //check local cash
            new CashApi(context).checkCashTransaction();
            new CashApi(context).checkGeneralCashTransaction();
        }catch (Exception e){
            FirebaseCrashlytics.getInstance().recordException(e);
            Log.d("mmmmm", "doInBackground: "+e.getMessage());
        }
        return "mmmmm";
    }
//    void checkCustomerDataInLocal(){
//        DBCustomerManager manager = new DBCustomerManager(context);
//        List<CustomerModel> customerModelList = manager.getAllCustomerData();
//        for (CustomerModel model:customerModelList) {
//            if(!model.cloudSubmitted){
//                if(model.consumer_uuid==null){
//                    //create consumer
//                    CustomerModel customerModel = new CustomerApi(context,model).createCustomer();
//                }else{
//                    //update Consumer
//                    CustomerModel customerModel = new CustomerApi(context,model).updateCustomer();
//                }
//            }
//        }
//
//    }
    void createOrUpdate(OrderModel order){
        CustomerRepository customerManager = new CustomerRepository(context);
        if(order.customer != null){
            if(order.customer.dbId > 1)
                order.customer = customerManager.getCustomerData(order.customer.dbId);
        }
        if(order.serverId ==0){
            createOrder(order);
        }else{
            Log.d("mmmmm", "doInBackground: Calling Update");
            if(order.order_type.equals("DELIVERY")){
                currentStatesList = deliveryStates;
            }else if(order.order_type.toUpperCase().equals("LOCAL")){
                currentStatesList = waiterStatesList;
            }else {
                currentStatesList = takeOutStates;
            }
            checkUpdate(order);
        }
    }
    void createOrder(OrderModel order){
        JSONObject jsonObject = new JSONObject();
        JSONObject delivery = new JSONObject();
        JSONObject shipping = new JSONObject();
        JSONObject shippingProperties = new JSONObject();
        JSONObject orderProperties = new JSONObject();
        JSONObject profile = new JSONObject();
        JSONArray items = new JSONArray();

        if(order.deliveryCharge>0){
            CartItem deliveryItem = new CartItem(
                    0,
                    "85ab60dd-f5a6-47d5-bbad-7781e8e2ed57",
                    "",
                    "",
                    new ArrayList<>(),
                    order.deliveryCharge,
                    0.0,
                    order.deliveryCharge,
                    order.deliveryCharge,
                    1,
                    1,
                    "",
                    "DELIVERY",0,false,false,false, "0","",new ArrayList<>(), true,
                    order.paymentMethod
            );
            order.items.add(deliveryItem);
        }

        try{
            for (CartItem cartItem: order.items) {
                if(cartItem.type.equals("DYNAMIC")){
                    items.put(dynamicItemToJSONOBJECT(cartItem));
                    orderProperties.put("print_"+cartItem.uuid,cartItem.printOrder);
                }else
                    items.put(itemToJSONOBJECT(cartItem));
            }

            if(order.customer != null){
                if(order.customer.profile!=null){
                    profile.put("first_name",order.customer.profile.first_name);
                    profile.put("last_name",order.customer.profile.last_name);
                    profile.put("phone",order.customer.profile.phone);
                    profile.put("email",order.customer.email);
                }
                delivery.put("email",order.customer.email);
                delivery.put("profile",profile);

                if(order.shippingAddress!=null){
                    if(order.shippingAddress.properties!=null) {
                        CustomerAddressProperties pro = order.shippingAddress.properties;
                        shippingProperties.put("zip",pro.zip);
                        shippingProperties.put("country",pro.country);
                        shippingProperties.put("city",pro.city);
                        shippingProperties.put("street_number","");
                        shippingProperties.put("state",pro.state);
                        shippingProperties.put("building",pro.building);
                        shippingProperties.put("street_name",pro.street_name);
                    }

                }
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }else{
                profile.put("first_name","");
                profile.put("last_name","");
                profile.put("phone","");
                delivery.put("email","");
                delivery.put("profile",profile);

                shippingProperties.put("zip","");
                shippingProperties.put("country","");
                shippingProperties.put("city","");
                shippingProperties.put("street_number","");
                shippingProperties.put("state","");
                shippingProperties.put("building","");
                shippingProperties.put("street_name","");
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }
            jsonObject.put("order_type",order.order_type.toUpperCase().replaceAll("COLLECTION","TAKEOUT"));
            jsonObject.put("requested_delivery_timestamp",new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date()));
            jsonObject.put("order_provider_uuid", providerID);
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            jsonObject.put("order_channel","EPOS");
            orderProperties.put("comment",order.comments);
            jsonObject.put("requester",delivery);
            jsonObject.put("shipping_address",shipping);
            jsonObject.put("order_properties",orderProperties);
            jsonObject.put("order_items",items);

        }catch (JSONException e){
            e.printStackTrace();
        }

        Log.d("order", "createOrder: "+jsonObject);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }

        if(response != null){
            if(response.code() == 201){
                String generatedId = response.header("Location").replaceAll("/delivery/order/","");

                order.serverId = Integer.parseInt(generatedId);
                if(order.customer!= null){
                    if(!order.customer.profile.email.isEmpty() && (!order.customer.profile.first_name.isEmpty() || !order.customer.profile.last_name.isEmpty() ) && !order.customer.profile.phone.isEmpty()){
                        if(order.customer.consumer_uuid==null){
//                            CustomerModel customerModel = new CustomerApi(context,order.customer).createCustomer();
//                            if(customerModel!=null){
//                                order.customer.consumer_uuid = customerModel.consumer_uuid;
//                            }
                        }
//                        boolean result = new CustomerApi(context,order.customer).changeCustomer(order.serverId,order.customer.consumer_uuid);
                    }
                }
                order.cloudSubmitted = false;
                OrderRepository db = new OrderRepository(context);
                db.updateAfterPushed(order);
                checkUpdate(order);

            }else{
                Log.e("mmmmm", "createOrder: "+response.code()+"failed to create order"+ jsonObject);
            }
        }
    }

    JSONObject itemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            JSONArray items = new JSONArray();
            for (ComponentSection section: cartItem.componentSections) {
                JSONObject obj = new JSONObject();
                obj.putOpt("product_uuid",section.selectedItem.productUUid);
                obj.putOpt("product_type","COMPONENT");
                obj.putOpt("units",1);
                items.put(obj);
            }
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_type",cartItem.type.isEmpty()?"ITEM":cartItem.type);
            product.putOpt("units",cartItem.quantity);
            product.putOpt("comment",cartItem.comment);
            product.putOpt("items",items);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }
    JSONObject dynamicItemToJSONOBJECT(CartItem cartItem){
        double price = 0;
        if(cartItem.offered){
            price = cartItem.total;
        }else{
            price = cartItem.price * cartItem.quantity;
        }

        JSONObject product = new JSONObject();
        try{
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_short_name",cartItem.shortName);
            product.putOpt("product_type","DYNAMIC");
            product.putOpt("units",cartItem.quantity);
            product.putOpt("net_amount",price);
            product.putOpt("currency","GBP");
            product.putOpt("discountable",true);
            product.putOpt("comment",cartItem.comment);

        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }

    int updateOrder(OrderModel order,boolean retry){
        JSONObject jsonObject = new JSONObject();

        try{
            jsonObject.put("order_id",order.serverId);
            jsonObject.put("order_type",order.order_type.toUpperCase().replaceAll("COLLECTION","TAKEOUT"));
            jsonObject.put("order_status", order.order_status);
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            if(order.currentDeliveryTime == null){

                order.currentDeliveryTime = DateTimeHelper.getTime();


            }
            Log.d("mmmmm", "Delivery time updateOrder: "+order.currentDeliveryTime);
            jsonObject.put("current_delivery_timestamp", DateTimeHelper.formatLocalDateTimeForServer(order.currentDeliveryTime));
            Log.d("mmmmm", "Delivery time updateOrder: "+DateTimeHelper.formatLocalDateTimeForServer(order.currentDeliveryTime));

            jsonObject.put("driver_provider_uuid", providerID);


        }catch (Exception e){
            e.printStackTrace();
        }
        Log.d("mmmmm", "updateOrder: "+jsonObject.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+order.serverId)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            if(response.code() == 202){
                OrderRepository db = new OrderRepository(context);
                OrderModel mod = db.getOrderData(order.db_id);
                order.cloudSubmitted = mod.order_status.replaceAll("REFUNDED","CANCELLED").equals(order.order_status);
                db.updateAfterPushed(order);
                Log.d("mmmmm", "updateOrder: "+"Updated => "+order.serverId+" to => " + order.order_status);
            }else{
                OrderRepository db = new OrderRepository(context);
                order.cloudSubmitted = false;
                db.updateAfterPushed(order);
                try {
                    Log.e("mmmmm", "createOrder: "+"failed to update"+jsonObject+response.code()+response.body().string() );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (retry)
                    checkUpdate(order);
            }
            return response.code();
        }else{
            OrderRepository db = new OrderRepository(context);
            order.cloudSubmitted = false;
            db.updateAfterPushed(order);
            return -1;
        }

    }

    void checkUpdate(OrderModel orderModel){
        OrderModel order = new OrderApi(context).getOrderData(orderModel.serverId);
        if(order != null){

            if(order.order_type.equals("DELIVERY")){
                currentStatesList = deliveryStates;
            }else if(order.order_type.toUpperCase().equals("LOCAL")){
                currentStatesList = waiterStatesList;
            }else {
                currentStatesList = takeOutStates;
            }

            Log.d("mmmm", "checkUpdate: server "+order.order_status+order.subTotal);
            Log.d("mmmm", "checkUpdate: local "+orderModel.order_status+orderModel.subTotal);

            if(orderModel.order_status.equals("CANCELLED")){
                updateOrder(orderModel,false);
            }else{

                if(order.order_channel.equals("EPOS") && order.order_type.equals("LOCAL")){
                    order.order_type = "COLLECTION";
                }

                int serverStatus = getIndex(order.order_status);
                int localStatus = getIndex(orderModel.order_status);

                Log.d("mmmm", "checkUpdate: "+serverStatus+" local "+localStatus);
                if(serverStatus < localStatus){
                    for (int i = serverStatus;i<=localStatus;i++){
                        orderModel.order_status = currentStatesList[i];
                        int res = updateOrder(orderModel,false);
                        if (res != 202){
                            break;
                        }
                    }
                }else if(serverStatus > localStatus){
                    OrderRepository db = new OrderRepository(context);
                    orderModel.cloudSubmitted = true;
                    orderModel.order_status = order.order_status;
                    db.updateFromServerStatus(orderModel);
                }else if(serverStatus == localStatus){
                    if(!order.currentDeliveryTime.equals(orderModel.currentDeliveryTime)){
                        updateOrder(orderModel,false);
                    }else{
                        OrderRepository db = new OrderRepository(context);
                        orderModel.cloudSubmitted = true;
                        db.updateFromServerStatus(orderModel);
                    }
                }
            }
        }else {
            Log.d("mmmmm", "checkUpdate: "+"Failed to Update");
        }
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onChanged();
        super.onPostExecute(s);
    }
    int getIndex(String value){
        int index = 0;
        for (int i = 0;i<currentStatesList.length;i++){
            if(currentStatesList[i].toUpperCase().equals(value)){
                index = i;
                break;
            }
        }
        return index;
    }

}
