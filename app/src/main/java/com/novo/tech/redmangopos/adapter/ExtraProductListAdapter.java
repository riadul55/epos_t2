package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ExtraProductListAdapter extends RecyclerView.Adapter<ExtraProductListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<Product> productList;
    CartItem cartItem;


    public ExtraProductListAdapter(Context mCtx, List<Product> orderList,CartItem cartItem) {
        this.mCtx = mCtx;
        this.productList = orderList;
        this.cartItem = cartItem;

    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_product, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int _position) {
        Product product = productList.get(holder.getAdapterPosition());
        int position = holder.getAdapterPosition();

        String price = String.format(Locale.getDefault(),"%.2f",product.price);
        if(product.componentList.size()>0 && !productList.get(position).productType.equals("BUNDLE")){
            price+="+";
        }
        holder.productTitle.setText(product.shortName);
        holder.productPrice.setText("£ "+price);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
                        break;
                    case MotionEvent.ACTION_UP:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
                        onItemClick(position);
                        break;
                }
                return true;
            }

        });

        boolean added = false;
        if (cartItem.extra != null) {
            for (CartItem _cartItem: cartItem.extra) {
                if(_cartItem.uuid.equals(product.productUUid)) {
                    added = true;
                    break;
                }
            }
        }

        if(added){
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#DC143C")));

        }else{
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
        }

    }

    void onItemClick(int position){
        JSONObject _itm = CartItem.toJSONCartItem(productList.get(position),null,1,productList.get(position).price,productList.get(position).discountable,false,false,"");
        CartItem _cartItem  = CartItem.fromJSON(_itm);
        addRemoveExtra(_cartItem);
    }

    void addRemoveExtra(CartItem _extraItem){
        boolean exist = false;
        if (cartItem.extra == null) {
            cartItem.extra = new ArrayList<>();
        }
        for (CartItem _cartItem: cartItem.extra) {
            if(_cartItem.uuid.equals(_extraItem.uuid)) {
                exist = true;
                break;
            }
        }
        if(exist){
            cartItem.extra.removeIf((a)->a.uuid.equals(_extraItem.uuid));
        }else{
            cartItem.extra.add(_extraItem);
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
}
