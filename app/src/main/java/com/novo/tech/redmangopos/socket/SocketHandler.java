package com.novo.tech.redmangopos.socket;

import android.util.Log;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SocketHandler {
    private final String TAG = SocketHandler.class.getSimpleName();
    private final String SOCKET_URL = "http://157.90.23.88:5000/";
    public static final String SOCKET_LISTENER_ACTION = "socket.io.epos.action";

    //socket emmit keys
    public static final String EMIT_ORDER_LIST = "ORDER_LIST";
    public static final String EMIT_ORDER_CREATE = "ORDER_CREATE";
    public static final String EMIT_ORDER_UPDATE = "ORDER_UPDATE";
    public static final String EMIT_LISTEN_SUCCESS = "ORDER_SUCCESS";
    public static final String EMIT_MERGED_ITEMS = "MERGED_ORDER_ITEMS"; //emit from epos
    public static final String EMIT_BOOKING_LIST = "SEND_BOOKING_LIST";

    //socket listener keys
    public static final String LISTEN_ORDER_LIST= "ORDER_LIST_";
    public static final String LISTEN_ORDER_CREATE = "ORDER_CREATE_";
    public static final String LISTEN_ORDER_UPDATE = "ORDER_UPDATE_";
    public static final String LISTEN_SUCCESS = "ORDER_SUCCESS_";

    public static final String LISTEN_GET_ITEMS = "LISTEN_GET_ITEMS_"; //listen from epos
    public static final String LISTEN_ORDER_PRINT = "LISTEN_ORDER_PRINT_"; //listen from epos
    public static final String LISTEN_TABLE_BOOKED = "LISTEN_TABLE_BOOKED_"; //listen from epos
    public static final String LISTEN_SET_ORDERS_PREF = "LISTEN_SET_ORDERS_PREF_"; //listen from epos

    //JSON keys
    public static final String PROVIDER_NAME = "provider_name";
    public static final String ORDER = "order";
    public static final String ORDER_LIST = "orderList";

    private Socket socket;

    public Socket init() {
        try {
            socket = IO.socket(SOCKET_URL);
        } catch (URISyntaxException e) {
            Log.e(TAG, e.getMessage());
        }
        return socket;
    }

    public void connect() {
        socket.connect();
    }
}
