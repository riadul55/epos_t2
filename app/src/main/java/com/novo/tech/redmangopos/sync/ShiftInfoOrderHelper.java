package com.novo.tech.redmangopos.sync;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.novo.tech.redmangopos.callback.ShiftInfoOrderCallBack;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.OrderShort;
import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;
import com.novo.tech.redmangopos.model.ShiftReportModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class ShiftInfoOrderHelper extends AsyncTask<String,Void, ShiftReportModel> {

    String date;
    ShiftReportModel reportModel = new ShiftReportModel(0,0,0,0,0);
    ShiftInfoOrderCallBack callBack;

    public ShiftInfoOrderHelper(String date, ShiftInfoOrderCallBack callBack) {
        this.date = date;
        this.callBack = callBack;
    }
    @Override
    protected ShiftReportModel doInBackground(String... strings) {
        return getShiftInfo(date);
    }
    ShiftReportModel getShiftInfo(String mDate){
        OkHttpClient client = new OkHttpClient();
        ArrayList<ShiftInfoOrderModel> shiftInfoModelArrayList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url(BASE_URL+"order?status=CLOSED&start_date="+mDate+" 00:00:00&end_date="+mDate+" 23:59:59")
                .get()
                .addHeader("ProviderSession",providerSession)
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);

                int onlineOrderTotal = 0,localOrderTotal = 0;
                double diningTotalAmount = 0,deliveredTotalAmount = 0,takeOutOrderTotal = 0;

                for (int i= 0;i<jsonArray.length();i++){
                    OrderShort obj = OrderShort.fromJSON(jsonArray.getJSONObject(i));
                    double amount = obj.discountableAmount+obj.notDiscountableAmount-obj.discountedAmount;
                    //total order Calculation
                    if(obj.order_channel.equals("ONLINE")){
                        onlineOrderTotal++;
                    }else{
                        localOrderTotal++;
                    }

                    //total amount calculation
                    if(obj.order_channel.equals("WAITERAPP")){
                        diningTotalAmount+= amount;
                    }else if(obj.order_type.equals("DELIVERY")){
                        deliveredTotalAmount+=amount;
                    }else if(obj.order_type.equals("TAKEOUT") || obj.order_type.equals("COLLECTION")){
                        takeOutOrderTotal+=amount;
                    }

                    reportModel =  new ShiftReportModel(onlineOrderTotal,localOrderTotal,diningTotalAmount,deliveredTotalAmount,takeOutOrderTotal);


                }
            }
            if (response.code() == 204){

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return reportModel;
    }
    @Override
    protected void onPostExecute(ShiftReportModel model) {
        callBack.onShiftInfoOrderResponse(model);
        super.onPostExecute(model);
    }
}
