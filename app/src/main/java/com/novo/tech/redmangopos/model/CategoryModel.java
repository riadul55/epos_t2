package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

/**
 * This class created by alamin
 */
public class CategoryModel {
    String key_name;
    String value;
    String display_name;
    String description;
    String content_type;
    String filename;
    String creation_date;
    String last_update;
    String sort_order;

    public CategoryModel() {
    }

    public CategoryModel(String key_name, String value, String display_name, String description, String content_type, String filename, String creation_date, String last_update, String sort_order) {
        this.key_name = key_name;
        this.value = value;
        this.display_name = display_name;
        this.description = description;
        this.content_type = content_type;
        this.filename = filename;
        this.creation_date = creation_date;
        this.last_update = last_update;
        this.sort_order = sort_order;
    }

    public static CategoryModel fromJSON(JSONObject jsonObject){
        return new CategoryModel(
                jsonObject.optString("key_name",""),
                jsonObject.optString("value",""),
                jsonObject.optString("display_name",""),
                jsonObject.optString("description",""),
                jsonObject.optString("content_type",""),
                jsonObject.optString("filename",""),
                jsonObject.optString("creation_date",""),
                jsonObject.optString("last_update",""),
                jsonObject.optString("sort_order","")

        );
    }

    public String getKey_name() {
        return key_name;
    }

    public void setKey_name(String key_name) {
        this.key_name = key_name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }
}
