package com.novo.tech.redmangopos.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.sync.sync_service.UpdateItemAsync;
import com.novo.tech.redmangopos.view.fragment.OrderComponentSubCartList;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ComponentSection;

import java.util.List;
import java.util.Locale;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;
public class ComponentListAdapter extends RecyclerView.Adapter<ComponentListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<Component> componentList;
    ComponentSection section;
    OrderComponentSubCartList cartListFragment ;
    Component selected;

    ComponentListAdapter.OnItemUpdateListener listener;
    public ComponentListAdapter(Context mCtx, ComponentSection componentSection,ComponentListAdapter.OnItemUpdateListener listener) {
        this.mCtx = mCtx;
        this.section = componentSection;
        this.componentList = section.components;
        this.listener = listener;
        if(section.selectedItem != null){
            this.selected = section.selectedItem;
        }else if(componentList.size()>0){
            this.selected = componentList.get(0);
        }
        cartListFragment = (OrderComponentSubCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_components, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        int _position = holder.getAdapterPosition();
        Component component = componentList.get(_position);
        String price = String.format(Locale.getDefault(),"%.2f",component.mainPrice);
        holder.productTitle.setText(component.shortName);
        holder.productPrice.setText("+£ "+price);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(_position, listener);
                selected = component;
                notifyDataSetChanged();
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showEditItemDialog(mCtx,_position);

                return true;
            }
        });
//        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                switch(event.getAction()) {
//
//                    case MotionEvent.ACTION_DOWN:
//                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
//                        onItemClick(_position);
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        if (component.productUUid.equals(selected.productUUid))
//                            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));
//                        else
//                            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
//                        selected = component;
//                        notifyDataSetChanged();
//                        break;
//                }
//
//
//                return true;
//            }
//
//        });
        if (component.productUUid.equals(selected.productUUid))
            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));
        else
            holder.catImage.setBackgroundColor(Color.parseColor("#FFA500"));

    }

    void onItemClick(int position, ComponentListAdapter.OnItemUpdateListener listener){
        section.selectedItem = componentList.get(position);

        if(componentList.get(position).subComponents != null && componentList.get(position).subComponents.size()>0){
            AlertDialog alertDialog = new AlertDialog.Builder(mCtx).create();
            LayoutInflater inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = (View) inflater.inflate(R.layout.sub_component_select, null);
            TextView textTitle = view.findViewById(R.id.textTitle);
            textTitle.setText(componentList.get(position).shortName);
            RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new GridLayoutManager(mCtx,4));
            recyclerView.addItemDecoration(new MediaSpaceDecoration(5,5));
            SubComponentListAdapter adapter = new SubComponentListAdapter(mCtx, componentList.get(position).subComponents, section, new OnItemUpdateListener() {
                @Override
                public void onClickAction() {
                    alertDialog.dismiss();
                    listener.onClickAction();
                }
            }, new RefreshCallBack() {
                @Override
                public void onChanged() {
                    alertDialog.dismiss();
                }
            });
            recyclerView.setAdapter(adapter);
            alertDialog.setView(view);
            alertDialog.show();
        }else{
            cartListFragment.onComponentChange(section);
        }
    }

    private void showEditItemDialog(Context context,int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialog.setContentView(R.layout.item_update_popup_layout);

        TextView itemName = (EditText) dialog.findViewById(R.id.updateItemName);
        TextView itemPrice = (EditText) dialog.findViewById(R.id.updateItemPrice);
        TextView itemDescription = (EditText) dialog.findViewById(R.id.updateItemDescription);
        MaterialCheckBox isAvailable = (MaterialCheckBox) dialog.findViewById(R.id.isAvailableCheckBox);
        LinearLayout availableLinerLayout = (LinearLayout) dialog.findViewById(R.id.availableLinerLayout);

        Button closeButton = (Button) dialog.findViewById(R.id.updateItemDetailsCloseButton);
        Button saveButton = (Button) dialog.findViewById(R.id.updateItemDetailsSaveButton);

        availableLinerLayout.setVisibility(View.GONE);
        itemName.setText(componentList.get(position).shortName);
//        itemPrice.setText(""+componentList.get(position).mainPrice);
        itemPrice.setText(String.format(Locale.getDefault(),"%.2f",componentList.get(position).mainPrice));
        itemDescription.setText(componentList.get(position).description);
        if (componentList.get(position).properties.available != null) {
            isAvailable.setChecked(componentList.get(position).properties.available.equals("1"));
        }
        isAvailable.setChecked(componentList.get(position).properties.available.equals("1"));
        double previousPrice=Double.parseDouble(itemPrice.getText().toString());

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPriceChange = previousPrice != (Double.parseDouble(itemPrice.getText().toString()));
                UpdateItemAsync async = new UpdateItemAsync(
                        context,
                        componentList.get(position).productUUid,
                        componentList.get(position).productType,
                        itemName.getText().toString(),
                        itemDescription.getText().toString(),
                        componentList.get(position).properties.available,
                        componentList.get(position).priceUUID,
                        Double.parseDouble(itemPrice.getText().toString()),
                        isPriceChange,
                        null,
                        listener
                );
                async.execute();
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
            }
        });


        dialog.create();
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return componentList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
    public interface OnItemUpdateListener {
        void onClickAction();
    }
}
