package com.novo.tech.redmangopos.model.kitchenPref;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PreparedOrder implements Serializable {
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("isNewOrder")
    @Expose
    private Boolean isNewOrder;
    @SerializedName("items")
    @Expose
    private List<PreparedItem> items = null;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Boolean getIsNewOrder() {
        return isNewOrder;
    }

    public void setIsNewOrder(Boolean isNewOrder) {
        this.isNewOrder = isNewOrder;
    }

    public List<PreparedItem> getItems() {
        return items;
    }

    public void setItems(List<PreparedItem> items) {
        this.items = items;
    }

}
