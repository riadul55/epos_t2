package com.novo.tech.redmangopos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonAddress {
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("posttown")
    @Expose
    private String posttown;
    @SerializedName("dependent_locality")
    @Expose
    private String dependentLocality;
    @SerializedName("double_dependent_locality")
    @Expose
    private String doubleDependentLocality;
    @SerializedName("thoroughfare")
    @Expose
    private String thoroughfare;
    @SerializedName("dependent_thoroughfare")
    @Expose
    private String dependentThoroughfare;
    @SerializedName("building_number")
    @Expose
    private Integer buildingNumber;
    @SerializedName("building_name")
    @Expose
    private String buildingName;
    @SerializedName("sub_building_name")
    @Expose
    private String subBuildingName;
    @SerializedName("po_box")
    @Expose
    private String poBox;
    @SerializedName("department_name")
    @Expose
    private String departmentName;
    @SerializedName("organisation_name")
    @Expose
    private String organisationName;
    @SerializedName("udprn")
    @Expose
    private Integer udprn;
    @SerializedName("postcode_type")
    @Expose
    private String postcodeType;
    @SerializedName("su_org_indicator")
    @Expose
    private String suOrgIndicator;
    @SerializedName("delivery_point_suffix")
    @Expose
    private String deliveryPointSuffix;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPosttown() {
        return posttown;
    }

    public void setPosttown(String posttown) {
        this.posttown = posttown;
    }

    public String getDependentLocality() {
        return dependentLocality;
    }

    public void setDependentLocality(String dependentLocality) {
        this.dependentLocality = dependentLocality;
    }

    public String getDoubleDependentLocality() {
        return doubleDependentLocality;
    }

    public void setDoubleDependentLocality(String doubleDependentLocality) {
        this.doubleDependentLocality = doubleDependentLocality;
    }

    public String getThoroughfare() {
        return thoroughfare;
    }

    public void setThoroughfare(String thoroughfare) {
        this.thoroughfare = thoroughfare;
    }

    public String getDependentThoroughfare() {
        return dependentThoroughfare;
    }

    public void setDependentThoroughfare(String dependentThoroughfare) {
        this.dependentThoroughfare = dependentThoroughfare;
    }

    public Integer getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Integer buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getSubBuildingName() {
        return subBuildingName;
    }

    public void setSubBuildingName(String subBuildingName) {
        this.subBuildingName = subBuildingName;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public Integer getUdprn() {
        return udprn;
    }

    public void setUdprn(Integer udprn) {
        this.udprn = udprn;
    }

    public String getPostcodeType() {
        return postcodeType;
    }

    public void setPostcodeType(String postcodeType) {
        this.postcodeType = postcodeType;
    }

    public String getSuOrgIndicator() {
        return suOrgIndicator;
    }

    public void setSuOrgIndicator(String suOrgIndicator) {
        this.suOrgIndicator = suOrgIndicator;
    }

    public String getDeliveryPointSuffix() {
        return deliveryPointSuffix;
    }

    public void setDeliveryPointSuffix(String deliveryPointSuffix) {
        this.deliveryPointSuffix = deliveryPointSuffix;
    }
}
