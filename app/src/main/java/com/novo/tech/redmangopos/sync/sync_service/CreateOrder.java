package com.novo.tech.redmangopos.sync.sync_service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;

public class CreateOrder extends AsyncTask<String, Void, OrderModel> {
    private Context context;
    private UpdateData data;

    public CreateOrder(Context context, UpdateData data) {
        this.context = context;
        this.data = data;
    }

    @SuppressLint("WrongThread")
    @Override
    protected OrderModel doInBackground(String... strings) {
        OrderRepository repository = new OrderRepository(context);
        OrderModel order = repository.getOrderData(data.getOrderId());
//        DBOrderManager dbOrderManager = new DBOrderManager(context);
//        dbOrderManager.open();
//        OrderModel order = dbOrderManager.getOrderData(data.getOrderId());
//        dbOrderManager.close();
        CustomerRepository customerManager = new CustomerRepository(context);
        if(order.customer != null){
            if(order.customer.dbId > 1) {
                order.customer = customerManager.getCustomerData(order.customer.dbId);
            }
        }


        JSONObject jsonObject = new JSONObject();
        JSONObject delivery = new JSONObject();
        JSONObject shipping = new JSONObject();
        JSONObject shippingProperties = new JSONObject();
        JSONObject orderProperties = new JSONObject();
        JSONObject profile = new JSONObject();
        JSONArray items = new JSONArray();

        if(order.deliveryCharge>0){
            CartItem deliveryItem = new CartItem(
                    0,
                    "delivery-charge",
                    "",
                    "",
                    new ArrayList<>(),
                    order.deliveryCharge,
                    0.0,
                    order.deliveryCharge,
                    order.deliveryCharge,
                    1,
                    1,
                    "",
                    "DELIVERY",0,false,false,false, "0","",new ArrayList<>(), true,
                    order.paymentMethod
            );
            order.items.add(deliveryItem);
        }

        try{
            for (CartItem cartItem: order.items) {
                if(cartItem.type.equals("DYNAMIC")){
                    items.put(dynamicItemToJSONOBJECT(cartItem));
                    orderProperties.put("print_"+cartItem.uuid,cartItem.printOrder);
                } else if (cartItem.type.equals("DELIVERY")) {
                    items.put(deliveryItemToJSONOBJECT(cartItem));
                } else
                    items.put(itemToJSONOBJECT(cartItem));
            }

            if(order.customer != null){
                if(order.customer.profile!=null){
                    profile.put("first_name",order.customer.profile.first_name);
                    profile.put("last_name",order.customer.profile.last_name);
                    profile.put("phone",order.customer.profile.phone);
                    profile.put("email",order.customer.email);
                }
                delivery.put("email",order.customer.email);
                delivery.put("profile",profile);

                if(order.shippingAddress!=null){
                    if(order.shippingAddress.properties!=null) {
                        CustomerAddressProperties pro = order.shippingAddress.properties;
                        shippingProperties.put("zip",pro.zip);
                        shippingProperties.put("country",pro.country);
                        shippingProperties.put("city",pro.city);
                        shippingProperties.put("street_number","");
                        shippingProperties.put("state",pro.state);
                        shippingProperties.put("building",pro.building);
                        shippingProperties.put("street_name",pro.street_name);
                    }

                }
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }else{
                profile.put("first_name","");
                profile.put("last_name","");
                profile.put("phone","");
                delivery.put("email","");
                delivery.put("profile",profile);

                shippingProperties.put("zip","");
                shippingProperties.put("country","");
                shippingProperties.put("city","");
                shippingProperties.put("street_number","");
                shippingProperties.put("state","");
                shippingProperties.put("building","");
                shippingProperties.put("street_name","");
                shipping.put("type","INVOICE");
                shipping.put("creator_type","PROVIDER");
                shipping.put("properties",shippingProperties);
            }
            jsonObject.put("order_type",order.order_type.toUpperCase().replaceAll("COLLECTION","TAKEOUT"));
            jsonObject.put("requested_delivery_timestamp",new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date()));
            jsonObject.put("order_provider_uuid", providerID);
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            jsonObject.put("order_channel","EPOS");
            orderProperties.put("comment",order.comments);
            jsonObject.put("requester",delivery);
            jsonObject.put("shipping_address",shipping);
            jsonObject.put("order_properties",orderProperties);
            jsonObject.put("order_items",items);
            if (order.bookingId != 0) {
                jsonObject.put("booking_id",order.bookingId);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

        Log.e("order", "createOrder: "+jsonObject);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }

        if(response != null){
            if(response.code() == 201){
                String generatedId = response.header("Location").replaceAll("/delivery/order/","");
                Log.e("Server Id", generatedId);
                order.serverId = Integer.parseInt(generatedId);
                order.cloudSubmitted = false;
                return order;
            }else{
                String body = "createOrder: code:"+response.code()+" | failed to create order | " + response.message() + " | " + jsonObject.toString() + " | url: " + BASE_URL+"order";
                new SendEmail(context, order, "ERROR", body).execute();
                Log.e("mmmmm", body);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(OrderModel order) {
        super.onPostExecute(order);
        if (order != null && order.order_status != null && order.serverId != 0) {
            OrderRepository db = new OrderRepository(context);
            db.updateAfterPushed(order);

            SharedPrefManager.removeUpdatedOrder(context, order.db_id, data.getStatus());
            long dateTime = System.currentTimeMillis();
            SharedPrefManager.setUpdateData(context, new UpdateData(order.db_id, order.serverId + "", "ONLINE", "REVIEW", "" + dateTime));
        }
    }

    JSONObject deliveryItemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            product.putOpt("product_uuid", cartItem.uuid);
            product.putOpt("product_type", cartItem.type);
            product.putOpt("units", cartItem.quantity);
            product.putOpt("net_amount", cartItem.total);
            product.putOpt("comment", cartItem.comment);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }

    JSONObject itemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            JSONArray items = new JSONArray();
            for (ComponentSection section: cartItem.componentSections) {
                JSONObject obj = new JSONObject();
                obj.putOpt("product_uuid",section.selectedItem.productUUid);
                obj.putOpt("product_type","COMPONENT");
                obj.putOpt("units",1);
                items.put(obj);
            }
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_type",cartItem.type.isEmpty()?"ITEM":cartItem.type);
            product.putOpt("units",cartItem.quantity);
            product.putOpt("comment",cartItem.comment);
            product.putOpt("items",items);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }

    JSONObject dynamicItemToJSONOBJECT(CartItem cartItem){
        double price = 0;
        if(cartItem.offered){
            price = cartItem.total;
        }else{
            price = cartItem.price * cartItem.quantity;
        }

        JSONObject product = new JSONObject();
        try{
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_short_name",cartItem.shortName);
            product.putOpt("product_type","DYNAMIC");
            product.putOpt("units",cartItem.quantity);
            product.putOpt("net_amount",price);
            product.putOpt("currency","GBP");
            product.putOpt("discountable",true);
            product.putOpt("comment",cartItem.comment);

        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }
}
