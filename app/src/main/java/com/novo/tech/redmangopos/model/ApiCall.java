package com.novo.tech.redmangopos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiCall {
    @SerializedName("calling_by")
    @Expose
    private String callingBy;
    @SerializedName("time")
    @Expose
    private String time;

    public ApiCall(String callingBy, String time) {
        this.callingBy = callingBy;
        this.time = time;
    }

    public String getCallingBy() {
        return callingBy;
    }

    public void setCallingBy(String callingBy) {
        this.callingBy = callingBy;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
