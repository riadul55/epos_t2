package com.novo.tech.redmangopos.storage;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.sync.OrderApi;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@SuppressLint("Range")
public class DBOrderManager {
    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;
    RefreshCallBack callBack;
    String[] columns = new String[]{"id", "order_id", "order_channel", "date", "date", "orderDateTime", "requestedDeliveryDateTime", "currentDeliveryDateTime",
            "deliveryType", "order_type", "order_status", "payment_method", "payment_status", "subTotal", "total", "discount", "discountCode", "tips",
            "receive", "cartItems", "cloudSubmitted", "fixedDiscount", "comments", "shift", "server_id", "customerId", "customerInfo", "cashId", "paymentId", "deliveryCharge",
            "discountPercentage", "refund", "adjustment", "adjustmentNote", "requester_type", "dueAmount", "paidTotal", "plasticBagCost", "containerBagCost", "tableBookingId", "discountableTotal",
            "tableBookingInfo", "shippingAddress"};

    public DBOrderManager(Context c) {
        context = c;
    }

    public DBOrderManager(Context c, RefreshCallBack callBack) {
        this.callBack = callBack;
        context = c;
    }

    public DBOrderManager open() throws SQLException {
        if (dbHelper == null)
            dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public ContentValues getOrderContent(OrderModel orderModel) {
        if (orderModel.comments == null) {
            orderModel.comments = "";
        }
        if (orderModel.shift == 0) {
            orderModel.shift = SharedPrefManager.getCurrentShift(context);
        }

        if (orderModel.orderDateTime == null) {
            Log.d("mmmmm", "getOrderContent: " + orderModel.order_id);
        }
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_id", orderModel.order_id);
        if (orderModel.serverId != 0) {
            contentValue.put("server_id", orderModel.serverId);
        }
        contentValue.put("order_channel", orderModel.order_channel);
        contentValue.put("date", DateTimeHelper.formatDBDate(orderModel.orderDateTime));
        contentValue.put("orderDateTime", orderModel.orderDateTime);
        contentValue.put("requestedDeliveryDateTime", orderModel.requestedDeliveryTime);
        contentValue.put("currentDeliveryDateTime", orderModel.currentDeliveryTime);
        contentValue.put("deliveryType", orderModel.deliveryType);
        contentValue.put("requester_type", orderModel.requester_type);
        contentValue.put("order_type", orderModel.order_type);
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("payment_method", orderModel.paymentMethod);
        contentValue.put("payment_status", orderModel.paymentStatus);
        contentValue.put("comments", orderModel.comments);
        contentValue.put("subTotal", orderModel.subTotal);
        contentValue.put("total", orderModel.total);
        contentValue.put("discountableTotal", orderModel.discountableTotal);
        contentValue.put("discount", orderModel.discountAmount);
        contentValue.put("discountCode", orderModel.discountCode);
        contentValue.put("plasticBagCost", orderModel.plasticBagCost);
        contentValue.put("containerBagCost", orderModel.containerBagCost);
        contentValue.put("adjustment", orderModel.adjustmentAmount);
        contentValue.put("adjustmentNote", orderModel.adjustmentNote);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("refund", orderModel.refundAmount);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("deliveryCharge", orderModel.deliveryCharge);
        contentValue.put("tips", orderModel.tips);
        contentValue.put("shift", orderModel.shift);
        contentValue.put("dueAmount", orderModel.dueAmount);
        contentValue.put("paidTotal", orderModel.totalPaid);
        contentValue.put("receive", orderModel.receive);
        contentValue.put("paymentId", orderModel.paymentUid == null ? "-1" : orderModel.paymentUid);
        contentValue.put("cashId", orderModel.cashId == 0 ? -1 : orderModel.cashId);
        contentValue.put("customerId", orderModel.customer == null ? 0 : orderModel.customer.dbId);
        contentValue.put("customerInfo", orderModel.customer != null ? GsonParser.getGsonParser().toJson(orderModel.customer) : "");
        contentValue.put("shippingAddress", orderModel.shippingAddress != null ? GsonParser.getGsonParser().toJson(orderModel.shippingAddress) : "");
        contentValue.put("tableBookingId", orderModel.bookingId);
        contentValue.put("tableBookingInfo", orderModel.tableBookingModel != null ? GsonParser.getGsonParser().toJson(orderModel.tableBookingModel) : "");
        contentValue.put("cloudSubmitted", 0);
        contentValue.put("fixedDiscount", orderModel.fixedDiscount ? 1 : 0);
        contentValue.put("cartItems", GsonParser.getGsonParser().toJson(orderModel.items));
        return contentValue;
    }

    public void deleteAllCustomerOrder(int keepOrder) {
        DBCustomerManager dbCustomerManager = new DBCustomerManager(context);
        List<CustomerModel> allData = dbCustomerManager.getAllCustomerData();
        List<Integer> ids = new ArrayList<>();
        for (CustomerModel item : allData) {
            open();
            Cursor cursor = database.rawQuery("SELECT id, customerId FROM " + DatabaseHelper.ORDER_TABLE_BACKUP_NAME + " WHERE customerId = ? ORDER BY date DESC LIMIT ?", new String[]{String.valueOf(item.dbId), String.valueOf(keepOrder)});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        int id = cursor.getInt(cursor.getColumnIndex("id"));
                        int cId = cursor.getInt(cursor.getColumnIndex("customerId"));
                        ids.add(id);
                    } while (cursor.moveToNext());
                }
//                open();
//                int deletedRows = database.delete(DatabaseHelper.ORDER_TABLE_BACKUP_NAME, "id NOT IN (?) AND customerId = ?", new String[]{ids.toString(), catId});

            }
            assert cursor != null;
            cursor.close();
            close();
        }
        Log.e("id list===>", ids.toString());

        open();
        List<Integer> ids2 = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT id FROM " + DatabaseHelper.ORDER_TABLE_BACKUP_NAME, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex("id"));
                    Log.e("id for del===>", id + "");

                    if (!ids.contains(id)) {
                        ids2.add(id);
                    }
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        Log.e("id list===>", ids2.toString());

        for (int i : ids2) {
            open();
            database.delete(DatabaseHelper.ORDER_TABLE_BACKUP_NAME, "id = ?", new String[]{String.valueOf(i)});
            close();
        }
    }

    public int addOrUpdateOrder2(OrderModel orderModel) {
        String[] columns = new String[]{"id"};
        int db_id = 0;
        try {
            Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=? ", new String[]{String.valueOf(orderModel.db_id)}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    update(orderModel);
                    db_id = orderModel.db_id;

                    if (orderModel.tableBookingModel != null && orderModel.order_status.equals("CLOSED")) {
                        SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, true);
                    }
                } else {
                    orderModel.order_id = getOrderMax() + 1;
                    long rowId = database.insert(DatabaseHelper.TABLE_NAME, null, getOrderContent(orderModel));
                    db_id = (int) rowId;

                    if (orderModel.tableBookingModel != null) {
                        SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
                    }
                }

            }
            assert cursor != null;
            cursor.close();



            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                jsonObject.put(SocketHandler.ORDER, "null");
            } catch (Exception e) {
                e.printStackTrace();
            }
            TheApp.webSocket.emit(SocketHandler.EMIT_LISTEN_SUCCESS, jsonObject);


            try {
                JSONObject newJson = new JSONObject();
                newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                JSONArray jsonArray = new JSONArray();
                for (int i=0;i<20;i++) {
                    boolean isBooked = SharedPrefManager.getTableBooked(context, i+1);
                    TableBookingModel tableBookingModel = new TableBookingModel(
                            1, 1, i+1, 1, isBooked, 1, 1, ""
                    );
                    jsonArray.put(TableBookingModel.toJson(tableBookingModel));
                }
                newJson.put("booking_list", jsonArray);
                TheApp.webSocket.emit(SocketHandler.EMIT_BOOKING_LIST, newJson);


                JSONObject emitGetOrders = new JSONObject();
                try {
                    emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                    emitGetOrders.put("data", "null");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return db_id;
    }

    public int addOrUpdateOrder(OrderModel orderModel) {
        String[] columns = new String[]{"id"};
        int db_id = 0;
        try {
            Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=? ", new String[]{String.valueOf(orderModel.db_id)}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    update(orderModel);
                    db_id = orderModel.db_id;

                    if (orderModel.tableBookingModel != null && orderModel.order_status.equals("CLOSED")) {
                        SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, true);
                    }

                    if (!orderModel.order_status.equals("REFUNDED")) {
                        long dateTime = System.currentTimeMillis();
                        SharedPrefManager.setUpdateData(context, new UpdateData(db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
                    }
                } else {
                    orderModel.order_id = getOrderMax() + 1;
                    long rowId = database.insert(DatabaseHelper.TABLE_NAME, null, getOrderContent(orderModel));
                    db_id = (int) rowId;

                    if (orderModel.tableBookingModel != null) {
                        SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
                    }

                    if (!orderModel.order_status.equals("REFUNDED")) {
                        long dateTime = System.currentTimeMillis();
                        SharedPrefManager.setUpdateData(context, new UpdateData(db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));
                    }
                }

            }
            assert cursor != null;
            cursor.close();

//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
//                jsonObject.put(SocketHandler.ORDER, "null");
//                TheApp.webSocket.emit(SocketHandler.EMIT_LISTEN_SUCCESS, jsonObject);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return db_id;
    }

    public int orderExist(int db_id) {
        open();
        int max = 0;
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=? ", new String[]{String.valueOf(db_id)}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }

    public int getOrderMax() {
        List<String> _date = DateTimeHelper.getTimeRange(context);
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(MAX(order_id),0) as max from " + DatabaseHelper.TABLE_NAME + " WHERE date BETWEEN ? AND ?", new String[]{_date.get(0), _date.get(1)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("max"));
            }
        }
        assert cursor != null;
        cursor.close();
        Log.d("mmmm", "getOrderMax: " + _date + "  " + max);
        return max;
    }

    public void deleteOldOrder(int day) {
        open();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -day);
        Date date = calendar.getTime();
        String _lastDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        Log.e("LastDate===>", _lastDate);
        Cursor cursor = database.rawQuery("DELETE from " + DatabaseHelper.TABLE_NAME + " WHERE date < ?", new String[]{_lastDate});
        assert cursor != null;
        cursor.close();
        close();
    }


    public OrderModel getOrderData(int id) {
        open();
        OrderModel order = new OrderModel();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                order = getOrderDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return order;
    }

    public List<OrderModel> getOrdersByBookingId(int id, String status) {
        Log.e("OrderStatus==>", status + "");
        List<String> timeRange = DateTimeHelper.getTimeRange(context);
        open();
        List<OrderModel> orders = new ArrayList<OrderModel>();
        Cursor cursor = null;
        if (status.equals("CLOSED")) {
            cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "tableBookingId=? AND order_status=? AND date BETWEEN ? AND ?", new String[]{String.valueOf(id), "CLOSED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        } else if (status.equals("REFUNDED")) {
            cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "tableBookingId=? AND order_status=? OR order_status=? AND date BETWEEN ? AND ?", new String[]{String.valueOf(id), "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        } else if (status.equals("CANCELLED")) {
            cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "tableBookingId=? AND order_status=? AND date BETWEEN ? AND ?", new String[]{String.valueOf(id), "CANCELLED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        } else if (status.equals("ONLINE")) {
            cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "tableBookingId=? AND order_channel=? AND order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ?", new String[]{String.valueOf(id), "ONLINE", "CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        } else {
            cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "tableBookingId=? AND order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ?", new String[]{String.valueOf(id), "CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        }
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public OrderModel getHistoryOrderData(int id) {
        open();
        OrderModel order = new OrderModel();
        Cursor cursor = database.query(DatabaseHelper.ORDER_TABLE_BACKUP_NAME, columns, "id=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                order = getOrderDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return order;
    }

    public List<OrderModel> fetchDashboardData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status!=? AND order_status!=?", new String[]{"CLOSED", "CANCELLED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> fetchDashboardClosedData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=?", new String[]{"CLOSED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> fetchDashboardRefundData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=?", new String[]{"CANCELLED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> fetchCustomerOrderData(String dbId) {
        int limit = SharedPrefManager.getCustomerOrderCount(context);
        Log.e("print", limit + "");
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * from " + DatabaseHelper.ORDER_TABLE_BACKUP_NAME + " WHERE customerId = ? ORDER BY 'date' LIMIT 0,?", new String[]{dbId, String.valueOf(limit)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    Log.e("backup order", orderModel.items.get(0).price + "");
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> fetchAllOrderData() {
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "cloudSubmitted=?", new String[]{"0"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getTodayOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        String dateStr = DateTimeHelper.getDBTime();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "date = ?", new String[]{dateStr}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getAllOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }


    public List<OrderModel> getActiveOrderForWaiter() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        List<String> timeRange = DateTimeHelper.getTimeRange(context);

        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new String[]{"CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getActiveOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        List<OrderModel> tableOrders = new ArrayList<>();
        List<String> timeRange = DateTimeHelper.getTimeRange(context);

//        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new String[]{"CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
//        if (cursor != null) {
//            if (cursor.moveToFirst()) {
//                do {
//                    OrderModel orderModel = getOrderDataFromCursor(cursor);
//                    orders.add(orderModel);
//                } while (cursor.moveToNext());
//            }
//        }
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new String[]{"CANCELLED", "CLOSED", "REFUNDED", timeRange.get(0), timeRange.get(1)}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if (orderModel.bookingId != 0) {
                        List<OrderModel> ordersByBookingId = getOrdersByBookingId(orderModel.bookingId, "");
                        boolean isLocalExist = false;
                        for (OrderModel model: ordersByBookingId) {
                            if (!model.order_channel.equals("ONLINE")) {
                                boolean exist = orderExist(tableOrders, model.db_id);
                                if (!exist) {
                                    tableOrders.add(model);
                                }
                                isLocalExist = true;
                                break;
                            }
                        }
                        if (!isLocalExist && ordersByBookingId.size() > 0) {
                            OrderModel orderData = null;
                            for (OrderModel model: ordersByBookingId) {
                                if (model.paymentMethod.equals("CASH")) {
                                    orderData = model;
                                    break;
                                }
                            }
                            if (orderData != null) {
                                boolean exist = orderExist(tableOrders, orderData.db_id);
                                if (!exist) {
                                    tableOrders.add(orderData);
                                }
                            } else {
                                boolean exist = orderExist(tableOrders, ordersByBookingId.get(0).db_id);
                                if (!exist) {
                                    tableOrders.add(ordersByBookingId.get(0));
                                }
                            }
                        }
                    } else {
                        orders.add(orderModel);
                    }
                } while (cursor.moveToNext());
            }
        }
        orders.addAll(tableOrders);
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public boolean orderExist(List<OrderModel> orders, int dbId) {
        boolean exist = false;
        for (OrderModel model: orders) {
            if (model.db_id == dbId) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    public void closeAllActiveOrder() {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_status", "CLOSED");
        int i = database.update(DatabaseHelper.TABLE_NAME, contentValue, "order_status !=? AND order_status!=? AND order_status!=?", new String[]{"CANCELLED", "CLOSED", "REFUNDED"});
        close();
    }

    public List<OrderModel> getCloseOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CLOSED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if (!orderModel.order_status.equals("REFUNDED"))
                        orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getRefundedOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? OR order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CLOSED", "REFUNDED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if (orderModel.order_status.equals("REFUNDED"))
                        orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getCanceledOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CANCELLED"}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getSearchOrder(String orderID) {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_id=?", new String[]{orderID}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getCustomerOrders(CustomerModel customerModel) {
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if (orderModel.customer != null)
                        if (customerModel.dbId == orderModel.customer.dbId) {
                            orders.add(orderModel);
                        }// do what ever you want here
                } while (cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        return orders;
    }

    public int update(OrderModel orderModel) {
        int i = database.update(DatabaseHelper.TABLE_NAME, getOrderContent(orderModel), "id=?", new String[]{String.valueOf(orderModel.db_id)});
        return i;
    }


    public void updateOnlineOrder(int id, List<CartItem> items) {
        OrderModel orderData = getOrderData(id);

        List<CartItem> mergedItems = new ArrayList<>();
        for (CartItem item: items) {
            boolean flag = false;
            for (int j=0;j<orderData.items.size();j++) {
                if (item.localId == orderData.items.get(j).localId) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                mergedItems.add(item);
            }
        }
        mergedItems.addAll(orderData.items);
        orderData.items = mergedItems;
        open();
        database.update(DatabaseHelper.TABLE_NAME, getOrderContent(orderData), "id=?", new String[]{String.valueOf(orderData.db_id)});
        close();
        Log.e("UpdatedItems==>", orderData.items.size() + "");
    }

    public void updateAfterPushed(OrderModel orderModel) {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("cloudSubmitted", orderModel.cloudSubmitted);
        contentValue.put("server_id", orderModel.serverId);
        int id = database.update(DatabaseHelper.TABLE_NAME, contentValue, "id=?", new String[]{String.valueOf(orderModel.db_id)});
        Log.d("mmmm", "updateAfterPushed: updated" + id);
        close();
    }

    public void updateFromServerStatus(OrderModel orderModel) {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("cloudSubmitted", orderModel.cloudSubmitted);
        contentValue.put("server_id", orderModel.serverId);
        int id = database.update(DatabaseHelper.TABLE_NAME, contentValue, "id=?", new String[]{String.valueOf(orderModel.db_id)});
        Log.d("mmmm", "updateFromServerStatus: updated " + id);
        close();
    }

    public void delete(OrderModel orderModel) {
        try {
            open();
            OrderModel orderData = getOrderData(orderModel.db_id);
            close();
            if (orderData.serverId != 0) {
                long dateTime = System.currentTimeMillis();
                SharedPrefManager.setUpdateData(context, new UpdateData(orderData.db_id, orderData.serverId + "", orderData.order_channel, "CANCELLED", "" + dateTime));

                deleteQuery(orderData.db_id);
            } else {
                deleteQuery(orderData.db_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteQuery(int id) {
        open();
        database.delete(DatabaseHelper.TABLE_NAME, "id =" + id, null);
        close();

        try {
            CashRepository dbCashManager = new CashRepository(context);
            dbCashManager.deleteOrderTransaction(id);
        } catch (Exception e) {}


        JSONObject emitGetOrders = new JSONObject();
        try {
            emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            emitGetOrders.put("data", "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);
    }

    private OrderModel getOrderDataFromCursor(Cursor cursor) {
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        CustomerAddress shippingAddress = null;
        TableBookingModel tableBookingModel = null;
        int db_id = cursor.getInt(cursor.getColumnIndex("id"));
        int order_id = cursor.getInt(cursor.getColumnIndex("order_id"));
        int serverId = cursor.getInt(cursor.getColumnIndex("server_id"));
        String date = cursor.getString(cursor.getColumnIndex("date"));
        String orderDateTime = cursor.getString(cursor.getColumnIndex("orderDateTime"));
        String requestedDeliveryDateTime = cursor.getString(cursor.getColumnIndex("requestedDeliveryDateTime"));
        String currentDeliveryDateTime = cursor.getString(cursor.getColumnIndex("currentDeliveryDateTime"));
        String deliveryType = cursor.getString(cursor.getColumnIndex("deliveryType"));
        String requester_type = cursor.getString(cursor.getColumnIndex("requester_type"));
        String order_type = cursor.getString(cursor.getColumnIndex("order_type"));
        String order_status = cursor.getString(cursor.getColumnIndex("order_status"));
        String order_channel = cursor.getString(cursor.getColumnIndex("order_channel"));
        String payment_method = cursor.getString(cursor.getColumnIndex("payment_method"));
        String payment_status = cursor.getString(cursor.getColumnIndex("payment_status"));
        String comments = cursor.getString(cursor.getColumnIndex("comments"));
        String discountCode = cursor.getString(cursor.getColumnIndex("discountCode"));
        int cashId = cursor.getInt(cursor.getColumnIndex("cashId"));
        String paymentId = cursor.getString(cursor.getColumnIndex("paymentId"));
        String _cartItems = cursor.getString(cursor.getColumnIndex("cartItems"));
        int shift = cursor.getInt(cursor.getColumnIndex("shift"));
        double dueAmount = cursor.getDouble(cursor.getColumnIndex("dueAmount"));
        double paidTotal = cursor.getDouble(cursor.getColumnIndex("paidTotal"));
        double subTotal = cursor.getDouble(cursor.getColumnIndex("subTotal"));
        double total = cursor.getDouble(cursor.getColumnIndex("total"));
        double discountableTotal = cursor.getDouble(cursor.getColumnIndex("discountableTotal"));
        double discount = cursor.getDouble(cursor.getColumnIndex("discount"));
        double adjustment = cursor.getDouble(cursor.getColumnIndex("adjustment"));
        double plasticBagCost = cursor.getDouble(cursor.getColumnIndex("plasticBagCost"));
        double containerBagCost = cursor.getDouble(cursor.getColumnIndex("containerBagCost"));
        String adjustmentNote = cursor.getString(cursor.getColumnIndex("adjustmentNote"));
        int discountPercentage = cursor.getInt(cursor.getColumnIndex("discountPercentage"));
        double tips = cursor.getDouble(cursor.getColumnIndex("tips"));
        double receive = cursor.getDouble(cursor.getColumnIndex("receive"));
        double deliveryCharge = cursor.getDouble(cursor.getColumnIndex("deliveryCharge"));
        double refund = cursor.getDouble(cursor.getColumnIndex("refund"));
        boolean cloudSubmitted = cursor.getInt(cursor.getColumnIndex("cloudSubmitted")) != 0;
        boolean fixedDiscount = cursor.getInt(cursor.getColumnIndex("fixedDiscount")) != 0;
        boolean isDiscountOverride = cursor.getInt(cursor.getColumnIndex("isDiscountOverride")) != 0;

        int bookingId = cursor.getInt(cursor.getColumnIndex("tableBookingId"));
        String tableBookingInfo = cursor.getString(cursor.getColumnIndex("tableBookingInfo"));
        int customerId = cursor.getInt(cursor.getColumnIndex("customerId"));
        String _customerInfo = cursor.getString(cursor.getColumnIndex("customerInfo"));
        String _shipping = cursor.getString(cursor.getColumnIndex("shippingAddress"));


        if (customerId != 0 && customerId != -1) {
            CustomerRepository customerManager = new CustomerRepository(context);
            customerModel = customerManager.getCustomerData(customerId);
        } else {
            customerModel = GsonParser.getGsonParser().fromJson(_customerInfo, CustomerModel.class);
        }
        try {
            customerModel = GsonParser.getGsonParser().fromJson(_customerInfo, CustomerModel.class);
            shippingAddress = GsonParser.getGsonParser().fromJson(_shipping, CustomerAddress.class);

            if (tableBookingInfo != null) {
                tableBookingModel = GsonParser.getGsonParser().fromJson(tableBookingInfo, TableBookingModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            JSONArray _data = new JSONArray(_cartItems.replaceAll("NaN", "0.0"));
            for (int i = 0; i < _data.length(); i++) {
                cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (order_type == null) {
            order_type = "N/A";
        }

        double cashPayment = 0;
        double cardPayment = 0;
        CashRepository cashManager = new CashRepository(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(db_id);

        for (TransactionModel model : allTransactions) {
            Log.e("inCash", model.inCash + "");
            Log.e("type", model.type + "");
            Log.e("amount", model.amount + "");
            if (model.inCash == 0) {
                if (model.type == 4) {
                    cardPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cardPayment -= model.amount;
                } else {
                    cardPayment += model.amount;
                }
            } else {
                if (model.type == 4) {
                    cashPayment -= model.amount;
                } else if (model.type == 2) {
                } else if (model.type == 3) {
//                    cashPayment -= model.amount;
                } else {
                    cashPayment += model.amount;
                }
            }
        }

        OrderModel orderModel = new OrderModel(order_id, order_channel, order_type, order_status, payment_method, subTotal, total, discountableTotal, cardPayment + cashPayment, dueAmount, discount, discountCode, tips, cartItemList, orderDateTime, currentDeliveryDateTime, requestedDeliveryDateTime, deliveryType, payment_status,
                receive, db_id, date, comments, shift, cloudSubmitted, fixedDiscount, isDiscountOverride, serverId, customerModel, cashId, paymentId, deliveryCharge, discountPercentage, refund, adjustment, adjustmentNote,
                requester_type, new ArrayList<>(), plasticBagCost, containerBagCost, cashPayment, cardPayment, bookingId, tableBookingModel, shippingAddress);
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        for (CartItem item : orderModel.items) {
            if (item.uuid.equals("plastic-bag")) {
                orderModel.plasticBagCost = item.quantity * item.price;
            } else if (item.uuid.equals("container")) {
                orderModel.containerBagCost = item.quantity * item.price;
            } else {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else {
                    price = item.total;
                }

                subTotalAmount += price;

                if (item.discountable) {
                    _discountableTotal += (item.subTotal * item.quantity);
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.discountableTotal = _discountableTotal;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips + orderModel.plasticBagCost + orderModel.containerBagCost;

        if (refund >= total && total != 0) {
            orderModel.order_status = "REFUNDED";
        }
        return orderModel;
    }

    public void addOnlineOrder(OrderModel orderModel) {
        try {
            open();
            int id = getOnlineOrderMax(DateTimeHelper.formatOnlyDBDate(orderModel.orderDate));
            if (id == -1) {
                id = 0;
            }
            orderModel.order_id = id + 1;
            String[] columns = new String[]{"id"};
            Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "server_id=? ", new String[]{String.valueOf(orderModel.serverId)}, null, null, null);
            long insertedId = -1;
            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    insertedId = database.insert(DatabaseHelper.TABLE_NAME, null, getOnlineOrderContent(orderModel));
                }
            }
            assert cursor != null;
            cursor.close();
            close();

            if (insertedId != -1) {
                double subTotalAmount = 0;
                for (CartItem item : orderModel.items) {
                    double price = 0;
                    if (!item.offered) {
                        price = (item.subTotal * item.quantity);
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    } else
                        price = item.total;

                    subTotalAmount += price;
                }

                orderModel.db_id = (int) insertedId;
                orderModel.subTotal = subTotalAmount;
                orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips;

                String paymentStatus = "UNPAID";
                if (orderModel.paymentMethod.equals("CARD")) {
                    paymentStatus = "PAID";
                    orderModel.totalPaid = orderModel.total;
                    orderModel.receive = orderModel.totalPaid;
                    orderModel.cardPaid = orderModel.totalPaid;
                    CashRepository cashManager = new CashRepository(context);
                    cashManager.addPaidTransaction(context, orderModel.total, 0, orderModel.db_id, false);
                }
                orderModel.paymentStatus = paymentStatus;
                open();
                update(orderModel);
                close();


                // book table for new order
                if (orderModel.tableBookingModel != null) {
                    SharedPrefManager.setTableBooked(context, orderModel.tableBookingModel.table_no, false);
                }

//                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                Ringtone r = RingtoneManager.getRingtone(context, notification);
//                r.play();
                Intent intent = new Intent(TheApp.ORDER_ALERT_ACTION_PLAY);
                context.sendBroadcast(intent);
                if (SharedPrefManager.getPrintOnOnlineOrder(context)) {
                    open();
                    orderModel = getOrderData(orderModel.db_id);
                    close();
                    CashMemoHelper.printKitchenMemo(orderModel, context, true);

                }

            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    public ContentValues getOnlineOrderContent(OrderModel orderModel) {
        if (orderModel.comments == null) {
            orderModel.comments = "";
        }
        if (orderModel.shift <= 0) {
            orderModel.shift = SharedPrefManager.getCurrentShift(context);
        }

        double subTotalAmount = 0;
        for (CartItem item : orderModel.items) {
            double price = 0;
            if (!item.offered) {
                price = (item.subTotal * item.quantity);

                if (item.extra != null) {
                    for (CartItem extraItem : item.extra) {
                        price += extraItem.price;
                    }
                }
            } else
                price = item.total;

            subTotalAmount += price;
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips;

        String paymentStatus = "UNPAID";
        if (orderModel.paymentMethod.equals("CARD")) {
            paymentStatus = "PAID";
            orderModel.totalPaid = orderModel.total;
            orderModel.receive = orderModel.totalPaid;
            orderModel.cardPaid = orderModel.totalPaid;
        }

        ContentValues contentValue = new ContentValues();
        contentValue.put("order_id", orderModel.order_id);
        contentValue.put("server_id", orderModel.serverId);
        contentValue.put("order_channel", orderModel.order_channel);
        contentValue.put("date", orderModel.orderDate.length() == 12 ? orderModel.orderDate : DateTimeHelper.formatOnlyDBDate(orderModel.orderDate));
        contentValue.put("orderDateTime", orderModel.orderDateTime);
        contentValue.put("requestedDeliveryDateTime", orderModel.requestedDeliveryTime);
        contentValue.put("currentDeliveryDateTime", orderModel.currentDeliveryTime);
        contentValue.put("deliveryType", orderModel.deliveryType);
        contentValue.put("order_type", orderModel.order_type);
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("payment_method", orderModel.paymentMethod);
        contentValue.put("dueAmount", orderModel.dueAmount);
        contentValue.put("payment_status", paymentStatus);
        contentValue.put("comments", orderModel.comments);
        contentValue.put("subTotal", orderModel.subTotal);
        contentValue.put("total", orderModel.total);
        contentValue.put("discountableTotal", orderModel.discountableTotal);
        contentValue.put("paidTotal", orderModel.totalPaid);
        contentValue.put("discount", orderModel.discountAmount);
        contentValue.put("discountCode", orderModel.discountCode);
        contentValue.put("plasticBagCost", orderModel.plasticBagCost);
        contentValue.put("containerBagCost", orderModel.containerBagCost);
        contentValue.put("adjustment", orderModel.adjustmentAmount);
        contentValue.put("adjustmentNote", orderModel.adjustmentNote);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("deliveryCharge", orderModel.deliveryCharge);
        contentValue.put("tips", orderModel.tips);
        contentValue.put("shift", orderModel.shift == -1 ? SharedPrefManager.getCurrentShift(context) : orderModel.shift);
        contentValue.put("paymentId", orderModel.paymentUid == null ? "-1" : orderModel.paymentUid);
        contentValue.put("cashId", orderModel.cashId == 0 ? -1 : orderModel.cashId);
        contentValue.put("receive", orderModel.receive);
        contentValue.put("cloudSubmitted", 1);
        contentValue.put("fixedDiscount", 0);
        contentValue.put("customerInfo", orderModel.customer != null ? GsonParser.getGsonParser().toJson(orderModel.customer) : "");
        contentValue.put("shippingAddress", orderModel.shippingAddress != null ? GsonParser.getGsonParser().toJson(orderModel.shippingAddress) : "");
        contentValue.put("customerId", orderModel.customer != null ? String.valueOf(orderModel.customer.dbId) : "");

        contentValue.put("tableBookingId", orderModel.bookingId);
        contentValue.put("tableBookingInfo", orderModel.tableBookingModel != null ? GsonParser.getGsonParser().toJson(orderModel.tableBookingModel) : "");

        contentValue.put("cartItems", GsonParser.getGsonParser().toJson(orderModel.items));
        return contentValue;
    }

    public int getOnlineOrderMax(String _date) {
        List<String> timeRange = DateTimeHelper.getTimeRange(context);
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(MAX(order_id),0) as max from " + DatabaseHelper.TABLE_NAME + " WHERE date BETWEEN ? AND ?", new String[]{timeRange.get(0), timeRange.get(1)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("max"));
            }
        }
        assert cursor != null;
        cursor.close();
        return max;
    }

    public int onlineTableOrderExist(int serverId) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from " + DatabaseHelper.TABLE_NAME + " WHERE server_id = ? AND tableBookingId != 0", new String[]{String.valueOf(serverId)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }

    public int onlineOrderExist(int serverId) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from " + DatabaseHelper.TABLE_NAME + " WHERE server_id = ?", new String[]{String.valueOf(serverId)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }

    public void dayCloseNow() {

        List<OrderModel> allActiveOrders = getTodayOrder();
        open();
        for (OrderModel _order : allActiveOrders) {
//            _order.items.forEach(a -> {
//                a.componentSections.forEach(b -> {
//                    b.selectedItem.subTotal = 0;
//                    b.selectedItem.mainPrice = 0;
//                });
//                a.subTotal = 0;
//                a.total = 0;
//                a.price = 0;
//                a.offerPrice = 0;
//            });

            addOrderToHistory(_order);
        }
        close();
        deleteOldOrder(SharedPrefManager.getDaysCount(context));
    }

    public int addOrderToHistory(OrderModel orderModel) {
        int db_id = 0;
        try {
            orderModel.order_id = getOrderHistoryMax() + 1;
            long rowId = database.insert(DatabaseHelper.ORDER_TABLE_BACKUP_NAME, null, getOrderContent(orderModel));
            db_id = (int) rowId;
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return db_id;

    }

    public void deleteAllOrder() {
        open();
        int id = database.delete(DatabaseHelper.TABLE_NAME, " 1", new String[]{});
        close();

        CashRepository cashManager = new CashRepository(context);
        cashManager.deleteAllTransaction();


    }

    public int getOrderHistoryMax() {
        String _date = DateTimeHelper.getDBTime();

        int max = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(MAX(order_id),0) as max from " + DatabaseHelper.ORDER_TABLE_BACKUP_NAME + " WHERE date = ?", new String[]{_date});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                max = cursor.getInt(cursor.getColumnIndex("max"));
            }
        }
        assert cursor != null;
        cursor.close();

        return max;
    }

    public void deleteOldCustomerOrder() {
        open();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String _lastDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        Cursor cursor = database.rawQuery("DELETE from " + DatabaseHelper.ORDER_TABLE_BACKUP_NAME + " WHERE date < ?", new String[]{_lastDate});
        assert cursor != null;
        cursor.close();
        close();
    }
}
