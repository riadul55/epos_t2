package com.novo.tech.redmangopos.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.databinding.ViewExtraItemBinding;
import com.novo.tech.redmangopos.databinding.ViewPriceInputBinding;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.CallerHistoryModel;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.view.activity.CallHistory;
import com.novo.tech.redmangopos.view.activity.CustomerDetails;
import com.novo.tech.redmangopos.view.activity.CustomerSelect;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;

import java.util.List;
import java.util.Locale;

public class CallerIdListAdapter extends RecyclerView.Adapter<CallerIdListAdapter.CallerListViewHolder> implements View.OnClickListener {

    CallHistory activity;
    List<CallerHistoryModel> callerHistoryModelList;

    public CallerIdListAdapter(CallHistory activity, List<CallerHistoryModel> callerHistoryModelList) {
        this.activity = activity;
        this.callerHistoryModelList = callerHistoryModelList;
    }

    @NonNull
    @Override
    public CallerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.model_call_history, parent, false);
        return new CallerListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallerListViewHolder holder, int position_) {
        int _position = holder.getAdapterPosition();
        CallerHistoryModel model = callerHistoryModelList.get(_position);
        holder.txSL.setText(String.valueOf(_position+1));
        holder.txCallerID.setText(model.phoneNumber);
        holder.txtDateTime.setText(model.time);
        if(model.customerModel!=null){
            holder.txCallerName.setText(model.customerModel.profile.first_name+" "+model.customerModel.profile.last_name);

        }

        holder.selectCreate.setOnClickListener(view -> {
            activity.createOrSelect(model.phoneNumber);
        });

    }



    @Override
    public int getItemCount() {
        return callerHistoryModelList.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class CallerListViewHolder extends RecyclerView.ViewHolder {
        TextView txSL,txCallerID,txtDateTime,txCallerName;
        AppCompatButton selectCreate;
        public CallerListViewHolder(View itemView) {
            super(itemView);
            txSL = itemView.findViewById(R.id.callHistorySL);
            txtDateTime = itemView.findViewById(R.id.callHistoryCallerTime);
            txCallerID = itemView.findViewById(R.id.callHistoryCallerId);
            txCallerName = itemView.findViewById(R.id.callHistoryCallerName);
            selectCreate = itemView.findViewById(R.id.createOrder);

        }
    }
}