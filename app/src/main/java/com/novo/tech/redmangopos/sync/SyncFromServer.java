package com.novo.tech.redmangopos.sync;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SyncFromServer extends AsyncTask<String, Integer, String> {
    RefreshCallBack callBack;
    protected Context context;
    String from, orderId;

    public SyncFromServer(Context context, RefreshCallBack callBack, String from, String orderId){
        this.context = context;
        this.callBack = callBack;
        this.from = from;
        this.orderId = orderId;
    }
    @Override
    protected String doInBackground(String... strings) {
        OrderRepository manager = new OrderRepository(context);
        if (isNetworkAvailable()) {
            try{
                int id = orderId != null ? manager.onlineTableOrderExist(Integer.parseInt(orderId)) : 0;
                if (id > 0) {
                    try {
                        OrderModel orderData = manager.getOrderData(id);
                        if (!orderData.paymentMethod.equals("CARD")) {
                            OrderModel _order = new OrderApi(context).getOrderData(orderData.serverId);
                            if (!_order.order_status.equals("CANCELLED") && !_order.order_status.equals("CLOSED") && !_order.order_status.equals("REFUNDED")) {

                                List<CartItem> oldItems = orderData.items;
                                Log.e("OldItems==>", oldItems.size() + "");
                                List<CartItem> newItems = _order.items;
                                Log.e("NewItems==>", newItems.size() + "");

                                OrderModel extraModel = getExtraItems(orderData, newItems, oldItems);

                                Log.e("ExtraItems==>", extraModel.items.size() + "");
                                if (extraModel.items.size() > 0 && SharedPrefManager.getPrintOnOnlineOrder(context)) {
                                    CashMemoHelper.printKitchenMemo(extraModel, context, true);

                                    manager.updateOnlineOrder(id, newItems);
                                }
                            }
                        }
                    } catch (Exception e){
                        Log.e("Error=>", e.getMessage());
                    }
                } else {
                    getReviewOrdersFromServer();
//                    if(SharedPrefManager.getWaiterApp(context))
//                        getReviewOrdersFromWaiter();
                    SharedPrefManager.setNotifyDate(context, null);
                }
            } catch (Exception e){
                Log.e("Error pulling", e.toString());
                FirebaseCrashlytics.getInstance().recordException(e);
            }
        }
        return null;
    }

    OrderModel orderModel;
    private OrderModel getExtraItems(OrderModel orderData, List<CartItem> newItems, List<CartItem> oldItems) {
        this.orderModel = orderData;
        List<CartItem> extraItems = new ArrayList<>();
        for (int i=0;i<newItems.size();i++) {
            boolean flag = false;
            for (int j=0;j<oldItems.size();j++) {
                if (newItems.get(i).localId == oldItems.get(j).localId) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                extraItems.add(newItems.get(i));
            }
        }
        orderModel.items = extraItems;
        return orderModel;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    void getReviewOrdersFromServer(){
        List<OrderModel> orders = new OrderApi(context).getOrderList("order?status=REVIEW&&channel=ONLINE");
        for (OrderModel order:orders) {
            OrderRepository manager = new OrderRepository(context);
                int id = manager.onlineOrderExist(order.serverId);
                if(id < 1){
                    if(!order.order_channel.equals("EPOS")){
                        OrderModel _order = new OrderApi(context).getOrderData(order.serverId);
                        _order.order_type = _order.order_type.toUpperCase().replaceAll("TAKEOUT","COLLECTION");
                        try{
                            SharedPrefManager.setPullBy(context, from);

                            JSONObject onlineIds = SharedPrefManager.getOnlineIds(context);
                            if (onlineIds != null) {
                                String[] ids = onlineIds.getString("ids").split(",");
                                if (!Arrays.asList(ids).contains(order.serverId + "")) {
                                    manager.addOnlineOrder(_order);
                                    SharedPrefManager.setOnlineIds(context, order.serverId + "");
                                }
                            } else {
                                manager.addOnlineOrder(_order);
                                SharedPrefManager.setOnlineIds(context, order.serverId + "");
                            }
                        }catch (Exception e){
                            Log.e("mmmmm", "getReviewOrdersFromServer: "+String.valueOf(_order.serverId)+" "+ e.getMessage());
                        }
                    }
                }
        }

        Log.d("mmmm", "getReviewOrdersFromServer: ");

    }
    void getReviewOrdersFromWaiter(){
        List<OrderModel> orders = new OrderApi(context).getOrderList("order?status=REVIEW&&channel=WAITERAPP");
        for (OrderModel order:orders) {
            OrderRepository manager = new OrderRepository(context);
            int id = manager.onlineOrderExist(order.serverId);
            if(id < 1){
                if(order.order_channel.equals("WAITERAPP")){
                    OrderModel _order = new OrderApi(context).getOrderData(order.serverId);
                    try{
                        SharedPrefManager.setPullBy(context, from);

                        JSONObject onlineIds = SharedPrefManager.getOnlineIds(context);
                        if (onlineIds != null) {
                            String[] ids = onlineIds.getString("ids").split(",");
                            if (!Arrays.asList(ids).contains(order.serverId + "")) {
                                manager.addOnlineOrder(_order);
                                SharedPrefManager.setOnlineIds(context, order.serverId + "");
                            }
                        } else {
                            manager.addOnlineOrder(_order);
                            SharedPrefManager.setOnlineIds(context, order.serverId + "");
                        }

                        if(_order.order_type.equals("LOCAL")){
                            if(_order.tableBookingModel != null){
                                SharedPrefManager.setTableBooked(context,_order.tableBookingModel.table_no,true);
                            }
                        }

                    }catch (Exception e){
                        Log.e("mmmmm", "getReviewOrdersFromServer: "+String.valueOf(_order.serverId)+" "+ e.getMessage());
                    }
                }
            }
        }

        Log.d("mmmm", "getReviewOrdersFromServer: ");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callBack.onChanged();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put(SocketHandler.ORDER, "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit(SocketHandler.EMIT_LISTEN_SUCCESS, jsonObject);


        try {
            JSONObject newJson = new JSONObject();
            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<20;i++) {
                boolean isBooked = SharedPrefManager.getTableBooked(context, i+1);
                TableBookingModel tableBookingModel = new TableBookingModel(
                        1, 1, i+1, 1, isBooked, 1, 1, ""
                );
                jsonArray.put(TableBookingModel.toJson(tableBookingModel));
            }
            newJson.put("booking_list", jsonArray);
            TheApp.webSocket.emit(SocketHandler.EMIT_BOOKING_LIST, newJson);
        } catch (Exception e) {
            e.printStackTrace();
        }


        JSONObject emitGetOrders = new JSONObject();
        try {
            emitGetOrders.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            emitGetOrders.put("data", "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        TheApp.webSocket.emit("SET_ORDERS_PREF", emitGetOrders);
    }
}
