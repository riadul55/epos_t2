package com.novo.tech.redmangopos.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.R;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d("mmmm", "From: " + remoteMessage.getFrom());
        Log.d("mmmm", "Notification Message Body: " + remoteMessage.getData());
        SharedPrefManager.setNotifyDate(getApplicationContext(), remoteMessage.getData().get("order_date"));
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Dashboard.NOTIFY_ACTIVITY_ACTION );
        broadcastIntent.putExtra("order_date", remoteMessage.getData().get("order_date"));
        broadcastIntent.putExtra("order_status", remoteMessage.getData().get("order_status"));
        broadcastIntent.putExtra("order_id", remoteMessage.getData().get("order_id"));
        sendBroadcast(broadcastIntent);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            getOreoNotification(remoteMessage);
//        } else {
//            getNotification(remoteMessage);
//        }
    }
    private void getOreoNotification(RemoteMessage remoteMessage) {
        String title = Objects.requireNonNull(remoteMessage.getNotification()).getTitle();
        String body = remoteMessage.getNotification().getBody();

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        Intent intent = new Intent(getApplicationContext(), Dashboard.class).putExtra("shift", "EXISTING");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), m, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        OreoNotification oreoNotification = new OreoNotification(this);
        NotificationCompat.Builder builder = oreoNotification.getOreoNotification(title, body, pendingIntent);
        builder.setSound(null);
//        builder.setDefaults(Notification.DEFAULT_ALL);
//        builder.setOnlyAlertOnce(true);
        oreoNotification.getManager().notify(m, builder.build());
    }


    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void getNotification(RemoteMessage message) {
        String title = Objects.requireNonNull(message.getNotification()).getTitle();
        String body = message.getNotification().getBody();
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        Intent intent = new Intent(getApplicationContext(), Dashboard.class).putExtra("shift", "EXISTING");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), m, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setShowWhen(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(null)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent);
        NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        noti.notify(m, builder.build());
    }
}