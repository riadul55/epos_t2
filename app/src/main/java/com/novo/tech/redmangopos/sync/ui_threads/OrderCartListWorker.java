package com.novo.tech.redmangopos.sync.ui_threads;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;

import java.util.ArrayList;

public class OrderCartListWorker extends AsyncTask<Void, Void, OrderModel> {
    Context context;
    String orderType;
    int orderModelId;
    CustomerModel customerModel;
    CustomerAddress shippingAddress;
    OnChangeListener listener;

    public OrderCartListWorker(
            Context context,
            String orderType,
            int orderModelId,
            CustomerModel customerModel,
            CustomerAddress shippingAddress,
            OnChangeListener listener
    ) {
        this.context = context;
        this.orderType = orderType;
        this.orderModelId = orderModelId;
        this.customerModel = customerModel;
        this.shippingAddress = shippingAddress;
        this.listener = listener;
    }

    @Override
    protected OrderModel doInBackground(Void... voids) {
        OrderModel orderModel = new OrderModel();
        if(orderType == null ){
            orderModel = SharedPrefManager.getOrderData(context, orderModelId);

            if (orderModel != null && orderModel.bookingId != 0) {
                orderModel.items = BookingOrdersHelper.getMergedItems(context, orderModel.bookingId, orderModel.order_status);
            }
        }else{
            //initialize some data to bypass null error
            orderModel.order_id = SharedPrefManager.getLastOrderId(context)+1;
            orderModel.items = new ArrayList<>();
            orderModel.order_type = orderType;
            orderModel.fixedDiscount = false;
            orderModel.order_channel = "EPOS";
            orderModel.customer = customerModel;
            orderModel.shippingAddress = shippingAddress;
            if(orderModel.order_status == null)
                orderModel.order_status = "NEW";
            if(orderModel.paymentMethod == null)
                orderModel.paymentMethod = "CASH";
        }
        return orderModel;
    }

    @Override
    protected void onPostExecute(OrderModel model) {
        super.onPostExecute(model);
        listener.onChange(model);
    }

    public interface OnChangeListener {
        void onChange(OrderModel model);
    }
}
