package com.novo.tech.redmangopos.extra;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class PrintView extends View {
    public PrintView(Context context) {
        super(context);
    }

    public PrintView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PrintView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PrintView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
