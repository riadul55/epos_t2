package com.novo.tech.redmangopos.room_db.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "customer")
public class Customer {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "first_name")
    @Nullable
    public String firstName;

    @ColumnInfo(name = "last_name")
    @Nullable
    public String lastName;

    @ColumnInfo(name = "user_name")
    @Nullable
    public String userName;

    @ColumnInfo(name = "phone")
    @Nullable
    public String phone;

    @ColumnInfo(name = "email")
    @Nullable
    public String email;

    @ColumnInfo(name = "consumer_uuid")
    @Nullable
    public String consumer_uuid;

    @ColumnInfo(name = "address")
    @Nullable
    public String address;

    @ColumnInfo(name = "cloud_submitted")
    @Nullable
    public boolean cloudSubmitted;
}
