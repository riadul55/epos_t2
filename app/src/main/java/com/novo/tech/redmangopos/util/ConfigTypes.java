package com.novo.tech.redmangopos.util;

public enum ConfigTypes {
    LAB,
    BALTISTAN,
    FLAVA,
    JKPERI,
    HITCHIN,
    BALDOCK,
    RIVER_SPICE,
    SPICE_KITCHEN,
    KEBAB_GERMAN,
    RUPA_TANDOORI,
    COOKEDBOX,
}
