package com.novo.tech.redmangopos.model.kitchenPref;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CartItem implements Serializable {
    @SerializedName("localId")
    @Expose
    private Integer localId;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("componentSections")
    @Expose
    private List<ComponentSection> componentSections = null;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("offerPrice")
    @Expose
    private Integer offerPrice;
    @SerializedName("subTotal")
    @Expose
    private Double subTotal;
    @SerializedName("Total")
    @Expose
    private Integer total;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("printOrder")
    @Expose
    private Integer printOrder;
    @SerializedName("discountable")
    @Expose
    private Boolean discountable;
    @SerializedName("offerAble")
    @Expose
    private Boolean offerAble;
    @SerializedName("offered")
    @Expose
    private Boolean offered;
    @SerializedName("kitchenItem")
    @Expose
    private String kitchenItem;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("extra")
    @Expose
    private List<CartItem> extra = null;
    private final static long serialVersionUID = -4005216180828459459L;

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<ComponentSection> getComponentSections() {
        return componentSections;
    }

    public void setComponentSections(List<ComponentSection> componentSections) {
        this.componentSections = componentSections;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(Integer printOrder) {
        this.printOrder = printOrder;
    }

    public Boolean getDiscountable() {
        return discountable;
    }

    public void setDiscountable(Boolean discountable) {
        this.discountable = discountable;
    }

    public Boolean getOfferAble() {
        return offerAble;
    }

    public void setOfferAble(Boolean offerAble) {
        this.offerAble = offerAble;
    }

    public Boolean getOffered() {
        return offered;
    }

    public void setOffered(Boolean offered) {
        this.offered = offered;
    }

    public String getKitchenItem() {
        return kitchenItem;
    }

    public void setKitchenItem(String kitchenItem) {
        this.kitchenItem = kitchenItem;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<CartItem> getExtra() {
        return extra;
    }

    public void setExtra(List<CartItem> extra) {
        this.extra = extra;
    }
}
