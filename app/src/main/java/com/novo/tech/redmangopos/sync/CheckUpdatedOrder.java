package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CheckUpdatedOrder extends AsyncTask<Void, Void, OrderModel> {
    private Context context;

    public CheckUpdatedOrder(Context context) {
        this.context = context;
    }

    @Override
    protected OrderModel doInBackground(Void... voids) {
        OrderRepository manager = new OrderRepository(context);
        List<OrderModel> orderList = new OrderApi(context).getOrderList("order?updated=80");
        for (OrderModel order: orderList) {
            if (!order.order_status.equals("CANCELLED") && !order.order_status.equals("CLOSED") && !order.order_status.equals("REFUNDED")) {
                int id = manager.onlineTableOrderExist(order.serverId);
                if (id > 0) {
                    try {
                        OrderModel orderData = manager.getOrderData(id);
                        if (!orderData.paymentMethod.equals("CARD")) {
                            OrderModel _order = new OrderApi(context).getOrderData(orderData.serverId);

                            List<CartItem> oldItems = orderData.items;
                            Log.e("OldItems==>", oldItems.size() + "");
                            List<CartItem> newItems = _order.items;
                            Log.e("NewItems==>", newItems.size() + "");

                            OrderModel extraModel = getExtraItems(orderData, newItems, oldItems);

                            Log.e("ExtraItems==>", extraModel.items.size() + "");
                            if (extraModel.items.size() > 0 && SharedPrefManager.getPrintOnOnlineOrder(context)) {
                                CashMemoHelper.printKitchenMemo(extraModel, context, true);
                            }
                            manager.updateOnlineOrder(id, newItems);
                        }

                    } catch (Exception e){
                        Log.e("Error=>", e.getMessage());
                    }
                }
            }
        }
        return null;
    }

    OrderModel orderModel;
    private OrderModel getExtraItems(OrderModel orderData, List<CartItem> newItems, List<CartItem> oldItems) {
        this.orderModel = orderData;
        List<CartItem> extraItems = new ArrayList<>();
        for (int i=0;i<newItems.size();i++) {
            boolean flag = false;
            for (int j=0;j<oldItems.size();j++) {
                if (newItems.get(i).localId == oldItems.get(j).localId) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                extraItems.add(newItems.get(i));
            }
        }
        orderModel.items = extraItems;
        return orderModel;
    }

    @Override
    protected void onPostExecute(OrderModel extraModel) {
        super.onPostExecute(extraModel);
        if (extraModel != null && extraModel.items != null) {
        }
    }
}
