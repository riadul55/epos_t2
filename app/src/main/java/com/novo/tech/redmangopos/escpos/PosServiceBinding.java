package com.novo.tech.redmangopos.escpos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.novo.tech.redmangopos.escpos.utils.StringUtils;
import com.novo.tech.redmangopos.receivers.PrinterBgService;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.zxy.tiny.Tiny;
import com.zxy.tiny.callback.BitmapCallback;

import net.posprinter.posprinterface.IMyBinder;
import net.posprinter.posprinterface.ProcessData;
import net.posprinter.posprinterface.UiExecute;
import net.posprinter.service.PosprinterService;
import net.posprinter.utils.BitmapToByteData;
import net.posprinter.utils.DataForSendToPrinterPos80;
import net.posprinter.utils.PosPrinterDev;

import java.util.ArrayList;
import java.util.List;

public class PosServiceBinding {
    private final Context context;

    public static boolean IS_CONNECTED = false;
    public static IMyBinder binder;
    public ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //Bind successfully
            binder = (IMyBinder) iBinder;
            Log.e("binder", "connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.e("disbinder", "disconnected");
        }
    };

    public PosServiceBinding(Context context) {
        this.context = context;
    }

    public void initBinding() {
        Intent posService = new Intent(context, PosprinterService.class);
        context.bindService(posService, conn, Context.BIND_AUTO_CREATE);

//        Intent printerService = new Intent(context, PrinterBgService.class);
//        context.startService(printerService);
    }

    public void disposeBinding() {
        binder.disconnectCurrentPort(new UiExecute() {
            @Override
            public void onsucess() {

            }

            @Override
            public void onfailed() {

            }
        });
        context.unbindService(conn);
//        context.stopService(new Intent(context, PrinterBgService.class));
    }

    public void connetNet(String ipAddress, OnDeviceConnect listener) {
        if (ipAddress == null || ipAddress.equals("")) {
            listener.onConnect(false);
        } else {
            //ipAddress :ip address; portal:9100
            binder.connectNetPort(ipAddress, 9100, new UiExecute() {
                @Override
                public void onsucess() {
                    IS_CONNECTED = true;
                    listener.onConnect(true);
                }

                @Override
                public void onfailed() {
                    IS_CONNECTED = false;
                    listener.onConnect(false);
                }
            });
//            if (IS_CONNECTED) {
//                listener.onConnect(true);
//            } else {
//                binder.checkLinkedState(new UiExecute() {
//                    @Override
//                    public void onsucess() {
//                        IS_CONNECTED = true;
//                        listener.onConnect(true);
//                    }
//
//                    @Override
//                    public void onfailed() {
//                        binder.connectNetPort(ipAddress, 9100, new UiExecute() {
//                            @Override
//                            public void onsucess() {
//                                IS_CONNECTED = true;
//                                listener.onConnect(true);
//                                //in this ,you could call acceptdatafromprinter(),when disconnect ,will execute onfailed();
//                                binder.acceptdatafromprinter(new UiExecute() {
//                                    @Override
//                                    public void onsucess() {
//
//                                    }
//
//                                    @Override
//                                    public void onfailed() {
//                                        IS_CONNECTED = false;
//                                        listener.onConnect(false);
//                                    }
//                                });
//                            }
//
//                            @Override
//                            public void onfailed() {
//                                IS_CONNECTED = false;
//                                listener.onConnect(false);
//                            }
//                        });
//                    }
//                });
//            }
        }
    }

    public void connetUSB(OnDeviceConnect listener) {
        List<String> usbList = PosPrinterDev.GetUsbPathNames(context);
        binder.connectUsbPort(context, usbList.get(0), new UiExecute() {
            @Override
            public void onsucess() {
                IS_CONNECTED = true;
                setPortType(PosPrinterDev.PortType.USB);
                listener.onConnect(true);
            }

            @Override
            public void onfailed() {
                IS_CONNECTED = false;
                listener.onConnect(false);
            }
        });
//        if (usbList != null && usbList.size() > 0) {
//            if (portType != PosPrinterDev.PortType.USB) {
//                binder.connectUsbPort(context, usbList.get(0), new UiExecute() {
//                    @Override
//                    public void onsucess() {
//                        IS_CONNECTED = true;
//                        setPortType(PosPrinterDev.PortType.USB);
//                        listener.onConnect(true);
//                    }
//
//                    @Override
//                    public void onfailed() {
//                        IS_CONNECTED = false;
//                        listener.onConnect(false);
//                    }
//                });
//            } else {
//                if (IS_CONNECTED) {
//                    listener.onConnect(true);
//                } else {
//                    binder.checkLinkedState(new UiExecute() {
//                        @Override
//                        public void onsucess() {
//                            IS_CONNECTED = true;
//                            listener.onConnect(true);
//                        }
//
//                        @Override
//                        public void onfailed() {
//                            binder.connectUsbPort(context, usbList.get(0), new UiExecute() {
//                                @Override
//                                public void onsucess() {
//                                    IS_CONNECTED = true;
//                                    setPortType(PosPrinterDev.PortType.USB);
//                                    listener.onConnect(true);
//                                }
//
//                                @Override
//                                public void onfailed() {
//                                    IS_CONNECTED = false;
//                                    listener.onConnect(false);
//                                }
//                            });
//                        }
//                    });
//                }
//            }
//        } else {
//            IS_CONNECTED = false;
//            listener.onConnect(false);
//        }
    }

    public void printText(String text, OnPrintProcess process) {
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
                process.onSuccess();
            }

            @Override
            public void onfailed() {
                process.onError("Print failed");
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list = new ArrayList<byte[]>();
                if (text == null || text.equals("")) {
                    process.onError("Printing text is empty!");
                } else {
                    list.add(DataForSendToPrinterPos80.initializePrinter());
                    byte[] data1 = StringUtils.strTobytes(text);
                    list.add(data1);
                    //should add the command of print and feed line,because print only when one line is complete, not one line, no print
                    list.add(DataForSendToPrinterPos80.printAndFeedLine());
                    //cut pager
                    list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));
                    return list;
                }
                return null;
            }
        });
    }

    public void printBitmap(Bitmap bitmap, OnPrintProcess process) {
        try {
//            Bitmap greyBmp = convertGreyImg(bitmap);

            Tiny.BitmapCompressOptions options = new Tiny.BitmapCompressOptions();
            Tiny.getInstance().source(bitmap).asBitmap().withOptions(options).compress(new BitmapCallback() {
                @Override
                public void callback(boolean isSuccess, Bitmap bitmap) {
                    if (isSuccess) {
                        Bitmap b2 = bitmap;

                        if (PosPrinterDev.PortType.USB != portType) {
                            printpicCode(b2, process);
                        } else {
                            b2 = resizeImage(b2, 600, true);
                            printUSBbitamp(b2, process);
                        }
                    }
                }
            });
        } catch (Exception e) {
            process.onError(e.toString());
        }
    }

    public static Bitmap convertGreyImg(Bitmap img) {
        int width = img.getWidth();
        int height = img.getHeight();

        int[] pixels = new int[width * height];

        img.getPixels(pixels, 0, width, 0, 0, width, height);


        //The arithmetic average of a grayscale image; a threshold
        double redSum = 0, greenSum = 0, blueSun = 0;
        double total = width * height;

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);


                redSum += red;
                greenSum += green;
                blueSun += blue;


            }
        }
        int m = (int) (redSum / total);

        //Conversion monochrome diagram
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int alpha1 = 0xFF << 24;
                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);


                if (red >= m) {
                    red = green = blue = 255;
                } else {
                    red = green = blue = 0;
                }
                grey = alpha1 | (red << 16) | (green << 8) | blue;
                pixels[width * i + j] = grey;


            }
        }
        Bitmap mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        mBitmap.setPixels(pixels, 0, width, 0, 0, width, height);


        return mBitmap;
    }


    private void printpicCode(final Bitmap printBmp, OnPrintProcess process) {
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
                process.onSuccess();
            }

            @Override
            public void onfailed() {
                process.onError("Failed");
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list = new ArrayList<byte[]>();
                list.add(DataForSendToPrinterPos80.initializePrinter());
                list.add(DataForSendToPrinterPos80.printRasterBmp(
                        0, printBmp, BitmapToByteData.BmpType.Threshold, BitmapToByteData.AlignType.Center, 576));
                list.add(DataForSendToPrinterPos80.printAndFeedForward(2));
                list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));
                return list;
            }
        });
    }

    /*
    print the bitmap ,the connection is USB
     */
    private void printUSBbitamp(final Bitmap printBmp, OnPrintProcess process) {

        int height = printBmp.getHeight();
        // if height > 200 cut the bitmap
        if (height > 200) {
            binder.writeDataByYouself(new UiExecute() {
                @Override
                public void onsucess() {
                    process.onSuccess();
                }

                @Override
                public void onfailed() {
                    process.onError("Failed");
                }
            }, new ProcessData() {
                @Override
                public List<byte[]> processDataBeforeSend() {
                    List<byte[]> list = new ArrayList<byte[]>();
                    list.add(DataForSendToPrinterPos80.initializePrinter());
                    List<Bitmap> bitmaplist = new ArrayList<>();
                    bitmaplist = cutBitmap(200, printBmp);//cut bitmap
                    if (bitmaplist.size() != 0) {
                        for (int i = 0; i < bitmaplist.size(); i++) {
                            list.add(DataForSendToPrinterPos80.printRasterBmp(0, bitmaplist.get(i), BitmapToByteData.BmpType.Threshold, BitmapToByteData.AlignType.Center, 576));
                        }
                    }
                    list.add(DataForSendToPrinterPos80.printAndFeedForward(2));
                    list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));
                    return list;
                }
            });
        } else {
            binder.writeDataByYouself(new UiExecute() {
                @Override
                public void onsucess() {
                    process.onSuccess();
                }

                @Override
                public void onfailed() {
                    process.onError("Failed");
                }
            }, new ProcessData() {
                @Override
                public List<byte[]> processDataBeforeSend() {
                    List<byte[]> list = new ArrayList<byte[]>();
                    list.add(DataForSendToPrinterPos80.initializePrinter());
                    list.add(DataForSendToPrinterPos80.printRasterBmp(
                            0, printBmp, BitmapToByteData.BmpType.Threshold, BitmapToByteData.AlignType.Center, 600));
                    list.add(DataForSendToPrinterPos80.printAndFeedForward(2));
                    list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));
                    return list;
                }
            });
        }

    }

    private List<Bitmap> cutBitmap(int h, Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        boolean full = height % h == 0;
        int n = height % h == 0 ? height / h : (height / h) + 1;
        Bitmap b;
        List<Bitmap> bitmaps = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (full) {
                b = Bitmap.createBitmap(bitmap, 0, i * h, width, h);
            } else {
                if (i == n - 1) {
                    b = Bitmap.createBitmap(bitmap, 0, i * h, width, height - i * h);
                } else {
                    b = Bitmap.createBitmap(bitmap, 0, i * h, width, h);
                }
            }

            bitmaps.add(b);
        }

        return bitmaps;
    }

    public static Bitmap resizeImage(Bitmap bitmap, int w, boolean ischecked) {

        Bitmap BitmapOrg = bitmap;
        Bitmap resizedBitmap = null;
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        if (width <= w) {
            return bitmap;
        }
        if (!ischecked) {
            int newWidth = w;
            int newHeight = height * w / width;

            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            // if you want to rotate the Bitmap
            // matrix.postRotate(45);
            resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0, width,
                    height, matrix, true);
        } else {
            resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0, w, height);
        }

        return resizedBitmap;
    }

    public static PosPrinterDev.PortType portType;//connect type

    private void setPortType(PosPrinterDev.PortType portType) {
        this.portType = portType;

    }
}
