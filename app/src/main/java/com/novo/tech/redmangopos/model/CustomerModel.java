package com.novo.tech.redmangopos.model;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomerModel implements Serializable {
    public int dbId;
    public String consumer_uuid,email,username;
    public CustomerProfile profile;
    public List<CustomerAddress> addresses;
    public boolean cloudSubmitted;

    public CustomerModel(int dbId, String consumer_uuid, String email, String username, CustomerProfile profile, List<CustomerAddress> addresses,boolean cloudSubmitted) {
        this.dbId = dbId;
        this.consumer_uuid = consumer_uuid;
        this.email = email;
        this.username = username;
        this.profile = profile;
        this.addresses = addresses;
        this.cloudSubmitted = cloudSubmitted;
    }

    public static CustomerModel fromJSON(JSONObject data){
        CustomerProfile profile = new CustomerProfile(null,"","","");
        List<CustomerAddress> addresses = new ArrayList<>();
        if(data.optJSONObject("profile")!=null){
            profile = CustomerProfile.fromJSON(data.optJSONObject("profile"));
        }
        if(data.optJSONArray("addresses")!=null){
            for (int i=0;i<data.optJSONArray("addresses").length();i++) {
                addresses.add(CustomerAddress.fromJSON(data.optJSONArray("addresses").optJSONObject(i)));
            }
        }

        return new CustomerModel(
                -1,
                data.optString("consumer_uuid"),
                data.optString("email"),
                data.optString("username"),
                profile,
                addresses,
                true
        );
    }
    public static CustomerModel fromBackupJSON(JSONObject data){
        String firstName = data.optString("first_name","");
        String lastName= data.optString("last_name","");
        String phone = data.optString("phone","");
        String email = data.optString("email","");

        CustomerProfile profile = new CustomerProfile(phone,lastName,firstName,email);

        List<CustomerAddress> addresses = new ArrayList<>();

        if(data.optJSONArray("addresses")!=null){
            List<CustomerAddress> customerAddress = CustomerAddress.fromBackupJSON(Objects.requireNonNull(data.optJSONArray("addresses")));
            addresses.addAll(customerAddress);
        }

        return new CustomerModel(
                -1,
                data.optString("consumer_uuid"),
                email,
                email,
                profile,
                addresses,
                true
        );
    }
}
