package com.novo.tech.redmangopos.model.kitchenPref;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Properties implements Serializable {
    @SerializedName("categories_epos")
    @Expose
    private String categoriesEpos;
    @SerializedName("spiced")
    @Expose
    private String spiced;
    @SerializedName("VE")
    @Expose
    private Boolean ve;
    @SerializedName("N")
    @Expose
    private Boolean n;
    @SerializedName("G")
    @Expose
    private Boolean g;
    @SerializedName("V")
    @Expose
    private Boolean v;
    @SerializedName("D")
    @Expose
    private Boolean d;
    @SerializedName("available_on")
    @Expose
    private String availableOn;
    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("sort_order")
    @Expose
    private Integer sortOrder;
    @SerializedName("offer")
    @Expose
    private Integer offer;
    @SerializedName("offer_price")
    @Expose
    private Integer offerPrice;
    @SerializedName("kitchen_item")
    @Expose
    private String kitchenItem;
    private final static long serialVersionUID = 9021070010298044026L;

    public String getCategoriesEpos() {
        return categoriesEpos;
    }

    public void setCategoriesEpos(String categoriesEpos) {
        this.categoriesEpos = categoriesEpos;
    }

    public String getSpiced() {
        return spiced;
    }

    public void setSpiced(String spiced) {
        this.spiced = spiced;
    }

    public Boolean getVe() {
        return ve;
    }

    public void setVe(Boolean ve) {
        this.ve = ve;
    }

    public Boolean getN() {
        return n;
    }

    public void setN(Boolean n) {
        this.n = n;
    }

    public Boolean getG() {
        return g;
    }

    public void setG(Boolean g) {
        this.g = g;
    }

    public Boolean getV() {
        return v;
    }

    public void setV(Boolean v) {
        this.v = v;
    }

    public Boolean getD() {
        return d;
    }

    public void setD(Boolean d) {
        this.d = d;
    }

    public String getAvailableOn() {
        return availableOn;
    }

    public void setAvailableOn(String availableOn) {
        this.availableOn = availableOn;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getOffer() {
        return offer;
    }

    public void setOffer(Integer offer) {
        this.offer = offer;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getKitchenItem() {
        return kitchenItem;
    }

    public void setKitchenItem(String kitchenItem) {
        this.kitchenItem = kitchenItem;
    }
}
