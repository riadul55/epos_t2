package com.novo.tech.redmangopos.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ProductProperties implements Serializable {
    public boolean D,V,G,N,VE,offer;
    public String category,spiced,available_on,available;
    public int sort_order,print_order;
    public double offerPrice;
    public String kitchenItem;
    public String percentage;

    public ProductProperties(boolean d, boolean v, boolean g, boolean n, boolean VE,boolean offer, String category, String spiced,int sort_order,int print_order,String available_on,String available,double offerPrice, String kitchenItem, String percentage) {
        D = d;
        V = v;
        G = g;
        N = n;
        this.VE = VE;
        this.category = category;
        this.spiced = spiced;
        this.sort_order = sort_order;
        this.print_order = print_order;
        this.available_on = available_on;
        this.available = available;
        this.offer = offer;
        this.offerPrice = offerPrice;
        this.kitchenItem = kitchenItem;
        this.percentage = percentage;
    }
    public static ProductProperties fromJSON(JSONObject data){
        return new ProductProperties(
                data.optBoolean("D"),
                data.optBoolean("V"),
                data.optBoolean("G"),
                data.optBoolean("N"),
                data.optBoolean("VE"),
                (data.optInt("offer",0)==1),
                data.optString("categories_epos"),
                data.optString("spiced"),
                data.optInt("sort_order",0),
                data.optInt("print_order",0),
                data.optString("available_on",""),
                data.optString("available",""),
                data.optDouble("offer_price",0.0),
                data.optString("kitchen_item"),
                data.optString("percentage")
                );
    }
    public static JSONObject toJSON(ProductProperties properties){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.putOpt("categories_epos",properties.category);
            jsonObject.putOpt("spiced",properties.spiced);
            jsonObject.putOpt("VE",properties.VE);
            jsonObject.putOpt("N",properties.N);
            jsonObject.putOpt("G",properties.G);
            jsonObject.putOpt("V",properties.V);
            jsonObject.putOpt("D",properties.D);
            jsonObject.putOpt("available_on",properties.available_on);
            jsonObject.putOpt("available",properties.available);
            jsonObject.putOpt("sort_order",properties.sort_order);
            jsonObject.putOpt("offer",properties.offer?1:0);
            jsonObject.putOpt("offer_price",properties.offerPrice);
            jsonObject.putOpt("kitchen_item", properties.kitchenItem);
            jsonObject.putOpt("percentage", properties.percentage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }

}