package com.novo.tech.redmangopos.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CustomerSearchAdapter;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CustomerSearch extends Fragment {
    //private static CustomerModel customerModel;
    CustomerSearchAdapter adapter;
    public EditText customerNamePhone;
//    NestedScrollView nestedSV;
    ListView listView;
    Button addPhone;
    String customerPhoneNo;
    public static CustomerModel customerModel;


    int offset = 0, limit = 20;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_customer_select, container, false);
        customerPhoneNo = requireArguments().getString("customerPhone","");

        customerNamePhone = v.findViewById(R.id.customerSearchTextBox);
        customerNamePhone.setEnabled(false);
        customerNamePhone.setEnabled(true);
//        nestedSV = v.findViewById(R.id.idNestedSV);
        listView = v.findViewById(R.id.customerSearchListView);
        listView.setAlpha(0f);
        listView.setTranslationY(50);
        addPhone = v.findViewById(R.id.phoneAdd);
        listView.animate().alpha(1f).translationYBy(-50).setDuration(900);
        customerNamePhone.setText(customerPhoneNo);

        setCustomerListView();
        customerNamePhone.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                offset = 0;
                setCustomerListView();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        addPhone.setOnClickListener(v1 -> {
            CustomerInput customerInput = (CustomerInput) ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
            assert customerInput != null;
            if(!customerNamePhone.getText().toString().isEmpty()){
                customerInput.customerModel = null;
                customerInput.selectedAddress.customerDbId = 0;
                customerInput.selectedAddress.dbId = 0;
                customerInput.phone.setText(customerNamePhone.getText());
                customerInput.primaryCheckbox.setChecked(false);
                customerInput.selectedAddress.dbId = 0;
                customerInput.street.setText("");
                customerInput.town.setText("");
                customerInput.zip.setText("");
                customerInput.houseNumber.setText("");
                customerInput.firstName.setText("");
                customerInput.lastName.setText("");
                customerInput.email.setText("");
                customerInput.loadCustomerAddress("0");
            }

        });

        if(!customerPhoneNo.isEmpty()){
            CustomerRepository manager = new CustomerRepository(getContext());
            CustomerModel _customerModel = manager.getCustomerDataByPhone(customerPhoneNo);
            if(_customerModel!= null){

                CustomerInput customerInput = (CustomerInput) getActivity().getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
                if(customerInput!=null){

                    customerInput.customerModel = _customerModel;
                    // customerInput.addNewCustomerFromDialog(model);
                    customerInput.selectButton.setVisibility(View.VISIBLE);
                    if(_customerModel.profile!=null){
                        customerInput.firstName.setText(_customerModel.profile.first_name);
                        customerInput.lastName.setText(_customerModel.profile.last_name);
                        customerInput.phone.setText(_customerModel.profile.phone);
                        customerInput.email.setText(_customerModel.email);
                    }

                    if(_customerModel.addresses != null){
                        if(_customerModel.addresses.size()!=0){
                            CustomerAddress address = _customerModel.addresses.get(0);
                            if(address.properties != null){

                                customerInput.street.setText(address.properties.street_number+" "+address.properties.street_name);
                                customerInput.town.setText(address.properties.city);
                                customerInput.zip.setText(address.properties.zip);
                                customerInput.houseNumber.setText(address.properties.building);

                            }
                        }
                    }
                }
            }
        }

        return v;
    }

    public void setCustomerListView(){
        adapter = new CustomerSearchAdapter(getContext(), getCustomerList(customerNamePhone.getText().toString()));
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastLastVisibleItem;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (lastLastVisibleItem != lastItem) {
                        lastLastVisibleItem = lastItem;

                        // update data
                        offset += limit;
                        adapter.updateList(getCustomerList(customerNamePhone.getText().toString()));
                    }
                }
            }
        });
    }
    List<CustomerModel> getCustomerList(String namePhone){
        CustomerRepository dbCustomerManager = new CustomerRepository(getContext());
        return dbCustomerManager.getCustomersBySearch(namePhone, offset);

//        if (namePhone.isEmpty()) {
//        } else {
//            DBCustomerManager dbCustomerManager = new DBCustomerManager(getContext());
//            List<CustomerModel> allData = dbCustomerManager.getCustomersBySearch(namePhone);
//            return allData;
////            List<CustomerModel> filteredData = new ArrayList<>();
////            for (CustomerModel model: allData) {
////                if(model.profile!=null){
////                    if(namePhone == null){
////                        filteredData = allData;
////                    }else if(namePhone.isEmpty()){
////                        filteredData = allData;
////                    }else if(model.profile.first_name.toLowerCase().contains(namePhone.toLowerCase()) || model.profile.last_name.toLowerCase().contains(namePhone.toLowerCase()) || model.profile.phone.toLowerCase().contains(namePhone.toLowerCase()) || model.email.toLowerCase().contains(namePhone.toLowerCase())){
////                        filteredData.add(model);
////                    }
////                }
////
////            }
//        }
    }
    public void backToOrder(CustomerModel model,String orderType,CustomerAddress customerAddress){
        Intent intent = new Intent();
        intent.putExtra("customer",model);
        intent.putExtra("shippingAddress",customerAddress);
        intent.putExtra("orderType",orderType);
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

}