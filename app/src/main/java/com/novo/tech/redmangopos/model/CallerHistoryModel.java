package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

import java.util.Map;

public class CallerHistoryModel {
    public String phoneNumber;
    public String time;
    public CustomerModel customerModel;

    public CallerHistoryModel(String phoneNumber, String time,CustomerModel customerModel) {
        this.phoneNumber = phoneNumber;
        this.time = time;
        this.customerModel = customerModel;
    }
}
