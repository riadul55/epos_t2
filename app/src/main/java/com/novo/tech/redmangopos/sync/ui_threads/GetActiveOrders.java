package com.novo.tech.redmangopos.sync.ui_threads;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.Dashboard;

import java.util.ArrayList;
import java.util.List;

public class GetActiveOrders extends AsyncTask<Void, Void, List<OrderModel>> {
    Context context;
    EditText searchBox;
    int displayType, tableNo, filterType;
    View support_top_edit_heading;
    TextView tv_top_edit_heading;
    TextView dashBoardType;
    OnActiveOrderListener listener;

    public GetActiveOrders(Context context, EditText searchBox, int displayType, int tableNo, int filterType, View support_top_edit_heading, TextView tv_top_edit_heading, TextView dashBoardType, OnActiveOrderListener listener) {
        this.context = context;
        this.searchBox = searchBox;
        this.displayType = displayType;
        this.tableNo = tableNo;
        this.filterType = filterType;
        this.support_top_edit_heading = support_top_edit_heading;
        this.tv_top_edit_heading = tv_top_edit_heading;
        this.dashBoardType = dashBoardType;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(searchBox.getText().toString().isEmpty()){
            if(displayType==-1){
                /**heading edit hide "a"*/
                support_top_edit_heading.setVisibility(View.VISIBLE);
                tv_top_edit_heading.setVisibility(View.GONE);
                dashBoardType.setText("CLOSED ORDER LIST");
            }else if(displayType == -2){
                /**heading edit hide "a"*/
                support_top_edit_heading.setVisibility(View.VISIBLE);
                tv_top_edit_heading.setVisibility(View.GONE);
                dashBoardType.setText("REFUNDED ORDER LIST");
            }else if(displayType == -3){
                dashBoardType.setText("CANCELLED ORDER LIST");
            }else{
                /**heading edit active "a"*/
                support_top_edit_heading.setVisibility(View.GONE);
                tv_top_edit_heading.setVisibility(View.VISIBLE);
                dashBoardType.setText("ACTIVE ORDERS");
            }
        }else{
            dashBoardType.setText("Search Order");
        }
    }

    @Override
    protected List<OrderModel> doInBackground(Void... voids) {
        OrderRepository manager = new OrderRepository(context);
        if(searchBox.getText().toString().isEmpty()){
            if(displayType == -1) {
                return manager.getCloseOrder();
            } else if(displayType == -2) {
                return manager.getRefundedOrder();
            }
            else if(displayType == -3) {
                return manager.getCanceledOrder();
            } else{
                List<OrderModel> orderModelList = new ArrayList<>();
                List<OrderModel> _list= manager.getActiveOrder();
                _list.forEach((obj)->{
                    if(tableNo!=0){
                        if(obj.tableBookingModel!= null)
                            if(obj.tableBookingModel.table_no == tableNo)
                                orderModelList.add(obj);
                    }
                    else if(filterType==0){
                        orderModelList.add(obj);
                    }else if(filterType==1){
                        if(obj.order_channel!=null)
                            if(obj.order_channel.equals("ONLINE")){
                                orderModelList.add(obj);
                            }
                    }else if(filterType==2){
                        if(obj.order_type.equals("COLLECTION")){
                            orderModelList.add(obj);
                        }
                    }else if(filterType==3){
                        if(obj.order_type.equals("DELIVERY")){
                            orderModelList.add(obj);
                        }
                    }
                });
                return orderModelList;
            }
        } else {
            return manager.getSearchOrder(searchBox.getText().toString());
        }
    }

    @Override
    protected void onPostExecute(List<OrderModel> orders) {
        super.onPostExecute(orders);
        listener.onActiveOrders(orders);
    }

    public interface OnActiveOrderListener {
        void onActiveOrders(List<OrderModel> orders);
    }
}
