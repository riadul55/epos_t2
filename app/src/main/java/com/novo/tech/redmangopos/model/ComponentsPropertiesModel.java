package com.novo.tech.redmangopos.model;
/**
 * This class created by alamin
 */
public class ComponentsPropertiesModel {
    String item_name;
    String sort_order;
    public ComponentsPropertiesModel() {
    }
    public ComponentsPropertiesModel(String item_name, String sort_order) {
        this.item_name = item_name;
        this.sort_order = sort_order;
    }
    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }


}
