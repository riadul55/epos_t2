package com.novo.tech.redmangopos.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.adapter.SelectionItemAdapter;
import com.novo.tech.redmangopos.databinding.SplitBillLayoutBinding;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.utils.OrderModelUtil;
import com.novo.tech.redmangopos.view.activity.OrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.PaymentCartListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrderDetailsCart extends Fragment implements View.OnClickListener {
    public TextView type,orderId,manualDiscount,subTotal,total,tips,status,refund,deliveryCharge,paySeal,adjustmentAmount,adjustmentNote,plasticBag,tableNo,containerBagAmount;
    RelativeLayout refundContainer,adjustmentContainer,plasticBagCartContainer,containerBagContainer,tableNoContainer;
    ImageView adjustmentNoteContainer;
    RecyclerView recyclerViewList;
    PaymentCartListAdapter adapter;
    LinearLayout back;
    public OrderModel orderModel = new OrderModel();
    OrderDetailsCart cartFragment;
    OrderDetailsAction actionFragment;
    OrderDetailsConsumerInfo consumerInfoFragment;
    LinearLayout kitchenPrint, btnSplitBill;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_order_details_cart, container, false);
        String data = requireArguments().getString("orderModel");
        orderModel = GsonParser.getGsonParser().fromJson(data,OrderModel.class);
        type = v.findViewById(R.id.orderDetailsCartOrderType);
        orderId = v.findViewById(R.id.orderDetailsCartOrderNumber);
        manualDiscount = v.findViewById(R.id.orderDetailsCartManualDiscount);
        deliveryCharge = v.findViewById(R.id.orderDetailsCartDeliveryCharge);
        refund = v.findViewById(R.id.orderDetailsCartRefundAmount);
        refundContainer = (RelativeLayout) v.findViewById(R.id.refundContainer);
        subTotal = v.findViewById(R.id.orderDetailsCartSubTotal);
        tableNoContainer = v.findViewById(R.id.tableNoContainer);
        tableNo = v.findViewById(R.id.orderCreateFragmentOrderTableNo);
        tips = v.findViewById(R.id.orderDetailsCartTips);
        total = v.findViewById(R.id.orderDetailsCartTotal);
        back = v.findViewById(R.id.orderDetailsCartBack);
        status = v.findViewById(R.id.orderDetailsCartOrderStatus);
        paySeal = v.findViewById(R.id.paySeal);
        adjustmentContainer = v.findViewById(R.id.creditInput);
        adjustmentNoteContainer = v.findViewById(R.id.iv_orderDetailsCartAdjustmentNote);
        plasticBagCartContainer = v.findViewById(R.id.plasticBagCartContainer);
        containerBagContainer = v.findViewById(R.id.containerBagCartContainer);
        adjustmentAmount = v.findViewById(R.id.orderDetailsCartAdjustment);
        adjustmentNote = v.findViewById(R.id.orderDetailsCartAdjustmentNote);
        plasticBag = v.findViewById(R.id.orderDetailsCartPlasticBagAmount);
        containerBagAmount = v.findViewById(R.id.orderDetailsCartContainerBagAmount);
        kitchenPrint = v.findViewById(R.id.orderDetailsCartKitchenPrint);
        kitchenPrint.setOnClickListener(this);
        btnSplitBill = v.findViewById(R.id.orderDetailsConsumerSplitPrint);
        btnSplitBill.setOnClickListener(this);

        if(orderModel.order_status== null){
            orderModel.order_status = "UNPAID";
        }

        paySeal.setText(orderModel.order_status.equals("CANCELLED")?"REFUNDED":orderModel.paymentStatus.toUpperCase());
        cartFragment = (OrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        actionFragment = (OrderDetailsAction) ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
        consumerInfoFragment = (OrderDetailsConsumerInfo)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsRight);
        recyclerViewList = v.findViewById(R.id.orderDetailsCartRecyclerView);
        orderId.setText("Order : # "+String.valueOf(orderModel.order_id));
        type.setText(orderModel.order_type);
        back.setOnClickListener(this);
        adjustmentContainer.setOnClickListener(this);
        adjustmentNoteContainer.setOnClickListener(this);
      //  refundContainer.setOnClickListener(this);
        status.setText(orderModel.order_status);
        if(orderModel.tableBookingModel!=null){
            tableNoContainer.setVisibility(View.VISIBLE);
            String tableStr = String.valueOf(orderModel.tableBookingModel==null?"N/A":orderModel.tableBookingModel.table_no);
            tableNo.setText(tableStr);

            if (orderModel.bookingId != 0) {
                orderModel.items = BookingOrdersHelper.getMergedItems(getContext(), orderModel.bookingId, orderModel.order_status);
            }
        }


        loadData();
        return v;
    }

    void loadData(){
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        List<CartItem> cartItems = new ArrayList<>(orderModel.items);
        cartItems.removeIf((a)->a.uuid.equals("plastic-bag"));
        cartItems.removeIf((a)->a.uuid.equals("container"));
        adapter = new PaymentCartListAdapter(getContext(), cartItems);
        recyclerViewList.setAdapter(adapter);

        calculateTotal();

    }
    public void calculateTotal(){
        try {
            CashRepository dbCashManager = new CashRepository(requireContext());
            List<TransactionModel> orderTransaction = dbCashManager.getOrderTransaction(orderModel.db_id);
            double totalRefundAmount = 0;
            for (int i=0;i<orderTransaction.size();i++) {
                if (orderTransaction.get(i).type == 4 && orderTransaction.get(i).inCash == 1) {
                    totalRefundAmount+=orderTransaction.get(i).amount;
                }
            }
            orderModel.refundAmount = totalRefundAmount;
        } catch (Exception e) {}

        double subTotalAmount = 0;
        for (CartItem item : orderModel.items){
            if(item.uuid.equals("plastic-bag")){
                orderModel.plasticBagCost = item.quantity*item.price;
            }else if(item.uuid.equals("container")){
                orderModel.containerBagCost = item.quantity*item.price;
            }else{
                double price = 0;
                if(!item.offered){
                    price=(item.subTotal*item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem: item.extra) {
                            price+=extraItem.price;
                        }
                    }

                }
                else
                    price = item.total;


                subTotalAmount += price;

            }
        }
        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.tips+orderModel.plasticBagCost+orderModel.containerBagCost;

        total.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.total));
        manualDiscount.setText("("+orderModel.discountPercentage+"%)  -"+String.format(Locale.getDefault(),"%.2f", orderModel.discountAmount));
        subTotal.setText("£ "+String.format(Locale.getDefault(),"%.2f", subTotalAmount));
        tips.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.tips));
        refund.setText("-£ "+String.format("%.2f", orderModel.refundAmount));
        adjustmentAmount.setText("£ "+String.format("%.2f", orderModel.adjustmentAmount));
        plasticBag.setText(String.format(Locale.getDefault(),"%.2f",orderModel.plasticBagCost));
        containerBagAmount.setText(String.format(Locale.getDefault(),"%.2f",orderModel.containerBagCost));
        if (orderModel.adjustmentNote==null)
        {
            adjustmentNote.setText(" ");
        }else {
            adjustmentNote.setText(" " + orderModel.adjustmentNote);
        }
        deliveryCharge.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.deliveryCharge));

        if(orderModel.refundAmount !=0){
            refundContainer.setVisibility(View.VISIBLE);
        }else{
            refundContainer.setVisibility(View.GONE);
        }

//        if(orderModel.adjustmentAmount > 0){
//            adjustmentContainer.setEnabled(false);
//            adjustmentNoteContainer.setEnabled(false);
//        }

        if(!SharedPrefManager.getPlasticBagMandatory(getContext())){
            plasticBagCartContainer.setVisibility(View.GONE);
        }

        if(orderModel.containerBagCost>0){
            containerBagContainer.setVisibility(View.VISIBLE);
        }else{
            containerBagContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.orderDetailsCartBack){
            ((OrderDetails)getContext()).onBackPressed();
        }else if(v.getId() == adjustmentContainer.getId()){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditAmountAlert();
            }
        }else if(v.getId() == adjustmentNoteContainer.getId()){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditNoteAlert();
            }
        }else if(v.getId() == refundContainer.getId()){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't refund at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                consumerInfoFragment.refundAlert();
            }
        } else if (v.getId() == kitchenPrint.getId()) {
            try {
                OrderRepository manager = new OrderRepository(getContext());
                OrderModel orderData = manager.getOrderData(orderModel.db_id);
                if (orderData.bookingId != 0) {
                    orderData.dueAmount = orderData.total - orderData.totalPaid;
                    orderData.items = BookingOrdersHelper.getMergedItems(getContext(), orderData.bookingId, orderData.order_status);
                    orderData.total = cartFragment.orderModel.total;
                    orderData.subTotal = cartFragment.orderModel.subTotal;
                }
                CashMemoHelper.printKitchenMemo(orderData,getContext(),true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (v.getId() == btnSplitBill.getId()) {
            OrderRepository manager = new OrderRepository(getContext());
            OrderModel order = manager.getOrderData(orderModel.db_id);

            order.items.removeIf((a) -> a.uuid.equals("plastic-bag"));
            order.items.removeIf((a) -> a.uuid.equals("container"));
            Log.e("items===>", order.items.size() + "");
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(requireContext());
            LayoutInflater inflater = LayoutInflater.from(requireContext());
            SplitBillLayoutBinding binding = SplitBillLayoutBinding.inflate(inflater);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
            SelectionItemAdapter adapter = new SelectionItemAdapter(getContext(), order.items, new SelectionItemAdapter.OnSectionListener() {
                @Override
                public void onItemSelect(List<CartItem> items) {
                    double total = 0.0;
                    for (int i=0;i<items.size();i++) {
                        double price = 0;

                        if(!items.get(i).offered){
                            price=(items.get(i).subTotal*items.get(i).quantity);

                            if (items.get(i).extra != null) {
                                for (CartItem extraItem: items.get(i).extra) {
                                    price+=extraItem.price;
                                }
                            }

                        }
                        else {
                            price = items.get(i).total;
                        }

                        total += price;
                    }

                    DecimalFormat df = new DecimalFormat("###.##");
                    binding.textTotal.setText("Total Price: £" + df.format(total));
                }
            });
            binding.recyclerView.setAdapter(adapter);
            binding.selectionPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<CartItem> selectedItems = adapter.getSelectedItems();
                    if (selectedItems != null) {
                        try {
                            OrderModel orderData = OrderModelUtil.getOrderModel(getContext(), order, selectedItems);
                            CashMemoHelper.printCustomerMemo(orderData,getContext(),true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            builder.setTitle("Print Bill");
            builder.setView(binding.getRoot());
            android.app.AlertDialog dialog = builder.create();
            dialog.getWindow().setLayout(1000, 700);
            dialog.show();
//            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialogInterface) {
//                    order.items = adapter.getCartItems();
//                }
//            });
        }
    }

    void creditAmountAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL);
        if (orderModel.adjustmentAmount>0)
            inputField.setText(String.format(Locale.getDefault(),"%.2f",orderModel.adjustmentAmount));
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Credit")
                .setView(inputField)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String str = inputField.getText().toString();
                        if(str.equals("")){
                            str = "0";
                        }
                        double amount = Double.parseDouble(str);
                        creditAmount(amount);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditAmount(double amount){
        orderModel.adjustmentAmount = amount;
        orderModel.paymentStatus = "UNPAID";
        actionFragment.orderModel.paymentStatus = "UNPAID";
        calculateTotal();
        actionFragment.orderModel.adjustmentAmount = amount;
        actionFragment.totalAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f",(orderModel.total)));
        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
        actionFragment.setActionView();
        actionFragment.setPay();
    }

    void creditNoteAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_TEXT);
        inputField.setText(orderModel.adjustmentNote);
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Credit Note")
                .setView(inputField)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        creditNote(inputField.getText().toString());
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditNote(String str){
        orderModel.adjustmentNote = str;
        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
        calculateTotal();
    }
}