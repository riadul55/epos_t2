package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CustomerOrderListAdapter;
import com.novo.tech.redmangopos.adapter.CustomerSearchAdapter;
import com.novo.tech.redmangopos.callback.CustomerOrderListCallBack;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.repositories.CustomerRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.sync.CustomerOrderService;
import com.novo.tech.redmangopos.sync.OrderApi;

import java.util.ArrayList;
import java.util.List;

public class CustomerOrderList extends Fragment {
    CustomerOrderListAdapter adapter;
    ListView listView;
    ProgressBar progressBar;
    TextView notFound;
    String customerPhoneNo;


    CustomerRepository customerManager = new CustomerRepository(getContext());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_customer_order_list, container, false);
        customerPhoneNo = requireArguments().getString("customerPhone","");
        listView = v.findViewById(R.id.orderListView);
        notFound = v.findViewById(R.id.noOrderFound);
        progressBar = v.findViewById(R.id.loader);
        listView.setAlpha(0f);
        listView.setTranslationY(50);
        listView.animate().alpha(1f).translationYBy(-50).setDuration(700);

        if (!customerPhoneNo.isEmpty()) {
            customerManager = new CustomerRepository(getContext());
            CustomerModel customerModel = customerManager.getCustomerDataByPhone(customerPhoneNo);
            if (customerModel != null) {
                getCustomerOrdersFromServer(String.valueOf(customerModel.dbId));
            }
        }
        return v;
    }
    public void getCustomerOrdersFromServer(String consumerId){
        adapter = new CustomerOrderListAdapter(getContext(),new ArrayList<>());
        listView.setAdapter(adapter);
        if(consumerId!= null){
            progressBar.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            notFound.setVisibility(View.GONE);
          //  new CustomerOrderService(getContext(),consumerId,this).execute();
            try {
                OrderRepository manager = new OrderRepository(getContext());
                List<OrderModel> orderModelList = manager.fetchCustomerOrderData(consumerId);
                Log.e("print", orderModelList.size() + "");
                onResponse(orderModelList);
            } catch (Exception e) {
                Log.e("print", e.toString());
            }
        }

    }


    public void onResponse(List<OrderModel> orders) {
        progressBar.setVisibility(View.GONE);
        if(orders.size()==0){
            notFound.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }else{
            listView.setVisibility(View.VISIBLE);
            notFound.setVisibility(View.GONE);
            if(getContext() != null){
                adapter = new CustomerOrderListAdapter(getContext(),orders);
                listView.setAdapter(adapter);
            }
        }


    }
}