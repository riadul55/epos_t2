package com.novo.tech.redmangopos.sync.sync_service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class UpdateOnlineTransaction extends AsyncTask<String, Void, Void> {
    private Context context;
    private UpdateData data;

    public UpdateOnlineTransaction(Context context, UpdateData data) {
        this.context = context;
        this.data = data;
    }

    @Override
    protected Void doInBackground(String... strings) {
        CashRepository cashManager = new CashRepository(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(data.getOrderId());

        Log.e("TransactionLength==>", allTransactions.size() + "");
        for (TransactionModel model: allTransactions) {
            Log.e("cashSubmitted==>", model.cashSubmitted + "");
            Log.e("cashServerId==>", model.serverId + "");
            Log.e("cashType==>", model.type + "");
            Log.e("cashIn==>", model.inCash + "");
            Log.e("cashAmount==>", model.amount + "");

            if(model.cashSubmitted==0){
                switch (model.type){
                    case 0:
                        String entryType = "CASH_ORDER";
                        if(model.inCash == 0){
                            entryType = "CARD_ORDER";
                        }
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),entryType,"");
                        break;
                    case 2:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"DISCOUNT","");
                        break;
                    case 3:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"TIP","");
                        break;
                    case 4:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"REFUND","");
                        break;
                    case 5:
                        submitCashOrder(model.cashId,model.amount,data.getServerId(),"ADJUSTMENT",model.note);
                        break;
                    default:
                }
            }
        }
        return null;
    }


    void submitCashOrder(int dbID,double amount,String orderId,String entryType,String comment){
        Log.e("mmmmmm", "submitCashOrder: "+orderId+" => "+entryType+" => "+amount);
        if (!orderId.equals("") && !orderId.equals("0")) {
            OkHttpClient client = new OkHttpClient();
            String token = providerSession;
            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("entry_type", entryType);
                jsonObject.put("amount",amount);
                jsonObject.put("comment",comment);
                jsonObject.put("payment_uuid","");
                jsonObject.put("provider_uuid", providerID);
            }catch (Exception e){
                e.printStackTrace();
            }
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());

            Request request = new Request.Builder()
                    .url(BASE_URL+"order/"+orderId+"/cash_entry")
                    .addHeader("Content-Type","application/json")
                    .post(requestBody)
                    .addHeader("ProviderSession",token==null?"":token)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                if(response!=null){
                    if(response.code() == 201){
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,true);
                        SharedPrefManager.removeUpdatedOrder(context, data.getOrderId(), data.getStatus());
                    }else if(response.code() == 409){
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,true);
                        SharedPrefManager.removeUpdatedOrder(context, data.getOrderId(), data.getStatus());
                    } else{
                        CashRepository cashManager = new CashRepository(context);
                        cashManager.updateTransaction(dbID,false);
                        Log.d("mmmmm", "submitCashOrder: "+orderId+"  "+response.body().string()+"  => "+jsonObject);
                    }

                    Log.d("mmmmmm", "submitCashOrder: result code = "+response.code());
                }

            }catch (Exception e){
                FirebaseCrashlytics.getInstance().recordException(e);
            }
        }
    }
}
