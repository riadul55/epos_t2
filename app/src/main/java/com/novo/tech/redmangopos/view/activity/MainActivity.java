package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callerId.app.TheApp;

public class MainActivity extends AppCompatActivity {
    public TextView connectStatus,deviceCallerId,deviceLineStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectStatus = findViewById(R.id.deviceStatus);
        deviceCallerId = findViewById(R.id.channelStatusCallerID);
        deviceLineStatus = findViewById(R.id.channelStatusLine);


    }

    @Override
    protected void onStart() {
        super.onStart();


        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if ("PHONE_CALL".equals(intent.getAction())) {
                    int calling = intent.getIntExtra("state", 0);


                    connectStatus.setText(TheApp.deviceConnect);
                    deviceCallerId.setText(TheApp.channelList.get(TheApp.selectedChannel).CallerId);
                    deviceLineStatus.setText(TheApp.channelList.get(TheApp.selectedChannel).LineStatus);
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("PHONE_CALL"));

    }
}