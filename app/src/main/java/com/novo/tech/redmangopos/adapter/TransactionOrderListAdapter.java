package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.view.fragment.TransactionDelete;
import com.novo.tech.redmangopos.model.OrderModel;

import java.util.ArrayList;
import java.util.List;

public class TransactionOrderListAdapter extends RecyclerView.Adapter<TransactionOrderListAdapter.OrderListViewHolder> {

    Context mCtx;
    public List<OrderModel> orderList;
    public List<OrderModel> selected = new ArrayList<>();
    TransactionDelete transactionDelete;


    public TransactionOrderListAdapter(Context mCtx, List<OrderModel> orderList) {
        this.mCtx = mCtx;
        this.orderList = orderList;
        transactionDelete =  (TransactionDelete)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.settingFragmentView);
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_transaction_item_view, parent, false);

        return new OrderListViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        Log.d("mmmmm", "onBindViewHolder: called");

        OrderModel order = orderList.get(position);
        holder.orderId.setText(String.valueOf(order.order_id));
        holder.orderType.setText(order.order_type.toUpperCase());
        if(order.serverId != 0)
            holder.serverId.setText(String.valueOf(order.serverId));
        if(order.customer!=null){
            if(order.customer.profile != null){
                holder.customerName.setText(order.customer.profile.first_name+"");
                holder.customerPhone.setText(order.customer.profile.phone);
            }

        }
        if(DateTimeHelper.getDBTime().equals(DateTimeHelper.formatDBDate(order.orderDateTime)))
            holder.orderTime.setText("Today " + order.orderDateTime.substring(11) );
        else
            holder.orderTime.setText(order.orderDateTime);

        if(order.deliveryType == null){
            holder.deliveryTime.setText("ASAP");
        }else if(order.deliveryType.equals("ASAP")){
            holder.deliveryTime.setText("ASAP");
        }else if(!order.currentDeliveryTime.equals("")){
            holder.deliveryTime.setText(order.currentDeliveryTime);
        }

        if(order.order_channel==null)
            order.order_channel = "EPOS";
        if(order.paymentStatus == null){
            order.paymentStatus = "UNPAID";
        }
        boolean selectedItem = false;
        for (OrderModel order1 : selected) {
            if(order1.db_id == orderList.get(position).db_id){
                selectedItem = true;
                break;
            }
        }
        holder.selectedBox.setChecked(selectedItem);

        if(selected.size()>0){
            transactionDelete.delete.setVisibility(View.VISIBLE);
        }else{
            transactionDelete.delete.setVisibility(View.GONE);
        }

        boolean finalSelectedItem = selectedItem;
        holder.selectedBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(finalSelectedItem){
                    selected.removeIf((a)->a.db_id==orderList.get(position).db_id);
                }else{
                    selected.add(orderList.get(position));
                }

                notifyDataSetChanged();
            }
        });
        boolean isSelected = false;
        for (OrderModel orderModel: selected) {
            if (orderModel.db_id == order.db_id) {
                isSelected = true;
                break;
            }
        }

        if(isSelected){
           // holder.itemView.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.Beige));
            holder.parent_card.setCardBackgroundColor(ContextCompat.getColor(mCtx,R.color.Beige));
        }else if(order.order_channel != null){
            if (order.order_channel.equals("ONLINE")){
                if(order.order_status.equals("REVIEW")){
                    holder.parent_card.setBackgroundColor(Color.parseColor("#e5f595"));
                }else
                    holder.parent_card.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.white));
            }
        }
        else{
            holder.parent_card.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.white));
        }

        holder.amount.setText("£ "+String.format("%.2f",order.total));
        holder.cashPaid.setText("£ "+String.format("%.2f",order.cashPaid));
        holder.cardPaid.setText("£ "+String.format("%.2f",order.cardPaid));

        holder.status.setText(order.order_status);
        holder.terminal.setText(order.order_channel);


        /*** adjustment with order status and icon 'a'***/
        if (order.order_status.equals("REVIEW"))
            holder.statusImage.setImageResource(R.drawable.ic_review);
        if (order.order_status.equals("PROCESSING"))
            holder.statusImage.setImageResource(R.drawable.ic_processing);
        if (order.order_status.equals("READY"))
            holder.statusImage.setImageResource(R.drawable.ic_ready);
        if (order.order_status.equals("SENT"))
            holder.statusImage.setImageResource(R.drawable.ic_send);
        if (order.order_status.equals("DELIVERING"))
            holder.statusImage.setImageResource(R.drawable.ic_delivering);
        if (order.order_status.equals("DELIVERED"))
            holder.statusImage.setImageResource(R.drawable.ic_delivered);
        if (order.order_status.equals("CLOSED"))
            holder.statusImage.setImageResource(R.drawable.ic_closed);
        if (order.order_status.equals("CANCELLED"))
            holder.statusImage.setImageResource(R.drawable.ic_cancelled);
        if (order.order_status.equals("NEW"))
            holder.statusImage.setImageResource(R.drawable.ic_baseline_star_outline_24);

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView orderId,orderType,orderTime,deliveryTime,status,amount,serverId,cardPaid,cashPaid;
        ImageView statusImage;
        TextView customerName,customerPhone,terminal;
        CardView parent_card;
        MaterialCheckBox selectedBox;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            orderId = itemView.findViewById(R.id.orderListSL);
            orderType = itemView.findViewById(R.id.orderListOrderType);
            selectedBox = (MaterialCheckBox) itemView.findViewById(R.id.itemCheckBox);
            orderTime = itemView.findViewById(R.id.orderListItemOrderTime);
            deliveryTime = itemView.findViewById(R.id.orderListItemDeliveryTime);
            status = itemView.findViewById(R.id.orderListItemStatus);
            statusImage = itemView.findViewById(R.id.orderListItemStatusImage);
            amount = itemView.findViewById(R.id.orderListItemAmount);
            customerName = itemView.findViewById(R.id.orderListItemUserName);
            customerPhone = itemView.findViewById(R.id.orderListItemUserPhone);
            terminal = itemView.findViewById(R.id.orderListItemTerminal);
            serverId = itemView.findViewById(R.id.serverId);
            parent_card = itemView.findViewById(R.id.parent_card);
            cashPaid = itemView.findViewById(R.id.orderListItemCashPaid);
            cardPaid = itemView.findViewById(R.id.orderListItemCardPaid);
        }
    }
}