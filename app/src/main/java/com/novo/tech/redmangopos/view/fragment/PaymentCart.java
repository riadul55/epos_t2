package com.novo.tech.redmangopos.view.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.activity.Dashboard;
import com.novo.tech.redmangopos.view.activity.OnlineOrderDetails;
import com.novo.tech.redmangopos.view.activity.OrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.PaymentCartListAdapter;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.utils.SunmiPrintHelper;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class PaymentCart extends Fragment implements View.OnClickListener {
    TextView type,orderId,manualDiscount,subTotal,total,tips,deliveryCharge,orderStatus,adjustmentTitle,adjustmentAmount,plasticBag,tableNo,containerBagAmount;
    RelativeLayout creditInputContainer,plasticBagCartContainer,containerBagContainer,tableNoContainer;
    ImageView adjustmentNoteContainer;
    RecyclerView recyclerViewList;
    PaymentCartListAdapter adapter;
    LinearLayout back,kitchen;
    PaymentCalculator paymentCalculator;
    public PaymentMethod paymentMethod;
    public CartItem bag = null,containerBag=null;
    public OrderModel orderModel = new OrderModel();
    public boolean plasticBagOption = false;
    public boolean containerOption = false;
    List<CartItem> cartItemList = new ArrayList<>();
    public boolean isCheckout = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_payment_cart, container, false);
        orderModel = (OrderModel) requireArguments().getSerializable("orderModel");
        isCheckout = requireArguments().getBoolean("isCheckout");
        Log.e("DiscountAmount==>", orderModel.discountAmount + "");
        type = v.findViewById(R.id.paymentCartOrderType);
        orderId = v.findViewById(R.id.paymentCartOrderNumber);
        manualDiscount = v.findViewById(R.id.paymentCartManualDiscount);
        subTotal = v.findViewById(R.id.paymentCartSubTotal);
        deliveryCharge = v.findViewById(R.id.paymentCartDeliveryCharge);
        tips = v.findViewById(R.id.paymentCartTips);
        orderStatus = v.findViewById(R.id.paymentCartOrderStatus);
        tableNoContainer = v.findViewById(R.id.tableNoContainer);
        tableNo = v.findViewById(R.id.orderCreateFragmentOrderTableNo);
        total = v.findViewById(R.id.paymentCartTotal);
        adjustmentAmount = (TextView) v.findViewById(R.id.orderDetailsCartAdjustment);
        adjustmentTitle = (TextView) v.findViewById(R.id.orderDetailsCartAdjustmentNote);
        adjustmentNoteContainer = v.findViewById(R.id.iv_orderDetailsCartAdjustmentNote);
        creditInputContainer = v.findViewById(R.id.creditInput);
        plasticBagCartContainer = v.findViewById(R.id.plasticBagCartContainer);
        containerBagContainer = v.findViewById(R.id.containerBagCartContainer);
        plasticBag = v.findViewById(R.id.orderDetailsCartPlasticBagAmount);
        containerBagAmount = v.findViewById(R.id.orderDetailsCartContainerBagAmount);
        back = v.findViewById(R.id.paymentCartBack);
        kitchen = v.findViewById(R.id.paymentCartKitchen);
        recyclerViewList = v.findViewById(R.id.paymentCartRecyclerView);
        orderId.setText("Order : # "+String.valueOf(orderModel.order_id));
        type.setText(orderModel.order_type);
        back.setOnClickListener(this);
        kitchen.setOnClickListener(this);
        creditInputContainer.setOnClickListener(this);
        adjustmentNoteContainer.setOnClickListener(this);
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));

        paymentCalculator = (PaymentCalculator) (requireActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCenter);
        paymentMethod = (PaymentMethod) (requireActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentRight);

        plasticBagOption = SharedPrefManager.getPlasticBagMandatory(getContext());
        containerOption = SharedPrefManager.getContainerBagEnable(getContext());

        bag = getPlasticItem();
        containerBag = getContainerItem();
        if(orderModel.tableBookingModel!=null){
            if (orderModel.bookingId !=0 && !orderModel.order_channel.equals("ONLINE")) {
                orderModel.items = BookingOrdersHelper.getMergedItems(getContext(), orderModel.bookingId, orderModel.order_status);
            }
        }

        adapter = new PaymentCartListAdapter(getContext(), cartItemList);
        recyclerViewList.setAdapter(adapter);

        if(orderModel.tableBookingModel!=null) {
            tableNoContainer.setVisibility(View.VISIBLE);
            String tableStr = String.valueOf(orderModel.tableBookingModel == null ? "N/A" : orderModel.tableBookingModel.table_no);
            tableNo.setText(tableStr);
        }

        loadData();
        return v;
    }

    public void loadData(){

        if(orderModel.items == null){
            orderModel.items = new ArrayList<>();
        }
        List<CartItem> _items = orderModel.items;
        if(_items==null){
            _items = new ArrayList<>();
        }
        for (CartItem it : _items){
            if(it.type.equals("DELIVERY")){
                deliveryCharge.setText(String.format(Locale.getDefault(),"%.2f",it.total));
                orderModel.deliveryCharge = it.total;
            }
        }
        _items.removeIf((obj)->obj.type.equals("DELIVERY"));
        cartItemList.clear();
        cartItemList.addAll(_items);
        cartItemList.removeIf((a)->a.uuid.equals("plastic-bag"));
        cartItemList.removeIf((a)->a.uuid.equals("container"));
        adapter.notifyDataSetChanged();

        calculateTotal();
    }

    @SuppressLint("DefaultLocale")
    public void calculateTotal(){
        if(orderModel.items == null){
            orderModel.items = new ArrayList<>();
        }
        orderModel.items.sort(Comparator.comparingInt(a -> a.localId));
        Collections.reverse(orderModel.items);

        try {
//            DBOrderManager orderManager = new DBOrderManager(requireContext());
            CashRepository dbCashManager = new CashRepository(requireContext());
//            List<TransactionModel> orderTransaction = new ArrayList<>();
//            if (orderModel.bookingId != 0) {
//                List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(orderModel.bookingId, "");
//
//                for (OrderModel model: ordersByBookingId) {
//                    dbCashManager.open();
//                    List<TransactionModel> transactions = dbCashManager.getOrderTransaction(model.db_id);
//                    dbCashManager.close();
//                    orderTransaction.addAll(transactions);
//                }
//            } else {
//                  orderTransaction.addAll(transactions);
//            }
            List<TransactionModel> orderTransaction = dbCashManager.getOrderTransaction(orderModel.db_id);

            double totalRefundAmount = 0;
            for (int i=0;i<orderTransaction.size();i++) {
                if (orderTransaction.get(i).type == 4 && orderTransaction.get(i).inCash == 1) {
                    totalRefundAmount+=orderTransaction.get(i).amount;
                }
            }
            orderModel.refundAmount = totalRefundAmount;
        } catch (Exception e) {}

        double subTotalAmount = 0;
        double discountableTotal = 0;
        double subTotalPaid = 0;
        for (CartItem item : orderModel.items){
            if(item.uuid.equals("plastic-bag")){
                orderModel.plasticBagCost = item.quantity*item.price;
                bag = item;
            }else if(item.uuid.equals("container")){
                orderModel.containerBagCost = item.quantity*item.price;
                containerBag = item;
            }else if(item.type.equals("DELIVERY")){
                deliveryCharge.setText(String.format(Locale.getDefault(),"%.2f",item.total));
                orderModel.deliveryCharge = item.total;
            }else {
                double price = 0;
                if(!item.offered){
                    price=(item.subTotal*item.quantity);
                    if (item.extra != null) {
                        for (CartItem extraItem: item.extra) {
                            price+=extraItem.price;
                        }
                    }
                }
                else
                    price = item.total;


                subTotalAmount += price;
                if(item.discountable){
                    discountableTotal+=price;
                } else {
                    if (orderModel.isDiscountOverride) {
                        discountableTotal+=price;
                    }
                }

                if (!item.paymentMethod.equals("CASH")) {
                    subTotalPaid += price;
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.subPaid = subTotalPaid;
        if(!orderModel.order_channel.equals("ONLINE") && !orderModel.fixedDiscount){
            orderModel.discountAmount = (discountableTotal*((float)orderModel.discountPercentage/100));
        }
        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.plasticBagCost+orderModel.containerBagCost+orderModel.tips;

        if(paymentMethod != null){
            paymentMethod.setPlasticBagValue(bag);
            paymentMethod.setContainerValue(containerBag);
        }

        if(orderModel.order_status==null){
            orderModel.order_status = "NEW";
        }

        orderStatus.setText(orderModel.order_status);

        total.setText("£ "+String.format("%.2f", orderModel.total));
        manualDiscount.setText((!orderModel.fixedDiscount?("( "+String.valueOf(orderModel.discountPercentage)+"%) "):"")+ "-£ "+String.format("%.2f", orderModel.discountAmount));
        subTotal.setText("£ "+String.format("%.2f", orderModel.subTotal));
        tips.setText("£ "+String.format("%.2f", orderModel.tips));
        deliveryCharge.setText("£ "+String.format("%.2f", orderModel.deliveryCharge));
        adjustmentAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.adjustmentAmount));
        adjustmentTitle.setText(orderModel.adjustmentNote);
        plasticBag.setText(String.format(Locale.getDefault(),"%.2f",orderModel.plasticBagCost));
        containerBagAmount.setText(String.format(Locale.getDefault(),"%.2f",orderModel.containerBagCost));
        orderModel.dueAmount = orderModel.total - orderModel.totalPaid - orderModel.subPaid;

        Log.e("TotalDueAmount===>",  orderModel.total + " - " + orderModel.totalPaid + " = " +orderModel.dueAmount);
        if(!plasticBagOption){
            plasticBagCartContainer.setVisibility(View.GONE);
        }
        if(!containerOption){
            containerBagContainer.setVisibility(View.GONE);
        }

//        if(orderModel.adjustmentAmount>0){
//            creditInputContainer.setVisibility(View.VISIBLE);
//            adjustmentNoteContainer.setVisibility(View.VISIBLE);
//        }else{
//            creditInputContainer.setVisibility(View.GONE);
//            adjustmentNoteContainer.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onClick(View v) {
        if (paymentMethod != null && paymentMethod.comments != null) {
            orderModel.comments = paymentMethod.comments.getText().toString();
        }

        if (orderModel.paymentStatus ==null)
            orderModel.paymentStatus="UNPAID";

        if(v.getId() == R.id.paymentCartBack){
            backToOrder();
        }else if(v.getId() == kitchen.getId()){
            try {
                OrderRepository manager = new OrderRepository(requireContext());
                OrderModel orderData = manager.getOrderData(orderModel.db_id);
                if (orderModel.bookingId != 0){
                    orderData.dueAmount = orderData.total - orderData.totalPaid;
                    orderData.items = BookingOrdersHelper.getMergedItems(getContext(), orderData.bookingId, orderData.order_status);
                    orderData.total = orderModel.total;
                    orderData.subTotal = orderModel.subTotal;
                }
                CashMemoHelper.printKitchenMemo(orderData,getContext(),true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(v.getId() == R.id.creditInput){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditAmountAlert();
            }
        }else if(v.getId() == R.id.iv_orderDetailsCartAdjustmentNote){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditNoteAlert();
            }
        }
    }
    void creditAmountAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL);
        if(orderModel.adjustmentAmount == 0){
            inputField.setText("");
        }else{
            inputField.setText(String.valueOf(orderModel.adjustmentAmount));
        }
        androidx.appcompat.app.AlertDialog builder = new androidx.appcompat.app.AlertDialog.Builder(getContext())
                .setTitle("Credit")
                .setView(inputField)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String str = inputField.getText().toString();
                        if(str.equals("")){
                            str = "0";
                        }
                        try{
                            double amount = Double.parseDouble(str);
                            creditAmount(amount);
                        }catch (Exception e){
                            Toast.makeText(getContext(),"Invalid input",Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditAmount(double amount){
        orderModel.adjustmentAmount = amount;
        calculateTotal();
       // actionFragment.totalAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f",(orderModel.total)));

        OrderRepository repository = new OrderRepository(getContext());
//        DBOrderManager dbOrderManager = new DBOrderManager(getContext());
        OrderModel orderData = repository.getOrderData(orderModel.db_id);
        orderData.adjustmentAmount = amount;
        SharedPrefManager.createUpdateOrder2(getContext(), orderData);
        Log.e("adjustmentAmount", amount + "");

        paymentCalculator.setData();
    }


    void creditNoteAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_TEXT);
        inputField.setText(orderModel.adjustmentNote);
        androidx.appcompat.app.AlertDialog builder = new androidx.appcompat.app.AlertDialog.Builder(getContext())
                .setTitle("Credit Note")
                .setView(inputField)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        creditNote(inputField.getText().toString());
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditNote(String str){
        orderModel.adjustmentNote = str;

        OrderRepository repository = new OrderRepository(getContext());
//        DBOrderManager dbOrderManager = new DBOrderManager(getContext());
        OrderModel orderData = repository.getOrderData(orderModel.db_id);
        orderData.adjustmentNote = str;
        SharedPrefManager.createUpdateOrder2(getContext(), orderData);

        calculateTotal();
    }

    public void payNow(boolean fullAmount,double amount,String paymentType){
        if(orderModel.order_status.equals("NEW"))
            orderModel.order_status="REVIEW";

        /**********
         * Update Cash Drawer
         */
        boolean inCash = false;
        if(paymentType.equals("CASH")){
            inCash = true;
            SunmiPrintHelper.getInstance().openCashBox();
        }

//        double givenAmount = amount;

        CashRepository dbCashManager = new CashRepository(getContext());
//        if (orderModel.tableBookingModel != null && orderModel.bookingId != 0) {
//            DBOrderManager orderManager = new DBOrderManager(getContext());
//            List<OrderModel> ordersByBookingId = orderManager.getOrdersByBookingId(orderModel.bookingId, "");
//            for (int i=0;i<ordersByBookingId.size();i++) {
//                OrderModel orderData = ordersByBookingId.get(i);
//                DecimalFormat df = new DecimalFormat("###.##");
//
//                if (orderData.paymentStatus.equals("UNPAID")) {
//
//                    double dueAmount = orderData.dueAmount == 0.0 ? orderData.total - orderData.totalPaid : orderData.dueAmount;
//
//                    Log.e("payNow==>", dueAmount + "==" + givenAmount);
//                    long val = 0;
//                    if (givenAmount > 0 && dueAmount > 0 && dueAmount <= givenAmount) {
//                        dbCashManager.open();
//                        val = dbCashManager.addTransaction(getContext(),dueAmount,0,orderData.db_id,inCash);
//                        dbCashManager.close();
//
//                        orderData.totalPaid+=dueAmount;
//                        orderData.dueAmount = orderData.total - orderData.totalPaid;
//
//                        orderModel.totalPaid+=orderData.totalPaid;
//
//                        givenAmount = Double.parseDouble(df.format(givenAmount - dueAmount));
//
//                        Log.e("payNow1==>", dueAmount + "==" + givenAmount);
//                    } else if (givenAmount > 0 && dueAmount >0 && dueAmount > givenAmount) {
//                        dbCashManager.open();
//                        val = dbCashManager.addTransaction(getContext(),givenAmount,0,orderData.db_id,inCash);
//                        dbCashManager.close();
//
//                        orderData.totalPaid+=givenAmount;
//                        orderData.dueAmount = orderData.total - orderData.totalPaid;
//
//                        orderModel.totalPaid+=orderData.totalPaid;
//
//                        givenAmount = Double.parseDouble(df.format(givenAmount - dueAmount));
//
//                        Log.e("payNow2==>", dueAmount + "==" + givenAmount);
//                    }
//
//                    if (val != 0) {
//                        if(orderData.order_channel.equals("ONLINE")){
//                            orderData.receive = orderData.subTotal;
//                        }else{
//                            orderData.receive = orderData.totalPaid;
//                        }
//                        if(orderData.dueAmount < 0.01){
//                            orderData.paymentStatus="PAID";
//                            if(SharedPrefManager.getOrderCloseOnPayment(getContext())){
//                                orderData.order_status = "CLOSED";
//                            }
//                        }
//                        else
//                            orderData.paymentStatus="UNPAID";
//
//                        if(inCash){
//                            orderData.cashId = (int)val;
//                        }else{
//                            orderData.paymentUid = String.valueOf(val);
//                        }
//
//                        DBOrderManager dbOrderManager = new DBOrderManager(getContext());
//                        dbOrderManager.open();
//                        dbOrderManager.addOrUpdateOrder2(orderData);
//                        dbOrderManager.close();
//
//
//                    }
//
////              SharedPrefManager.createUpdateOrder(getContext(),orderModel);
//                }
//            }
//
//            orderModel.dueAmount = orderModel.total - orderModel.totalPaid;
//
//            if(orderModel.order_channel.equals("ONLINE")){
//                orderModel.receive = orderModel.subTotal;
//            }else{
//                orderModel.receive = orderModel.totalPaid;
//            }
//            if(orderModel.dueAmount < 0.01){
//                orderModel.paymentStatus="PAID";
//                if(SharedPrefManager.getOrderCloseOnPayment(getContext())){
//                    orderModel.order_status = "CLOSED";
//                }
//            }
//            else
//                orderModel.paymentStatus="UNPAID";
//
//
//
//            Log.e("payNow4==>", givenAmount + "==" + orderModel.totalPaid);
//        } else {
//        }

        long val = dbCashManager.addTransaction(getContext(),amount,0,orderModel.db_id,inCash);


        OrderRepository orderManager = new OrderRepository(getContext());
        OrderModel orderData = orderManager.getOrderData(orderModel.db_id);
        orderModel.items = orderData.items;
        orderModel.totalPaid+=amount;
        orderModel.dueAmount = orderModel.total - orderModel.totalPaid - orderModel.subPaid;

        if(orderModel.order_channel.equals("ONLINE")){
            orderModel.receive = orderModel.subTotal;
        }else{
            orderModel.receive = orderModel.totalPaid;
        }
        if(orderModel.dueAmount < 0.01){
            orderModel.paymentStatus="PAID";
            if(SharedPrefManager.getOrderCloseOnPayment(getContext())){
                orderModel.order_status = "CLOSED";
            }
        }
        else
            orderModel.paymentStatus="UNPAID";

        if(inCash){
            orderModel.cashId = (int)val;
        }else{
            orderModel.paymentUid = String.valueOf(val);
        }
        SharedPrefManager.createUpdateOrder2(getContext(),orderModel);
        /*****
         * END
         */
        if(SharedPrefManager.getPrintOnPayButtonClick(getContext()) && !(orderModel.order_type.equals("LOCAL") && orderModel.order_channel.equals("WAITERAPP")) && !orderModel.order_channel.equals("ONLINE")){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                    .setTitle("Do you want to print?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                OrderRepository manager = new OrderRepository(requireContext());
                                OrderModel orderData = manager.getOrderData(orderModel.db_id);
                                CashMemoHelper.printCustomerMemo(orderData,getContext(),SharedPrefManager.getPrintCount(getContext()) == 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            afterAction();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            afterAction();
                        }
                    });
            builder.show();

        }else{
            afterAction();
        }
    }
    void afterAction(){
        if(orderModel.order_status.equals("CLOSED")){
            backToDashboard();
        }else if(orderModel.dueAmount < 0.001){
            Intent in=new Intent(getContext(), OrderDetails.class);
            if(orderModel.order_channel != null)
                if(!orderModel.order_channel.equals("EPOS")){
                    in = new Intent(getContext(), OnlineOrderDetails.class);
                }
            Bundle b = new Bundle();
            b.putInt("orderData",orderModel.db_id);
            in.putExtras(b);
            in.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
            requireActivity().finish();
        }
    }

    public void voidOrderAlert(){
        String[] options = { "Some item are not available ", "Customer request", "Technical issue ", "Recall- Opened", "Recall-Unopened ", "Other",};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Void order cause of");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                voidOrder(options[which]);
            }
        });
        builder.create().show();
    }
    void voidOrder(String message){
        OrderRepository orderManager = new OrderRepository(getContext());
        orderManager.delete(orderModel);
        Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//        gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        gotoScreenVar.putExtra("shift","EXISTING");
        startActivity(gotoScreenVar);

    }
    public void backToOrder(){
        if (paymentMethod != null && paymentMethod.comments != null) {
            orderModel.comments = paymentMethod.comments.getText().toString();
        } else {
            orderModel.comments = "";
        }
//        orderModel.discountAmount = (orderModel.subTotal*((float)paymentMethod.discountPercentage/100));
//        orderModel.discountPercentage = paymentMethod.discountPercentage;
//        SharedPrefManager.createUpdateOrder(getContext(),orderModel);

        OrderRepository repository = new OrderRepository(getContext());
        OrderModel orderData = repository.getOrderData(orderModel.db_id);
        orderData.comments = orderModel.comments;
        SharedPrefManager.createUpdateOrder2(getContext(),orderData);

        if(orderModel.paymentStatus!= null){
            if (orderModel.paymentStatus.equals("PAID") || orderModel.order_status.equals("REFUNDED")){
                backToDashboard();
            }else{
                if (isCheckout) {
                    backToEdit();
                } else {
                    backToDashboard();
                }
            }
        }else{
            if (isCheckout) {
                backToEdit();
            } else {
                backToDashboard();
            }
        }
    }
    public void inCreasePlasticBag(){
        boolean exist = false;
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).uuid.equals("plastic-bag")){
                exist = true;
                orderModel.items.get(i).quantity++;
                loadData();
                paymentCalculator.setData();

                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                for (int j=0;j<orderData.items.size();j++) {
                    if (orderData.items.get(j).uuid.equals("plastic-bag")) {
                        orderData.items.get(j).quantity++;
                        SharedPrefManager.createUpdateOrder2(getContext(), orderData);
                    }
                }
            }
        }
        if(!exist){
            CartItem cartItem = getPlasticItem();
            if(cartItem != null){
                cartItem.quantity = 1;
                cartItem.subTotal = cartItem.price * cartItem.quantity;
                cartItem.total = cartItem.subTotal;
                orderModel.items.add(getPlasticItem());
                loadData();
                paymentCalculator.setData();


                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                orderData.items.add(getPlasticItem());
                SharedPrefManager.createUpdateOrder2(getContext(), orderData);
            }
        }
    }
    public void deCreasePlasticBag(){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).uuid.equals("plastic-bag")){
                if(orderModel.items.get(i).quantity > 0)
                    orderModel.items.get(i).quantity--;
                loadData();
                paymentCalculator.setData();


                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                for (int j=0;j<orderData.items.size();j++) {
                    if (orderData.items.get(j).uuid.equals("plastic-bag")) {
                        if (orderData.items.get(j).quantity > 0) {
                            orderData.items.get(j).quantity--;
                        }
                        SharedPrefManager.createUpdateOrder2(getContext(), orderData);
                    }
                }
            }
        }
    }
    public void inCreaseContainerBag(){
        boolean exist = false;
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).uuid.equals("container")){
                exist = true;
                orderModel.items.get(i).quantity++;
                loadData();
                paymentCalculator.setData();


                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                for (int j=0;j<orderData.items.size();j++) {
                    if (orderData.items.get(j).uuid.equals("container")) {
                        orderData.items.get(j).quantity++;
                        SharedPrefManager.createUpdateOrder2(getContext(), orderData);
                    }
                }
            }
        }
        if(!exist){
            CartItem cartItem = getContainerItem();
            if(cartItem != null){
                cartItem.quantity = 1;
                cartItem.subTotal = cartItem.price * cartItem.quantity;
                cartItem.total = cartItem.subTotal;
                orderModel.items.add(getContainerItem());
                loadData();
                paymentCalculator.setData();


                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                orderData.items.add(getContainerItem());
                SharedPrefManager.createUpdateOrder2(getContext(), orderData);
            }
        }
    }
    public void deCreaseContainerBag(){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).uuid.equals("container")){
                if(orderModel.items.get(i).quantity > 0)
                    orderModel.items.get(i).quantity--;
                loadData();
                paymentCalculator.setData();


                OrderRepository repository = new OrderRepository(getContext());
                OrderModel orderData = repository.getOrderData(orderModel.db_id);
                for (int j=0;j<orderData.items.size();j++) {
                    if (orderData.items.get(j).uuid.equals("container")) {
                        if (orderData.items.get(j).quantity > 0) {
                            orderData.items.get(j).quantity--;
                        }
                        SharedPrefManager.createUpdateOrder2(getContext(), orderData);
                    }
                }
            }
        }
    }

    void backToEdit(){
//        Intent intent = new Intent();
//        intent.putExtra("orderModel",orderModel);
//        requireActivity().setResult(RESULT_OK, intent);
        requireActivity().finish();
    }
    private void backToDashboard() {
        Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//        gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        gotoScreenVar.putExtra("shift","EXISTING");
        startActivity(gotoScreenVar);
    }

    private CartItem getPlasticItem(){
        Product pro = SharedPrefManager.getPlasticBagProduct(getContext());
        if(pro != null){
            return new CartItem(
                    0,
                    pro.productUUid,
                    pro.description,
                    pro.description,
                    new ArrayList<>(),
                    pro.price,
                    pro.offerPrice,
                    pro.price,
                    pro.price,
                    0,
                    0,
                    "",
                    "",
                    0,
                    false,false,false, pro.properties.kitchenItem, "",new ArrayList<>(), true,
                    "CASH"
            );
        }else
            return  null;

    }
    private CartItem getContainerItem(){
        Product pro = SharedPrefManager.getContainerBagProduct(getContext());
        if(pro != null){
            return new CartItem(
                    0,
                    pro.productUUid,
                    pro.description,
                    pro.description,
                    new ArrayList<>(),
                    pro.price,
                    pro.offerPrice,
                    pro.price,
                    pro.price,
                    0,
                    0,
                    "",
                    "",
                    0,
                    false,false,false, pro.properties.kitchenItem,"",new ArrayList<>(), true,
                    "CASH"
            );
        }else
            return  null;

    }
}
