package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.novo.tech.redmangopos.model.ApiCall;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendEmail extends AsyncTask<String, Integer, String> {
    protected Context context;

    public SendEmail(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {
        ArrayList<ApiCall> pullBy = SharedPrefManager.getPullBy(context);
        if (!pullBy.isEmpty()) {
            JSONArray jsonArray = new JSONArray();
            for (int i=0;i<pullBy.size();i++) {
                JSONObject item = new JSONObject();
                try {
                    item.put("calling_by", pullBy.get(i).getCallingBy());
                    item.put("time", pullBy.get(i).getTime());
                    jsonArray.put(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (isNetworkAvailable()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("subject", "API Requests track report");
                    jsonObject.put("status_update_text", "API Requests track report");
                    jsonObject.put("body", jsonArray);
                    jsonObject.put("app_name", "EPOS T2");
                    jsonObject.put("business_name", AppConstant.business);
                    jsonObject.put("email_type", "API_CALL_REPORT");
                } catch (Exception e) {
                    e.printStackTrace();
                }


                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
                Request request = new Request.Builder()
                        .url("https://email.redmango.online/api/email/send")
                        .post(requestBody)
                        .addHeader("Content-Type","application/json")
                        .addHeader("Accept","application/json")
                        .addHeader("Authorization", "Bearer 3|JdUKCKGyctQ3xb7iCBTU2Fnt5eJOJuaBZnCLI9lJ")
                        .build();
                Response response = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }

                if(response != null){
                    if(response.code() == 200){
                        SharedPrefManager.setEmailSendingTime(context, System.currentTimeMillis());
                        SharedPrefManager.clearPullBy(context);
                    }else{
                        Log.e("mmmmm", "createOrder: "+response.code()+"failed to create order"+ jsonObject);
                    }
                }
            }
        }
        return "process";
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
