package com.novo.tech.redmangopos.socket;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;

public class OrderModelToJson {

    public static JSONObject fromModel(OrderModel orderModel, Context context){
        JSONObject contentValue = new JSONObject();
        try{
            if (orderModel.comments == null) {
                orderModel.comments = "";
            }
            if (orderModel.shift <= 0) {
                orderModel.shift = SharedPrefManager.getCurrentShift(context);
            }

            double subTotalAmount = 0;
            for (CartItem item : orderModel.items) {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else
                    price = item.total;

                subTotalAmount += price;
            }

            orderModel.subTotal = subTotalAmount;
            orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips;

            String paymentStatus = orderModel.paymentStatus;
            if (orderModel.paymentMethod.equals("CARD")) {
                orderModel.totalPaid = orderModel.total;
                orderModel.receive = orderModel.totalPaid;
                orderModel.cardPaid = orderModel.totalPaid;
            }

            contentValue.put("db_id", orderModel.db_id);
            contentValue.put("order_id", orderModel.order_id);
            contentValue.put("server_id", orderModel.serverId);
            contentValue.put("order_channel", orderModel.order_channel);
            contentValue.put("date", orderModel.orderDate.length() == 12 ? orderModel.orderDate : DateTimeHelper.formatOnlyDBDate(orderModel.orderDate));
            contentValue.put("orderDateTime", orderModel.orderDateTime);
            contentValue.put("requestedDeliveryDateTime", orderModel.requestedDeliveryTime);
            contentValue.put("currentDeliveryDateTime", orderModel.currentDeliveryTime);
            contentValue.put("deliveryType", orderModel.deliveryType);
            contentValue.put("order_type", orderModel.order_type);
            contentValue.put("order_status", orderModel.order_status);
            contentValue.put("payment_method", orderModel.paymentMethod);
            contentValue.put("dueAmount", orderModel.dueAmount);
            contentValue.put("payment_status", paymentStatus);
            contentValue.put("comments", orderModel.comments);
            contentValue.put("subTotal", orderModel.subTotal);
            contentValue.put("total", orderModel.total);
            contentValue.put("discountableTotal", orderModel.discountableTotal);
            contentValue.put("paidTotal", orderModel.totalPaid);
            contentValue.put("discount", orderModel.discountAmount);
            contentValue.put("discountCode", orderModel.discountCode);
            contentValue.put("plasticBagCost", orderModel.plasticBagCost);
            contentValue.put("containerBagCost", orderModel.containerBagCost);
            contentValue.put("adjustment", orderModel.adjustmentAmount);
            contentValue.put("adjustmentNote", orderModel.adjustmentNote);
            contentValue.put("discountPercentage", orderModel.discountPercentage);
            contentValue.put("deliveryCharge", orderModel.deliveryCharge);
            contentValue.put("tips", orderModel.tips);
            contentValue.put("shift", orderModel.shift == -1 ? SharedPrefManager.getCurrentShift(context) : orderModel.shift);
            contentValue.put("paymentId", orderModel.paymentUid == null ? "-1" : orderModel.paymentUid);
            contentValue.put("cashId", orderModel.cashId == 0 ? -1 : orderModel.cashId);
            contentValue.put("receive", orderModel.receive);
            contentValue.put("refund", orderModel.refundAmount);
            contentValue.put("cloudSubmitted", 1);
            contentValue.put("fixedDiscount", 0);
            contentValue.put("customerInfo", orderModel.customer != null ? GsonParser.getGsonParser().toJson(orderModel.customer) : "");
            contentValue.put("shippingAddress", orderModel.shippingAddress != null ? GsonParser.getGsonParser().toJson(orderModel.shippingAddress) : "");
            contentValue.put("customerId", orderModel.customer != null ? String.valueOf(orderModel.customer.dbId) : "");

            contentValue.put("tableBookingId", orderModel.bookingId);
            contentValue.put("tableBookingInfo", orderModel.tableBookingModel != null ? GsonParser.getGsonParser().toJson(orderModel.tableBookingModel) : "");

            contentValue.put("cartItems", GsonParser.getGsonParser().toJson(orderModel.items));
        }catch (JSONException e){
            e.printStackTrace();
        }
        return contentValue;
    }

}
