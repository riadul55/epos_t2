package com.novo.tech.redmangopos.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "orders";
    public static final String ORDER_TABLE_BACKUP_NAME = "orders_BACKUP";
    public static final String SHIFT_TABLE_NAME = "shift";
    public static final String CASH_TABLE_NAME = "cash";
    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String ADDRESS_TABLE_NAME = "address";
    public static final String PRODUCT_TABLE_NAME = "customProduct";
    public static final String POST_CODE_TABLE_NAME = "postCodes";

    // Database Information
    static final String DB_NAME = "NOVO_TECH_DELIVERY_ORDER.DB";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_ORDER_TABLE = "CREATE TABLE IF NOT EXISTS orders ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "order_channel TEXT, " +
            "requester_type TEXT, " +
            "order_id INTEGER, " +
            "server_id INTEGER, " +
            "date NUMERIC, " +
            "orderDateTime NUMERIC, " +
            "requestedDeliveryDateTime NUMERIC, " +
            "currentDeliveryDateTime NUMERIC, " +
            "deliveryType TEXT, " +
            "order_type TEXT, " +
            "order_status TEXT, " +
            "payment_method TEXT, " +
            "payment_status TEXT, " +
            "comments TEXT, " +
            "subTotal REAL, " +
            "total REAL, " +
            "discountableTotal REAL, " +
            "paidTotal REAL, " +
            "dueAmount REAL, " +
            "discountPercentage INTEGER, " +
            "discount REAL, " +
            "plasticBagCost REAL, " +
            "containerBagCost REAL, " +
            "discountCode TEXT, " +
            "refund REAL, " +
            "adjustment REAL, " +
            "adjustmentNote Text, " +
            "tips REAL, " +
            "deliveryCharge REAL, " +
            "receive REAL, " +
            "cartItems BLOB, " +
            "cloudSubmitted NUMERIC," +
            "fixedDiscount NUMERIC," +
            "shift TEXT," +
            "customerId TEXT," +
            "shippingAddress TEXT," +
            "cashId TEXT," +
            "paymentId TEXT," +
            "tableBookingId INTEGER," +
            "tableBookingInfo TEXT," +
            "customerInfo TEXT," +
            "UNIQUE(date,order_id))";
    private static final String CREATE_ORDER_HISTORY_TABLE = "CREATE TABLE IF NOT EXISTS "+ORDER_TABLE_BACKUP_NAME+" ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "order_channel TEXT, " +
            "requester_type TEXT, " +
            "order_id INTEGER, " +
            "server_id INTEGER, " +
            "date NUMERIC, " +
            "orderDateTime NUMERIC, " +
            "requestedDeliveryDateTime NUMERIC, " +
            "currentDeliveryDateTime NUMERIC, " +
            "deliveryType TEXT, " +
            "order_type TEXT, " +
            "order_status TEXT, " +
            "payment_method TEXT, " +
            "payment_status TEXT, " +
            "comments TEXT, " +
            "subTotal REAL, " +
            "total REAL, " +
            "discountableTotal REAL, " +
            "paidTotal REAL, " +
            "dueAmount REAL, " +
            "discountPercentage INTEGER, " +
            "discount REAL, " +
            "plasticBagCost REAL, " +
            "containerBagCost REAL, " +
            "discountCode TEXT, " +
            "refund REAL, " +
            "adjustment REAL, " +
            "adjustmentNote Text, " +
            "tips REAL, " +
            "deliveryCharge REAL, " +
            "receive REAL, " +
            "cartItems BLOB, " +
            "cloudSubmitted NUMERIC," +
            "fixedDiscount NUMERIC," +
            "shift TEXT," +
            "customerId TEXT," +
            "shippingAddress TEXT," +
            "cashId TEXT," +
            "paymentId TEXT," +
            "tableBookingId INTEGER," +
            "tableBookingInfo TEXT," +
            "customerInfo TEXT," +
            "UNIQUE(date,order_id))";
    private static final String CREATE_SHIFT_TABLE = "CREATE TABLE IF NOT EXISTS shift ( id INTEGER PRIMARY KEY AUTOINCREMENT, openTime NUMERIC,closeTime NUMERIC,cloudSubmitted NUMERIC)";
    private static final String CREATE_CASH_TABLE = "CREATE TABLE IF NOT EXISTS cash ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "dateTime NUMERIC," +
            "amount REAL," +
            "user TEXT," +
            "type NUMERIC," +
            "shift NUMERIC," +
            "order_id NUMERIC," +
            "cloudSubmitted NUMERIC," +
            "inCash NUMERIC," +
            "localPayment NUMERIC" +
            ")";
    private static final String CREATE_PRODUCT_TABLE = "CREATE TABLE IF NOT EXISTS customProduct ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "productId TEXT," +
            "name TEXT," +
            "printOrder NUMERIC," +
            "discountable NUMERIC," +
            "cloudSubmitted NUMERIC," +
            "price NUMERIC)";

    private static final String CREATE_CUSTOMER_TABLE = "CREATE TABLE IF NOT EXISTS CUSTOMER ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "firstName TEXT, " +
            "lastName TEXT, " +
            "userName TEXT, " +
            "cloudSubmitted TEXT, " +
            "phone TEXT, " +
            "email TEXT, " +
            "consumer_uuid Text,"+
            "address TEXT)";

    private static final String CREATE_ADDRESS_TABLE = "CREATE TABLE IF NOT EXISTS "+ADDRESS_TABLE_NAME+" ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "customerDbId TEXT, " +
            "uuid TEXT, " +
            "type TEXT, " +
            "building TEXT, " +
            "streetName TEXT, " +
            "city TEXT, " +
            "state TEXT, " +
            "zip TEXT," +
            "isPrimary TEXT" +
            ")";

    private static final String CREATE_POST_CODE_TABLE = "CREATE TABLE IF NOT EXISTS "+POST_CODE_TABLE_NAME+" ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "postcode TEXT, " +
            "posttown TEXT, " +
            "dependent_locality TEXT, " +
            "double_dependent_locality TEXT, " +
            "thoroughfare TEXT, " +
            "dependent_thoroughfare TEXT, " +
            "building_number TEXT, " +
            "building_name TEXT," +
            "sub_building_name TEXT," +
            "po_box TEXT," +
            "department_name TEXT," +
            "organisation_name TEXT," +
            "udprn TEXT," +
            "postcode_type TEXT," +
            "su_org_indicator TEXT," +
            "delivery_point_suffix TEXT" +
            ")";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ORDER_TABLE);
        db.execSQL(CREATE_ORDER_HISTORY_TABLE);
        db.execSQL(CREATE_SHIFT_TABLE);
        db.execSQL(CREATE_CASH_TABLE);
        db.execSQL(CREATE_CUSTOMER_TABLE);
        db.execSQL("CREATE INDEX idx1 ON CUSTOMER(id, phone, consumer_uuid, firstName, lastName, email)");
        db.execSQL(CREATE_PRODUCT_TABLE);
        db.execSQL(CREATE_ADDRESS_TABLE);
        db.execSQL(CREATE_POST_CODE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ORDER_TABLE_BACKUP_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SHIFT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CASH_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CUSTOMER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PRODUCT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ADDRESS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + POST_CODE_TABLE_NAME);
        onCreate(db);
    }
}