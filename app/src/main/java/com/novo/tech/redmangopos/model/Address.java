package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class Address {
    public String streetName,building,city,state,zip;

    public Address(String streetName, String building, String city, String state, String zip) {
        this.streetName = streetName;
        this.building = building;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }
    public static Address fromJSON(JSONObject data){
        return new Address(
                data.optString("street_name",""),
                data.optString("building",""),
                data.optString("city",""),
                data.optString("state",""),
                data.optString("zip","")
        );
    }
}
