package com.novo.tech.redmangopos.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.novo.tech.redmangopos.model.JsonAddress;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.util.ConfigTypes;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("Range")
public class AddressDbHelper extends SQLiteAssetHelper {

    public static final String DB_NAME = getDBName();
    public static final int DB_VERSION = getDBVersion();

    public static String getDBName() {
        if (AppConstant.types == ConfigTypes.JKPERI) {
            return "ketterings.db.sqlite";
        } else if (AppConstant.types == ConfigTypes.SPICE_KITCHEN || AppConstant.types == ConfigTypes.RUPA_TANDOORI) {
            return "stevenage.db.sqlite";
        } else if (AppConstant.types == ConfigTypes.KEBAB_GERMAN) {
            return "bedford.db.sqlite";
        } else {
            return "address.db.sqlite";
        }
    }

    public static int getDBVersion() {
        return 1;
//        if (AppConstant.types == ConfigTypes.JKPERI) {
//        } else {
//            return 3;
//        }
    }

    String [] sqlSelect = {"postcode", "posttown", "dependent_locality", "double_dependent_locality",
    "thoroughfare", "dependent_thoroughfare", "building_number", "building_name", "sub_building_name",
    "po_box", "department_name", "organisation_name", "udprn", "postcode_type", "su_org_indicator",
    "delivery_point_suffix"};

    public AddressDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public List<JsonAddress> getListAddress() {
        JsonAddress address = null;
        List<JsonAddress> addressList = new ArrayList<JsonAddress>();
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("address_list");
        Cursor cursor = qb.query(db, sqlSelect, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            addressList.add(getAddressFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return addressList;
    }

    public List<JsonAddress> getSearchResult(String str) {
        List<JsonAddress> addressList = new ArrayList<JsonAddress>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID, postcode FROM address_list WHERE postcode LIKE ? LIMIT 50", new String[] {"%"+str+"%"});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            JsonAddress address = new JsonAddress();
            address.setId(cursor.getInt(cursor.getColumnIndex("ID")));
            address.setPostcode(cursor.getString(cursor.getColumnIndex("postcode")));
            addressList.add(address);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return addressList;
    }


    public List<JsonAddress> getSelectedResult(String str) {
        List<JsonAddress> addressList = new ArrayList<JsonAddress>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM address_list WHERE postcode = ? LIMIT 50", new String[] {str});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            addressList.add(getAddressFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return addressList;
    }

    JsonAddress getAddressFromCursor(Cursor cursor) {
        JsonAddress address = new JsonAddress();
        address.setPostcode(cursor.getString(cursor.getColumnIndex("postcode")));
        address.setPosttown(cursor.getString(cursor.getColumnIndex("posttown")));
        address.setDependentLocality(cursor.getString(cursor.getColumnIndex("dependent_locality")));
        address.setDoubleDependentLocality(cursor.getString(cursor.getColumnIndex("double_dependent_locality")));
        address.setThoroughfare(cursor.getString(cursor.getColumnIndex("thoroughfare")));
        address.setDependentThoroughfare(cursor.getString(cursor.getColumnIndex("dependent_thoroughfare")));
        address.setBuildingNumber(cursor.getInt(cursor.getColumnIndex("building_number")));
        address.setBuildingName(cursor.getString(cursor.getColumnIndex("building_name")));
        address.setSubBuildingName(cursor.getString(cursor.getColumnIndex("sub_building_name")));
        address.setPoBox(cursor.getString(cursor.getColumnIndex("po_box")));
        address.setDepartmentName(cursor.getString(cursor.getColumnIndex("department_name")));
        address.setOrganisationName(cursor.getString(cursor.getColumnIndex("organisation_name")));
        address.setUdprn(cursor.getInt(cursor.getColumnIndex("udprn")));
        address.setPostcodeType(cursor.getString(cursor.getColumnIndex("postcode_type")));
        address.setSuOrgIndicator(cursor.getString(cursor.getColumnIndex("su_org_indicator")));
        address.setDeliveryPointSuffix(cursor.getString(cursor.getColumnIndex("delivery_point_suffix")));
        return address;
    }
}
