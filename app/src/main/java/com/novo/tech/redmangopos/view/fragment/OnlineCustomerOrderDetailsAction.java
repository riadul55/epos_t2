package com.novo.tech.redmangopos.view.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.room_db.repositories.CashRepository;
import com.novo.tech.redmangopos.view.activity.OnlineOrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.ActionButtonsAdapter;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.view.activity.OrderDetails;

import java.util.Locale;

public class OnlineCustomerOrderDetailsAction extends Fragment implements MyCallBack {
    OrderModel orderModel;
    TextView totalAmount,orderDate,paymentMessage;
    public OnlineCustomerOrderDetailsCart cartListFragment;
    public OnlineCustomerOrderDetailsConsumerInfo consumerInfoFragment;
    ActionButtonsAdapter adapter;
    GridView gridView;
    AppCompatButton copyOrder;
    AppCompatButton payNow,closeNow;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_action, container, false);
        cartListFragment = (OnlineCustomerOrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        consumerInfoFragment = (OnlineCustomerOrderDetailsConsumerInfo)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsRight);
        totalAmount = v.findViewById(R.id.orderDetailsActionsTotalAmount);
        orderDate = v.findViewById(R.id.orderDetailsActionsTotalOrderDate);
        gridView = v.findViewById(R.id.actionGridView);
        paymentMessage = v.findViewById(R.id.paymentMessage);
        copyOrder = v.findViewById(R.id.payNowButton);
        orderModel = cartListFragment.orderModel;
        totalAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f",(orderModel.total)));
        orderDate.setText(orderModel.orderDateTime);
        closeNow = v.findViewById(R.id.closeButton);

        setActionView();
        setPay();

        copyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cash using as copy order
                copyOrder();
            }
        });

        closeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeTheOrder();
            }
        });
        return v;
    }
    void setActionView(){
        adapter = new ActionButtonsAdapter(getContext(),orderModel,this,true);
        gridView.setAdapter(adapter);
    }
    void setPay(){
        copyOrder.setText("Copy This Order");
    }

    @Override
    public void voidCallBack() {

    }

    void closeTheOrder(){
        orderModel = ((OrderDetails)getContext()).orderModel;
        if(!orderModel.paymentStatus.equals("PAID")){
            Toast.makeText(getContext(),"Unpaid Order can't closed",Toast.LENGTH_LONG).show();
        }else{
            orderModel.order_status = "CLOSED";
            orderModel.adjustmentAmount =  cartListFragment.orderModel.adjustmentAmount;
            orderModel.adjustmentNote =  cartListFragment.orderModel.adjustmentNote;
            cartListFragment.status.setText(orderModel.order_status);
            afterClose();

        }
    }

    void copyOrder(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Are sure you want copy this order as new order?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        orderModel.serverId= 0;
                        orderModel.order_id = SharedPrefManager.getLastOrderId(getContext())+1;
                        orderModel.order_status = "NEW";
                        orderModel.totalPaid = 0;
                        orderModel.discountAmount = 0;
                        orderModel.tips = 0;
                        orderModel.paymentStatus = "UNPAID";
                        orderModel.order_channel = "EPOS";
                        orderModel.orderDateTime = DateTimeHelper.getTime();
                        int id = SharedPrefManager.createUpdateOrder(getContext(),orderModel);
                        if(id!=0){
//                            Intent gotoScreenVar = new Intent(getContext(), Dashboard.class);
//                            gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            gotoScreenVar.putExtra("shift","EXISTING");
//                            startActivity(gotoScreenVar);

                            Intent in=new Intent(getContext(), OrderCreate.class);
                            Bundle b = new Bundle();
                            b.putInt("orderModel", id);
                            in.putExtras(b);
                            startActivity(in);
                        }
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();

    }




    @Override
    public void OrderCallBack(OrderModel order) {
//        long dateTime = System.currentTimeMillis();
//        SharedPrefManager.setUpdateData(getContext(), new UpdateData(order.db_id, order.serverId + "", order.order_channel, order.order_status, "" + dateTime));


        orderModel = ((OnlineOrderDetails)getContext()).orderModel;
        if(order.order_status.equals("CLOSED") && !orderModel.paymentStatus.equals("PAID")){
            Toast.makeText(getContext(),"Unpaid course can't closed",Toast.LENGTH_LONG).show();
        }else{
            orderModel.order_status = order.order_status;
            SharedPrefManager.createUpdateOrder(getContext(),orderModel);
            cartListFragment.status.setText(orderModel.order_status);
            if (order.order_status.equals("CLOSED")){
                order.adjustmentAmount =  cartListFragment.orderModel.adjustmentAmount;
                order.adjustmentNote =  cartListFragment.orderModel.adjustmentNote;
                if(order.adjustmentAmount !=0){
                    CashRepository dbCashManager = new CashRepository(getContext());
                    dbCashManager.addTransaction(getContext(),order.adjustmentAmount,5,order.db_id,true);
                }
            }
        }

    }

    public void afterClose(){
        boolean inCash = false;
        if(orderModel.paymentMethod.equals("CASH")){
            inCash = true;
        }
        CashRepository dbCashManager = new CashRepository(getContext());

        if(orderModel.discountAmount!=0){
            dbCashManager.addTransaction(getContext(),orderModel.discountAmount,2,orderModel.db_id,inCash);
        }
        if(orderModel.tips!=0){
            dbCashManager.addTransaction(getContext(),orderModel.tips,3,orderModel.db_id,inCash);
        }
//        if(orderModel.refundAmount!=0){
//            dbCashManager.addTransaction(getContext(),orderModel.refundAmount,4,orderModel.db_id,inCash);
//        }
        if(orderModel.adjustmentAmount> 0){
            dbCashManager.addTransaction(getContext(),orderModel.adjustmentAmount,5,orderModel.db_id,inCash);
        }

        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
//        long dateTime = System.currentTimeMillis();
//        SharedPrefManager.setUpdateData(getContext(), new UpdateData(orderModel.db_id, orderModel.serverId + "", orderModel.order_channel, orderModel.order_status, "" + dateTime));


        OrderDetails OrderDetails = ((OrderDetails)getContext());
        if(OrderDetails != null){
            OrderDetails.onBackPressed();
        }
    }

    public void payNow(String paymentMethod){
        orderModel = ((OnlineOrderDetails)getContext()).orderModel;
        consumerInfoFragment.voidOrder.setVisibility(View.INVISIBLE);
        orderModel.paymentStatus="PAID";
        orderModel.paymentMethod=paymentMethod;
        /**********
         * Update Cash Drawer
         */
        boolean inCash = false;
        if(orderModel.paymentMethod.equals("CASH")){
            inCash = true;
        }
        CashRepository dbCashManager = new CashRepository(getContext());
        long val = dbCashManager.addTransaction(getContext(),orderModel.receive,0,orderModel.db_id,inCash);
        if(inCash){
            orderModel.cashId = (int)val;
        }else{
            orderModel.paymentUid = String.valueOf(val);
        }

        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
        /*****
         * END
         */
        setPay();
        setActionView();
    }
}