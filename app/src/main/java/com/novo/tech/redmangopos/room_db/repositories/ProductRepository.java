package com.novo.tech.redmangopos.room_db.repositories;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.room_db.AppDatabase;
import com.novo.tech.redmangopos.room_db.dao.ProductDao;
import com.novo.tech.redmangopos.room_db.entities.Category;
import com.novo.tech.redmangopos.room_db.entities.Inventory;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.entities.ProductTb;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private Context context;
    private ProductDao productDao;
    private LiveData<List<Category>> properties;

    public ProductRepository(Context context) {
        this.context = context;
        AppDatabase database = AppDatabase.getInstance(context);
        productDao = database.PropertyDao();
        properties = productDao.getPropertiesLive();
    }

    public LiveData<List<Category>> getPropertiesLive() {
        return properties;
    }

    public List<Property> getAllProperties() {
        List<Property> propertyList = new ArrayList<>();
        List<Category> allProperty = productDao.getAllProperty();
        for (Category category: allProperty) {
            propertyList.add(getPropertyFromCategory(category));
        }
        return propertyList;
    }

    public List<Product> getAllProducts() {
        List<Product> productList = new ArrayList<>();
        List<ProductTb> allProduct = productDao.getAllProduct();
        for (ProductTb productTb: allProduct) {
            productList.add(getProductFromEntity(productTb));
        }
        return productList;
    }

    public Inventory getStockByUuId(String productUuid) {
        return productDao.getStockByUuId(productUuid);
    }

    public void addProperty(Category category) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.insertProperty(category);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void addProperties(List<Category> categories) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.insertProperties(categories);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void addProduct(ProductTb productTb) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.insertProduct(productTb);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void addAllProducts(List<ProductTb> products) {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.insertProducts(products);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void deleteAllProducts() {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.deleteAllProducts();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void deleteAllProperties() {
        new BackgroundTask(new BackgroundTask.OnListener() {
            @Override
            public void doInBackground() {
                productDao.deleteAllCategories();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private static class BackgroundTask extends AsyncTask<Order, Void, Void> {
        private BackgroundTask.OnListener listener;
        private BackgroundTask(BackgroundTask.OnListener listener) {
            this.listener = listener;
        }
        @Override
        protected Void doInBackground(Order... model) {
            listener.doInBackground();
            return null;
        }

        interface OnListener {
            void doInBackground();
        }
    }

    private Property getPropertyFromCategory(Category category) {
        return new Property(category.keyName, category.items);
    }

    private Product getProductFromEntity(ProductTb table) {
        return new Product(table.productType, table.productUUid, table.shortName, table.description, table.priceUUID, table.price, table.offerPrice, table.visible, table.discountable, table.properties, table.files, table.componentList);
    }
}
