package com.novo.tech.redmangopos.socket;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.novo.tech.redmangopos.callerId.app.TheApp;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.kitchenPref.Component;
import com.novo.tech.redmangopos.model.kitchenPref.ComponentSection;
import com.novo.tech.redmangopos.model.kitchenPref.PreparedItem;
import com.novo.tech.redmangopos.model.kitchenPref.PreparedOrder;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PreferenceWorker extends AsyncTask<Void, Void, JSONObject> {
    Context context;
    JSONObject jsonObject;

    public PreferenceWorker(Context context, JSONObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        OrderRepository manager = new OrderRepository(context);
        JSONObject newJson = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            if (!jsonObject.get("data").equals("null")) {
                String data = jsonObject.get("data").toString();
                Log.e("PreferenceWorker==>", data);
                PreparedOrder order = GsonParser.getGsonParser().fromJson(data, PreparedOrder.class);
                SharedPrefManager.savePrefForKitchen(context, order);
                Log.e("prefKitchenJson==>", order.getOrderId() + "");
            }

            List<OrderModel> activeOrder = manager.getActiveOrder();
//            List<OrderModel> orderList = new ArrayList<>();
            for (OrderModel model : activeOrder) {
                model.items.removeIf((element) -> element.uuid.equals("plastic-bag"));
                model.items.removeIf((element) -> element.uuid.equals("container-bag"));
                model.items.removeIf((element) -> !element.kitchenItem.equals("1"));


                boolean isKitchenAvailable = false;
                for (CartItem element : model.items) {
                    if (element.kitchenItem.equals("1")) {
                        isKitchenAvailable = true;
                        break;
                    }
                }

                if (isKitchenAvailable) {
                    JSONObject jsonObject = OrderModelToJson.fromModel(model, context);
                    PreparedOrder prefData = getPrefData(model);
                    jsonObject.put("prefData", GsonParser.getGsonParser().toJson(prefData));
                    jsonArray.put(jsonObject);
                }
            }

            newJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            newJson.put(SocketHandler.ORDER_LIST, jsonArray);
            newJson.put("duration", SharedPrefManager.getCookDuration(context) * 60);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newJson;
    }

    PreparedOrder getPrefData(OrderModel order) {
        List<PreparedOrder> kitchenPrefData = SharedPrefManager.getKitchenPrefData(context);
        for (int i=0;i<kitchenPrefData.size();i++) {
            if (kitchenPrefData.get(i).getOrderId() == order.order_id) {
                return kitchenPrefData.get(i);
            }
        }
        PreparedOrder preparedOrder = new PreparedOrder();
        preparedOrder.setOrderId(order.order_id);
        preparedOrder.setIsNewOrder(true);
        return preparedOrder;
    }

    @Override
    protected void onPostExecute(JSONObject v) {
        super.onPostExecute(v);
//        try {
//            TheApp.webSocket.emit(SocketHandler.EMIT_ORDER_LIST, v);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        JSONObject emitJson = new JSONObject();
        try {
//            List<PreparedOrder> updateDataList = SharedPrefManager.getKitchenPrefData(context);
//            emitJson.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
//            emitJson.put("data", GsonParser.getGsonParser().toJson(updateDataList));
//            for (PreparedOrder order: updateDataList) {
//                Log.e("PrefWorkerOrder==>", order.getOrderId() + "");
//            }
            Log.e("PrefWorker==>", v.toString());
            TheApp.webSocket.emit("GET_ORDERS_PREF", v);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
