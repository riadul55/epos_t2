package com.novo.tech.redmangopos.view.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.databinding.ViewDiscountInputBinding;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.UpdateData;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class PaymentMethod extends Fragment implements View.OnClickListener {
    String paymentMethod = "CASH";
    RelativeLayout paymentMethodDiscount,deliveryTimeContainer,shippingCharge,paymentMethodPlasticBagContainer,containerBagContainer;
    LinearLayout paymentMethodVoid, print;
    TextView discount,deliveryTimeTextView,shipping,customerName,postCode,addressTextBox,phone;
    Button btnCash, btnCard;
    PaymentCart cartListFragment;
    PaymentCalculator paymentCalculator;
    String deliveryTime;
    ImageButton increase,deCrease;
    ImageButton increaseContainer,deCreaseContainer;
    public EditText comments;
    public TextView plasticBagValue,containerValue;
    int discountPercentage = 0;
    int discountType = 0;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_payment_method, container, false);
        increase = v.findViewById(R.id.plasticBagIncrease);
        deCrease = v.findViewById(R.id.plasticBagDecrease);
        increaseContainer = v.findViewById(R.id.containerIncrease);
        deCreaseContainer = v.findViewById(R.id.containerDecrease);
        plasticBagValue = (TextView) v.findViewById(R.id.paymentMethodPlasticBagValue);
        containerValue = (TextView) v.findViewById(R.id.paymentMethodContainerValue);
        cartListFragment = (PaymentCart)(getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCart);
        paymentCalculator = (PaymentCalculator) (getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutPaymentCenter);
        discount =  v.findViewById(R.id.paymentMethodDiscountValue);
        shipping =  v.findViewById(R.id.paymentMethodDeliveryChargeValue);
        customerName =  v.findViewById(R.id.orderDetailsConsumerFirstName);
        postCode =  v.findViewById(R.id.orderDetailsConsumerPostCode);
        addressTextBox =  v.findViewById(R.id.orderDetailsConsumerAddress);
        phone =  v.findViewById(R.id.orderDetailsConsumerPhone);
        paymentMethodDiscount = v.findViewById(R.id.paymentMethodDiscount);
        shippingCharge = v.findViewById(R.id.paymentMethodDeliveryCharge);
        deliveryTimeContainer = v.findViewById(R.id.paymentMethodDeliveryTime);
        paymentMethodPlasticBagContainer = v.findViewById(R.id.paymentMethodPlasticBag);
        containerBagContainer = v.findViewById(R.id.paymentMethodContainer);
        deliveryTimeTextView = v.findViewById(R.id.paymentMethodDeliveryTimeValue);
        comments = v.findViewById(R.id.commentsBox);
        btnCash = v.findViewById(R.id.paymentMethodNameCash);
        btnCard = v.findViewById(R.id.paymentMethodNameCard);
        print = v.findViewById(R.id.paymentCartPrint);
        print.setOnClickListener(this);
        comments.setText(cartListFragment.orderModel.comments);
        shipping.setText(String.format(Locale.getDefault(),"%.2f",cartListFragment.orderModel.deliveryCharge));
        paymentMethodVoid = v.findViewById(R.id.paymentMethodVoid);
        paymentMethodDiscount.setOnClickListener(this);
        deliveryTimeContainer.setOnClickListener(this);
        paymentMethodVoid.setOnClickListener(this);
        shippingCharge.setOnClickListener(this);
        increase.setOnClickListener(this);
        deCrease.setOnClickListener(this);
        increaseContainer.setOnClickListener(this);
        deCreaseContainer.setOnClickListener(this);
        discountPercentage = cartListFragment.orderModel.discountPercentage;
        loadDiscountView();
        handlePaymentMethod(v);

        if(cartListFragment.orderModel.order_channel==null){
            cartListFragment.orderModel.order_channel="EPOS";
        }
        if(cartListFragment.orderModel.order_channel.equals("ONLINE")){
            paymentMethodVoid.setVisibility(View.GONE);
            deliveryTimeContainer.setVisibility(View.GONE);
            shippingCharge.setVisibility(View.GONE);
            paymentMethodDiscount.setVisibility(View.GONE);
            comments.setVisibility(View.GONE);
        }
        if(!cartListFragment.plasticBagOption)
            paymentMethodPlasticBagContainer.setVisibility(View.GONE);
        if(!cartListFragment.containerOption){
            containerBagContainer.setVisibility(View.GONE);
        }
        setPlasticBagValue(cartListFragment.bag);

        if(cartListFragment.orderModel.customer!=null){
            CustomerModel customerModel = cartListFragment.orderModel.customer;
            if(customerModel.profile!=null){
                customerName.setText(customerModel.profile.first_name+" "+customerModel.profile.last_name);
                phone.setText(customerModel.profile.phone);
            }
//            if(customerModel.addresses.size()>0){
//                CustomerAddress address =  customerModel.addresses.get(0);
//                if(address.properties!=null){
//                    postCode.setText(address.properties.zip);
//                    CustomerAddressProperties pro = address.properties;
//                    String _address = pro.building + " " + pro.street_number + " " + pro.street_name;
//                    addressTextBox.setText(_address);
//                }
//            }
        }
        if(cartListFragment.orderModel.shippingAddress!=null){
            CustomerAddress address =  cartListFragment.orderModel.shippingAddress;
            if(address.properties!=null){
                postCode.setText(address.properties.zip);
                CustomerAddressProperties pro = address.properties;
                String _address = pro.building + " " + pro.street_number + " " + pro.street_name;
                addressTextBox.setText(_address);
            }
        }

        setContainerValue(cartListFragment.containerBag);
        setPlasticBagValue(cartListFragment.bag);
        setDeliveryTimeValue();

        return v;
    }

    public void setDeliveryTimeValue() {
        if (cartListFragment != null &&
                cartListFragment.orderModel != null &&
                cartListFragment.orderModel.currentDeliveryTime != null) {
            deliveryTimeTextView.setText(cartListFragment.orderModel.currentDeliveryTime);
        } else {
            deliveryTimeTextView.setText("ASAP");
        }
    }

    public void setPlasticBagValue(CartItem bag){
        if(plasticBagValue != null && bag != null)
            plasticBagValue.setText(bag.quantity+" x "+bag.price+" = "+String.format(Locale.getDefault(),"%.2f",bag.quantity * bag.price));

    }
    public void setContainerValue(CartItem bag){
        if(containerValue != null && bag != null)
            containerValue.setText(bag.quantity+" x "+bag.price+" = "+String.format(Locale.getDefault(),"%.2f",bag.quantity * bag.price));

    }
    void handlePaymentMethod(View v){
        changeBtnColors(paymentMethod);
        btnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = btnCash.getText().toString();
                try {
                    cartListFragment.orderModel.paymentMethod = paymentMethod;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                changeBtnColors(paymentMethod);
            }
        });
        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = btnCard.getText().toString();
                try {
                    cartListFragment.orderModel.paymentMethod = paymentMethod;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                changeBtnColors(paymentMethod);
            }
        });
    }

    void changeBtnColors(String method) {
        if (method.equals("CASH")) {
            btnCash.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.selected_btn_color));
            btnCard.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.unselected_btn_color));
        } else {
            btnCash.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.unselected_btn_color));
            btnCard.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.selected_btn_color));
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.paymentMethodDiscount){
                getDiscountValue();
        }else if(v.getId() == R.id.paymentMethodVoid){
            cartListFragment.voidOrderAlert();
        }else if(v.getId() == R.id.paymentMethodDeliveryTime){
               // getOptionInput();
            getDateTimeInput();
        }else if(v.getId() == R.id.paymentMethodDeliveryCharge){
                getChargeInput();
        }else if(v.getId() == increase.getId()){
            //increase plastic bag
            cartListFragment.inCreasePlasticBag();
        }else if(v.getId() == deCrease.getId()){
            //decrease Plastic bag
            cartListFragment.deCreasePlasticBag();

        }else if(v.getId() == increaseContainer.getId()){
            //increase plastic bag
            cartListFragment.inCreaseContainerBag();
        }else if(v.getId() == deCreaseContainer.getId()){
            //decrease Plastic bag
            cartListFragment.deCreaseContainerBag();

        }else if(v.getId() == print.getId()){
            try {
                OrderRepository manager = new OrderRepository(requireContext());
                OrderModel orderData = manager.getOrderData(cartListFragment.orderModel.db_id);
                if (cartListFragment.orderModel.bookingId != 0){
                    orderData.dueAmount = orderData.total - orderData.totalPaid;
                    orderData.items = cartListFragment.orderModel.items;
                    orderData.total = cartListFragment.orderModel.total;
                    orderData.subTotal = cartListFragment.orderModel.subTotal;
                }
                CashMemoHelper.printCustomerMemo(orderData,getContext(),false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("DefaultLocale")
    void loadDiscountView(){
        discount.setText("-£"+String.format("%.2f",cartListFragment.orderModel.discountAmount)+(!cartListFragment.orderModel.fixedDiscount?(" ("+String.valueOf(cartListFragment.orderModel.discountPercentage)+"%)"):""));
    }
    @SuppressLint("DefaultLocale")
    void afterDiscountInput(){
        if(discountPercentage > 0){
            cartListFragment.orderModel.discountPercentage = discountPercentage;
            cartListFragment.orderModel.discountAmount = cartListFragment.orderModel.discountableTotal * ((float)discountPercentage/100);
        }
        cartListFragment.calculateTotal();
        paymentCalculator.setData();
        loadDiscountView();
    }
    void getDiscountValue(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        ViewDiscountInputBinding discountInputBinding = ViewDiscountInputBinding.inflate(inflater);
        discountInputBinding.discountNotification.setText((discountType==1?"£0":"0%") +" - will be applied");
        discountInputBinding.isOverrideDiscount.setChecked(cartListFragment.orderModel.isDiscountOverride);
        if(discountType==0){
            discountInputBinding.percentage.setChecked(true);

        }else{
            discountInputBinding.fixed.setChecked(true);
        }

        discountInputBinding.discountType.getChildAt(discountType).setSelected(true);
        discountInputBinding.discountType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.percentage){
                    discountType = 0;
                }else{
                    discountType = 1;
                }

                double amount = 0;
                try {
                    amount = Double.parseDouble(discountInputBinding.currentDiscount.getText().toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(discountType==0){
                    discountInputBinding.discountNotification.setText(String.valueOf((int) Math.round(amount))+"% - will be applied");
                }else{
                    discountInputBinding.discountNotification.setText("£"+String.valueOf(amount)+" - will be applied");
                }


            }
        });
        discountInputBinding.currentDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                double amount = 0;
                try {
                   amount = Double.parseDouble(charSequence.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(discountType==0){
                    discountInputBinding.discountNotification.setText(String.valueOf(amount)+"% - will be applied");
                }else{
                    discountInputBinding.discountNotification.setText("£"+String.valueOf(amount)+"- will be applied");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        discountInputBinding.isOverrideDiscount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cartListFragment.orderModel.isDiscountOverride = b;
            }
        });

        builder.setTitle("Discount")
                .setView(discountInputBinding.getRoot())
                .setPositiveButton("Apply", (dialog, which) -> {
                if(!discountInputBinding.currentDiscount.getText().toString().isEmpty()){
                  //  discountInputBinding.discountType.;
                    double amount = 0;
                    try{
                       amount =  Double.parseDouble(discountInputBinding.currentDiscount.getText().toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(amount > 0){
                        if(discountType==0){
                            discountPercentage = (int) Math.round(amount);
                            cartListFragment.orderModel.fixedDiscount = false;
                            afterDiscountInput();
//                            dialog.dismiss();
                        }else{
                            discountPercentage = 0;
                            cartListFragment.orderModel.discountAmount = amount;
                            cartListFragment.orderModel.discountPercentage = 0;
                            cartListFragment.orderModel.fixedDiscount = true;
                            cartListFragment.calculateTotal();
                            paymentCalculator.setData();
                            loadDiscountView();
                        }
                    }

                    OrderRepository repository = new OrderRepository(getContext());
                    OrderModel orderData = repository.getOrderData(cartListFragment.orderModel.db_id);
                    orderData.discountAmount = cartListFragment.orderModel.discountAmount;
                    orderData.discountPercentage = cartListFragment.orderModel.discountPercentage;
                    orderData.fixedDiscount = cartListFragment.orderModel.fixedDiscount;
                    orderData.isDiscountOverride = cartListFragment.orderModel.isDiscountOverride;
                    SharedPrefManager.createUpdateOrder2(getContext(), orderData);

                    dialog.dismiss();
                } })
                .setNegativeButton("Back",(dialog,which)->{
                    dialog.dismiss();
                });
       AlertDialog dialog = builder.create();
       dialog.show();
//        discountInputBinding.percentage5.setOnClickListener((v)->{
//            discountPercentage = 5;
//            afterDiscountInput();
//            dialog.dismiss();
//
//        });
//        discountInputBinding.percentage10.setOnClickListener((v)->{
//            discountPercentage = 10;
//            afterDiscountInput();
//            dialog.dismiss();
//        });
//        discountInputBinding.percentage15.setOnClickListener((v)->{
//            discountPercentage = 15;
//            afterDiscountInput();
//            dialog.dismiss();
//        });
//        discountInputBinding.percentage20.setOnClickListener((v)->{
//            discountPercentage = 20;
//            afterDiscountInput();
//            dialog.dismiss();
//        });
    }
    void getChargeInput(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        EditText editText = new EditText(getContext());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(editText);


        builder.setTitle("Delivery Charge");
        builder.setPositiveButton("Apply", (dialogInterface, i) -> {
            double chargeAmount = 0;
            if(!editText.getText().toString().isEmpty()){
                try {
                    chargeAmount = Double.parseDouble(editText.getText().toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            shipping.setText("£ "+String.format("%.2f",chargeAmount));

            OrderRepository repository = new OrderRepository(getContext());
            OrderModel orderData = repository.getOrderData(cartListFragment.orderModel.db_id);

            Product deliveryItem  = SharedPrefManager.getDeliveryProduct(getContext());
            if(deliveryItem != null){
                CartItem cartItem = new CartItem(
                        getMaxId()+1,
                        deliveryItem.productUUid,
                        deliveryItem.shortName,
                        deliveryItem.shortName,
                        new ArrayList<>(),
                        chargeAmount,
                        deliveryItem.offerPrice,
                        chargeAmount,
                        chargeAmount,
                        1,
                        1,
                        "",
                        "DELIVERY",
                        0,
                        false,false,false, deliveryItem.properties.kitchenItem,"",new ArrayList<>(), true,
                        "CASH"
                );
                cartListFragment.orderModel.items.removeIf((itm)->itm.type.equals("DELIVERY"));
                cartListFragment.orderModel.items.add(cartItem);

                orderData.items.removeIf((itm)->itm.type.equals("DELIVERY"));
                orderData.items.add(cartItem);

                cartListFragment.loadData();
            }

            afterDiscountInput();


            orderData.discountAmount = cartListFragment.orderModel.discountAmount;
            orderData.discountPercentage = cartListFragment.orderModel.discountPercentage;
            orderData.fixedDiscount = cartListFragment.orderModel.fixedDiscount;
            SharedPrefManager.createUpdateOrder2(getContext(), orderData);

        }).setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());


        AlertDialog dialog = builder.create();
        dialog.show();
    }
    int getMaxId(){
        int max = 0;
        for (CartItem item : cartListFragment.orderModel.items){
            if(item.localId > max)
                max = item.localId;
        }
        return max;
    }
    void  getDateTimeInput(){
        DecimalFormat fm = new DecimalFormat("00");
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.date_time_picker, (ViewGroup) getView(), false);
        RelativeLayout getDate,getTime;
        TextView dateView,timeView;
        getDate = viewInflated.findViewById(R.id.datePickerDate);
        getTime = viewInflated.findViewById(R.id.datePickerTime);
        dateView = viewInflated.findViewById(R.id.datePickerDateValue);
        timeView = viewInflated.findViewById(R.id.datePickerTimeValue);

        dateView.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(calendar.getTime()));
        timeView.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.getTime()));
        getDate.setOnClickListener(v -> getDateInput(dateView));
        getTime.setOnClickListener(v -> getTimeInput(timeView));
        builder.setTitle("Select Delivery Time");
        builder.setView(viewInflated);
        builder.setPositiveButton("Save", (dialog, which) -> {
            deliveryTime = dateView.getText()+" "+timeView.getText();
            deliveryTimeTextView.setText(deliveryTime);
            cartListFragment.orderModel.deliveryType="";
            cartListFragment.orderModel.currentDeliveryTime = deliveryTime;

            OrderRepository repository = new OrderRepository(getContext());
            OrderModel orderData = repository.getOrderData(cartListFragment.orderModel.db_id);
            orderData.deliveryType = cartListFragment.orderModel.deliveryType;
            orderData.currentDeliveryTime = cartListFragment.orderModel.currentDeliveryTime;
            SharedPrefManager.createUpdateOrder2(getContext(), orderData);

            cartListFragment.loadData();

            if (cartListFragment.orderModel.order_channel.equals("ONLINE")) {
                long dateTime = System.currentTimeMillis();
                SharedPrefManager.setUpdateData(getContext(), new UpdateData(cartListFragment.orderModel.db_id, cartListFragment.orderModel.serverId + "", cartListFragment.orderModel.order_channel, "TIME_CHANGE", "" + dateTime));
            }
        });
        builder.show();

    }
    void getTimeInput(TextView timeView){
        Calendar mcurrentTime = Calendar.getInstance();
        DecimalFormat fm = new DecimalFormat("00");
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
         MaterialTimePicker picker =
                new MaterialTimePicker.Builder()
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .setHour(12)
                        .setMinute(10)
                        .setTitleText("Select TIme")
                        .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                        .setHour(hour)
                        .setMinute(minute)
                        .setTimeFormat(TimeFormat.CLOCK_12H)
                        .build();
         picker.addOnPositiveButtonClickListener(v -> {
            int selectedHour = picker.getHour();
            int selectedMinute = picker.getMinute();
             String AM_PM ;
             if(selectedHour < 12) {
                 AM_PM = "AM";
             } else {
                 AM_PM = "PM";
                 if(selectedHour>12){
                     selectedHour-=12;
                 }
             }
             timeView.setText(fm.format(selectedHour)+":"+ fm.format(selectedMinute)+" : "+AM_PM);
         });
         picker.show(getChildFragmentManager(), picker.toString());
    }
    void getDateInput(TextView dateView){
        DecimalFormat fm = new DecimalFormat("00");
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog dialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> dateView.setText(fm.format(dayOfMonth)+"/"+fm.format(month+1)+"/"+year), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }
}