package com.novo.tech.redmangopos.callback;
/**
 * This class created by alamin
 */
public interface CategoryImageCallBack {
    void onCategoryImage(String ImagePath);
}
