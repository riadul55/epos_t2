package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.widget.AppCompatButton;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.utils.BookingOrdersHelper;
import com.novo.tech.redmangopos.view.fragment.OrderDetailsAction;
import com.novo.tech.redmangopos.view.fragment.OrderDetailsCart;
import com.novo.tech.redmangopos.view.fragment.OrderDetailsConsumerInfo;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

public class OrderDetails extends BaseActivity {
    int orderID;
    public OrderModel orderModel;
    boolean customerOrder = false;
    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;
    OrderRepository orderManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        orderManager = new OrderRepository(getApplicationContext());
        orderID = getIntent().getIntExtra("orderData",0);
        customerOrder = getIntent().getBooleanExtra("customerOrder",false);
        if(customerOrder){
            orderModel = orderManager.getHistoryOrderData(orderID);
        }else
            orderModel = SharedPrefManager.getOrderData(getApplicationContext(),orderID);

        if(orderModel.items == null){
            orderModel.items = new ArrayList<>();
        }
        if(orderModel.paymentStatus==null)
            orderModel.paymentStatus = "UNPAID";

        if(orderModel.order_status==null){
            orderModel.order_status = "NEW";
        }

        if(orderModel != null){
            openCartFragment();
            openActionFragment();
            openConsumerFragment();
        }
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);
        callingPanel = findViewById(R.id.CallingView);
        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });

    }

    void openCartFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCart, OrderDetailsCart.class,bundle)
                .commit();
    }
    void openActionFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        bundle.putBoolean("customerOrder", customerOrder);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCenter, OrderDetailsAction.class,bundle)
                .commit();
    }
    void openConsumerFragment(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel",orderModel);
        bundle.putBoolean("customerOrder", customerOrder);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsRight, OrderDetailsConsumerInfo.class,bundle)
                .commit();
    }

    void backToHome(){
        if(customerOrder){
            finish();
        }else{
            Intent gotoScreenVar = new Intent(OrderDetails.this, Dashboard.class);
//            gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            gotoScreenVar.putExtra("shift","EXISTING");
            startActivity(gotoScreenVar);
        }

    }
    @Override
    public void onBackPressed() {
        backToHome();
    }

    @Override
    public void onChanged() {

    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}