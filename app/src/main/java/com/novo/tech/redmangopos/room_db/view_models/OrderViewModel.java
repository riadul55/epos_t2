package com.novo.tech.redmangopos.room_db.view_models;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.room_db.entities.Order;
import com.novo.tech.redmangopos.room_db.repositories.OrderRepository;
import java.util.List;

public class OrderViewModel extends AndroidViewModel {
    private Context context;
    private OrderRepository repository;

    public OrderViewModel(@NonNull Application application) {
        super(application);
        this.context = application.getApplicationContext();
        repository = new OrderRepository(application.getApplicationContext());
    }

    public LiveData<List<Order>> getAllOrders() {
        return repository.getAllOrders();
    }

}
