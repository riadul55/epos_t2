package com.novo.tech.redmangopos.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.escpos.utils.Conts;
import com.novo.tech.redmangopos.model.AppConfig;
import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.sync_service.UpdateOnlineOrderStatusAsync;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BusinessSetting extends Fragment implements UpdateOnlineOrderStatusAsync.UpdateStatusListener {
    Button changeButton;
    RadioGroup printType;
    RadioGroup printCount;
    RadioGroup discountType;
    int style = 0;
    int countIndex = 0;
    int discountTypeIndex = 0;
    TextView existingTime,changeText,openTimeTextView, changeOpenTime;
    EditText businessName,businessLocation,fontSize,phone,discountCollection,discountDelivery, defaultDCharge,minAmountCollection, minAmountDelivery,minAmount,keepOrderDays,noOfOrderPerCustomer;
    MaterialCheckBox onlineOrder,plasticBag,containerBag,takeoutOrder,tableOrder,payButtonPrint,printOnOnlineOrder,waiterAppIncluded,payLaterPrint,fullPaymentCloseButton,serviceChargeIncluded,doublePrintTakeout,printOrderNo, autoRefresh, orderPulling;
    String shiftCloseTime, shiftOpenTime;
    ProgressDialog statusDialog;
    AppCompatSpinner printerList;

    LinearLayout layoutPrinterConn;
    AppCompatSpinner printerConnection;

    LinearLayout layoutPrinterIp;
    EditText printerIpAddress;
    EditText cookDuration;

    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bussiness_setting, container, false);
        statusDialog=new ProgressDialog(getContext());
        existingTime = v.findViewById(R.id.shiftTimeTextView);
        changeButton = v.findViewById(R.id.shiftTimeChangeButton);
        changeText = v.findViewById(R.id.changeText);
        changeOpenTime = v.findViewById(R.id.changeOpenTime);
        openTimeTextView = v.findViewById(R.id.openTimeTextView);
        printType = v.findViewById(R.id.printType);
        printCount = v.findViewById(R.id.printCount);
        discountType = (RadioGroup) v.findViewById(R.id.discountOn);
        businessName = v.findViewById(R.id.editTextBusinessName);
        fontSize = v.findViewById(R.id.editTextPrintFontSize);
        phone = v.findViewById(R.id.editTextBusinessPhone);
        businessLocation = v.findViewById(R.id.editTextBusinessLocation);
        plasticBag = v.findViewById(R.id.checkBoxBagMandatory);
        onlineOrder = v.findViewById(R.id.businessStatusCheckBox);
        containerBag = v.findViewById(R.id.checkEnableContainerBag);
        takeoutOrder = v.findViewById(R.id.enableTakeout);
        tableOrder = v.findViewById(R.id.enableTable);
        payButtonPrint = v.findViewById(R.id.enablePrintOnPayButton);
        printOnOnlineOrder = v.findViewById(R.id.enableOnlineAutoPrint);
        waiterAppIncluded = v.findViewById(R.id.waiterAppIncluded);
        payLaterPrint = v.findViewById(R.id.enablePrintOnPayLaterButton);
        serviceChargeIncluded = v.findViewById(R.id.serviceChargeIncluded);
        doublePrintTakeout = v.findViewById(R.id.doublePrintTakeout);
        fullPaymentCloseButton = v.findViewById(R.id.enableCloseOrderOnFullPayment);
        discountCollection = v.findViewById(R.id.editTextDiscountCollection);
        discountDelivery = v.findViewById(R.id.editTextDiscountDelivery);
        defaultDCharge = v.findViewById(R.id.editTextDefaultDCharge);
        minAmountCollection = v.findViewById(R.id.editTextMinAmountCollection);
        minAmountDelivery = v.findViewById(R.id.editTextMinAmountDelivery);
        printOrderNo = v.findViewById(R.id.printOrderNo);
        autoRefresh = v.findViewById(R.id.autoRefresh);
        orderPulling = v.findViewById(R.id.orderPulling);
        keepOrderDays = v.findViewById(R.id.editTextKeepOrderDays);
        noOfOrderPerCustomer = v.findViewById(R.id.editTextKeepCustomerOrder);
        cookDuration = v.findViewById(R.id.editTextKitchenCookDuration);
        changeButton.setOnClickListener(v1 -> updateData());
        changeText.setOnClickListener(v12 -> getShiftCloseTime());
        changeOpenTime.setOnClickListener(v12 -> getShiftOpenTime());

        printerList = v.findViewById(R.id.printerList);
        layoutPrinterConn = v.findViewById(R.id.layoutPrinterConn);
        printerConnection  = v.findViewById(R.id.printerConnection);
        layoutPrinterIp = v.findViewById(R.id.layoutPrinterIp);
        printerIpAddress = v.findViewById(R.id.printerIpAddress);

        printType.setOnCheckedChangeListener((group, checkedId) -> {
            if(checkedId == R.id.style_1){
                style = 0;
            }else if(checkedId == R.id.style_2){
                style = 1;
            }
        });
        printCount.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.singlePrint) {
                countIndex = 0;
            } else if(checkedId == R.id.doublePrint){
                countIndex = 1;
            }else if(checkedId == R.id.triplePrint){
                countIndex = 2;
            }
        });

        discountType.setOnCheckedChangeListener((group, checkedId) -> {
            if(checkedId == R.id.none){
                discountTypeIndex = 0;
            }else if(checkedId == R.id.allOrder){
                discountTypeIndex = 1;
            }else if(checkedId == R.id.collectionOnly){
                discountTypeIndex = 2;
            }
        });


        printerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    layoutPrinterConn.setVisibility(View.GONE);
                    layoutPrinterIp.setVisibility(View.GONE);
                } else {
                    layoutPrinterConn.setVisibility(View.VISIBLE);
                    layoutPrinterIp.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        printerConnection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    layoutPrinterIp.setVisibility(View.VISIBLE);
                } else {
                    layoutPrinterIp.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        displayData();

        return v;
    }


    void getShiftCloseTime(){
        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), (timePicker, selectedHour, selectedMinute) -> shiftCloseTime = selectedHour+":"+selectedMinute, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();
    }

    void getShiftOpenTime(){
        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                shiftOpenTime = selectedHour+":"+selectedMinute;
                try {
                    Date parse = new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(shiftOpenTime);
                    String format = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(parse);
                    openTimeTextView.setText(format);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();
    }
    void updateData(){
        if(shiftCloseTime != null){
            SharedPrefManager.setShiftCloseTime(getContext(),shiftCloseTime);
        }
        if(shiftOpenTime != null){
            SharedPrefManager.setShiftOpenTime(getContext(),shiftOpenTime);
        }
        int discountCollectionAmount  = 0;
        int discountDeliveryAmount = 0;
        int dayCount = 0;
        int amountToGetDiscountCollection  = 0;
        int amountToGetDiscountDelivery   = 0;
        int perCustomerOrder = 0;
        double deliveryCharge = 0.0;
        try{
            discountCollectionAmount = Integer.parseInt(discountCollection.getText().toString());
            discountDeliveryAmount = Integer.parseInt(discountDelivery.getText().toString());
            dayCount = Integer.parseInt(keepOrderDays.getText().toString());
            amountToGetDiscountCollection = Integer.parseInt(minAmountCollection.getText().toString());
            amountToGetDiscountDelivery = Integer.parseInt(minAmountDelivery.getText().toString());
            perCustomerOrder = Integer.parseInt(noOfOrderPerCustomer.getText().toString());
            deliveryCharge = Double.parseDouble(defaultDCharge.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        SharedPrefManager.setBusinessName(getContext(),businessName.getText().toString());
        SharedPrefManager.setBusinessPhone(getContext(),phone.getText().toString());
        SharedPrefManager.setBusinessLocation(getContext(),businessLocation.getText().toString());
        SharedPrefManager.setDiscountCollection(getContext(),discountCollectionAmount);
        SharedPrefManager.setDiscountDelivery(getContext(),discountDeliveryAmount);
        SharedPrefManager.setDaysCount(getContext(),dayCount);
        SharedPrefManager.setCustomerOrderCount(getContext(),perCustomerOrder);
        SharedPrefManager.setDeliveryCharge(getContext(), deliveryCharge);
        SharedPrefManager.setMinAmountCollection(getContext(),amountToGetDiscountCollection);
        SharedPrefManager.setMinAmountDelivery(getContext(),amountToGetDiscountDelivery);
        SharedPrefManager.setPlasticBagMandatory(getContext(),plasticBag.isChecked());
        SharedPrefManager.setContainerBagEnable(getContext(),containerBag.isChecked());
        SharedPrefManager.setEnableTakeoutOrder(getContext(),takeoutOrder.isChecked());
        SharedPrefManager.setEnableTableOrder(getContext(),tableOrder.isChecked());
        SharedPrefManager.setPrintOnPayButtonClick(getContext(),payButtonPrint.isChecked());
        SharedPrefManager.setEnableWaiterApp(getContext(),waiterAppIncluded.isChecked());
        SharedPrefManager.setPrintOnOnlineOrder(getContext(),printOnOnlineOrder.isChecked());
        SharedPrefManager.setPrintOnPayLaterButtonClick(getContext(),payLaterPrint.isChecked());
        SharedPrefManager.setOrderCloseOnPayment(getContext(),fullPaymentCloseButton.isChecked());
        SharedPrefManager.setDiscountType(getContext(),discountTypeIndex);
        SharedPrefManager.setEnablePrintOrderNo(getContext(),printOrderNo.isChecked());
        SharedPrefManager.setEnableAutoRefresh(getContext(),autoRefresh.isChecked());
        SharedPrefManager.setEnableOrderPull(getContext(),orderPulling.isChecked());
        SharedPrefManager.setDoublePrintTakeout(getContext(),doublePrintTakeout.isChecked());
        SharedPrefManager.setPrintFontSize(getContext(),Integer.parseInt(fontSize.getText().toString().isEmpty()?"30":fontSize.getText().toString()));
        SharedPrefManager.setPrintStyle(getContext(),style);
        SharedPrefManager.setPrintCount(getContext(), countIndex);
        SharedPrefManager.setServiceChargeIncluded(getContext(),serviceChargeIncluded.isChecked());

        SharedPrefManager.setPrinterIndex(getContext(), printerList.getSelectedItemPosition());
        SharedPrefManager.setConnectionIndex(getContext(), printerConnection.getSelectedItemPosition());
        SharedPrefManager.setPrinterIp(getContext(), printerIpAddress.getText().toString());
        SharedPrefManager.setCookDuration(getContext(), Integer.parseInt(cookDuration.getText().toString()));

        updateOnlineOrderStatus();
//        successMessage();
//        displayData();
    }

    void updateOnlineOrderStatus() {
        Log.e("Online order status updating", "...");
        String status;
        if(onlineOrder.isChecked()){
            status="open";
        }else{
            status="closed";
        }
        progressDialog = ProgressDialog.show(getContext(), "", "Loading. Please wait...", true);
        progressDialog.show();
        UpdateOnlineOrderStatusAsync async = new UpdateOnlineOrderStatusAsync(
                getContext(),
                status,
                onlineOrder.isChecked(),
                this
        );
        async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    void successMessage(){
        new AlertDialog.Builder(getContext())
                .setTitle("Saved Successfully Completed")
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).create()
                .show();
    }

    void displayData(){
        Date time = SharedPrefManager.getShiftCloseTime(getContext());
        String str = "12.00 AM";
        if(time != null){
            str = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(time);
        }

        Date openTime = SharedPrefManager.getShiftOpenTime(getContext());
        String strOpen = "10.00 AM";
        if(openTime != null){
            strOpen = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(openTime);
        }
        openTimeTextView.setText(strOpen);
        style = SharedPrefManager.getPrintStyle(getContext());
        ((RadioButton)printType.getChildAt(style)).setChecked(true);
        countIndex = SharedPrefManager.getPrintCount(getContext());
        ((RadioButton)printCount.getChildAt(countIndex)).setChecked(true);
        existingTime.setText(str);
        businessName.setText(SharedPrefManager.getBusinessName(getContext()));
        discountCollection.setText(String.valueOf(SharedPrefManager.getDiscountCollection(getContext())));
        discountDelivery.setText(String.valueOf(SharedPrefManager.getDiscountDelivery(getContext())));
        keepOrderDays.setText(String.valueOf(SharedPrefManager.getDaysCount(getContext())));
        noOfOrderPerCustomer.setText(String.valueOf(SharedPrefManager.getCustomerOrderCount(getContext())));
        defaultDCharge.setText(String.valueOf(SharedPrefManager.getDeliveryCharge(getContext())));

        minAmountCollection.setText(String.valueOf(SharedPrefManager.getMinAmountCollection(getContext())));
        minAmountDelivery.setText(String.valueOf(SharedPrefManager.getMinAmountDelivery(getContext())));

        businessLocation.setText(SharedPrefManager.getBusinessLocation(getContext()));
        plasticBag.setChecked(SharedPrefManager.getPlasticBagMandatory(getContext()));
        onlineOrder.setChecked(SharedPrefManager.getOnlineOrderStatus(getContext()));
        containerBag.setChecked(SharedPrefManager.getContainerBagEnable(getContext()));
        takeoutOrder.setChecked(SharedPrefManager.getEnableTakeoutOrder(getContext()));
        tableOrder.setChecked(SharedPrefManager.getEnableTableOrder(getContext()));
        printOrderNo.setChecked(SharedPrefManager.getEnablePrintOrderNo(getContext()));
        autoRefresh.setChecked(SharedPrefManager.getEnableAutoRefresh(getContext()));
        orderPulling.setChecked(SharedPrefManager.getEnableOrderPull(getContext()));
        payButtonPrint.setChecked(SharedPrefManager.getPrintOnPayButtonClick(getContext()));
        waiterAppIncluded.setChecked(SharedPrefManager.getWaiterApp(getContext()));
        printOnOnlineOrder.setChecked(SharedPrefManager.getPrintOnOnlineOrder(getContext()));
        serviceChargeIncluded.setChecked(SharedPrefManager.getServiceChargeIncluded(getContext()));
        fullPaymentCloseButton.setChecked(SharedPrefManager.getOrderCloseOnPayment(getContext()));
        payLaterPrint.setChecked(SharedPrefManager.getPrintOnPayLaterButtonClick(getContext()));
        ((RadioButton)discountType.getChildAt(SharedPrefManager.getDiscountType(getContext()))).setChecked(true);
        doublePrintTakeout.setChecked(SharedPrefManager.getDoublePrintTakeout(getContext()));
        fontSize.setText(String.valueOf(SharedPrefManager.getPrintSize(getContext())));
        phone.setText(String.valueOf(SharedPrefManager.getBusinessPhone(getContext())));
        cookDuration.setText(String.valueOf(SharedPrefManager.getCookDuration(getContext())));

        int printerIndex = SharedPrefManager.getPrinterIndex(getContext());
        printerList.setSelection(printerIndex);
        if (printerIndex == Conts.PRINTER_SUNMI){
            layoutPrinterConn.setVisibility(View.GONE);
        } else {
            layoutPrinterConn.setVisibility(View.VISIBLE);
        }
        int connectionIndex = SharedPrefManager.getConnectionIndex(getContext());
        printerConnection.setSelection(connectionIndex);
        if (connectionIndex == Conts.CONNECTION_IP) {
            layoutPrinterIp.setVisibility(View.VISIBLE);
        } else {
            layoutPrinterIp.setVisibility(View.GONE);
        }
        printerIpAddress.setText(SharedPrefManager.getPrinterIp(getContext()));
        getGlobalConfigs();
    }

    void getGlobalConfigs() {
//        ProgressDialog dialog = ProgressDialog.show(getContext(), "", "Loading. Please wait...", true);
//        dialog.show();

        Retrofit retrofit = new RetrofitClient(getContext()).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        Call<AppConfig> call = api.getAppConfig();
        call.enqueue(new Callback<AppConfig>() {
            @Override
            public void onResponse(Call<AppConfig> call, Response<AppConfig> response) {
//                dialog.dismiss();
                if (response.code() == 200 && response.body() != null) {
                    try {
                        SharedPrefManager.setAppConfig(getContext(), response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AppConfig appConfig = response.body();

                    if(appConfig.getBusinessStatus().equals("open")){
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            String currentDate = sdf.format(new Date());
                            onlineOrder.setChecked(!appConfig.getBusinessOffDate().equals(currentDate));
                            SharedPrefManager.setOnlineOrderStatus(getContext(), true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else if(appConfig.getBusinessStatus().equals("closed")){
                        try {
                            onlineOrder.setChecked(false);
                            SharedPrefManager.setOnlineOrderStatus(getContext(), false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<AppConfig> call, Throwable t) {
//                dialog.dismiss();
                try {
                    requireActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onlineOrder.setChecked(SharedPrefManager.getOnlineOrderStatus(getContext()));
                        }
                    });
                } catch (Exception e) {}
            }
        });
    }

    @Override
    public void onUpdate() {
        progressDialog.dismiss();
        successMessage();
        displayData();
    }
}