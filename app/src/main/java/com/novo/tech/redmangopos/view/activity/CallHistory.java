package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CallerIdListAdapter;
import com.novo.tech.redmangopos.model.CallerHistoryModel;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

public class CallHistory extends BaseActivity {

    List<CallerHistoryModel> callerHistoryModelList = new ArrayList<>();
    CallerIdListAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    DividerItemDecoration dividerItemDecoration;
    LinearLayout backButton;
    AppCompatButton accept,decline;
    TextView phoneNo;
    View callingPanel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_history);
        recyclerView = findViewById(R.id.callerHistoryRecyclerView);
        backButton = findViewById(R.id.backButton);
        layoutManager = new LinearLayoutManager(this);
        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        adapter = new CallerIdListAdapter(CallHistory.this,callerHistoryModelList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        backButton.setOnClickListener(view -> {
          finish();
        });
        callingPanel = findViewById(R.id.CallingView);
        accept = findViewById(R.id.callReceive);
        decline = findViewById(R.id.callCancel);
        phoneNo = findViewById(R.id.callNumber);

        accept.setOnClickListener(v->{
            callingPanel.setVisibility(View.GONE);
//            createOrSelect(TheApp.mainChannel.CallerId);
            createOrSelect(phoneNo.getText().toString());
        });
        decline.setOnClickListener(view -> {
            callingPanel.setVisibility(View.GONE);
            refreshCallingView(null);
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    void loadHistoryFromLocal(){
        callerHistoryModelList.clear();
        callerHistoryModelList.addAll(SharedPrefManager.getCallHistory(getApplicationContext()));
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadHistoryFromLocal();
    }

    @Override
    public void onChanged() {

    }

    @Override
    public void onPhoneCall(boolean isVisible, String phone) {
        callingPanel.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        phoneNo.setText(phone);
    }
}

