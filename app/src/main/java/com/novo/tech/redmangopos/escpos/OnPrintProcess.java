package com.novo.tech.redmangopos.escpos;

public interface OnPrintProcess {
    void onSuccess();
    void onError(String msg);
}
