package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.novo.tech.redmangopos.view.activity.OnlineCustomerOrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.view.activity.OrderDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class CustomerOrderListAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<OrderModel> orderList;

    public CustomerOrderListAdapter(Context applicationContext, List<OrderModel> orderList) {
        this.context = applicationContext;
        this.orderList = orderList;
        inflater = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.model_customer_order_list_item, null);
        TextView status,amount,serverId;
        status = view.findViewById(R.id.orderStatus);
        amount = view.findViewById(R.id.amount);
        serverId = view.findViewById(R.id.serverId);
        MaterialButton detailsButton = view.findViewById(R.id.detailsButton);
        OrderModel order = orderList.get(position);

        serverId.setText(formatDBDate(order.orderDate));
        if(order.order_channel==null)
            order.order_channel = "EPOS";
        if(order.paymentStatus == null){
            order.paymentStatus = "UNPAID";
        }
        detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context, OrderDetails.class);
                Bundle b = new Bundle();
                b.putInt("orderData",order.db_id);
                b.putBoolean("customerOrder",true);
                in.putExtras(b);
                context.startActivity(in);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(order.order_channel !=null) {
//                    if (order.order_channel.toUpperCase().equals("ONLINE")) {
//                        ((Dashboard) mCtx).openOnlineOrderDetailsPage(order.db_id);
//                    } else {
//                        ((Dashboard) mCtx).openOrderDetailsPage(order.db_id);
//                    }
//                }
//                else
//                    ((Dashboard)mCtx).openOrderDetailsPage(order.db_id);
            }
        });
        if (order.order_status.equals("REFUNDED")){
           amount.setText(String.format("%.2f",order.total+order.adjustmentAmount));
        }else{
            amount.setText(String.format("%.2f",order.total-order.refundAmount+order.adjustmentAmount));
        }
        status.setText(order.order_status);
        return view;
    }

   String formatDBDate(String dateTime12H){
       SimpleDateFormat oldFormat = new SimpleDateFormat("yyyyMMdd",Locale.getDefault());
       SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String dateTime=dateTime12H;
        if(dateTime==null){
            dateTime = "N/A";
        }else if(dateTime.isEmpty()){
            dateTime = "N/A";
        }else{
            try {
                dateTime = format.format(oldFormat.parse(dateTime12H));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return dateTime;
    }
}